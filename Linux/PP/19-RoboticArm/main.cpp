/*
    Assignment: Robotic Arm
*/

#include <iostream> // fprintf
// c++ standardization, no .h, "namespace cache"
#include <stdio.h>
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()

#include <X11/Xlib.h>   // similar to Windows.h
#include <X11/Xutil.h>  // XVisualInfo
#include <X11/XKBlib.h> // Keyboard Utility Header
#include <X11/keysym.h> // keycode to symbol mapping

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h> // GLX Bridging API for XWindow -> OGL. Like in MS Win WGL
#include "vmath.h"
#include "Logic.h"

// namespaces
using namespace std; // part of iostream, no need to write std:: scope resolution optr
using namespace vmath;

// global variable declarations
bool bFullscreen = false; // CPP bool
XVisualInfo *gpXVisualInfo = NULL; // Xutil.h PixelFormatDescriptor
Colormap gColormap;
int giWindowWidth = 800;  // will be changed later. Hence no macro.
int giWindowHeight = 600; // nor constant

GLXContext gGLXContext = NULL;

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;

// entry-point function
// CUI, GUI sarva dharma samabhav
int main(void)
{
    // function prototypes
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void uninitialize();

    void initialize(void);
    void resize(int width, int height);
    void display(void);
    void update(void);

    // variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    // code
    gpFile = fopen("log.txt", "w");
    if(gpFile == NULL)
    {
        printf("Error: File creation failed.\n");
    }
    else
    {
        printf("Log file successfully created.\n");
        fprintf(gpFile, "Log file successfully created.\n");
    }
    
    CreateWindow(); // this fn made 4 vars alive

    // Message Loop
    XEvent event;
    KeySym keysym;
    char keys[50];

    initialize();

    while (bDone == false)
    {
        while (XPending(gpDisplay)) // PeekMsg
        {

            // GetMessage &msg
            XNextEvent(gpDisplay, &event);

            // message type from client
            switch (event.type)
            {
            case MapNotify:
                // WM_CREATE difference is that has power to both create as well as show window
                break;

            case KeyPress:
                keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
		XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                // XKBlib.h
                // Which page to use from Unicode, 0 is default which is ASCII
                // Is shift key pressed?
                switch (keysym)
                {
                case XK_Escape:
                    // uninitialize(); // remember lecture why not calls merged
                    // exit(0);        // stdlib.h msg.wParam
                    bDone = true;
                    break;

                case XK_f:
                case XK_F:
                    if (bFullscreen == false)
                    {
                        ToggleFullscreen();
                        bFullscreen = true;
                    }
                    else
                    {
                        ToggleFullscreen();
                        bFullscreen = false;
                    }
                    break;

                case 'a':
                case 'A':
                    gbAnimate = !gbAnimate;
                    break;

                case 'l':
                case 'L':
                    gbShowLight = !gbShowLight;
                    break;

                default:
			switch (keys[0])
			{
			case 'S':
				shoulder = (shoulder + 5) % 360;
				break;

			case 's':
				shoulder = (shoulder - 5) % 360;
				break;

			case 0x45: // 'E'
				elbow = (elbow + 10) % 360;
				break;

			case 0x65: // 'e'
				elbow = (elbow - 10) % 360;
				break;
			}
                    break;
                }
                break;
                // end of case KeyPress

            case ButtonPress:
                switch (event.xbutton.button)
                {
                case 1: // left
                    break;
                case 2: // middle
                    break;
                case 3: // right
                    break;
                case 4: // wheel up
                    break;
                case 5: // wheel down
                    break;
                default:
                    break;
                }
                break;
                // end of case ButtonPress

            case MotionNotify:
                // WM_MOUSEMOVE
                break;

            case ConfigureNotify:
                // WM_SIZE
                winWidth = event.xconfigure.width;
                winHeight = event.xconfigure.height;

                resize(winWidth, winHeight);
                break;

            case Expose:
                // WM_PAINT
                break;

            case DestroyNotify:
                // WM_DESTROY but behavior is similar to WM_CLOSE
                // asks to close yes, no
                break;

            case 33:
                // WM_CLOSE important XProtocol book part 1 of 9
                // only close msg is sent, later need to handle what action to take
                // uninitialize();
                // exit(0);

                bDone = true;
                // remember lecture, why seperate two calls. resize, device dependent handling, again uninit and re-display
                break;

            default:
                break;
            }

        } // end of XPending

        update();
        display();

    } // end of bDone

    // can't use caps TRUE, must be 1
    // no seperate WndProc kept, DispatchMessage
    // while (1){ theBlock in XPending }

    uninitialize();

    return (0);
}

void CreateWindow(void)
{
    // function prototypes
    void uninitialize(void);

    // variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    GLXFBConfig *pGLXFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig;
    XVisualInfo *pTempXVisualInfo = NULL;
    int iNoOfFBConfigs = 0;

    // The best expected super frame buffer. 11 attributes
    static int frameBufferAttributes[] = 
    {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, // Printer or Window
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, // 24 older, 32 now
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8, // Reflection or Shadow
        GLX_DOUBLEBUFFER, True,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Open X Display.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = DefaultDepth(gpDisplay, defaultScreen); // not required now, but kept if reverted back code

    // retrieve all FBConfigs driver has. Returns array
    pGLXFBConfig = glXChooseFBConfig(gpDisplay,
        defaultScreen,
        frameBufferAttributes,
        &iNoOfFBConfigs);

    printf("%d Matching FBConfigs\n", iNoOfFBConfigs);
    fprintf(gpFile, "%d Matching FBConfigs\n", iNoOfFBConfigs);

    // now get the best one from available fbconfigs
    // code reference NGine
    int bestFrameBufferConfig = -1;
    int bestNoOfSamples = -1;
    int worstFrameBufferConfig = -1;
    int worstNoOfSamples = -1;

    for (int i = 0; i < iNoOfFBConfigs; i++)
    {
        pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);

        // for each obtained FBConfig get temporary VSInfo
        // its use is just to check the capability for calling below 2 calls
        if(pTempXVisualInfo)
        {
            int sampleBuffers, samples;

            // Get no of sample buffers and samples from respective fbconfig
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);           
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNoOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples)
            {
                worstFrameBufferConfig = i;
                worstNoOfSamples = samples;
            }
        }

        XFree(pTempXVisualInfo);
    }
    
    // now assign the best one
    bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
    gGLXFBConfig = bestGLXFBConfig;
    XFree(pGLXFBConfig);

    // accordingly now get the best visual. Only driver has it
    gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);





    // // Bridging API gives visual for your provided framebuf attribs
    // gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    winAttribs.border_pixel = 0;
    winAttribs.border_pixmap = 0; // all unix based pixmap, MS bitmap
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    // Don't allocate memory if we are going to save it
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);

    if (!gWindow)
    {
        printf("ERROR : Unable to create Main Window.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to create Main Window.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    XStoreName(gpDisplay, gWindow, "PP Robotic Arm");

    // Important action to take on close 33
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);

    fprintf(gpFile, "CreateWindow successful.\n");
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    GLXContext currentGLXContext = glXGetCurrentContext();

    // In multi monitor scenario current and global can be different
    if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
    {
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    if (gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }    

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    // PP shader dtor
	uninitializePP();

    if (gpFile)
    {
        printf("Closing log file.\n");
        fprintf(gpFile, "Closing log file.\n");
        fclose(gpFile);
        gpFile = NULL;
    }    
}

void initialize(void)
{
    // function prototype
    void uninitialize(void);
    void resize(int width, int height);
	void setupPP(void);

	// PP variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

    glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

    if(glXCreateContextAttribsARB == NULL)
    {
        printf("Error: Failed to GetProcAddr...\n");
        fprintf(gpFile, "Error: Failed to GetProcAddr...\n");
        uninitialize();
        exit(1);
    }
    else
    {
        printf("GetProcAddr successful.\n");
        fprintf(gpFile, "GetProcAddr successful.\n");
    }
    
    // verify version
    GLint attribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
        GLX_CONTEXT_MINOR_VERSION_ARB, 5,
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        None
    }; 
    gGLXContext = glXCreateContextAttribsARB(gpDisplay,
        gGLXFBConfig,
        0, // multi monitors sharing grahics context
        true, // Hardware rendering context
        attribs);

    if(!gGLXContext)
    {
        // if not obtained the highest one, specify the lowest
        // it will give you nearest higher one to it

        // For sure 1.0 will be there 
        GLint attribs[] = 
        {
            GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
            GLX_CONTEXT_MINOR_VERSION_ARB, 0,
            None
        };

        gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, true, attribs);
    }

    // check obtained context is really hardware rendering context
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
        printf("Error: Obtained context is not hardware rendering context...\n");
        fprintf(gpFile, "Error: Obtained context is not hardware rendering context...\n");
    }
    else
    {
        printf("Success: Obtained context is hardware rendering context.\n");
        fprintf(gpFile, "Success: Obtained context is hardware rendering context.\n");
    }

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	setupPP();

	glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
	glEnable(GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
	glDepthFunc(GL_LEQUAL); // 3D change 4.2] Less than or Equal to
	// glDisable(GL_CULL_FACE);

    // usual code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Cheks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when renedering starts, display().
	//    "OpenGL is a state machine."
	
    printf("\ninitialize successful.\n");
    fprintf(gpFile, "\ninitialize successful.\n");
	
	// warm up call to resize, convention and not compulsion
	resize(giWindowWidth, giWindowHeight); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()
}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

    perspectiveProjectionMatrix = perspective(45.0f,
        (GLfloat)width/(GLfloat)height,
        0.1f,
        100.0f);
}

void setupPP(void)
{
	GLenum result;
	result = glewInit();
	if (result != GLEW_OK)
	{
		printf("\nGLEW init failed!!\n");
		fprintf(gpFile, "\nGLEW init failed!!\n");
		uninitialize();
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nglewInit successful.");
	}

	if (!createShaders())
	{
		printf("\ncreateShaders init failed!!\n");
		fprintf(gpFile, "\ncreateShaders init failed!!\n");
		uninitialize();
		exit(0);
	}

	initVAOs();

	perspectiveProjectionMatrix = mat4::identity();
}

//"program": "${fileDirname}/${fileBasenameNoExtension}",
// 
// shree@shree-Inspiron-5558:~/Documents/RTR/PP/Lights$ file libSphere.so 
// libSphere.so: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, BuildID[sha1]=85218b6b5beec48c1bfd6a346350c31d08e1713e, not stripped
// shree@shree-Inspiron-5558:~/Documents/RTR/PP/Lights$ cd 02-DiffuseOnSphere/
// shree@shree-Inspiron-5558:~/Documents/RTR/PP/Lights/02-DiffuseOnSphere$ ls -l libSphere.so
// -rw-r--r-- 1 shree shree 36056 Mar  8  2018 libSphere.so
// shree@shree-Inspiron-5558:~/Documents/RTR/PP/Lights/02-DiffuseOnSphere$ chmod 755 libSphere.so
// 
// 
/*

shree@shree-Inspiron-5558:~/Documents/RTR/PP/19-RoboticArm$ g++ -o RoboticArm main.cpp VarDefinitions.cpp Logic.cpp Display.cpp -L . -lX11 -lGLEW -lGL -lSphere
VarDefinitions.cpp:1:9: warning: #pragma once in main file
 #pragma once
         ^~~~
Logic.cpp:1:9: warning: #pragma once in main file
 #pragma once
         ^~~~
shree@shree-Inspiron-5558:~/Documents/RTR/PP/19-RoboticArm$ ./RoboticArm 
Log file successfully created.
10 Matching FBConfigs
GetProcAddr successful.
Success: Obtained context is hardware rendering context.

initialize successful.
shree@shree-Inspiron-5558:~/Documents/RTR/PP/19-RoboticArm$ 

*/
