/*
    Assignment: PP Interleaved
*/

#include <iostream> // not used, just in future if required
// c++ standardization, no .h, "namespace cache"
#include <stdio.h>
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()

#include <X11/Xlib.h>   // similar to Windows.h
#include <X11/Xutil.h>  // XVisualInfo
#include <X11/XKBlib.h> // Keyboard Utility Header
#include <X11/keysym.h> // keycode to symbol mapping

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h> // GLX Bridging API for XWindow -> OGL. Like in MS Win WGL
#include "vmath.h"
#include <SOIL/SOIL.h>

// namespaces
using namespace std; // part of iostream, no need to write std:: scope resolution optr
using namespace vmath;

// global variable declarations
bool bFullscreen = false; // CPP bool
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL; // Xutil.h PixelFormatDescriptor
Colormap gColormap;
Window gWindow;           // Kind of WND class
int giWindowWidth = 800;  // will be changed later. Hence no macro.
int giWindowHeight = 600; // nor constant

static GLXContext gGLXContext; // static if multiple monitors

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;

FILE *gpFile;

// PP
GLenum result;
GLuint gShaderProgramObject;
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao_pyramid; //vertex array object
GLuint vao_cube; //vertex array object
GLuint vbo_position_pyramid;	//vertex buffer object
GLuint vbo_position_cube;	//vertex buffer object

GLuint mvpUniform; // model view projection
mat4 perspectiveProjectionMatrix;

GLfloat angleCube = 0.0f;
GLfloat anglePyramid = 0.0f;
GLfloat ascendingPyramid = true;
GLfloat ascendingCube = true;

GLuint vbo_texture_cube;
GLuint vbo_texture_pyramid;
GLuint texture_kundali;
GLuint texture_stone;
GLuint samplerUniform;

GLuint vbo_cube;
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint lightPositionUniform;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShininessUniform;
GLuint lKeyIsPressedUniform;
GLuint texture_marble;
bool bLight = false;

float light_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
float light_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
float light_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
float light_position[4] = { 100.0f,100.0f,100.0f,1.0f };

float material_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
float material_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;	//also try with 50.0f 

// entry-point function
// CUI, GUI sarva dharma samabhav
int main(void)
{
    // function prototypes
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void uninitialize();

    void initialize(void);
    void resize(int width, int height);
    void display(void);
    void update(void);

    // variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    // code
    gpFile = fopen("log.txt", "w");
    if(gpFile == NULL)
    {
        printf("Error: File creation failed.\n");
    }
    else
    {
        printf("Log file successfully created.\n");
        fprintf(gpFile, "Log file successfully created.\n");
    }
    
    CreateWindow(); // this fn made 4 vars alive

    // Message Loop
    XEvent event;
    KeySym keysym;

    initialize();

    while (bDone == false)
    {
        while (XPending(gpDisplay)) // PeekMsg
        {

            // GetMessage &msg
            XNextEvent(gpDisplay, &event);

            // message type from client
            switch (event.type)
            {
            case MapNotify:
                // WM_CREATE difference is that has power to both create as well as show window
                break;

            case KeyPress:
                keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                // XKBlib.h
                // Which page to use from Unicode, 0 is default which is ASCII
                // Is shift key pressed?
                switch (keysym)
                {
                case XK_Escape:
                    // uninitialize(); // remember lecture why not calls merged
                    // exit(0);        // stdlib.h msg.wParam
                    bDone = true;
                    break;

                case XK_f:
                case XK_F:
                    if (bFullscreen == false)
                    {
                        ToggleFullscreen();
                        bFullscreen = true;
                    }
                    else
                    {
                        ToggleFullscreen();
                        bFullscreen = false;
                    }
                    break;

		case 'L':
		case 'l':
			if (bLight == false)
			{
				bLight = true;
			}
			else
			{
				bLight = false;
			}
			break;

                default:
                    break;
                }
                break;
                // end of case KeyPress

            case ButtonPress:
                switch (event.xbutton.button)
                {
                case 1: // left
                    break;
                case 2: // middle
                    break;
                case 3: // right
                    break;
                case 4: // wheel up
                    break;
                case 5: // wheel down
                    break;
                default:
                    break;
                }
                break;
                // end of case ButtonPress

            case MotionNotify:
                // WM_MOUSEMOVE
                break;

            case ConfigureNotify:
                // WM_SIZE
                winWidth = event.xconfigure.width;
                winHeight = event.xconfigure.height;

                resize(winWidth, winHeight);
                break;

            case Expose:
                // WM_PAINT
                break;

            case DestroyNotify:
                // WM_DESTROY but behavior is similar to WM_CLOSE
                // asks to close yes, no
                break;

            case 33:
                // WM_CLOSE important XProtocol book part 1 of 9
                // only close msg is sent, later need to handle what action to take
                // uninitialize();
                // exit(0);

                bDone = true;
                // remember lecture, why seperate two calls. resize, device dependent handling, again uninit and re-display
                break;

            default:
                break;
            }

        } // end of XPending

        update();
        display();

    } // end of bDone

    // can't use caps TRUE, must be 1
    // no seperate WndProc kept, DispatchMessage
    // while (1){ theBlock in XPending }

    uninitialize();

    return (0);
}

void CreateWindow(void)
{
    // function prototypes
    void uninitialize(void);

    // variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    GLXFBConfig *pGLXFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig;
    XVisualInfo *pTempXVisualInfo = NULL;
    int iNoOfFBConfigs = 0;

    // The best expected super frame buffer. 11 attributes
    static int frameBufferAttributes[] = 
    {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, // Printer or Window
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, // 24 older, 32 now
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8, // Reflection or Shadow
        GLX_DOUBLEBUFFER, True,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Open X Display.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = DefaultDepth(gpDisplay, defaultScreen); // not required now, but kept if reverted back code

    // retrieve all FBConfigs driver has. Returns array
    pGLXFBConfig = glXChooseFBConfig(gpDisplay,
        defaultScreen,
        frameBufferAttributes,
        &iNoOfFBConfigs);

    printf("%d Matching FBConfigs\n", iNoOfFBConfigs);
    fprintf(gpFile, "%d Matching FBConfigs\n", iNoOfFBConfigs);

    // now get the best one from available fbconfigs
    // code reference NGine
    int bestFrameBufferConfig = -1;
    int bestNoOfSamples = -1;
    int worstFrameBufferConfig = -1;
    int worstNoOfSamples = -1;

    for (int i = 0; i < iNoOfFBConfigs; i++)
    {
        pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);

        // for each obtained FBConfig get temporary VSInfo
        // its use is just to check the capability for calling below 2 calls
        if(pTempXVisualInfo)
        {
            int sampleBuffers, samples;

            // Get no of sample buffers and samples from respective fbconfig
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);           
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNoOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples)
            {
                worstFrameBufferConfig = i;
                worstNoOfSamples = samples;
            }
        }

        XFree(pTempXVisualInfo);
    }
    
    // now assign the best one
    bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
    gGLXFBConfig = bestGLXFBConfig;
    XFree(pGLXFBConfig);

    // accordingly now get the best visual. Only driver has it
    gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);





    // // Bridging API gives visual for your provided framebuf attribs
    // gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    winAttribs.border_pixel = 0;
    winAttribs.border_pixmap = 0; // all unix based pixmap, MS bitmap
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    // Don't allocate memory if we are going to save it
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);

    if (!gWindow)
    {
        printf("ERROR : Unable to create Main Window.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to create Main Window.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    XStoreName(gpDisplay, gWindow, "PP Interleaved");

    // Important action to take on close 33
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);

    fprintf(gpFile, "CreateWindow successful.\n");
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    GLXContext currentGLXContext = glXGetCurrentContext();

    // In multi monitor scenario current and global can be different
    if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
    {
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    if (gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }    

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    // PP shader dtor
	// Safe Release
	// Don't declare shader objects globally, use locally in initialize n use as necessary
	if (vbo_texture_cube)
	{
		glDeleteBuffers(1, &vbo_texture_cube);
		vbo_texture_cube = 0;
	}
	glDeleteTextures(1, &texture_kundali);
	texture_kundali = 0;
	if (vbo_position_cube)
	{
		glDeleteBuffers(1, &vbo_position_cube);
		vbo_position_cube = 0;
	}
	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}

	if (vbo_texture_pyramid)
	{
		glDeleteBuffers(1, &vbo_texture_pyramid);
		vbo_texture_pyramid = 0;
	}
	glDeleteTextures(1, &texture_stone);
	texture_stone = 0;
	if (vbo_position_pyramid)
	{
		glDeleteBuffers(1, &vbo_position_pyramid);
		vbo_position_pyramid = 0;
	}
	if (vao_pyramid)
	{
		glDeleteVertexArrays(1, &vao_pyramid);
		vao_pyramid = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNo;

		glUseProgram(gShaderProgramObject);

		// ask pgm how many shaders attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject,
				shaderCount,
				&shaderCount, /// using same var
				pShaders);

			for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNo]);
				glDeleteShader(pShaders[shaderNo]);
				pShaders[shaderNo] = 0;
			}
			free(pShaders);
		}

		glDeleteProgram(gShaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

    if (gpFile)
    {
        printf("Closing log file.\n");
        fprintf(gpFile, "Closing log file.\n");
        fclose(gpFile);
        gpFile = NULL;
    }    
}

void initialize(void)
{
    // function prototype
    void uninitialize(void);
    void resize(int width, int height);
    bool loadTexture(GLuint *texture, const char *path);

	// PP variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

    glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

    if(glXCreateContextAttribsARB == NULL)
    {
        printf("Error: Failed to GetProcAddr...\n");
        fprintf(gpFile, "Error: Failed to GetProcAddr...\n");
        uninitialize();
        exit(1);
    }
    else
    {
        printf("GetProcAddr successful.\n");
        fprintf(gpFile, "GetProcAddr successful.\n");
    }
    
    // verify version
    GLint attribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
        GLX_CONTEXT_MINOR_VERSION_ARB, 5,
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        None
    }; 
    gGLXContext = glXCreateContextAttribsARB(gpDisplay,
        gGLXFBConfig,
        0, // multi monitors sharing grahics context
        true, // Hardware rendering context
        attribs);

    if(!gGLXContext)
    {
        // if not obtained the highest one, specify the lowest
        // it will give you nearest higher one to it

        // For sure 1.0 will be there 
        GLint attribs[] = 
        {
            GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
            GLX_CONTEXT_MINOR_VERSION_ARB, 0,
            None
        };

        gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, true, attribs);
    }

    // check obtained context is really hardware rendering context
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
        printf("Error: Obtained context is not hardware rendering context...\n");
        fprintf(gpFile, "Error: Obtained context is not hardware rendering context...\n");
    }
    else
    {
        printf("Success: Obtained context is hardware rendering context.\n");
        fprintf(gpFile, "Success: Obtained context is hardware rendering context.\n");
    }

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	result = glewInit();
	if(result != GLEW_OK)
	{
		printf("\nGLEW init failed!!\n");
		fprintf(gpFile, "\nGLEW init failed!!\n");
		uninitialize();
		exit(0);
	}

	// Pass Through Shader code
    // step 1] define vertex shader obj
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// step 2] write vertex shader code
	const GLchar *vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"in vec2 vTexcoord;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lkeyispressed;" \
		"uniform vec4 u_light_position;" \
		"out vec3 t_norm;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"out vec4 out_color;" \
		"out vec2 out_texcoord;" \
		"void main(void)" \
		"{" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_v_matrix * u_m_matrix * vPosition;" \
		"t_norm=mat3(u_v_matrix * u_m_matrix)*vNormal;" \
		"light_direction=vec3(u_light_position - eye_coordinates);" \
		"viewer_vector=normalize(vec3(-eye_coordinates.xyz));" \
		"}" \
		"gl_Position=u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"out_color=vColor;" \
		"out_texcoord =vTexcoord;" \
		"}";

	// step 3] specify above source code to vertex shader obj
	glShaderSource(gVertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode,
		NULL);

	// step 4] compile the vertex shader
	glCompileShader(gVertexShaderObject);


	// steps for catching errors
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(gVertexShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "\nVertex Shader: Compilation Error: %s", szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\nVertex Shader: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fprintf(gpFile, "\nVertex Shader: Something went wrong, infoLogLength is zero...");
		}
		uninitialize();
		exit(0); // Why zero? Bcoz error is from GPU, not from CPU/OS
	}
	else
	{
		fprintf(gpFile, "\nVertex Shader compiled successfully.");
	}


	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;


	// step 1] define fragment shader obj
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// step 2] write fragment shader code
	const GLchar *fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
        	"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"in vec3 t_norm;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"in vec2 out_texcoord;" \
		"uniform sampler2D u_sampler;" \
		"in vec4 out_color;" \
		"vec4 Texture;" \
		"out vec4 FragColor;" \
		"uniform int u_lkeyispressed;" \
		"void main(void)" \
		"{" \
		"vec3 fong_ads_light;" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec3 normalized_tnorm=normalize(t_norm);" \
		"vec3 normalized_lightdirection=normalize(light_direction);" \
		"vec3 normalized_viewervector=normalize(viewer_vector);" \
		"vec3 reflection_vector=reflect(-normalized_lightdirection,normalized_tnorm);" \
		"float tn_dot_ld=max(dot(normalized_lightdirection,normalized_tnorm),0.0);" \
		"vec3 ambient=u_la * u_ka;" \
		"vec3 diffuse=u_ld * u_kd * tn_dot_ld;" \
		"vec3 specular=u_ls * u_ks * pow(max(dot(reflection_vector,normalized_viewervector),0.0),u_material_shininess);" \
		"fong_ads_light= ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"fong_ads_light=vec3(1.0,1.0,1.0);" \
		"}" \
		"Texture=texture(u_sampler,out_texcoord);" \
		"FragColor=Texture * out_color * vec4(fong_ads_light,1.0);" \
		"}";

	// step 3] specify above source code to fragment shader obj
	glShaderSource(gFragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	// step 4] compile the fragment shader
	glCompileShader(gFragmentShaderObject);


	// steps for catching errors
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(gFragmentShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "\nFragment Shader: Compilation Error: %s", szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\nFragment Shader: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fprintf(gpFile, "\nFragment Shader: Something went wrong, infoLogLength is zero...");
		}
		uninitialize();
		exit(0); // Why zero? Bcoz error is from GPU, not from CPU/OS
	}
	else
	{
		fprintf(gpFile, "\nFragment Shader compiled successfully.");
	}

	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;


	// create shader program obj
	// step 1] create
	gShaderProgramObject = glCreateProgram();

	// step 2] Attach shaders
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Pre-Linking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexcoord");

	// step 3] Link program
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetProgramInfoLog(gShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "\nShader Program: Link Error: %s", szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\nShader Program: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fprintf(gpFile, "\nShader Program: Something went wrong, infoLogLength is zero...");
		}
		uninitialize();
		exit(0); // Why zero? Bcoz error is from GPU, not from CPU/OS
	}
	else
	{
		fprintf(gpFile, "\nShader program linked successfully.");
	}

	// Post-Linking retrieving uniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

	// reset
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

    
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lkeyispressed");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

	//============== CUBE ====================
	const GLfloat cubeVCNT[] = 
	{
	1.0f, 1.0f, -1.0f, 1.0, 0.0, 0.0, 0.0f , 1.0f, 0.0f, 0.0f, 1.0f,
	-1.0f, 1.0f, -1.0f, 1.0, 0.0, 0.0, 0.0f , 1.0f, 0.0f, 0.0f, 0.0f,
	-1.0f, 1.0f, 1.0f, 1.0, 0.0, 0.0, 0.0f , 1.0f, 0.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 1.0f, 1.0, 0.0, 0.0, 0.0f , 1.0f, 0.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f, 0.0, 1.0, 0.0, 0.0f , -1.0f, 0.0f, 1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f, 0.0, 1.0, 0.0, 0.0f , -1.0f, 0.0f, 0.0f, 1.0f,
	-1.0f, -1.0f, -1.0f, 0.0, 1.0, 0.0, 0.0f , -1.0f, 0.0f, 0.0f, 0.0f,
	1.0f, -1.0f, -1.0f, 0.0, 1.0, 0.0, 0.0f , -1.0f, 0.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 1.0f, 0.0, 0.0, 1.0, 0.0f , 0.0f, 1.0f, 0.0f, 0.0f,
	-1.0f, 1.0f,1.0f, 0.0, 0.0, 1.0, 0.0f , 0.0f, 1.0f, 1.0f, 0.0f,
	-1.0f, -1.0f, 1.0f, 0.0, 0.0, 1.0, 0.0f , 0.0f, 1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f, 0.0, 0.0, 1.0, 0.0f , 0.0f, 1.0f, 0.0f, 1.0f,
	1.0f, -1.0f, -1.0f, 0.0, 1.0, 1.0, 0.0f , 0.0f, -1.0f, 1.0f, 0.0f,
	-1.0f, -1.0f,-1.0f, 0.0, 1.0, 1.0, 0.0f , 0.0f, -1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, -1.0f, 0.0, 1.0, 1.0, 0.0f , 0.0f, -1.0f, 0.0f, 1.0f,
	1.0f, 1.0f, -1.0f, 0.0, 1.0, 1.0, 0.0f , 0.0f, -1.0f, 0.0f, 0.0f,
	-1.0f, 1.0f, 1.0f, 1.0, 0.0, 1.0, -1.0f , 0.0f, 0.0f, 1.0f, 0.0f,
	-1.0f, 1.0f,-1.0f, 1.0, 0.0, 1.0, -1.0f , 0.0f, 0.0f, 1.0f, 1.0f,
	-1.0f, -1.0f,-1.0f, 1.0, 0.0, 1.0, -1.0f , 0.0f, 0.0f, 0.0f, 1.0f,
	-1.0f, -1.0f, 1.0f, 1.0, 0.0, 1.0, -1.0f , 0.0f, 0.0f, 0.0f, 0.0f,
	1.0f, 1.0f, -1.0f, 1.0, 1.0, 0.0, 1.0f , 0.0f, 0.0f, 0.0f, 0.0f,
	1.0f, 1.0f, 1.0f, 1.0, 1.0, 0.0, 1.0f , 0.0f, 0.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 1.0f, 1.0, 1.0, 0.0, 1.0f , 0.0f, 0.0f, 1.0f, 1.0f,
	1.0f, -1.0f, -1.0f, 1.0, 1.0, 0.0, 1.0f , 0.0f, 0.0f, 0.0f, 1.0f
	};

	//create vao
	//for cube
	glGenVertexArrays(1, &vao_cube);
	glBindVertexArray(vao_cube);
	glGenBuffers(1, &vbo_cube);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVCNT), cubeVCNT, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), (void *)(0 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(9 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
	glEnable(GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
	glDepthFunc(GL_LEQUAL); // 3D change 4.2] Less than or Equal to

    // usual code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Cheks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when renedering starts, display().
	//    "OpenGL is a state machine."
	

	bool ret = loadTexture(&texture_marble,"marble.bmp");
    	glEnable(GL_TEXTURE_2D);

	perspectiveProjectionMatrix = mat4::identity();

    printf("\ninitialize successful.\n");
    fprintf(gpFile, "\ninitialize successful.\n");
	
	// warm up call to resize, convention and not compulsion
	resize(giWindowWidth, giWindowHeight); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()
}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

    perspectiveProjectionMatrix = perspective(45.0f,
        (GLfloat)width/(GLfloat)height,
        0.1f,
        100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use Pass Through Shader Program
	glUseProgram(gShaderProgramObject); // Binding shader pgm to OpenGL pgm

	// your code here
    // 9 steps
	// declaration of matrices
	mat4 translationMatrix;
	mat4 scaleMatrix;
	mat4 rotationMatrix;
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;

	// initialize above matrices to identity
	translationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();

	//============== CUBE ====================
	modelMatrix = translate(0.0f, 0.0f, -6.0f) * rotate(angleCube, angleCube, angleCube);// *rotationMatrix;
	projectionMatrix = perspectiveProjectionMatrix;

	//send necessary matrices to shader in respective uniforms
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, projectionMatrix);


	glBindVertexArray(vao_cube);

	// ==== Work with texture now ABU ====
	glActiveTexture(GL_TEXTURE0); // matches to our AMC_ATTRIBUTE_TEXCOORD0
	// texture unit. 80 supported
	glBindTexture(GL_TEXTURE_2D, texture_marble);
	glUniform1i(samplerUniform, 0); // GL_TEXTURE0 zeroth unit

	if (bLight == true)
	{
		glUniform1i(lKeyIsPressedUniform, 1);
		glUniform3fv(laUniform, 1, light_ambient);
		glUniform3fv(ldUniform, 1, light_diffuse);
		glUniform3fv(lsUniform, 1, light_specular);
		glUniform4fv(lightPositionUniform, 2, light_position);
		glUniform3fv(kaUniform, 1, material_ambient);
		glUniform3fv(kdUniform, 1, material_diffuse);
		glUniform3fv(ksUniform, 1, material_specular);
		glUniform1f(materialShininessUniform, material_shininess);
	}
	else
	{
		glUniform1i(lKeyIsPressedUniform, 0);
	}

	glDrawArrays(GL_TRIANGLE_FAN,
		0,
		4); // how many to draw? Ex: Triangle - 3 vertices, Quad 4 vert
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindVertexArray(0);

	glUseProgram(0); // Unbinding

	glXSwapBuffers(gpDisplay, gWindow);
}

bool loadTexture(GLuint *texture, const char *path)
{
    // variable declaration
    int imageWidth;
    int imageHeight;
    bool bResult = false; // C++ bool
    unsigned char *imageData = NULL;

    imageData = SOIL_load_image(path, &imageWidth, &imageHeight, 0, SOIL_LOAD_RGB);
    // returns byte[] = char[], 0 = no masking from my side

    if(imageData == NULL)
    {
        bResult = false;
		fprintf(gpFile, "Error: loadTexture failed!!!\n");
        return bResult;
    }
    else
    {
		fprintf(gpFile, "SOIL_load_image successful.\n");
        bResult = true;
    }
    
    // convention and not compulsion
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4); // state machine, gap of 4 b/w 2 rows RGBA

    glGenTextures(1, texture); // empty in, filled out. Memory from graphics card
    glBindTexture(GL_TEXTURE_2D, *texture); //

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    // fill data
    glTexImage2D(GL_TEXTURE_2D,
        0,
        GL_RGB,
        imageWidth,
        imageHeight,
        0,
        GL_RGB,
        GL_UNSIGNED_BYTE,
        imageData);

    glGenerateMipmap(GL_TEXTURE_2D); // new addition

    // no explicit unbind in FFP, must in PP
    glBindTexture(GL_TEXTURE_2D, 0);

    SOIL_free_image_data(imageData); // Don't worry data is pushed into GPU memory. CPU mem not required now.

    return bResult;
}

void update()
{
	// Rotate vice versa	
	if (ascendingPyramid)
	{
		anglePyramid += 0.05f;
		if (anglePyramid > 360.0f)
		{
			ascendingPyramid = false;
		}
	}
	else
	{
		anglePyramid -= 0.05f;
		if (anglePyramid < 0.0f)
		{
			ascendingPyramid = true;
		}
	}

	// Rotate vice versa	
	if (ascendingCube)
	{
		angleCube += 0.05f;
		if (angleCube > 360.0f)
		{
			ascendingCube = false;
		}
	}
	else
	{
		angleCube -= 0.05f;
		if (angleCube < 0.0f)
		{
			ascendingCube = true;
		}
	}
}


//"program": "${fileDirname}/${fileBasenameNoExtension}",
//
/*
shree@shree-Inspiron-5558:~/Documents/RTR/PP/21-Interleaved$ g++ -o Interleaved Interleaved.cpp -lX11 -lGLEW -lGL -lSOIL
shree@shree-Inspiron-5558:~/Documents/RTR/PP/21-Interleaved$ ./Interleaved 
Log file successfully created.
10 Matching FBConfigs
GetProcAddr successful.
Success: Obtained context is hardware rendering context.

initialize successful.
Closing log file.
shree@shree-Inspiron-5558:~/Documents/RTR/PP/21-Interleaved$
*/
