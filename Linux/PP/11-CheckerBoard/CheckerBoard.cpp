/*
    Assignment: PP 11] Checker Board using Dynamic Draw GL_DYNAMIC_DRAW
	Base App: 10] Tweaked Smiley
*/

#include <iostream> // not used, just in future if required
// c++ standardization, no .h, "namespace cache"
#include <stdio.h>
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()

#include <X11/Xlib.h>   // similar to Windows.h
#include <X11/Xutil.h>  // XVisualInfo
#include <X11/XKBlib.h> // Keyboard Utility Header
#include <X11/keysym.h> // keycode to symbol mapping

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h> // GLX Bridging API for XWindow -> OGL. Like in MS Win WGL
#include "vmath.h"
#include <SOIL/SOIL.h>

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

// namespaces
using namespace std; // part of iostream, no need to write std:: scope resolution optr
using namespace vmath;

// global variable declarations
bool bFullscreen = false; // CPP bool
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL; // Xutil.h PixelFormatDescriptor
Colormap gColormap;
Window gWindow;           // Kind of WND class
int giWindowWidth = 800;  // will be changed later. Hence no macro.
int giWindowHeight = 600; // nor constant

static GLXContext gGLXContext; // static if multiple monitors

typedef GLXContext (*glXCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;

FILE *gpFile;

// PP
GLenum result;
GLuint gShaderProgramObject;
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao; //vertex array object
GLuint vbo;	//vertex buffer object
GLuint mvpUniform; // model view projection
mat4 perspectiveProjectionMatrix;

GLuint vbo_texture;
GLuint texImage;
GLuint samplerUniform;

GLubyte checkImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];

// entry-point function
// CUI, GUI sarva dharma samabhav
int main(void)
{
    // function prototypes
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void uninitialize();

    void initialize(void);
    void resize(int width, int height);
    void display(void);

    // variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    // code
    gpFile = fopen("log.txt", "w");
    if(gpFile == NULL)
    {
        printf("Error: File creation failed.\n");
    }
    else
    {
        printf("Log file successfully created.\n");
        fprintf(gpFile, "Log file successfully created.\n");
    }
    
    CreateWindow(); // this fn made 4 vars alive

    // Message Loop
    XEvent event;
    KeySym keysym;

    initialize();

    while (bDone == false)
    {
        while (XPending(gpDisplay)) // PeekMsg
        {

            // GetMessage &msg
            XNextEvent(gpDisplay, &event);

            // message type from client
            switch (event.type)
            {
            case MapNotify:
                // WM_CREATE difference is that has power to both create as well as show window
                break;

            case KeyPress:
                keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                // XKBlib.h
                // Which page to use from Unicode, 0 is default which is ASCII
                // Is shift key pressed?
                switch (keysym)
                {
                case XK_Escape:
                    // uninitialize(); // remember lecture why not calls merged
                    // exit(0);        // stdlib.h msg.wParam
                    bDone = true;
                    break;

                case XK_f:
                case XK_F:
                    if (bFullscreen == false)
                    {
                        ToggleFullscreen();
                        bFullscreen = true;
                    }
                    else
                    {
                        ToggleFullscreen();
                        bFullscreen = false;
                    }
                    break;

                default:
                    break;
                }
                break;
                // end of case KeyPress

            case ButtonPress:
                switch (event.xbutton.button)
                {
                case 1: // left
                    break;
                case 2: // middle
                    break;
                case 3: // right
                    break;
                case 4: // wheel up
                    break;
                case 5: // wheel down
                    break;
                default:
                    break;
                }
                break;
                // end of case ButtonPress

            case MotionNotify:
                // WM_MOUSEMOVE
                break;

            case ConfigureNotify:
                // WM_SIZE
                winWidth = event.xconfigure.width;
                winHeight = event.xconfigure.height;

                resize(winWidth, winHeight);
                break;

            case Expose:
                // WM_PAINT
                break;

            case DestroyNotify:
                // WM_DESTROY but behavior is similar to WM_CLOSE
                // asks to close yes, no
                break;

            case 33:
                // WM_CLOSE important XProtocol book part 1 of 9
                // only close msg is sent, later need to handle what action to take
                // uninitialize();
                // exit(0);

                bDone = true;
                // remember lecture, why seperate two calls. resize, device dependent handling, again uninit and re-display
                break;

            default:
                break;
            }

        } // end of XPending

        display();

    } // end of bDone

    // can't use caps TRUE, must be 1
    // no seperate WndProc kept, DispatchMessage
    // while (1){ theBlock in XPending }

    uninitialize();

    return (0);
}

void CreateWindow(void)
{
    // function prototypes
    void uninitialize(void);

    // variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    GLXFBConfig *pGLXFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig;
    XVisualInfo *pTempXVisualInfo = NULL;
    int iNoOfFBConfigs = 0;

    // The best expected super frame buffer. 11 attributes
    static int frameBufferAttributes[] = 
    {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, // Printer or Window
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, // 24 older, 32 now
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        GLX_STENCIL_SIZE, 8, // Reflection or Shadow
        GLX_DOUBLEBUFFER, True,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Open X Display.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = DefaultDepth(gpDisplay, defaultScreen); // not required now, but kept if reverted back code

    // retrieve all FBConfigs driver has. Returns array
    pGLXFBConfig = glXChooseFBConfig(gpDisplay,
        defaultScreen,
        frameBufferAttributes,
        &iNoOfFBConfigs);

    printf("%d Matching FBConfigs\n", iNoOfFBConfigs);
    fprintf(gpFile, "%d Matching FBConfigs\n", iNoOfFBConfigs);

    // now get the best one from available fbconfigs
    // code reference NGine
    int bestFrameBufferConfig = -1;
    int bestNoOfSamples = -1;
    int worstFrameBufferConfig = -1;
    int worstNoOfSamples = -1;

    for (int i = 0; i < iNoOfFBConfigs; i++)
    {
        pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);

        // for each obtained FBConfig get temporary VSInfo
        // its use is just to check the capability for calling below 2 calls
        if(pTempXVisualInfo)
        {
            int sampleBuffers, samples;

            // Get no of sample buffers and samples from respective fbconfig
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);           
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNoOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples)
            {
                worstFrameBufferConfig = i;
                worstNoOfSamples = samples;
            }
        }

        XFree(pTempXVisualInfo);
    }
    
    // now assign the best one
    bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
    gGLXFBConfig = bestGLXFBConfig;
    XFree(pGLXFBConfig);

    // accordingly now get the best visual. Only driver has it
    gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);





    // // Bridging API gives visual for your provided framebuf attribs
    // gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    winAttribs.border_pixel = 0;
    winAttribs.border_pixmap = 0; // all unix based pixmap, MS bitmap
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    // Don't allocate memory if we are going to save it
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);

    if (!gWindow)
    {
        printf("ERROR : Unable to create Main Window.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to create Main Window.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    XStoreName(gpDisplay, gWindow, "PP 10] Tweaked Smiley");

    // Important action to take on close 33
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);

    fprintf(gpFile, "CreateWindow successful.\n");
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    GLXContext currentGLXContext = glXGetCurrentContext();

    // In multi monitor scenario current and global can be different
    if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
    {
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    if (gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }    

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    // PP shader dtor
	// Safe Release
	// Don't declare shader objects globally, use locally in initialize n use as necessary
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

    if (vbo_texture)
	{
		glDeleteBuffers(1, &vbo_texture);
		vbo_texture = 0;
	}
	glDeleteTextures(1, &texImage);
	texImage = 0;

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNo;

		glUseProgram(gShaderProgramObject);

		// ask pgm how many shaders attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject,
				shaderCount,
				&shaderCount, /// using same var
				pShaders);

			for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNo]);
				glDeleteShader(pShaders[shaderNo]);
				pShaders[shaderNo] = 0;
			}
			free(pShaders);
		}

		glDeleteProgram(gShaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

    if (gpFile)
    {
        printf("Closing log file.\n");
        fprintf(gpFile, "Closing log file.\n");
        fclose(gpFile);
        gpFile = NULL;
    }    
}

void initialize(void)
{
    // function prototype
    void uninitialize(void);
    void resize(int width, int height);
    void loadTexture(void);

	// PP variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

    glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");

    if(glXCreateContextAttribsARB == NULL)
    {
        printf("Error: Failed to GetProcAddr...\n");
        fprintf(gpFile, "Error: Failed to GetProcAddr...\n");
        uninitialize();
        exit(1);
    }
    else
    {
        printf("GetProcAddr successful.\n");
        fprintf(gpFile, "GetProcAddr successful.\n");
    }
    
    // verify version
    GLint attribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
        GLX_CONTEXT_MINOR_VERSION_ARB, 5,
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        None
    }; 
    gGLXContext = glXCreateContextAttribsARB(gpDisplay,
        gGLXFBConfig,
        0, // multi monitors sharing grahics context
        true, // Hardware rendering context
        attribs);

    if(!gGLXContext)
    {
        // if not obtained the highest one, specify the lowest
        // it will give you nearest higher one to it

        // For sure 1.0 will be there 
        GLint attribs[] = 
        {
            GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
            GLX_CONTEXT_MINOR_VERSION_ARB, 0,
            None
        };

        gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, true, attribs);
    }

    // check obtained context is really hardware rendering context
    if(!glXIsDirect(gpDisplay, gGLXContext))
    {
        printf("Error: Obtained context is not hardware rendering context...\n");
        fprintf(gpFile, "Error: Obtained context is not hardware rendering context...\n");
    }
    else
    {
        printf("Success: Obtained context is hardware rendering context.\n");
        fprintf(gpFile, "Success: Obtained context is hardware rendering context.\n");
    }

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	result = glewInit();
	if(result != GLEW_OK)
	{
		printf("\nGLEW init failed!!\n");
		fprintf(gpFile, "\nGLEW init failed!!\n");
		uninitialize();
		exit(0);
	}

	// Pass Through Shader code
    // step 1] define vertex shader obj
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// step 2] write vertex shader code
	const GLchar *vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
        "in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
        "in vec2 vTexCoord;" \
		"out vec2 out_texcoord;" \
		"void main(void)" \
		"{" \
        "	gl_Position = u_mvp_matrix * vPosition;" \
		"	out_texcoord = vTexCoord;" \
		"}";

	// step 3] specify above source code to vertex shader obj
	glShaderSource(gVertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode,
		NULL);

	// step 4] compile the vertex shader
	glCompileShader(gVertexShaderObject);


	// steps for catching errors
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(gVertexShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "\nVertex Shader: Compilation Error: %s", szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\nVertex Shader: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fprintf(gpFile, "\nVertex Shader: Something went wrong, infoLogLength is zero...");
		}
		uninitialize();
		exit(0); // Why zero? Bcoz error is from GPU, not from CPU/OS
	}
	else
	{
		fprintf(gpFile, "\nVertex Shader compiled successfully.");
	}


	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;


	// step 1] define fragment shader obj
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// step 2] write fragment shader code
	const GLchar *fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
        "out vec4 FragColor;" \
        "in vec2 out_texcoord;" \
		"uniform sampler2D u_sampler;" \
		"void main(void)" \
		"{" \
        "	FragColor = texture(u_sampler, out_texcoord);" \
		"}";

	// step 3] specify above source code to fragment shader obj
	glShaderSource(gFragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	// step 4] compile the fragment shader
	glCompileShader(gFragmentShaderObject);


	// steps for catching errors
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(gFragmentShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "\nFragment Shader: Compilation Error: %s", szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\nFragment Shader: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fprintf(gpFile, "\nFragment Shader: Something went wrong, infoLogLength is zero...");
		}
		uninitialize();
		exit(0); // Why zero? Bcoz error is from GPU, not from CPU/OS
	}
	else
	{
		fprintf(gpFile, "\nFragment Shader compiled successfully.");
	}

	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;


	// create shader program obj
	// step 1] create
	gShaderProgramObject = glCreateProgram();

	// step 2] Attach shaders
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Pre-Linking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");

	// step 3] Link program
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetProgramInfoLog(gShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "\nShader Program: Link Error: %s", szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\nShader Program: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fprintf(gpFile, "\nShader Program: Something went wrong, infoLogLength is zero...");
		}
		uninitialize();
		exit(0); // Why zero? Bcoz error is from GPU, not from CPU/OS
	}
	else
	{
		fprintf(gpFile, "\nShader program linked successfully.");
	}

	// Post-Linking retrieving uniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

	// reset
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

    
	const GLfloat texcoords[] =
	{
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f
	};

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER,
		4 * 2 * sizeof(GL_FLOAT),
		NULL,
		GL_DYNAMIC_DRAW); // attachya atta oot

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // xyx
		GL_FLOAT,
		GL_FALSE, // isNormalized 0 to 1 NDC 
		0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
		NULL); // no stride therefore no offest

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind


	glGenBuffers(1, &vbo_texture);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(texcoords),
		texcoords,
		GL_STATIC_DRAW); // attachya atta oot
	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,
		2, // s,t
		GL_FLOAT,
		GL_FALSE, // isNormalized 0 to 1 NDC 
		0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
		NULL); // no stride therefore no offest
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind

	glBindVertexArray(0);


	glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
	glEnable(GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
	glDepthFunc(GL_LEQUAL); // 3D change 4.2] Less than or Equal to

    // usual code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Cheks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when renedering starts, display().
	//    "OpenGL is a state machine."
	

	loadTexture();
    glEnable(GL_TEXTURE_2D);

	perspectiveProjectionMatrix = mat4::identity();

    printf("\ninitialize successful.\n");
    fprintf(gpFile, "\ninitialize successful.\n");
	
	// warm up call to resize, convention and not compulsion
	resize(giWindowWidth, giWindowHeight); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()
}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

    perspectiveProjectionMatrix = perspective(45.0f,
        (GLfloat)width/(GLfloat)height,
        0.1f,
        100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use Pass Through Shader Program
	glUseProgram(gShaderProgramObject); // Binding shader pgm to OpenGL pgm

	// your code here
    // 9 steps
	// declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	// initialize above 2 matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	// here in this pgm no transformation, but in later pgms
    modelViewMatrix = translate(0.0f, 0.0f, -5.0f); // NOTE: Change

	// do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum

	// send necessary matrices to shader in respective uniforms
	// display fn is dynamic, called in loop
	glUniformMatrix4fv(mvpUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelViewProjectionMatrix); // kashala chiktavaychay // verify
	// OpenGL/GLSL is column major, DirectX is row major

	// ==== Work with texture now ABU ====
	glActiveTexture(GL_TEXTURE0); // matches to our AMC_ATTRIBUTE_TEXCOORD0
	// texture unit. 80 supported
	glBindTexture(GL_TEXTURE_2D, texImage);
	glUniform1i(samplerUniform, 0); // GL_TEXTURE0 zeroth unit


	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	static const GLfloat quadVertices1[] =
	{
		0.0f, 1.0f, 0.0f,
		-2.0f, 1.0f, 0.0f,
		-2.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f
	};

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices1), quadVertices1, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// draw the scene
	glDrawArrays(GL_TRIANGLE_FAN,
		0, // from which array element to start. You can put different geometries in single array-interleaved
		4); // how many to draw? Ex: Triangle - 3 vertices, Quad 4 vert

	static const GLfloat quadVertices2[] =
	{
		2.41421f, 1.0f, -1.41421f,
		1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		2.41421f, -1.0f, -1.41421f,
	};
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices2), quadVertices2, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0); // Unbinding

	glXSwapBuffers(gpDisplay, gWindow);
}


void loadTexture(void)
{
    // function declaration
	void makeCheckImage(void);

	makeCheckImage();
	// convention and not compulsion
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // state machine, gap of 1 byte b/w 2 rows. No specific format

	glGenTextures(1, &texImage); // empty in, filled out. Memory from graphics card
	glBindTexture(GL_TEXTURE_2D, texImage); //

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// fill data
	glTexImage2D(GL_TEXTURE_2D,
		0, // use same for all near, far; no mipmap
		GL_RGBA,
		CHECK_IMAGE_WIDTH,
		CHECK_IMAGE_HEIGHT,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		checkImage);
	// No Mipmap here for procedural texture. Required only for stock

	// glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

void makeCheckImage(void)
{
	int i, j, c;

	for (i = 0; i < CHECK_IMAGE_HEIGHT; i++)
	{
		for (j = 0; j < CHECK_IMAGE_WIDTH; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = (GLubyte)255; // alpha
		}
	}
}


//"program": "${fileDirname}/${fileBasenameNoExtension}",
//
//shree@shree-Inspiron-5558:~/Documents/RTR/PP/11-CheckerBoard$ g++ -o CheckerBoard CheckerBoard.cpp -lX11 -lGLEW -lGL -lSOIL
//shree@shree-Inspiron-5558:~/Documents/RTR/PP/11-CheckerBoard$ ./CheckerBoard 
//Log file successfully created.
//10 Matching FBConfigs
//GetProcAddr successful.
//Success: Obtained context is hardware rendering context.

//initialize successful.
//Closing log file.
//shree@shree-Inspiron-5558:~/Documents/RTR/PP/11-CheckerBoard$ 

