#pragma once
#include "VarDeclarations.h"
#include <stdio.h>

int MyCount = 10;
FILE *gpFile = NULL;
bool gbShowGraph = false;

Display *gpDisplay = NULL;
Window gWindow;

bool bShouldPlay = true;
bool isLoaded = false;
bool gbShowRatio = false;

mat4 perspectiveProjectionMatrix;

struct ShaderUniforms GenericUniforms; // Must declare vars else unresolved symbols linking error

GLuint genericShaderProgramObject = 0;
GLuint vao_ratioline = -1;
GLuint vbo_ratioline_position = -1;
GLuint vbo_ratioline_color = -1;

GLuint vao_staticindia = -1;
GLuint vbo_staticindia_position = -1;
GLuint vbo_staticindia_color = -1;

const Triplet gDeepSaffron = { 1.0f,  0.59765625f,  0.19921875f };
const Triplet gIndiaGreen = { 0.07421875f,  0.53125f,  0.03125f };

Geometry GeometryList[NO_OF_GEOMETRIES];
GLuint vao_list[NO_OF_GEOMETRIES];
GLuint vbo_position_list[NO_OF_GEOMETRIES];
GLuint vbo_color_list[NO_OF_GEOMETRIES];
GLuint vbo_element_list[NO_OF_GEOMETRIES];

const float gStepRow = 2.0f / RATIO_ROWS;
const float gStepCol = 2.0f / RATIO_COLS;
const float gLetterHeight = gStepRow * 2.5f;
const float gBigLetterWidth = gStepCol * 1.5f;
const float gUnitMagnitude = gStepCol * 1.25f;
