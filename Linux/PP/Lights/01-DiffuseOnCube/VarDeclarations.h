#pragma once
#include <stdio.h> // file
#include <GL/glew.h>
#include "vmath.h"
#include <GL/glx.h> // For HDC only GLX Bridging API for XWindow -> OGL. Like in MS Win WGL

// static more than one storage class can not be specified
extern GLXContext gGLXContext; // Common context static if multiple monitors
extern Display *gpDisplay;
extern Window gWindow; // Kind of WND class

extern int MyCount;
extern FILE *gpFile;

extern GLuint vao_cube;
extern GLuint vbo_position_cube;
extern GLuint vbo_light_cube;
extern GLuint gShaderProgramObject;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

extern GLuint mvUniform; // model view
extern GLuint projectionUniform;
using namespace vmath;
extern mat4 perspectiveProjectionMatrix;

extern GLfloat angleCube;
extern GLfloat ascendingCube;

extern bool gbShowLight;
extern bool gbAnimate;

extern GLuint ldUniform;
extern GLuint kdUniform;
extern GLuint lightPoistionUniform;
extern GLuint isLKeyPressedUniform;
