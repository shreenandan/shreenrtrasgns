#pragma once
#include "VarDeclarations.h"

void incrCount(void);
int getCount(void);

void display(void);
void update(void);

void uninitializePP(void);
bool createFragmentShader(GLuint *fragmentShaderObject);
bool createVertexShader(GLuint *vertexShaderObject);
bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);
bool createShaders(void);
bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject);
void initVAOs(void);
