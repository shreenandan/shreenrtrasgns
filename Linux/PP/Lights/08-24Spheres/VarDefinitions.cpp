#pragma once
#include "VarDeclarations.h"
#include <stdio.h>

Display *gpDisplay = NULL;
Window gWindow;

int MyCount = 10;
FILE *gpFile = NULL;
GLuint vao_sphere_pv = -1;
GLuint vbo_position_sphere = -1;
GLuint vbo_light_sphere = -1;
GLuint gShaderProgramObject_pv = 0;
GLuint vao_sphere_pf = -1;
GLuint gShaderProgramObject_pf = 0;

mat4 perspectiveProjectionMatrix;

GLfloat angleCube = 0.0f;
GLfloat ascendingCube = true;

bool gbShowLight = false;
bool gbAnimate = false;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gVbo_sphere_element = -1;

int gNumVertices = -1;
int gNumElements = -1;

enum ShaderModes gSelectedMode = PER_VERTEX;
struct ShaderUniforms PerVertexUniforms, PerFragmentUniforms; // Must declare vars else unresolved symbols linking error

int gWidth = 800;
int gHeight = 600;

bool gbXKeyPressed = false;
bool gbYKeyPressed = false;
bool gbZKeyPressed = false;
