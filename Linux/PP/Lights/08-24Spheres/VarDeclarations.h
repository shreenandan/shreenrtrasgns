#pragma once
#include <stdio.h> // file
#include <GL/glew.h>
#include "vmath.h"
#include <GL/glx.h> // For HDC only GLX Bridging API for XWindow -> OGL. Like in MS Win WGL

// static more than one storage class can not be specified
extern GLXContext gGLXContext; // Common context static if multiple monitors
extern Display *gpDisplay;
extern Window gWindow; // Kind of WND class

extern int MyCount;
extern FILE *gpFile;
extern GLuint vao_sphere_pv;
extern GLuint vbo_position_sphere;
extern GLuint vbo_light_sphere;
extern GLuint gVbo_sphere_element;
extern GLuint gShaderProgramObject_pv;
extern GLuint vao_sphere_pf;
extern GLuint gShaderProgramObject_pf;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

using namespace vmath;
extern mat4 perspectiveProjectionMatrix;

extern GLfloat angleCube;
extern GLfloat ascendingCube;

extern bool gbShowLight;
extern bool gbAnimate;

extern float sphere_vertices[1146];
extern float sphere_normals[1146];
extern float sphere_textures[764];
extern unsigned short sphere_elements[2280];

extern int gNumVertices;
extern int gNumElements;

enum ShaderModes { PER_VERTEX, PER_FRAGMENT };
extern enum ShaderModes gSelectedMode;

struct ShaderUniforms
{
	GLuint modelUniform = -1;
	GLuint viewUniform = -1;
	GLuint projectionUniform = -1;

	GLuint laUniform = -1;
	GLuint ldUniform = -1;
	GLuint lsUniform = -1;

	GLuint kaUniform = -1;
	GLuint kdUniform = -1;
	GLuint ksUniform = -1;
	GLuint shinynessUniform = -1;

	GLuint lightPoistionUniform = -1;
	GLuint isLKeyPressedUniform = -1;
};

extern struct ShaderUniforms PerVertexUniforms, PerFragmentUniforms;

extern int gWidth;
extern int gHeight;

extern bool gbXKeyPressed;
extern bool gbYKeyPressed;
extern bool gbZKeyPressed;
