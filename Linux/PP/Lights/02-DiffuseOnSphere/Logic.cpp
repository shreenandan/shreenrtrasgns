#pragma once
#include "Logic.h"
#include <GL/glew.h>
#include <stdio.h> // file
#include "Sphere.h"

void incrCount(void)
{
	++MyCount;
}

int getCount(void)
{
	return MyCount;
}

void uninitializePP(void)
{
	// PP shader dtor
	// Safe Release
	// Don't declare shader objects globally, use locally in initialize n use as necessary
	if (vbo_light_sphere)
	{
		glDeleteBuffers(1, &vbo_light_sphere);
		vbo_light_sphere = 0;
	}
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNo;

		glUseProgram(gShaderProgramObject);

		// ask pgm how many shaders attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders); // using same var

			for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNo]);
				glDeleteShader(pShaders[shaderNo]);
				pShaders[shaderNo] = 0;
			}
			free(pShaders);
		}

		glDeleteProgram(gShaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nClosing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

const char* GLenumToString(GLenum shaderType)
{
	switch (shaderType)
	{
	case GL_VERTEX_SHADER:
		return "Vertex Shader";
	case GL_FRAGMENT_SHADER:
		return "Fragment Shader";
	default:
		return "Wrong Shader";
	}
}

bool createShaders(void)
{
	bool createVertexShader(GLuint *vertexShaderObject);
	bool createFragmentShader(GLuint *fragmentShaderObject);
	bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject);

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	bool successful = false;

	if (!createVertexShader(&vertexShaderObject))
	{
		return false;
	}
	if (!createFragmentShader(&fragmentShaderObject))
	{
		return false;
	}
	return createShaderProgramAndLink(vertexShaderObject, fragmentShaderObject);
}

bool createVertexShader(GLuint *vertexShaderObject)
{
	bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);

	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mv_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"in vec3 vNormal;" \
		"uniform int u_isLKeyPressed;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd;" \
		"uniform vec4 u_light_position;" \
		"out vec3 diffuseColor;" \
		"\n" \
		"void main(void)" \
		"{" \
		"	if(u_isLKeyPressed == 1)" \
		"	{" \
		"		vec4 eye_coordinates = u_mv_matrix * vPosition;" \
		"		mat3 normalMatrix = mat3(transpose(inverse(u_mv_matrix)));" \
		"		vec3 tNorm = normalize(normalMatrix * vNormal);" \
		"		vec3 source = normalize( vec3(u_light_position - eye_coordinates) );" \
		"		diffuseColor = u_ld * u_kd * max(dot(source, tNorm), 0.0);" \
		"		" \
		"	}" \
		"	gl_Position = u_projection_matrix * u_mv_matrix * vPosition;" \
		"}";

		// u_light_position must be vec4
		//"		vec3 source = vec3(u_light_position - eye_coordinates).xyz;" \

	return createShader(vertexShaderObject, GL_VERTEX_SHADER, vertexShaderSourceCode);
}

bool createFragmentShader(GLuint *fragmentShaderObject)
{
	bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);

	const GLchar *fragmentShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec3 diffuseColor;" \
		"uniform int u_isLKeyPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	if(u_isLKeyPressed == 1)" \
		"	{" \
		"		FragColor = vec4(diffuseColor, 1.0);" \
		"	}" \
		"	else" \
		"	{" \
		"		FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"	}" \
		"}";

	return createShader(fragmentShaderObject, GL_FRAGMENT_SHADER, fragmentShaderSourceCode);
}

bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode)
{
	const char* GLenumToString(GLenum shaderType);

	*shaderObject = glCreateShader(typeOfShader);
	glShaderSource(*shaderObject, 1, (const GLchar**)&shaderSourceCode, NULL);
	glCompileShader(*shaderObject);

	// steps for catching errors
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(*shaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*shaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(*shaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n%s: Compilation Error: %s", GLenumToString(typeOfShader), szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\n%s: failed to malloc szInfoLog...", GLenumToString(typeOfShader));
			}
		}
		else
		{
			fprintf(gpFile, "\n%s: Something went wrong, infoLogLength is zero...", GLenumToString(typeOfShader));
		}
		return false;
	}
	else
	{
		fprintf(gpFile, "\n%s compiled successfully.", GLenumToString(typeOfShader));
	}

	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	return true;
}

bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject)
{
	// step 1] create shader program obj
	gShaderProgramObject = glCreateProgram();

	// step 2] Attach shaders
	glAttachShader(gShaderProgramObject, vertexShaderObject);
	glAttachShader(gShaderProgramObject, fragmentShaderObject);

	// Pre-Linking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// step 3] Link program
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\nShader Program: Link Error: %s", szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\nShader Program: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fprintf(gpFile, "\nShader Program: Something went wrong, infoLogLength is zero...");
		}
		return false;
	}
	else
	{
		fprintf(gpFile, "\nShader program linked successfully.");
	}

	// Post-Linking retrieving uniform location
	mvUniform = glGetUniformLocation(gShaderProgramObject, "u_mv_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	isLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_isLKeyPressed");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	lightPoistionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	// reset
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	return true;
}

void bindVao(GLuint *theVao)
{
	glGenVertexArrays(1, theVao);
	glBindVertexArray(*theVao);
}

void unbindVao()
{
	glBindVertexArray(0);
}

void bindVbo(GLuint *theVbo)
{
	glGenBuffers(1, theVbo);
	glBindBuffer(GL_ARRAY_BUFFER, *theVbo);
}

void unbindVbo()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void bindVaoVbo(GLuint *theVao, GLuint *theVbo)
{
	glGenVertexArrays(1, theVao);
	glBindVertexArray(*theVao);

	glGenBuffers(1, theVbo);
	glBindBuffer(GL_ARRAY_BUFFER, *theVbo);
}

void unbindVboVao()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void fillBufferData(const float data[], int sizeofData, int noOfElementsInOneTuple, bool isStatic, int amc_attribute)
{
	if (isStatic)
	{
		glBufferData(GL_ARRAY_BUFFER, sizeofData, data, GL_STATIC_DRAW); // attachya atta oot
	}
	else
	{
		glBufferData(GL_ARRAY_BUFFER, sizeofData, NULL, GL_DYNAMIC_DRAW);
	}

	glVertexAttribPointer(amc_attribute,
		noOfElementsInOneTuple, // 3=xyx, 2=st
		GL_FLOAT,
		GL_FALSE, // isNormalized 0 to 1 NDC 
		0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
		NULL); // no stride therefore no offest

	glEnableVertexAttribArray(amc_attribute);
}

void initVaoSphere(void)
{
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	bindVao(&vao_sphere);

	bindVbo(&vbo_position_sphere);
	fillBufferData(sphere_vertices, sizeof(sphere_vertices), 3, true, AMC_ATTRIBUTE_POSITION);
	unbindVbo();

	bindVbo(&vbo_light_sphere);
	fillBufferData(sphere_normals, sizeof(sphere_normals), 3, true, AMC_ATTRIBUTE_NORMAL);
	unbindVbo();

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	unbindVao();
}

void initVAOs(void)
{
	void initVaoSphere(void);
	
	initVaoSphere();
}
