#pragma once
#include "VarDeclarations.h"
#include <stdio.h>

Display *gpDisplay = NULL;
Window gWindow;

int MyCount = 10;
FILE *gpFile = NULL;
GLuint vao_pyramid = -1;
GLuint vbo_position_pyramid = -1;
GLuint vbo_lights = -1;
GLuint gShaderProgramObject = 0;

GLuint modelUniform = -1;
GLuint viewUniform = -1;
GLuint projectionUniform = -1;
mat4 perspectiveProjectionMatrix;

GLfloat angleCube = 0.0f;
GLfloat ascendingCube = true;

bool gbShowLight = false;
bool gbAnimate = false;

GLuint laUniform = -1;
GLuint ldUniform = -1;
GLuint lsUniform = -1;

GLuint kaUniform = -1;
GLuint kdUniform = -1;
GLuint ksUniform = -1;
GLuint shinynessUniform = -1;

GLuint lightPoistionUniform = -1;
GLuint isLKeyPressedUniform = -1;

GLuint la2Uniform = -1;
GLuint ld2Uniform = -1;
GLuint ls2Uniform = -1;
GLuint lightPoistion2Uniform = -1;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gVbo_sphere_element = -1;

int gNumVertices = -1;
int gNumElements = -1;