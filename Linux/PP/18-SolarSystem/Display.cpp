#include "Logic.h"

void update(void)
{
}

void display(void)
{
	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]

	// Use Pass Through Shader Program
	glUseProgram(gShaderProgramObject); // Binding shader pgm to OpenGL pgm

	// your code here
	// 9 steps
	// declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	mat4 translationMatrix;

	// initialize above 2 matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	translationMatrix = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	// here in this pgm no transformation, but in later pgms
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);

	// do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum

	// send necessary matrices to shader in respective uniforms
	// display fn is dynamic, called in loop
	glUniformMatrix4fv(mvpUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelViewProjectionMatrix); // kashala chiktavaychay // verify
	// OpenGL/GLSL is column major, DirectX is row major



	
	// 1] SUN
	glBindVertexArray(vao_sphere);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 1.0f, 1.0f, 0.0f);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	// 2] EARTH
	modelViewProjectionMatrix = modelViewProjectionMatrix * rotate((GLfloat)year, 0.0f, 1.0f, 0.0f) * translate(1.1f, 0.0f, 0.0f) * scale(0.4f, 0.4f, 0.4f);
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao_sphere);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.4f, 0.9f, 1.0f);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_LINES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	// 3] MOON
	modelViewProjectionMatrix = modelViewProjectionMatrix * rotate((GLfloat)day, 0.0f, 1.0f, 0.0f) * translate(1.0f, 0.0f, 0.0f) * scale(0.3f, 0.3f, 0.3f);
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao_sphere);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 1.0f, 1.0f, 1.0f);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);



	/*
	if (gbShowLight)
	{
		glUniform1i(isLKeyPressedUniform, 1);
		glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f);
		glUniform3f(kdUniform, 0.50f, 0.50f, 0.50f);
		glUniform4f(lightPoistionUniform, 3.0f, 3.0f, 2.0f, 1.0f);
	}
	else
	{
		glUniform1i(isLKeyPressedUniform, 0);
	}
	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao_sphere);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind vao
	glBindVertexArray(0);
	*/


	glUseProgram(0); // Unbinding

	glXSwapBuffers(gpDisplay, gWindow);
}
