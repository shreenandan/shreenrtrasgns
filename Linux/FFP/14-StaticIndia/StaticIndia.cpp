/*
	Assignment: Static India
	Date: Jul 2019
 */

#include <iostream> // not used, just in future if required
// c++ standardization, no .h, "namespace cache"
#include <stdio.h>
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()

#include <X11/Xlib.h>   // similar to Windows.h
#include <X11/Xutil.h>  // XVisualInfo
#include <X11/XKBlib.h> // Keyboard Utility Header
#include <X11/keysym.h> // keycode to symbol mapping

#include <GL/gl.h>
#include <GL/glx.h> // GLX Bridging API for XWindow -> OGL. Like in MS Win WGL
#include <GL/glu.h> // Perspective change 1]

#define _USE_MATH_DEFINES
#include <cmath>

#define LINES_TO_DRAW_GRAPH 20
#define RATIO_COLS 16.0f
#define RATIO_ROWS 9.0f

// namespaces
using namespace std; // part of iostream, no need to write std:: scope resolution optr

// global variable declarations
bool bFullscreen = false; // CPP bool
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL; // Xutil.h PixelFormatDescriptor
Colormap gColormap;
Window gWindow;           // Kind of WND class
int giWindowWidth = 800;  // will be changed later. Hence no macro.
int giWindowHeight = 600; // nor constant

static GLXContext gGLXContext; // static if multiple monitors

FILE *gpFile = NULL;

// Drawing global variable declarations
bool gbShowGraph = false;
bool gbShowRatio = false;

typedef struct Coordinate
{
	float x;
	float y;
	float z;
}Coord;

typedef struct Triplet
{
	GLfloat red;
	GLfloat green;
	GLfloat blue;
}Triplet;

// FF9933
const Triplet gDeepSaffron = { 1.0f,  0.59765625f,  0.19921875f };
//0.59765625
//0.19921875

// 138808
const Triplet gIndiaGreen = { 0.07421875f,  0.53125f,  0.03125f };
//0.07421875
//0.53125
//0.03125

// entry-point function
// CUI, GUI sarva dharma samabhav
int main(void)
{
    // function prototypes
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void uninitialize();

    void initialize(void);
    void resize(int width, int height);
    void display(void);

    // variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    // code
    gpFile = fopen("log.txt", "w");
    if (gpFile == NULL)
	{
		printf("\nLog file can not be created...\n\n");
		exit(0); // Abortive but 0 and 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}

    CreateWindow(); // this fn made 4 vars alive

    // Message Loop
    XEvent event;
    KeySym keysym;

    initialize();

    while (bDone == false)
    {
        while (XPending(gpDisplay)) // PeekMsg
        {

            // GetMessage &msg
            XNextEvent(gpDisplay, &event);

            // message type from client
            switch (event.type)
            {
            case MapNotify:
                // WM_CREATE difference is that has power to both create as well as show window
                break;

            case KeyPress:
                keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                // XKBlib.h
                // Which page to use from Unicode, 0 is default which is ASCII
                // Is shift key pressed?
                switch (keysym)
                {
                case XK_Escape:
                    // uninitialize(); // remember lecture why not calls merged
                    // exit(0);        // stdlib.h msg.wParam
                    bDone = true;
                    break;

                case XK_f:
                case XK_F:
                    if (bFullscreen == false)
                    {
                        ToggleFullscreen();
                        bFullscreen = true;
                    }
                    else
                    {
                        ToggleFullscreen();
                        bFullscreen = false;
                    }
                    break;
                
                case 't':
                case XK_T:
                    gbShowGraph = !gbShowGraph;
                    break;
                    
                case XK_r:
                case 'R':
                    gbShowRatio = !gbShowRatio;
                    break;

                default:
                    break;
                }
                break;
                // end of case KeyPress

            case ButtonPress:
                switch (event.xbutton.button)
                {
                case 1: // left
                    break;
                case 2: // middle
                    break;
                case 3: // right
                    break;
                case 4: // wheel up
                    break;
                case 5: // wheel down
                    break;
                default:
                    break;
                }
                break;
                // end of case ButtonPress

            case MotionNotify:
                // WM_MOUSEMOVE
                break;

            case ConfigureNotify:
                // WM_SIZE
                winWidth = giWindowWidth = event.xconfigure.width;
                winHeight = giWindowHeight = event.xconfigure.height;

                resize(winWidth, winHeight);
                break;

            case Expose:
                // WM_PAINT
                break;

            case DestroyNotify:
                // WM_DESTROY but behavior is similar to WM_CLOSE
                // asks to close yes, no
                break;

            case 33:
                // WM_CLOSE important XProtocol book part 1 of 9
                // only close msg is sent, later need to handle what action to take
                // uninitialize();
                // exit(0);

                bDone = true;
                // remember lecture, why seperate two calls. resize, device dependent handling, again uninit and re-display
                break;

            default:
                break;
            }

        } // end of XPending

        display();

    } // end of bDone

    // can't use caps TRUE, must be 1
    // no seperate WndProc kept, DispatchMessage
    // while (1){ theBlock in XPending }

    uninitialize();

    return (0);
}

void CreateWindow(void)
{
    // function prototypes
    void uninitialize(void);

    // variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    // Double Buffer 8 bit
    static int frameBufferAttributes[] = 
    {
        GLX_RGBA,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Open X Display.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = DefaultDepth(gpDisplay, defaultScreen); // not required now, but kept if reverted back code

    // Bridging API gives visual for your provided framebuf attribs
    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    /*
    // Imagination code
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    int matchingVisual = XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
    // status check not really required
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Get A Visual.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }
    */

    winAttribs.border_pixel = 0;
    winAttribs.border_pixmap = 0; // all unix based pixmap, MS bitmap
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    // Don't allocate memory if we are going to save it
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);

    if (!gWindow)
    {
        printf("ERROR : Unable to create Main Window.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to create Main Window.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    XStoreName(gpDisplay, gWindow, "FFP Static India");

    // Important action to take on close 33
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
    fprintf(gpFile, "CreateWindow successful.\n");
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    GLXContext currentGLXContext = glXGetCurrentContext();

    // In multi monitor scenario current and global can be different
    if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
    {
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    if (gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }    

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if (gpFile)
	{
		fprintf(gpFile, "Closing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void MakeFullScreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
    bFullscreen = true;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
    // function prototype
    void uninitialize(void);
    void resize(int width, int height);
    void MakeFullScreen(void);
    
    gGLXContext = glXCreateContext(gpDisplay, 
        gpXVisualInfo,
        NULL,
        GL_TRUE);

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // usual code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Cheks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when renedering starts, display().
	//    "OpenGL is a state machine."

    MakeFullScreen();
    fprintf(gpFile, "initialize successful.\n");

	// warm up call to resize, convention and not compulsion
	resize(giWindowWidth, giWindowHeight); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()
}

void resize(int width, int height)
{
	// Perspective change 2]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular
    
	 // Perspective change 3] NOTE: Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	 // Perspective change 4]
	gluPerspective(0.0f,
        (GLfloat)width / (GLfloat)height,
        0.1f,
        100.0f);
}

void display(void)
{    
    void drawGraph(void);
	void drawRatioLine(void);
	void drawStaticIndia(void);

	glClear(GL_COLOR_BUFFER_BIT);
	// Double Buffer change 7] glFlush(); removed

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Perspective change 4]
	glTranslatef(0.0f, 0.0f, 0.0f); // Z negative = inside/away from you
	/*gluLookAt(0.0f, 0.0f, 3.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f);*/

	if (gbShowGraph)
		drawGraph();
	if (gbShowRatio)
		drawRatioLine();

	drawStaticIndia();

	glXSwapBuffers(gpDisplay, gWindow);
}

void drawGraph(void)
{
	// variable declarations
	int i = 0;
	float step = 1.0f / LINES_TO_DRAW_GRAPH;
	float xcoord = step; // 0.0f;
	float ycoord = step; // 0.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-xcoord, 1.0f);
		glVertex2f(-xcoord, -1.0f);
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord += step;
	}

	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		glVertex2f(-1.0f, -ycoord);
		glVertex2f(1.0f, -ycoord);
		ycoord += step;
	}

	glEnd();
}

void drawRatioLine(void)
{
	// variable declarations
	int i = 0;
	float stepRow = 2.0f / RATIO_ROWS;
	float stepCol = 2.0f / RATIO_COLS;
	float xcoord = 1.0f;
	float ycoord = 1.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	for (i = 0; i <= RATIO_COLS; i++)
	{
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord -= stepCol;
	}

	for (i = 0; i <= RATIO_ROWS; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		ycoord -= stepRow;
	}

	glEnd();
}

void drawTriangle(void)
{
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, -1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(1.0f, -1.0f);
	glEnd();
}

void drawStaticIndia(void)
{
	// function declarations
	void drawTricolorInLetterA(float side1X1, float side1Y1, float side1X2, float side1Y2,
		float side2X1, float side2Y1, float side2X2, float side2Y2);

	// variable declarations
	int i = 0;
	static float stepRow = 2.0f / RATIO_ROWS;
	static float stepCol = 2.0f / RATIO_COLS;
	static float xcoord = 1.0f;
	static float ycoord = 1.0f;
	static float theHeight = stepRow * 2.5f;
	static float IposX; // = -1.0f + (stepCol * 3.0f);
	static float bigLetterWidth = stepCol * 1.5f;
	static float distanceInLetter = stepCol / 2.0f;
	static Coord p1 = { 0,0,0 }, p2 = { 0,0,0 }, p3 = { 0,0,0 };
	IposX = -1.0f + (stepCol * 4.75f);// +distanceInLetter;

	glLineWidth(5.0f);
	glBegin(GL_LINES);

	// I
	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(IposX, theHeight);
	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(IposX, -theHeight);

	// N
	IposX += distanceInLetter;
	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(IposX, theHeight);
	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(IposX, -theHeight);

	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(IposX, theHeight);
	IposX += bigLetterWidth;
	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(IposX, -theHeight);

	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(IposX, theHeight);
	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(IposX, -theHeight);

	// D
	IposX += distanceInLetter;
	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(IposX, theHeight);
	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(IposX, -theHeight);

	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(IposX, theHeight);
	glVertex2f(IposX + bigLetterWidth, theHeight);

	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(IposX, -theHeight);
	glVertex2f(IposX + bigLetterWidth, -theHeight);
	IposX += bigLetterWidth;

	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(IposX, theHeight);
	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(IposX, -theHeight);

	// I
	IposX += distanceInLetter;
	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(IposX, theHeight);
	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(IposX, -theHeight);

	// A
	IposX += distanceInLetter;
	p1.x = IposX, p1.y = -theHeight;
	IposX += (bigLetterWidth / 2.0f);
	p2.x = IposX, p2.y = theHeight;
	IposX += (bigLetterWidth / 2.0f);
	p3.x = IposX, p3.y = -theHeight;

	drawTricolorInLetterA(p1.x, p1.y, p2.x, p2.y, p2.x, p2.y, p3.x, p3.y);

	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(p1.x, -theHeight);
	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(p2.x, theHeight);

	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex2f(p2.x, theHeight);
	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex2f(p3.x, -theHeight);

	glEnd();
}

// step 1] find mid points of both sides
// step 2] can draw middle line joining both mid points
// step 3] find lengths of all three sides
// step 4] Upper: now we have lengths of all sides. find one angle. Inverse cos
//			cos A = (b2 + c2 - a2) /2bc		A = cos inverse(value)
// step 5] Lower: angle = 180 - upper. Inverse cos
// step 6].a find next point of side 1 nextS1A'(x + pos * cos(A), y + pos * sin(A))
// step 6].b find next point of side 2 nextS2A'(x + pos * cos(A), y + pos * sin(A))
// step 7] draw line from obtained points nextS1A' <--> nextS2A'
// step 8].a find prev point of side 1 prevS1A'(x + pos * cos(A), y + pos * sin(A))
// step 8].b find prev point of side 2 prevS2A'(x + pos * cos(A), y + pos * sin(A))
// step 9] draw line from obtained points prevS1A' <--> prevS2A'
void drawTricolorInLetterA(float side1X1, float side1Y1, float side1X2, float side1Y2,
	float side2X1, float side2Y1, float side2X2, float side2Y2)
{
	// midpoint = (x1+x2)/2, (y1+y2)/2
	static Coord side1MidPoint, side2MidPoint;

	side1MidPoint.x = (side1X1 + side1X2) / 2.0f;
	side1MidPoint.y = (side1Y1 + side1Y2) / 2.0f;
	side1MidPoint.z = 0.0f;

	side2MidPoint.x = (side2X1 + side2X2) / 2.0f;
	side2MidPoint.y = (side2Y1 + side2Y2) / 2.0f;
	side2MidPoint.z = 0.0f;

	static float lengthB = sqrtf(powf((side1MidPoint.x - side1X2), 2) + powf((side1MidPoint.y - side1Y2), 2));
	static float lengthC = sqrtf(powf((side1MidPoint.x - side2MidPoint.x), 2) + powf((side1MidPoint.y - side2MidPoint.y), 2));
	static float lengthA = sqrtf(powf((side2MidPoint.x - side2X2), 2) + powf((side2MidPoint.y - side2Y2), 2));

	static float side1CosA = ((lengthB*lengthB) + (lengthC*lengthC) - (lengthA*lengthA)) / (2.0f *lengthB * lengthC);
	static float side1AngleA = acosf(side1CosA) * (180.0f / M_PI); // radian to degree
	static float side1CosB = ((lengthA*lengthA) + (lengthC*lengthC) - (lengthB*lengthB)) / (2.0f *lengthA * lengthC);
	static float side1AngleB = acosf(side1CosB) * (180.0f / M_PI); // radian to degree

	static float nextS1Ax = side1MidPoint.x + (0.002f * cosf(side1AngleA));
	static float nextS1Ay = side1MidPoint.y + (0.035f * sinf(side1AngleA));
	static float nextS2Ax = side2MidPoint.x - (0.002f * cosf(side1AngleB));
	static float nextS2Ay = side2MidPoint.y + (0.035f * sinf(side1AngleB));

	static float prevS1Ax = side1MidPoint.x - (0.002f * cosf(side1AngleA));
	static float prevS1Ay = side1MidPoint.y - (0.035f * sinf(side1AngleA));
	static float prevS2Ax = side2MidPoint.x + (0.002f * cosf(side1AngleB));
	static float prevS2Ay = side2MidPoint.y - (0.035f * sinf(side1AngleB));

	glColor3f(gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue);
	glVertex3f(prevS1Ax, prevS1Ay, side1MidPoint.z);
	glVertex3f(prevS2Ax, prevS2Ay, side2MidPoint.z);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(side1MidPoint.x, side1MidPoint.y, side1MidPoint.z);
	glVertex3f(side2MidPoint.x, side2MidPoint.y, side2MidPoint.z);
	
	glColor3f(gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue);
	glVertex3f(nextS1Ax, nextS1Ay, side1MidPoint.z);
	glVertex3f(nextS2Ax, nextS2Ay, side2MidPoint.z);

}

//"program": "${fileDirname}/${fileBasenameNoExtension}",
//

