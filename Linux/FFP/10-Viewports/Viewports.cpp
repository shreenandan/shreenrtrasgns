/*
	Assignment: Implement PERSPECTIVE VIEWPORT transformation on numeric keys.
	Date: Jul 2019
 */

#include <iostream> // not used, just in future if required
// c++ standardization, no .h, "namespace cache"
#include <stdio.h>
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()

#include <X11/Xlib.h>   // similar to Windows.h
#include <X11/Xutil.h>  // XVisualInfo
#include <X11/XKBlib.h> // Keyboard Utility Header
#include <X11/keysym.h> // keycode to symbol mapping

#include <GL/gl.h>
#include <GL/glx.h> // GLX Bridging API for XWindow -> OGL. Like in MS Win WGL
#include <GL/glu.h> // Perspective change 1]

#define _USE_MATH_DEFINES
#include <cmath>

#define LINES_TO_DRAW_GRAPH 20
#define LINES_TO_DRAW_CIRCLE 1000.0f

// namespaces
using namespace std; // part of iostream, no need to write std:: scope resolution optr

// global variable declarations
bool bFullscreen = false; // CPP bool
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL; // Xutil.h PixelFormatDescriptor
Colormap gColormap;
Window gWindow;           // Kind of WND class
int giWindowWidth = 800;  // will be changed later. Hence no macro.
int giWindowHeight = 600; // nor constant

static GLXContext gGLXContext; // static if multiple monitors

FILE *gpFile = NULL;
float depth = -3.0f; // default

// entry-point function
// CUI, GUI sarva dharma samabhav
int main(void)
{
    // function prototypes
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void uninitialize();

    void initialize(void);
    void resize(int width, int height);
    void display(void);

    void changeViewportOnNumpad(KeySym keysym, int wndWidth, int wndHeight);

    // variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    // code
    gpFile = fopen("log.txt", "w");
    if (gpFile == NULL)
	{
		printf("\nLog file can not be created...\n\n");
		exit(0); // Abortive but 0 and 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}

    CreateWindow(); // this fn made 4 vars alive

    // Message Loop
    XEvent event;
    KeySym keysym;

    initialize();

    while (bDone == false)
    {
        while (XPending(gpDisplay)) // PeekMsg
        {

            // GetMessage &msg
            XNextEvent(gpDisplay, &event);

            // message type from client
            switch (event.type)
            {
            case MapNotify:
                // WM_CREATE difference is that has power to both create as well as show window
                break;

            case KeyPress:
                keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                // XKBlib.h
                // Which page to use from Unicode, 0 is default which is ASCII
                // Is shift key pressed?
                switch (keysym)
                {
                case XK_Escape:
                    // uninitialize(); // remember lecture why not calls merged
                    // exit(0);        // stdlib.h msg.wParam
                    bDone = true;
                    break;

                case XK_f:
                case XK_F:
                    if (bFullscreen == false)
                    {
                        ToggleFullscreen();
                        bFullscreen = true;
                    }
                    else
                    {
                        ToggleFullscreen();
                        bFullscreen = false;
                    }
                    break;
                    
                default:
                    changeViewportOnNumpad(keysym, giWindowWidth, giWindowHeight);
                    break;
                }
                break;
                // end of case KeyPress

            case ButtonPress:
                switch (event.xbutton.button)
                {
                case 1: // left
                    break;
                case 2: // middle
                    break;
                case 3: // right
                    break;
                case 4: // wheel up
                    break;
                case 5: // wheel down
                    break;
                default:
                    break;
                }
                break;
                // end of case ButtonPress

            case MotionNotify:
                // WM_MOUSEMOVE
                break;

            case ConfigureNotify:
                // WM_SIZE
                winWidth = giWindowWidth = event.xconfigure.width;
                winHeight = giWindowHeight = event.xconfigure.height;

                resize(winWidth, winHeight);
                break;

            case Expose:
                // WM_PAINT
                break;

            case DestroyNotify:
                // WM_DESTROY but behavior is similar to WM_CLOSE
                // asks to close yes, no
                break;

            case 33:
                // WM_CLOSE important XProtocol book part 1 of 9
                // only close msg is sent, later need to handle what action to take
                // uninitialize();
                // exit(0);

                bDone = true;
                // remember lecture, why seperate two calls. resize, device dependent handling, again uninit and re-display
                break;

            default:
                break;
            }

        } // end of XPending

        display();

    } // end of bDone

    // can't use caps TRUE, must be 1
    // no seperate WndProc kept, DispatchMessage
    // while (1){ theBlock in XPending }

    uninitialize();

    return (0);
}

void CreateWindow(void)
{
    // function prototypes
    void uninitialize(void);

    // variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    // Double Buffer 8 bit
    static int frameBufferAttributes[] = 
    {
        GLX_RGBA,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Open X Display.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = DefaultDepth(gpDisplay, defaultScreen); // not required now, but kept if reverted back code

    // Bridging API gives visual for your provided framebuf attribs
    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    /*
    // Imagination code
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    int matchingVisual = XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
    // status check not really required
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Get A Visual.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }
    */

    winAttribs.border_pixel = 0;
    winAttribs.border_pixmap = 0; // all unix based pixmap, MS bitmap
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    // Don't allocate memory if we are going to save it
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);

    if (!gWindow)
    {
        printf("ERROR : Unable to create Main Window.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to create Main Window.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    XStoreName(gpDisplay, gWindow, "FFP Multiple Viewports");

    // Important action to take on close 33
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
    fprintf(gpFile, "CreateWindow successful.\n");
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    GLXContext currentGLXContext = glXGetCurrentContext();

    // In multi monitor scenario current and global can be different
    if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
    {
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    if (gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }    

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if (gpFile)
	{
		fprintf(gpFile, "Closing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void initialize(void)
{
    // function prototype
    void uninitialize(void);
    void resize(int width, int height);

    gGLXContext = glXCreateContext(gpDisplay, 
        gpXVisualInfo,
        NULL,
        GL_TRUE);

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // usual code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Cheks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when renedering starts, display().
	//    "OpenGL is a state machine."

    fprintf(gpFile, "initialize successful.\n");

	// warm up call to resize, convention and not compulsion
	resize(giWindowWidth, giWindowHeight); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()
}

void resize(int width, int height)
{
	// Perspective change 2]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular
    
	 // Perspective change 3] NOTE: Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	 // Perspective change 4]
	gluPerspective(45.0f,
        (GLfloat)width / (GLfloat)height,
        0.1f,
        100.0f);
}

void display(void)
{    
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Perspective change 5]
    glTranslatef(0.0f, 0.0f, -3); // Z negative = inside/away from you

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, 1.0f); // Perspective no change

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, -1.0f); // Perspective no change

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(1.0f, -1.0f); // Perspective no change
	glEnd();

	glXSwapBuffers(gpDisplay, gWindow);
}

void changeViewportOnNumpad(KeySym keysym, int wndWidth, int wndHeight)
{
    depth = -3.0f;
    printf("%d %x %c %ld\n", keysym, keysym, keysym, keysym);
	switch (keysym)
	{
	case XK_0: // Normal
	case XK_KP_0:
    case 0xff9e:
		glViewport(0, 0, (GLsizei)wndWidth, (GLsizei)wndHeight);
		break;

	case XK_1: // Left Bottom
	case XK_KP_1:
    case 0xff9c:
		glViewport(0, 0, (GLsizei)wndWidth / 2, (GLsizei)wndHeight / 2);
		break;

	case XK_2: // Right Bottom
	case XK_KP_2:
    case 0xff99:
		glViewport((GLsizei)wndWidth / 2, 0, (GLsizei)wndWidth / 2, (GLsizei)wndHeight / 2);
		break;

	case XK_3: // Top Right
	case XK_KP_3:
    case 0xff9b:
		glViewport((GLsizei)wndWidth / 2, (GLsizei)wndHeight / 2, (GLsizei)wndWidth / 2, (GLsizei)wndHeight / 2);
		break;

	case XK_4: // Top Left
	case XK_KP_4:
    case 0xff96:
		glViewport(0, (GLsizei)wndHeight / 2, (GLsizei)wndWidth / 2, (GLsizei)wndHeight / 2);
		break;

	case XK_5: // Left Half
	case XK_KP_5:
    case 0xff9d:
		depth = bFullscreen ? depth : -4.0f;
		glViewport(0, 0, (GLsizei)wndWidth / 2, (GLsizei)wndHeight);
		break;

	case XK_6: // Right Half
	case XK_KP_6:
    case 0xff98:
		depth = bFullscreen ? depth : -4.0f;
		glViewport((GLsizei)wndWidth / 2, 0, (GLsizei)wndWidth / 2, (GLsizei)wndHeight);
		break;

	case XK_7: // Top Half
	case XK_KP_7:
    case 0xff95:
		glViewport(0, (GLsizei)wndHeight / 2, (GLsizei)wndWidth, (GLsizei)wndHeight / 2);
		break;

	case XK_8: // Bottom Half
	case XK_KP_8:
    case 0xff97:
		glViewport(0, 0, (GLsizei)wndWidth, (GLsizei)wndHeight / 2);
		break;

	case XK_9: // Middle
	case XK_KP_9:
    case 0xff9a:
		glViewport((GLsizei)wndWidth / 4, (GLsizei)wndHeight / 4, (GLsizei)wndWidth / 2, (GLsizei)wndHeight / 2);
		break;

	default:
		break;
	}
}

//"program": "${fileDirname}/${fileBasenameNoExtension}",
//
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/10-Viewports$ g++ -o Viewports Viewports.cpp -lX11 -lGL -lGLU
// Viewports.cpp: In function ‘void changeViewportOnNumpad(KeySym, int, int)’:
// Viewports.cpp:461:60: warning: format ‘%d’ expects argument of type ‘int’, but argument 2 has type ‘KeySym {aka long unsigned int}’ [-Wformat=]
//      printf("%d %x %c %ld\n", keysym, keysym, keysym, keysym);
//                                                             ^
// Viewports.cpp:461:60: warning: format ‘%x’ expects argument of type ‘unsigned int’, but argument 3 has type ‘KeySym {aka long unsigned int}’ [-Wformat=]
// Viewports.cpp:461:60: warning: format ‘%c’ expects argument of type ‘int’, but argument 4 has type ‘KeySym {aka long unsigned int}’ [-Wformat=]
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/10-Viewports$ ./Viewports 
