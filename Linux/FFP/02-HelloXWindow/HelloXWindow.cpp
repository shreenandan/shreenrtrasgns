#include <iostream> // not used, just in future if required
// c++ standardization, no .h, "namespace cache"
#include <stdio.h>
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()

#include <X11/Xlib.h>   // similar to Windows.h
#include <X11/Xutil.h>  // XVisualInfo
#include <X11/XKBlib.h> // Keyboard Utility Header
#include <X11/keysym.h> // keycode to symbol mapping

// namespaces
using namespace std; // part of iostream, no need to write std:: scope resolution optr

// global variable declarations
bool bFullscreen = false; // CPP bool
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL; // Xutil.h PixelFormatDescriptor
Colormap gColormap;
Window gWindow;           // Kind of WND class
int giWindowWidth = 800;  // will be changed later. Hence no macro.
int giWindowHeight = 600; // nor constant

// entry-point function
// CUI, GUI sarva dharma samabhav
int main(void)
{
    // function prototypes
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void uninitialize();

    // variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    static XFontStruct *pXFontStruct = NULL;
    static GC gc;
    XGCValues gcValues; // PAINSTRUCT ps
    XColor text_color;
    char str[255] = "Hello World!!!"; 
    int strLength;
    static int strWidth; // fontWidth
    static int fontHeight;

    // code
    CreateWindow(); // this fn made 4 vars alive

    // Message Loop
    XEvent event;
    KeySym keysym;

    // can't use caps TRUE, must be 1
    // no seperate WndProc kept, DispatchMessage
    while (1)
    {
        // GetMessage &msg
        XNextEvent(gpDisplay, &event);

        // message type from client
        switch (event.type)
        {
        case MapNotify:
            // WM_CREATE difference is that has power to both create as well as show window
            pXFontStruct = XLoadQueryFont(gpDisplay, "fixed"); // default system font
            // no validation required as there will always be one font
            break;

        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            // XKBlib.h
            // Which page to use from Unicode, 0 is default which is ASCII
            // Is shift key pressed?
            switch (keysym)
            {
            case XK_Escape:
                uninitialize(); // remember lecture why not calls merged
                exit(0);        // stdlib.h msg.wParam
                break;

            case XK_f:
            case XK_F:
                if (bFullscreen == false)
                {
                    ToggleFullscreen();
                    bFullscreen = true;
                }
                else
                {
                    ToggleFullscreen();
                    bFullscreen = false;
                }
                break;

            default:
                break;
            }
            break;
            // end of case KeyPress

        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1: // left
                break;
            case 2: // middle
                break;
            case 3: // right
                break;
            case 4: // wheel up
                break;
            case 5: // wheel down
                break;
            default:
                break;
            }
            break;
            // end of case ButtonPress

        case MotionNotify:
            // WM_MOUSEMOVE
            break;

        case ConfigureNotify:
            // WM_SIZE
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            break;

        case Expose:
            // WM_PAINT
            
            gc = XCreateGC(gpDisplay, gWindow, 0, &gcValues); // 0 = all
            XSetFont(gpDisplay, gc, pXFontStruct->fid);
            XAllocNamedColor(gpDisplay, 
                gColormap,
                "green",
                &text_color, // if not found, returns nearest matching in 4th
                &text_color); // if exact found returned in 5th
            XSetForeground(gpDisplay, gc, text_color.pixel);

            strLength = strlen(str); // stdlib.h, if error include string.h
            strWidth = XTextWidth(pXFontStruct, str, strLength); // no fid
            // baseline is always present
            fontHeight = pXFontStruct->ascent + pXFontStruct->descent;

            XDrawString(gpDisplay, 
                gWindow,
                gc,
                winWidth/2 - strWidth/2,
                winHeight/2 - fontHeight/2,
                str,
                strLength);
            break;

            case DestroyNotify:
            // WM_DESTROY but behavior is similar to WM_CLOSE
            // asks to close yes, no
            break;

            case 33:
            // WM_CLOSE important XProtocol book part 1 of 9
            // only close msg is sent, later need to handle what action to take
            XFreeGC(gpDisplay, gc);
            XUnloadFont(gpDisplay, pXFontStruct->fid);

            uninitialize();
            exit(0);
            // remember lecture, why seperate two calls. resize, device dependent handling, again uninit and re-display
            break;

        default:
            break;
        }
    }

    uninitialize();
    
    return(0);
}

void CreateWindow(void)
{
    // function prototypes
    void uninitialize(void);

    // variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    // code
    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }
    
    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

    // Imagination code
    gpXVisualInfo = (XVisualInfo*)malloc(sizeof(XVisualInfo));
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    int matchingVisual = XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
    // status check not really required
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Get A Visual.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    winAttribs.border_pixel = 0;
    winAttribs.border_pixmap = 0; // all unix based pixmap, MS bitmap
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
        RootWindow(gpDisplay, gpXVisualInfo->screen),
        gpXVisualInfo->visual,
        AllocNone);
        // Don't allocate memory if we are going to save it
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask 
        | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
    
    gWindow = XCreateWindow(gpDisplay,
        RootWindow(gpDisplay, gpXVisualInfo->screen),
        0,
        0,
        giWindowWidth,
        giWindowHeight,
        0,
        gpXVisualInfo->depth,
        InputOutput,
        gpXVisualInfo->visual,
        styleMask,
        &winAttribs);

    if (!gWindow)
    {
        printf("ERROR : Unable to create Main Window.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    XStoreName(gpDisplay, gWindow, "Hello Text XWindow");

    // Important action to take on close 33
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    False,
    StructureNotifyMask,
    &xev);
}

void uninitialize(void)
{
    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }
    
    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }
    
    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }    
}

//"program": "${fileDirname}/${fileBasenameNoExtension}",
// 
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/01-XWindow$ ls                 XWindowFullscreen.cpp
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/01-XWindow$ g++ -o xwindow 
// .vscode/               XWindowFullscreen.cpp  
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/01-XWindow$ g++ -o xwindow XWindowFullscreen.cpp -lX11
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/01-XWindow$ ls                 xwindow  XWindowFullscreen.cpp
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/01-XWindow$ ./xwindow 
