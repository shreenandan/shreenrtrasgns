#include <AL/al.h>
#include <AL/alc.h>
#include <iostream>
#include <AL/alext.h>
#include <AL/efx.h>

#include <iostream>
#ifdef WIN32
    #include <windows.h>
#else
    #include <unistd.h>
#endif // win32

using namespace std;

int main(void)
{
    ALCdevice *alcDevice = alcOpenDevice(NULL);
    if(!alcDevice)
        printf("\nError: failed open device");

    ALCcontext *pc = alcCreateContext(alcDevice, NULL);
    if(!pc)
    {
        alcCloseDevice(alcDevice);
        printf("\nError: failed create context");
    }

    ALCboolean cc = alcMakeContextCurrent(pc);

    ALuint id = 0, bid = 0;
    alGenSources(1, &id);
    alSourcei(id, AL_LOOPING, false);

    alGenBuffers(1, &bid);

    ALsizei size = 22050 * 5;
    char *data = new char[size];

    alBufferData(bid,AL_FORMAT_MONO8, data, size, 22050);
    alSourcei(id, AL_BUFFER, bid);

    for (int i = 0; i < size; i++)
    {
        data[i] = i;
    }
    

    alSourcePlay(id);

    #ifdef WIN32
        Sleep(milliseconds);
    #else
        usleep(7000 * 1000);
    #endif // win32

    alSourceStop(id);
    
    alDeleteSources(1, &id);
    alDeleteBuffers(1, &bid);
    
    cc = alcMakeContextCurrent(NULL);
    alcDestroyContext(pc);
    alcCloseDevice(alcDevice);

    return 0;
}

// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/OpenAL-1$ g++ openal1.cpp -lopenal
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/OpenAL-1$ ./a.out 