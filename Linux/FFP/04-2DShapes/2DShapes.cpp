/*
	Assignment: Draw yellow colored all shapes in perspective.
	Graph, Outer Circle, Triangle, Inner Circle, Rectangle
	Additional feature: Press 'T' or 't' to toggle(show/hide) graph. Default shown.
	Date: Jul 2019
 */

#include <iostream> // not used, just in future if required
// c++ standardization, no .h, "namespace cache"
#include <stdio.h>
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()

#include <X11/Xlib.h>   // similar to Windows.h
#include <X11/Xutil.h>  // XVisualInfo
#include <X11/XKBlib.h> // Keyboard Utility Header
#include <X11/keysym.h> // keycode to symbol mapping

#include <GL/gl.h>
#include <GL/glx.h> // GLX Bridging API for XWindow -> OGL. Like in MS Win WGL
#include <GL/glu.h> // Perspective change 1]

#define _USE_MATH_DEFINES
#include <cmath>

#define LINES_TO_DRAW_GRAPH 20
#define LINES_TO_DRAW_CIRCLE 1000.0f

// namespaces
using namespace std; // part of iostream, no need to write std:: scope resolution optr

// global variable declarations
bool bFullscreen = false; // CPP bool
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL; // Xutil.h PixelFormatDescriptor
Colormap gColormap;
Window gWindow;           // Kind of WND class
int giWindowWidth = 800;  // will be changed later. Hence no macro.
int giWindowHeight = 600; // nor constant

static GLXContext gGLXContext; // static if multiple monitors

bool gbShowGraph = true;
// Drawing global variable declarations
float gfMagnitude = 0.5f; // 0.96f;
float gfZero = 0.0f;

// entry-point function
// CUI, GUI sarva dharma samabhav
int main(void)
{
    // function prototypes
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void uninitialize();

    void initialize(void);
    void resize(int width, int height);
    void display(void);

    // variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    // code
    CreateWindow(); // this fn made 4 vars alive

    // Message Loop
    XEvent event;
    KeySym keysym;

    initialize();

    while (bDone == false)
    {
        while (XPending(gpDisplay)) // PeekMsg
        {

            // GetMessage &msg
            XNextEvent(gpDisplay, &event);

            // message type from client
            switch (event.type)
            {
            case MapNotify:
                // WM_CREATE difference is that has power to both create as well as show window
                break;

            case KeyPress:
                keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                // XKBlib.h
                // Which page to use from Unicode, 0 is default which is ASCII
                // Is shift key pressed?
                switch (keysym)
                {
                case XK_Escape:
                    // uninitialize(); // remember lecture why not calls merged
                    // exit(0);        // stdlib.h msg.wParam
                    bDone = true;
                    break;

                case XK_f:
                case XK_F:
                    if (bFullscreen == false)
                    {
                        ToggleFullscreen();
                        bFullscreen = true;
                    }
                    else
                    {
                        ToggleFullscreen();
                        bFullscreen = false;
                    }
                    break;
                
                case XK_T:
                case XK_t:
                    gbShowGraph = !gbShowGraph;
                    break;
                    
                default:
                    break;
                }
                break;
                // end of case KeyPress

            case ButtonPress:
                switch (event.xbutton.button)
                {
                case 1: // left
                    break;
                case 2: // middle
                    break;
                case 3: // right
                    break;
                case 4: // wheel up
                    break;
                case 5: // wheel down
                    break;
                default:
                    break;
                }
                break;
                // end of case ButtonPress

            case MotionNotify:
                // WM_MOUSEMOVE
                break;

            case ConfigureNotify:
                // WM_SIZE
                winWidth = event.xconfigure.width;
                winHeight = event.xconfigure.height;

                resize(winWidth, winHeight);
                break;

            case Expose:
                // WM_PAINT
                break;

            case DestroyNotify:
                // WM_DESTROY but behavior is similar to WM_CLOSE
                // asks to close yes, no
                break;

            case 33:
                // WM_CLOSE important XProtocol book part 1 of 9
                // only close msg is sent, later need to handle what action to take
                // uninitialize();
                // exit(0);

                bDone = true;
                // remember lecture, why seperate two calls. resize, device dependent handling, again uninit and re-display
                break;

            default:
                break;
            }

        } // end of XPending

        display();

    } // end of bDone

    // can't use caps TRUE, must be 1
    // no seperate WndProc kept, DispatchMessage
    // while (1){ theBlock in XPending }

    uninitialize();

    return (0);
}

void CreateWindow(void)
{
    // function prototypes
    void uninitialize(void);

    // variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    // Double Buffer 8 bit
    static int frameBufferAttributes[] = 
    {
        GLX_RGBA,
        GLX_DOUBLEBUFFER, true,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = DefaultDepth(gpDisplay, defaultScreen); // not required now, but kept if reverted back code

    // Bridging API gives visual for your provided framebuf attribs
    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    /*
    // Imagination code
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    int matchingVisual = XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
    // status check not really required
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Get A Visual.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }
    */

    winAttribs.border_pixel = 0;
    winAttribs.border_pixmap = 0; // all unix based pixmap, MS bitmap
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    // Don't allocate memory if we are going to save it
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);

    if (!gWindow)
    {
        printf("ERROR : Unable to create Main Window.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    XStoreName(gpDisplay, gWindow, "FFP All Shapes 2D");

    // Important action to take on close 33
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    GLXContext currentGLXContext = glXGetCurrentContext();

    // In multi monitor scenario current and global can be different
    if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
    {
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    if (gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }    

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

void initialize(void)
{
    // function prototype
    void uninitialize(void);
    void resize(int width, int height);

    gGLXContext = glXCreateContext(gpDisplay, 
        gpXVisualInfo,
        NULL,
        GL_TRUE);

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // usual code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Cheks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when renedering starts, display().
	//    "OpenGL is a state machine."

	// warm up call to resize, convention and not compulsion
	resize(giWindowWidth, giWindowHeight); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()
}

void resize(int width, int height)
{
	// Perspective change 2]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular
    
	 // Perspective change 3] NOTE: Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	 // Perspective change 4]
	gluPerspective(45.0f,
        (GLfloat)width / (GLfloat)height,
        0.1f,
        100.0f);
}

void display(void)
{
    // function declarations
	void drawTriangle(float radius, float red, float green, float blue);
	float getSidesAndPerimeterFromTriangle(float size, float *sideA, float *sideB, float *sideC);
	void getIncircleOrigin(float perimeter, float sideA, float Ax, float Ay, float sideB, float Bx, float By, float sideC, float Cx, float Cy, float *incircleOx, float *incircleOy);
	float getIncircleRadius(const float halfPerimeter, const float sideA, const float sideB, const float sideC);
	void drawCircle(float radius, float red, float green, float blue);
	void drawGraph(void);
	void getOuterCircleOrigin(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float *outerCircleOx, float *outerCircleOy);
	void drawRectangle(float size, float red, float green, float blue);

	float sideA = 0.0f, sideB = 0.0f, sideC = 0.0f;
	float perimeter = 0.0f, halfPerimeter = 0.0f;
	float incircleOx = 0.0f, incircleOy = 0.0f;
	float radius = 0.0f;
	float outerCircleOx = 0.0f, outerCircleOy = 0.0f;
    
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Perspective change 5]
	glTranslatef(0.0f, 0.0f, -2.5f);

	if (gbShowGraph)
		drawGraph();

	glPointSize(1.0f);
	drawTriangle(gfMagnitude, 1.0f, 1.0f, 0.0f);
	drawRectangle(gfMagnitude, 1.0f, 1.0f, 0.0f);

	perimeter = getSidesAndPerimeterFromTriangle(gfMagnitude, &sideA, &sideB, &sideC);
	halfPerimeter = perimeter / 2.0f;

	getIncircleOrigin(perimeter, sideA, -gfMagnitude, -gfMagnitude, sideB, gfMagnitude, -gfMagnitude, sideC, 0, gfMagnitude, &incircleOx, &incircleOy);
	radius = getIncircleRadius(halfPerimeter, sideA, sideB, sideC);

	glTranslatef(incircleOx, incircleOy, 0.0f); // Another way but will reset everything //glLoadIdentity(); //glTranslatef(incircleOx, incircleOy, -1.5f);
	drawCircle(radius, 1.0f, 1.0f, 0.0f);

	getOuterCircleOrigin(-gfMagnitude, -gfMagnitude, gfMagnitude, -gfMagnitude, gfMagnitude, gfMagnitude, &outerCircleOx, &outerCircleOy); // or use Pythagoras theorem
	radius = sqrt(pow((outerCircleOx - gfMagnitude), 2) + pow((outerCircleOy - (gfMagnitude)), 2)); // or use Pythagoras theorem
	glTranslatef(-incircleOx, -incircleOy, 0.0f);
	glTranslatef(outerCircleOx, outerCircleOy, 0.0f);
	drawCircle(radius, 1.0f, 1.0f, 0.0f);

	glXSwapBuffers(gpDisplay, gWindow);
}

void getOuterCircleOrigin(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float *outerCircleOx, float *outerCircleOy)
{
	// Origin of the circle passing through 3 points
	float theD = 2.0f * ((Ax*(By - Cy)) + (Bx * (Cy - Ay)) + (Cx * (Ay - By)));
	*outerCircleOx = ((((Ax*Ax) + (Ay*Ay)) * (By - Cy)) + (((Bx*Bx) + (By*By)) * (Cy - Ay)) + (((Cx*Cx) + (Cy*Cy)) * (Ay - By))) / theD;
	*outerCircleOy = ((((Ax*Ax) + (Ay*Ay)) * (Cy - By)) + (((Bx*Bx) + (By*By)) * (Ay - Cy)) + (((Cx*Cx) + (Cy*Cy)) * (By - Ay))) / theD;
}

float getIncircleRadius(const float halfPerimeter, const float sideA, const float sideB, const float sideC)
{
	float area = 0.0f;
	area = (sqrt(halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC)));
	return (area / halfPerimeter);
}

void getIncircleOrigin(float perimeter, float sideA, float Ax, float Ay, float sideB, float Bx, float By, float sideC, float Cx, float Cy, float *incircleOx, float *incircleOy)
{
	*incircleOx = ((sideA * Ax) + (sideB * Bx) + (sideC * Cx)) / perimeter;
	*incircleOy = ((sideA * Ay) + (sideB * By) + (sideC * Cy)) / perimeter;
}

float getSidesAndPerimeterFromTriangle(float size, float *sideA, float *sideB, float *sideC)
{
	*sideA = sqrt(pow((0.0f - size), 2) + pow((size - (-size)), 2));
	*sideB = sqrt(pow((0.0f - (-size)), 2) + pow((size - (-size)), 2));
	*sideC = sqrt(pow(((-size) - size), 2) + pow(((-size) - (-size)), 2)); // Apex
	return (*sideA + *sideB + *sideC);
}

void drawGraph(void)
{
	// variable declarations
	int i = 0;
	float step = 1.0f / LINES_TO_DRAW_GRAPH;
	float xcoord = step; // 0.0f;
	float ycoord = step; // 0.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-xcoord, 1.0f);
		glVertex2f(-xcoord, -1.0f);
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord += step;
	}

	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		glVertex2f(-1.0f, -ycoord);
		glVertex2f(1.0f, -ycoord);
		ycoord += step;
	}

	glEnd();
}

void drawTriangle(float size, float red, float green, float blue)
{
	glBegin(GL_LINE_LOOP);
	glColor3f(red, green, blue);
	glVertex2f(0, size);
	glVertex2f(-size, -size);
	glVertex2f(size, -size);
	glEnd();
}

void drawRectangle(float size, float red, float green, float blue)
{
	glBegin(GL_LINE_LOOP);
	glColor3f(red, green, blue);
	glVertex2f(size, size);
	glVertex2f(-size, size);
	glVertex2f(-size, -size);
	glVertex2f(size, -size);
	glEnd();
}

void drawCircle(float radius, float red, float green, float blue)
{
	glBegin(GL_POINTS);
	glColor3f(red, green, blue);
	for (GLfloat angle = 0.0f; angle <= 2.0f * M_PI; angle += 0.001f)
	{
		glVertex3f(cos(angle) * radius, sin(angle) * radius, 0.0f);
	}
	glEnd();
}


//"program": "${fileDirname}/${fileBasenameNoExtension}",
//
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/04-2DShapes$ g++ -o 2DShapes 2DShapes.cpp -lX11 -lGL -lGLX 
// /tmp/cchbfpYI.o: In function `resize(int, int)':
// 2DShapes.cpp:(.text+0x7ec): undefined reference to `gluPerspective'
// collect2: error: ld returned 1 exit status
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/04-2DShapes$ g++ -o 2DShapes 2DShapes.cpp -lX11 -lGL -lGLX -lGLU
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/04-2DShapes$ ./2DShapes 
// shree@shree-Inspiron-5558:~/Documents/RTR/Linux/04-2DShapes$ 

