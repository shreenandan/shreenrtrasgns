/*
	Assignment: Dynamic India
	Date: Jul 2019
 */

#include <iostream> // not used, just in future if required
// c++ standardization, no .h, "namespace cache"
#include <stdio.h>
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()

#include <X11/Xlib.h>   // similar to Windows.h
#include <X11/Xutil.h>  // XVisualInfo
#include <X11/XKBlib.h> // Keyboard Utility Header
#include <X11/keysym.h> // keycode to symbol mapping

#include <GL/gl.h>
#include <GL/glx.h> // GLX Bridging API for XWindow -> OGL. Like in MS Win WGL
#include <GL/glu.h> // Perspective change 1]

#define _USE_MATH_DEFINES
#include <cmath>

#define LINES_TO_DRAW_GRAPH 20
#define RATIO_COLS 16.0f
#define RATIO_ROWS 9.0f

// namespaces
using namespace std; // part of iostream, no need to write std:: scope resolution optr

// global variable declarations
bool bFullscreen = false; // CPP bool
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL; // Xutil.h PixelFormatDescriptor
Colormap gColormap;
Window gWindow;           // Kind of WND class
int giWindowWidth = 800;  // will be changed later. Hence no macro.
int giWindowHeight = 600; // nor constant

static GLXContext gGLXContext; // static if multiple monitors

FILE *gpFile = NULL;

// Drawing global variable declarations
bool gbShowGraph = false;
bool gbShowRatio = false;
const float stepRow = 2.0f / RATIO_ROWS;
const float stepCol = 2.0f / RATIO_COLS;
bool bShouldPlay = false;

const GLubyte gAiroplaneColorUb[3] = { 186, 226, 238 };
const GLfloat gColorDeepSaffron[3] = { 1.0f,  0.59765625f,  0.19921875f }; // FF9933
const GLfloat gColorIndiaGreen[3] = { 0.07421875f,  0.53125f,  0.03125f }; // 138808
const GLfloat gColorWhite[3] = { 1.0f, 1.0f, 1.0f };

// entry-point function
// CUI, GUI sarva dharma samabhav
int main(void)
{
    // function prototypes
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void uninitialize();

    void initialize(void);
    void resize(int width, int height);
    void display(void);

    // variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    bool bDone = false;

    // code
    gpFile = fopen("log.txt", "w");
    if (gpFile == NULL)
	{
		printf("\nLog file can not be created...\n\n");
		exit(0); // Abortive but 0 and 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}

    CreateWindow(); // this fn made 4 vars alive

    // Message Loop
    XEvent event;
    KeySym keysym;

    initialize();

    while (bDone == false)
    {
        while (XPending(gpDisplay)) // PeekMsg
        {

            // GetMessage &msg
            XNextEvent(gpDisplay, &event);

            // message type from client
            switch (event.type)
            {
            case MapNotify:
                // WM_CREATE difference is that has power to both create as well as show window
                break;

            case KeyPress:
                keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                // XKBlib.h
                // Which page to use from Unicode, 0 is default which is ASCII
                // Is shift key pressed?
                switch (keysym)
                {
                case XK_Escape:
                    // uninitialize(); // remember lecture why not calls merged
                    // exit(0);        // stdlib.h msg.wParam
                    bDone = true;
                    break;

                case XK_f:
                case XK_F:
                    if (bFullscreen == false)
                    {
                        ToggleFullscreen();
                        bFullscreen = true;
                    }
                    else
                    {
                        ToggleFullscreen();
                        bFullscreen = false;
                    }
                    break;
                
                case 't':
                case XK_T:
                    gbShowGraph = !gbShowGraph;
                    break;
                    
                case XK_r:
                case 'R':
                    gbShowRatio = !gbShowRatio;
                    break;

                case 'P':
                case XK_p:
			        bShouldPlay = !bShouldPlay;
                    break;

                default:
                    break;
                }
                break;
                // end of case KeyPress

            case ButtonPress:
                switch (event.xbutton.button)
                {
                case 1: // left
                    break;
                case 2: // middle
                    break;
                case 3: // right
                    break;
                case 4: // wheel up
                    break;
                case 5: // wheel down
                    break;
                default:
                    break;
                }
                break;
                // end of case ButtonPress

            case MotionNotify:
                // WM_MOUSEMOVE
                break;

            case ConfigureNotify:
                // WM_SIZE
                winWidth = giWindowWidth = event.xconfigure.width;
                winHeight = giWindowHeight = event.xconfigure.height;

                resize(winWidth, winHeight);
                break;

            case Expose:
                // WM_PAINT
                break;

            case DestroyNotify:
                // WM_DESTROY but behavior is similar to WM_CLOSE
                // asks to close yes, no
                break;

            case 33:
                // WM_CLOSE important XProtocol book part 1 of 9
                // only close msg is sent, later need to handle what action to take
                // uninitialize();
                // exit(0);

                bDone = true;
                // remember lecture, why seperate two calls. resize, device dependent handling, again uninit and re-display
                break;

            default:
                break;
            }

        } // end of XPending

        display();

    } // end of bDone

    // can't use caps TRUE, must be 1
    // no seperate WndProc kept, DispatchMessage
    // while (1){ theBlock in XPending }

    uninitialize();

    return (0);
}

void CreateWindow(void)
{
    // function prototypes
    void uninitialize(void);

    // variable declarations
    XSetWindowAttributes winAttribs;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    // Double Buffer 8 bit
    static int frameBufferAttributes[] = 
    {
        GLX_RGBA,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Open X Display.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = DefaultDepth(gpDisplay, defaultScreen); // not required now, but kept if reverted back code

    // Bridging API gives visual for your provided framebuf attribs
    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    /*
    // Imagination code
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Allocate Memory For Visual Info.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    int matchingVisual = XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
    // status check not really required
    if (gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to Get A Visual.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }
    */

    winAttribs.border_pixel = 0;
    winAttribs.border_pixmap = 0; // all unix based pixmap, MS bitmap
    winAttribs.background_pixmap = 0;
    winAttribs.colormap = XCreateColormap(gpDisplay,
                                          RootWindow(gpDisplay, gpXVisualInfo->screen),
                                          gpXVisualInfo->visual,
                                          AllocNone);
    // Don't allocate memory if we are going to save it
    gColormap = winAttribs.colormap;

    winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttribs);

    if (!gWindow)
    {
        printf("ERROR : Unable to create Main Window.\nExitting Now...\n");
        fprintf(gpFile, "ERROR : Unable to create Main Window.\nExitting Now...\n");
        uninitialize();
        exit(1); // abortive
    }

    XStoreName(gpDisplay, gWindow, "FFP Dynamic India");

    // Important action to take on close 33
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

    XMapWindow(gpDisplay, gWindow);
    fprintf(gpFile, "CreateWindow successful.\n");
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void uninitialize(void)
{
    GLXContext currentGLXContext = glXGetCurrentContext();

    // In multi monitor scenario current and global can be different
    if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
    {
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    if (gGLXContext)
    {
        glXDestroyContext(gpDisplay, gGLXContext);
    }    

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if (gpFile)
	{
		fprintf(gpFile, "Closing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void MakeFullScreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    // 1]Save current state 2]notify event and act
    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;
	bFullscreen = true;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
    // function prototype
    void uninitialize(void);
    void resize(int width, int height);
    void MakeFullScreen(void);
    
    gGLXContext = glXCreateContext(gpDisplay, 
        gpXVisualInfo,
        NULL,
        GL_TRUE);

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

    // usual code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Cheks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when renedering starts, display().
	//    "OpenGL is a state machine."

    MakeFullScreen();
    fprintf(gpFile, "initialize successful.\n");

	// warm up call to resize, convention and not compulsion
	resize(giWindowWidth, giWindowHeight); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()
}

void resize(int width, int height)
{
	// Perspective change 2]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular
    
	 // Perspective change 3] NOTE: Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	 // Perspective change 4]
	gluPerspective(45.0f,
        (GLfloat)width / (GLfloat)height,
        0.1f,
        100.0f);
}

void display(void)
{    
    void drawGraph(void);
	void drawRatioLine(void);
	bool drawDynamicIndia(void);
	void drawPlane();
	bool drawCircle(float radius, float start, float end);
	bool drawCircleClockwise(float radius, float start, float end);
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth);
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth, float alpha);
	void drawTricolorInLetterA(void);

	static bool isLoaded = false;
	if (!bShouldPlay)
	{
		if (!isLoaded)
			glClear(GL_COLOR_BUFFER_BIT);

		glXSwapBuffers(gpDisplay, gWindow);
		isLoaded = true;
		return;
	}

	static float planeBtx = -2.88f;
	static float planeAtx = 0.0f;
	static float planeCtx = 0.0f;
	static const float bigLetterWidth = stepCol * 2.0f;
	static float theHeight = stepRow * 3.0f;
	static float airoplanePassed = false;
	static float tricolorAlpha = 0.0f;

	glClear(GL_COLOR_BUFFER_BIT); // Double Buffer change 7] glFlush(); removed
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Perspective change 4]
	glTranslatef(0.0f, 0.0f, -3.0f); // Z negative = inside/away from you
	/*gluLookAt(0.0f, 0.0f, 3.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f);*/

	if (gbShowGraph)
		drawGraph();
	if (gbShowRatio)
		drawRatioLine();

	if (drawDynamicIndia())
	{
		glTranslatef(planeBtx, 0.0f, 0.0f);
		drawPlane();

		glLoadIdentity();
		if (planeAtx <= (1.24f + 0.8f))
		{
			glTranslatef(-2.2f + 1.24f + planeAtx, -1.24f, -3.0f);
			if (drawCircleClockwise(1.24f, M_PI + (M_PI_2 / 4.0f), M_PI_2))
				planeAtx += 0.000098f;
		}
		else if (planeAtx > 1.24f + 0.8f)
		{
			glTranslatef(-2.2f + 1.24f + planeAtx, 1.24f, -3.0f);
			drawCircle(1.24f, M_PI + M_PI_2, (M_PI * 2.0f) + M_PI_4);
		}

		glLoadIdentity();
		if (planeCtx <= (1.24f + 0.8f))
		{
			glTranslatef(-2.2f + 1.24f + planeCtx, 1.24f, -3.0f);
			if (drawCircle(1.24f, M_PI - (M_PI_2 / 4.0f), M_PI + M_PI_2))
				planeCtx += 0.000098f;
		}
		else if (planeCtx > 1.24f + 0.8f)
		{
			glTranslatef(-2.2f + 1.24f + planeAtx, -1.24f, -3.0f);
			drawCircleClockwise(1.24f, M_PI_2, 0.0f - M_PI_4);
		}

		if (planeBtx < -2.2f + 1.24f)
			planeBtx += 0.000098f;
		else if (planeBtx < 1.24f + 0.8f)
			planeBtx += 0.000098f;
		else if (planeBtx < 2.56f)
			planeBtx += 0.0001f;

		if (planeBtx > 0.9f) //9 best with 0.00015
		{
			glLoadIdentity();
			glTranslatef(stepCol * 8.0f, 0.0f, -3.0f);
			drawTricolorInLetterA();
		}
	}

	glXSwapBuffers(gpDisplay, gWindow);
}

void drawGraph(void)
{
	// variable declarations
	int i = 0;
	float step = 1.0f / LINES_TO_DRAW_GRAPH;
	float xcoord = step; // 0.0f;
	float ycoord = step; // 0.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-xcoord, 1.0f);
		glVertex2f(-xcoord, -1.0f);
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord += step;
	}

	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		glVertex2f(-1.0f, -ycoord);
		glVertex2f(1.0f, -ycoord);
		ycoord += step;
	}

	glEnd();
}

void drawRatioLine(void)
{
	// variable declarations
	int i = 0;
	float stepRow = 2.0f / RATIO_ROWS;
	float stepCol = 2.0f / RATIO_COLS;
	float xcoord = 1.0f;
	float ycoord = 1.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	for (i = 0; i <= RATIO_COLS; i++)
	{
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord -= stepCol;
	}

	for (i = 0; i <= RATIO_ROWS; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		ycoord -= stepRow;
	}

	glEnd();
}

bool drawLetterI1(float *translationStepI1, const float stopMarkerI1, const float theHeight)
{
	glColor3fv(gColorDeepSaffron);
	glVertex2f(*translationStepI1, theHeight);
	glColor3fv(gColorIndiaGreen);
	glVertex2f(*translationStepI1, -theHeight);
	if (*translationStepI1 < stopMarkerI1)
	{
		*translationStepI1 += 0.0002f;
		return false;
	}
	return true;
}

bool drawLetterA(float *translationStepA, const float startMarkerAx, const float theHeight, const float bigLetterWidth)
{
	glColor3fv(gColorIndiaGreen);
	glVertex2f(*translationStepA, -theHeight);
	glColor3fv(gColorDeepSaffron);
	glVertex2f(*translationStepA + (bigLetterWidth / 2.0f), theHeight);

	glColor3fv(gColorDeepSaffron);
	glVertex2f(*translationStepA + (bigLetterWidth / 2.0f), theHeight);
	glColor3fv(gColorIndiaGreen);
	glVertex2f(*translationStepA + bigLetterWidth, -theHeight);
	if (*translationStepA > startMarkerAx)
	{
		*translationStepA -= 0.0002f;
		return false;
	}
	return true;
}

bool drawLetterN(float *translationStepN, const float startMarkerNx, const float theHeight, const float bigLetterWidth)
{
	glColor3fv(gColorDeepSaffron);
	glVertex2f(startMarkerNx, *translationStepN + (theHeight * 2.0f));
	glColor3fv(gColorIndiaGreen);
	glVertex2f(startMarkerNx, *translationStepN);

	glColor3fv(gColorDeepSaffron);
	glVertex2f(startMarkerNx, *translationStepN + (theHeight * 2.0f));
	glColor3fv(gColorIndiaGreen);
	glVertex2f(startMarkerNx + bigLetterWidth, *translationStepN);

	glColor3fv(gColorDeepSaffron);
	glVertex2f(startMarkerNx + bigLetterWidth, *translationStepN + (theHeight * 2.0f));
	glColor3fv(gColorIndiaGreen);
	glVertex2f(startMarkerNx + bigLetterWidth, *translationStepN);

	if (*translationStepN > -theHeight)
	{
		*translationStepN -= 0.0002f;
		return false;
	}
	return true;
}

bool drawLetterI2(float *translationStepI2, const float startMarkerI2x, const float theHeight)
{
	glColor3fv(gColorDeepSaffron);
	glVertex2f(startMarkerI2x, *translationStepI2);
	glColor3fv(gColorIndiaGreen);
	glVertex2f(startMarkerI2x, *translationStepI2 - (theHeight * 2.0f));

	if (*translationStepI2 < theHeight)
	{
		*translationStepI2 += 0.0002f;// 0.0002f;
		return false;
	}
	return true;
}

bool drawLetterD(float *alphaD, const float startMarkerDx, const float theHeight, const float bigLetterWidth)
{
	glColor4f(gColorDeepSaffron[0], gColorDeepSaffron[1], gColorDeepSaffron[2], *alphaD);
	glVertex2f(startMarkerDx, theHeight);
	glColor4f(gColorIndiaGreen[0], gColorIndiaGreen[1], gColorIndiaGreen[2], *alphaD);
	glVertex2f(startMarkerDx, -theHeight);

	glColor4f(gColorDeepSaffron[0], gColorDeepSaffron[1], gColorDeepSaffron[2], *alphaD);
	glVertex2f(startMarkerDx, theHeight);
	glVertex2f(startMarkerDx + bigLetterWidth, theHeight - (theHeight / 3.0f));

	glColor4f(gColorIndiaGreen[0], gColorIndiaGreen[1], gColorIndiaGreen[2], *alphaD);
	glVertex2f(startMarkerDx, -theHeight);
	glVertex2f(startMarkerDx + bigLetterWidth, -theHeight + (theHeight / 3.0f));

	glColor4f(gColorDeepSaffron[0], gColorDeepSaffron[1], gColorDeepSaffron[2], *alphaD);
	glVertex2f(startMarkerDx + bigLetterWidth, theHeight - (theHeight / 3.0f));
	glColor4f(gColorIndiaGreen[0], gColorIndiaGreen[1], gColorIndiaGreen[2], *alphaD);
	glVertex2f(startMarkerDx + bigLetterWidth, -theHeight + (theHeight / 3.0f));

	if (*alphaD < 1.0f)
	{
		*alphaD += 0.0001f;
		return false;
	}
	return true;
}

bool drawDynamicIndia(void)
{
	// function declarations
	bool drawLetterI1(float *translationStepI1, const float stopMarkerI1, const float theHeight);
	bool drawLetterA(float *translationStepA, const float startMarkerAx, const float theHeight, const float bigLetterWidth);
	bool drawLetterN(float *translationStepN, const float startMarkerNx, const float theHeight, const float bigLetterWidth);
	bool drawLetterI2(float *translationStepI2, const float startMarkerI2x, const float theHeight);
	bool drawLetterD(float *alphaD, const float startMarkerDx, const float theHeight, const float bigLetterWidth);

	// variable declarations
	static const float theHeight = stepRow * 2.50f;
	static const float bigLetterWidth = stepCol * 2.0f;
	static bool isPlacedI1 = false;
	static bool isPlacedN = false;
	static bool isPlacedD = false;
	static bool isPlacedI2 = false;
	static bool isPlacedA = false;
	static float translationStepI1 = -2.95f;
	static const float stopMarkerI1 = -1.0f + (stepCol); // on first column
	static float translationStepN = 1.24f;
	static const float startMarkerNx = -1.0f + (stepCol * 3.0f);
	static float translationStepA = 2.2f;
	static const float startMarkerAx = -1.0f + (stepCol * 13.0f);
	static float translationStepI2 = -1.24f;
	static const float startMarkerI2x = -1.0f + (stepCol * 11.0f);
	static float alphaD = 0.0f;
	static const float startMarkerDx = -1.0f + (stepCol * 7.0f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLineWidth(15.0f);
	glBegin(GL_LINES);

	isPlacedI1 = drawLetterI1(&translationStepI1, stopMarkerI1, theHeight);
	if (isPlacedI1)
		isPlacedA = drawLetterA(&translationStepA, startMarkerAx, theHeight, bigLetterWidth);
	if (isPlacedA)
		isPlacedN = drawLetterN(&translationStepN, startMarkerNx, theHeight, bigLetterWidth);
	if (isPlacedN)
		isPlacedI2 = drawLetterI2(&translationStepI2, startMarkerI2x, theHeight);
	if (isPlacedI2)
		isPlacedD = drawLetterD(&alphaD, startMarkerDx, theHeight, bigLetterWidth);

	glEnd();
	glDisable(GL_BLEND);

	return isPlacedD;
}

bool drawCircleClockwise(float radius, float start, float end)
{
	void drawPlane();

	static GLfloat angle = start;
	static GLfloat theX;
	static GLfloat theY;

	//glPointSize(2.0f);
	glBegin(GL_POINTS);
	//glColor3fv(gColorWhite);

	for (GLfloat angleTemp = start; angleTemp > angle; angleTemp -= 0.001f)
	{
		theX = cos(angleTemp) * radius;
		theY = sin(angleTemp) * radius;
		//glVertex3f(theX, theY, 0.0f);
	}
	glEnd();

	glTranslatef(theX, theY, 0.0f);
	glRotatef((angle - M_PI_2) * (180.0f / M_PI), 0.0f, 0.0f, 1.0f);
	drawPlane();

	if (angle > end)
	{
		angle -= 0.0001f;
		return false;
	}
	else
		return true;
}

bool drawCircle(float radius, float start, float end)
{
	void drawPlane();

	static GLfloat angle = start;
	static GLfloat theX;
	static GLfloat theY;

	//glPointSize(2.0f);
	glBegin(GL_POINTS);
	//glColor3fv(gColorIndiaGreen);

	for (GLfloat angleTemp = start; angleTemp < angle; angleTemp += 0.001f)
	{
		theX = cos(angleTemp) * radius;
		theY = sin(angleTemp) * radius;
		//glVertex3f(theX, theY, 0.0f);
	}
	glEnd();

	glTranslatef(theX, theY, 0.0f);
	glRotatef((angle + M_PI_2) * (180.0f / M_PI), 0.0f, 0.0f, 1.0f);
	drawPlane();

	if (angle < end)
	{
		angle += 0.0001f;
		return false;
	}
	else
		return true;
}

// step 1] find mid points of both sides
// step 2] can draw middle line joining both mid points
// step 3] find lengths of all three sides
// step 4] Upper: now we have lengths of all sides. find one angle. Inverse cos
//			cos A = (b2 + c2 - a2) /2bc		A = cos inverse(value)
// step 5] Lower: angle = 180 - upper. Inverse cos
// step 6].a find next point of side 1 nextS1A'(x + pos * cos(A), y + pos * sin(A))
// step 6].b find next point of side 2 nextS2A'(x + pos * cos(A), y + pos * sin(A))
// step 7] draw line from obtained points nextS1A' <--> nextS2A'
// step 8].a find prev point of side 1 prevS1A'(x + pos * cos(A), y + pos * sin(A))
// step 8].b find prev point of side 2 prevS2A'(x + pos * cos(A), y + pos * sin(A))
// step 9] draw line from obtained points prevS1A' <--> prevS2A'
void drawTricolorInLetterA(float side1X1, float side1Y1, float side1X2, float side1Y2,
	float side2X1, float side2Y1, float side2X2, float side2Y2)
{
	typedef struct Coordinate
	{
		float x;
		float y;
		float z;
	}Coord;

	// midpoint = (x1+x2)/2, (y1+y2)/2
	static Coord side1MidPoint, side2MidPoint;

	side1MidPoint.x = (side1X1 + side1X2) / 2.0f;
	side1MidPoint.y = (side1Y1 + side1Y2) / 2.0f;
	side1MidPoint.z = 0.0f;

	side2MidPoint.x = (side2X1 + side2X2) / 2.0f;
	side2MidPoint.y = (side2Y1 + side2Y2) / 2.0f;
	side2MidPoint.z = 0.0f;

	static float lengthB = sqrtf(powf((side1MidPoint.x - side1X2), 2) + powf((side1MidPoint.y - side1Y2), 2));
	static float lengthC = sqrtf(powf((side1MidPoint.x - side2MidPoint.x), 2) + powf((side1MidPoint.y - side2MidPoint.y), 2));
	static float lengthA = sqrtf(powf((side2MidPoint.x - side2X2), 2) + powf((side2MidPoint.y - side2Y2), 2));

	static float side1CosA = ((lengthB*lengthB) + (lengthC*lengthC) - (lengthA*lengthA)) / (2.0f *lengthB * lengthC);
	static float side1AngleA = acosf(side1CosA) * (180.0f / M_PI); // radian to degree
	static float side1CosB = ((lengthA*lengthA) + (lengthC*lengthC) - (lengthB*lengthB)) / (2.0f *lengthA * lengthC);
	static float side1AngleB = acosf(side1CosB) * (180.0f / M_PI); // radian to degree

	static float nextS1Ax = side1MidPoint.x + (0.008f * cosf(side1AngleA));
	static float nextS1Ay = side1MidPoint.y + (0.040f * sinf(side1AngleA));
	static float nextS2Ax = side2MidPoint.x - (0.008f * cosf(side1AngleB));
	static float nextS2Ay = side2MidPoint.y + (0.040f * sinf(side1AngleB));

	static float prevS1Ax = side1MidPoint.x - (0.001f * cosf(side1AngleA));
	static float prevS1Ay = side1MidPoint.y - (0.080f * sinf(side1AngleA));
	static float prevS2Ax = side2MidPoint.x + (0.001f * cosf(side1AngleB));
	static float prevS2Ay = side2MidPoint.y - (0.080f * sinf(side1AngleB));

	glColor3fv(gColorDeepSaffron);
	glVertex3f(prevS1Ax, prevS1Ay, side1MidPoint.z);
	glVertex3f(prevS2Ax, prevS2Ay, side2MidPoint.z);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(side1MidPoint.x, side1MidPoint.y, side1MidPoint.z);
	glVertex3f(side2MidPoint.x, side2MidPoint.y, side2MidPoint.z);

	glColor3fv(gColorIndiaGreen);
	glVertex3f(nextS1Ax, nextS1Ay, side1MidPoint.z);
	glVertex3f(nextS2Ax, nextS2Ay, side2MidPoint.z);
}

void drawPlane()
{
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLubyte colorVector[3], const float lineWidth);
	void drawLines(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth);
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth);

	static const float unitMagnitude = stepCol * 1.25f;
	static const float half = (unitMagnitude / 2.0f);
	static const float percent10 = (unitMagnitude * 0.1f);
	static const float percent15 = (unitMagnitude * 0.15f);
	static const float percent20 = (unitMagnitude * 0.2f);
	static const float percent25 = (unitMagnitude * 0.25f);
	static const float percent30 = (unitMagnitude * 0.3f);
	static const float percent35 = (unitMagnitude * 0.35f);
	static const float percent40 = (unitMagnitude * 0.4f);
	static const GLfloat airoplaneTail[8][2] =
	{
		{-unitMagnitude, 0.0f },
		{-unitMagnitude - percent10, half},
		{-unitMagnitude + percent30, half},
		{-unitMagnitude + percent40, percent15},
		{-unitMagnitude + percent40, -percent15},
		{-unitMagnitude + percent30, -half},
		{-unitMagnitude - percent10, -half},
		{-unitMagnitude, 0.0f}
	};
	static const GLfloat airoplaneMiddle[4][2] =
	{
		{0.0f, percent25},
		{-unitMagnitude + percent20, percent15},
		{-unitMagnitude + percent20, -percent15},
		{0.0f, -percent25}
	};
	static const GLfloat airoplaneHead[12][2] =
	{
		{0.0f - percent10, percent25},
		{0.0f - percent10, unitMagnitude},
		{0.0f + percent30, unitMagnitude},
		{0.0f + half, percent30},
		{unitMagnitude, percent35},
		{unitMagnitude + percent40, percent10},
		{unitMagnitude + percent40, -percent10},
		{unitMagnitude, -percent35},
		{0.0f + half, -percent30},
		{0.0f + percent30, -unitMagnitude},
		{0.0f - percent10, -unitMagnitude},
		{0.0f - percent10, -percent25}
	};

	GLfloat side1MidPointx = (percent40 + percent20 + percent30 + percent15) / 2.0f;
	GLfloat midPointy = (percent20 + (-percent20)) / 2.0f;
	GLfloat side2MidPointx = (percent40 + percent20 + percent15 + percent40 + percent20) / 2.0f;

	static const GLfloat IAF[14][2] =
	{
		{percent40, percent20},
		{percent40, -percent20},
		{percent30 + percent15, -percent20},
		{percent40 + percent20, percent20},
		{percent40 + percent20, percent20},
		{percent40 + percent20 + percent15,-percent20},
		{side1MidPointx, midPointy},
		{side2MidPointx, midPointy},
		{percent40 * 2.0f, percent20},
		{percent40 * 2.0f, -percent20},
		{percent40 * 2.0f, percent20},
		{(percent40 * 2.0f) + percent20, percent20},
		{percent40 * 2.0f, 0.0f},
		{(percent40 * 2.0f) + percent15, 0.0f}
	};
	static const GLfloat exhaustSaffron[5][2] =
	{
		{-unitMagnitude, percent10 },
		{-unitMagnitude - percent10, percent15 },
		{-unitMagnitude * 2.0f, percent15 },
		{-unitMagnitude * 2.0f, percent15 - percent10 },
		{-unitMagnitude - percent10, percent15 - percent10 }
	};
	static const GLfloat exhaustWhite[5][2] =
	{
		{-unitMagnitude, 0.0f },
		{-unitMagnitude - percent10, percent15 - percent10 },
		{-unitMagnitude * 2.0f, percent15 - percent10 },
		{-unitMagnitude * 2.0f, -(percent15 - percent10) },
		{-unitMagnitude - percent10, -(percent15 - percent10) }
	};
	static const GLfloat exhaustGreen[5][2] =
	{
		{-unitMagnitude, -percent10 },
		{-unitMagnitude - percent10, -percent15 },
		{-unitMagnitude * 2.0f, -percent15 },
		{-unitMagnitude * 2.0f, -(percent15 - percent10) },
		{-unitMagnitude - percent10, -(percent15 - percent10) }
	};

	//glRotatef(45.0f, 0.0f, 0.0f, 1.0f);
	//glRotatef(60.0f, 1.0f, 0.0f, 0.0f);
	drawPolygon(airoplaneTail, 8, gAiroplaneColorUb, 1.0f);
	drawPolygon(airoplaneMiddle, 4, gAiroplaneColorUb, 1.0f);
	drawPolygon(airoplaneHead, 12, gAiroplaneColorUb, 1.0f);
	drawLines(IAF, 14, gColorWhite, 1.50f);
	drawPolygon(exhaustSaffron, 5, gColorDeepSaffron, 1.0f);
	drawPolygon(exhaustWhite, 5, gColorWhite, 1.0f);
	drawPolygon(exhaustGreen, 5, gColorIndiaGreen, 1.0f);
}

void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLubyte colorVector[3], float lineWidth)
{
	glLineWidth(lineWidth);
	glBegin(GL_POLYGON);
	glColor3ubv(colorVector);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawLines(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth)
{
	glLineWidth(lineWidth);
	glBegin(GL_LINES);
	glColor3fv(colorVector);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth)
{
	glLineWidth(lineWidth);
	glBegin(GL_POLYGON);
	glColor3fv(colorVector);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth, float alpha)
{
	GLfloat params[4] = { colorVector[0], colorVector[1], colorVector[2], alpha };
	glLineWidth(lineWidth);
	glBegin(GL_POLYGON);
	glColor4fv(params);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawLines(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth, float alpha)
{
	GLfloat params[4] = { colorVector[0], colorVector[1], colorVector[2], alpha };
	glLineWidth(lineWidth);
	glBegin(GL_LINES);
	glColor4fv(params);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawTricolorInLetterA(void)
{
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth, float alpha);

	static float tricolorAlpha = 0.0f;
	static const float unitMagnitude = stepCol * 1.25f;
	static const float half = (unitMagnitude / 2.0f);
	static const float percent10 = (unitMagnitude * 0.1f);
	static const float percent15 = (unitMagnitude * 0.15f);
	static const float percent20 = (unitMagnitude * 0.2f);
	static const float percent25 = (unitMagnitude * 0.25f);
	static const float percent30 = (unitMagnitude * 0.3f);
	static const float percent35 = (unitMagnitude * 0.35f);
	static const float percent40 = (unitMagnitude * 0.4f);
	static const GLfloat exhaustSaffron[4][2] =
	{
		{-unitMagnitude - percent20, percent15 },
		{-unitMagnitude * 2.0f, percent15 },
		{-unitMagnitude * 2.0f, percent15 - percent10 },
		{-unitMagnitude - percent20, percent15 - percent10 }
	};
	static const GLfloat exhaustWhite[4][2] =
	{
		{-unitMagnitude - percent20, percent15 - percent10 },
		{-unitMagnitude * 2.0f, percent15 - percent10 },
		{-unitMagnitude * 2.0f, -(percent15 - percent10) },
		{-unitMagnitude - percent20, -(percent15 - percent10) }
	};
	static const GLfloat exhaustGreen[4][2] =
	{
		{-unitMagnitude - percent20, -percent15 },
		{-unitMagnitude * 2.0f, -percent15 },
		{-unitMagnitude * 2.0f, -(percent15 - percent10) },
		{-unitMagnitude - percent20, -(percent15 - percent10) }
	};

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	drawPolygon(exhaustSaffron, 4, gColorDeepSaffron, 1.0f, tricolorAlpha);
	drawPolygon(exhaustWhite, 4, gColorWhite, 1.0f, tricolorAlpha);
	drawPolygon(exhaustGreen, 4, gColorIndiaGreen, 1.0f, tricolorAlpha);

	glDisable(GL_BLEND);

	if (tricolorAlpha < 1.0f)
		tricolorAlpha += 0.00025;
}

//"program": "${fileDirname}/${fileBasenameNoExtension}",
//

