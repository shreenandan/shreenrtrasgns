#pragma once
// #include <stdio.h> // file
// #include <GL/glew.h>
// #include "vmath.h"
// #include <Windows.h> // For HDC only

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import <Foundation/Foundation.h>

#import "mysphere.h"
#import "vmath.h"

// extern HDC ghdc; // Common context

extern int MyCount;
extern FILE *gpFile;

extern GLuint vao_sphere_pv;
extern GLuint vbo_position_sphere;
extern GLuint vbo_light_sphere;
extern GLuint gVbo_sphere_element;
extern GLuint gShaderProgramObject_pv;
extern GLuint vao_sphere_pf;
extern GLuint gShaderProgramObject_pf;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

using namespace vmath;
extern mat4 perspectiveProjectionMatrix;

extern GLfloat angleCube;
extern GLfloat ascendingCube;

extern bool gbShowLight;
extern bool gbAnimate;
extern bool gbXKeyPressed;
extern bool gbYKeyPressed;
extern bool gbZKeyPressed;

extern float sphere_vertices[1146];
extern float sphere_normals[1146];
extern float sphere_textures[764];
extern unsigned short sphere_elements[2280];

extern int gNumVertices;
extern int gNumElements;

enum ShaderModes { PER_VERTEX, PER_FRAGMENT };
extern enum ShaderModes gSelectedMode;

struct ShaderUniforms
{
	GLuint modelUniform = -1;
	GLuint viewUniform = -1;
	GLuint projectionUniform = -1;

	GLuint laUniform = -1;
	GLuint ldUniform = -1;
	GLuint lsUniform = -1;

	GLuint kaUniform = -1;
	GLuint kdUniform = -1;
	GLuint ksUniform = -1;
	GLuint shinynessUniform = -1;

	GLuint lightPoistionUniform = -1;
	GLuint isLKeyPressedUniform = -1;
};

extern struct ShaderUniforms PerVertexUniforms, PerFragmentUniforms;
