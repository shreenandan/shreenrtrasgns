#pragma once
// #include <stdio.h> // file
// #include <GL/glew.h>
// #include "vmath.h"
// #include <Windows.h> // For HDC only

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import <Foundation/Foundation.h>

#import "vmath.h"

// extern HDC ghdc; // Common context

extern int MyCount;
extern FILE *gpFile;

extern GLuint vao_cube;
extern GLuint vbo_position_cube;
extern GLuint vbo_light_cube;
extern GLuint gShaderProgramObject;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

extern GLuint mvUniform; // model view
extern GLuint projectionUniform;
using namespace vmath;
extern mat4 perspectiveProjectionMatrix;

extern GLfloat angleCube;
extern GLfloat ascendingCube;

extern bool gbShowLight;
extern bool gbAnimate;

extern GLuint ldUniform;
extern GLuint kdUniform;
extern GLuint lightPoistionUniform;
extern GLuint isLKeyPressedUniform;
