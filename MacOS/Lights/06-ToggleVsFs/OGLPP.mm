/*
	Assignment: PP Lights 6] Toggle per vertex, per vertex ADS Phong light on Sphere.
	Q = Quit, Esc = Fullscreen, V = Vertex Shader, F = Fragment Shader, L = Light on/off
	Date: 05 Jan 2020
	Try Albedo. Use of struct and common functions with pointers
 */

#pragma once
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> 

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

#import "VarDeclarations.h"
#import "Logic.h"

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end 

@interface GLView : NSOpenGLView
@end 

// Entry-point function 
int main(int argc, const char * argv[])
{ 
    // code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc]init]]; 
    [NSApp run]; 
    [pPool release]; 

    return(0);
}

// interface implementations
@implementation AppDelegate
{
    @private 
    NSWindow *window;
    GLView *glView;
} 

- (void) applicationDidFinishLaunching:(NSNotification *)aNotification
{ 
    // code
    // log file
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile = fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile == NULL)
    {
        printf("Can not create log file.\nExitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program started successfully.\n");
    
    // window 
    NSRect win_rect; 
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    // create simple window 
    window=[[NSWindow alloc] initWithContentRect:win_rect 
                styleMask:NSWindowStyleMaskTitled | 
                NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
                backing:NSBackingStoreBuffered 
                defer:NO];
                
    [window setTitle:@"macOS Light 6] Toggle VS, FS ADS Phong light on Sphere"];
    [window center]; 

    glView=[[GLView alloc]initWithFrame:win_rect];
    [window setContentView:glView];
    [window setDelegate:self]; 
    [window makeKeyAndOrderFront:self]; 
}

- (void) applicationWillTerminate:(NSNotification *)notification
{ 
    // code
    // fprintf(gpFile, "\nTerminating program.\n");
    
    // if(gpFile)
    // {
    //     fclose(gpFile);
    //     gpFile = NULL;
    // }

	uninitializePP();
}

- (void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

- (void)dealloc
{
    // code
    [glView release];
    [window release]; 
    [super dealloc]; 
}

@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
}

-(id)initWithFrame:(NSRect)frame;
{ 
    // code
    self=[super initWithFrame: frame]; 

    if(self) 
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // Must specify the 4.1 core profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            // specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        }; // last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No valid OpenGL pixel format is available.\n Exitting...\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; // It automatically release older context, if present and sets newer
    }
    
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL info
    fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    //////////////////////////////
    [self setupPP];

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // glEnable(GL_CULL_FACE);
    
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue
    
	incrCount();
	fprintf(gpFile, "\ninitialize successful. MyCount = %d\n", getCount());
    //////////////////////////////

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void) reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect = [self bounds];
    
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    if (height == 0) {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    perspectiveProjectionMatrix = vmath::perspective(45.0f,
                                            (width / height),
                                             0.1f,
                                             100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    // code
    [self drawView];
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    display();

    if (gbAnimate)
    {
        update();
    }
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder 
{ 
    // code 
    [[self window]makeFirstResponder:self]; 
    return(YES); 
}

-(void)keyDown :(NSEvent *)theEvent
{ 
    // code 
    int key=(int)[[theEvent characters] characterAtIndex:0]; 
    switch(key)
    {
        case 'q':
        case 'Q':
            [self release];
            [NSApp terminate:self];
            break;
            
        case 27: // Esc key
            [[self window]toggleFullScreen:self]; // repainting occurs automatically 
            break;
            
		// case 0x46: // 'f' or 'F'
        case 'F':
        case 'f':
			gSelectedMode = PER_FRAGMENT;
			break;
            
		case 'v':
		case 'V':
			gSelectedMode = PER_VERTEX;
			break;

		case 'a':
		case 'A':
			gbAnimate = !gbAnimate;
			break;

		case 'l':
		case 'L':
			gbShowLight = !gbShowLight;
			break;

        default:
            break;
    }
}

- (void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent 
{ 
    // code 
} 

-(void)rightMouseDown:(NSEvent *)theEvent 
{ 
    // code
}

- (void) dealloc
{
	// uninitializePP();

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
} 

-(void) setupPP
{
    fprintf(gpFile, "\nglewInit successful.");

    if (!createShaders(PER_VERTEX, &gShaderProgramObject_pv, &PerVertexUniforms))
	{
		[self release];
        [NSApp terminate:self];
		// exit(0);
	}

    if (!createShaders(PER_FRAGMENT, &gShaderProgramObject_pf, &PerFragmentUniforms))
    {
        [self release];
        [NSApp terminate:self];
    }

	initVAOs();

	perspectiveProjectionMatrix = mat4::identity();
}

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
