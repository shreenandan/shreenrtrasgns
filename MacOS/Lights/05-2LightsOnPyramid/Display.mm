#include "Logic.h"
#import <Foundation/Foundation.h>
// #include <Windows.h>

float material_ambient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float material_diffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float material_specular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float material_shinyness = 128.0f; // 128

struct LightStruct
{
	float ambient[4]  = {0.0f, 0.0f, 0.0f, 0.0f};
	float diffuse[4]  = {0.0f, 0.0f, 0.0f, 0.0f};
	float specular[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	float position[4] = {0.0f, 0.0f, 0.0f, 0.0f};
} RedLight, BlueLight;

struct MaterialStruct
{
	float ambient[4]  = {0.0f, 0.0f, 0.0f, 0.0f};
	float diffuse[4]  = {0.0f, 0.0f, 0.0f, 0.0f};
	float specular[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	float shinyness = 128.0f;
} PyramidMaterial;

void initLights(void)
{
	RedLight.ambient[0] = 0;
	RedLight.ambient[1] = 0;
	RedLight.ambient[2] = 0;
	RedLight.ambient[3] = 0;

	RedLight.diffuse[0] = 1.0f;
	RedLight.diffuse[1] = 0;
	RedLight.diffuse[2] = 0;
	RedLight.diffuse[3] = 1.0f;

	RedLight.specular[0] = 1.0f;
	RedLight.specular[1] = 0;
	RedLight.specular[2] = 0;
	RedLight.specular[3] = 1.0f;

	RedLight.position[0] = -3.0f;
	RedLight.position[1] = 0;
	RedLight.position[2] = 0;
	RedLight.position[3] = 1.0f;

	BlueLight.ambient[0] = 0;
	BlueLight.ambient[1] = 0;
	BlueLight.ambient[2] = 0;
	BlueLight.ambient[3] = 0;

	BlueLight.diffuse[0] = 0;
	BlueLight.diffuse[1] = 0;
	BlueLight.diffuse[2] = 1.0f;
	BlueLight.diffuse[3] = 1.0f;

	BlueLight.specular[0] = 0;
	BlueLight.diffuse[1] = 0;
	BlueLight.diffuse[2] = 1.0f;
	BlueLight.specular[3] = 1.0f;

	BlueLight.position[0] = 3.0f;
	BlueLight.position[1] = 0;
	BlueLight.position[2] = 0;
	BlueLight.position[3] = 1.0f;
}

void update(void)
{
	// Rotate vice versa
	if (ascendingCube)
	{
		angleCube += 0.09f;
		if (angleCube > 360.0f)
		{
			ascendingCube = false;
		}
	}
	else
	{
		angleCube -= 0.09f;
		if (angleCube < 0.0f)
		{
			ascendingCube = true;
		}
	}
}

void display(void)
{
	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]

	// Use Pass Through Shader Program
	glUseProgram(gShaderProgramObject); // Binding shader pgm to OpenGL pgm

	// your code here
	// 9 steps
	// declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 rotationMatrix;
	mat4 translationMatrix;

	// initialize above 2 matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	translationMatrix = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	rotationMatrix = rotate(0.0f, angleCube, 0.0f);
	translationMatrix = translate(0.0f, 0.0f, -5.0f);
	modelMatrix = translationMatrix * rotationMatrix;

	// do necessary matrix multiplication
	projectionMatrix = perspectiveProjectionMatrix;// *modelViewMatrix;
	// In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum

	// send necessary matrices to shader in respective uniforms
	// display fn is dynamic, called in loop
	glUniformMatrix4fv(modelUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelMatrix); // kashala chiktavaychay // verify
	// OpenGL/GLSL is column major, DirectX is row major
	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);

	if (gbShowLight)
	{
		glUniform1i(isLKeyPressedUniform, 1);
		glUniform4fv(lightPoistionUniform, 1, RedLight.position);

		glUniform3fv(laUniform, 1, RedLight.ambient);
		glUniform3fv(ldUniform, 1, RedLight.diffuse);
		glUniform3fv(lsUniform, 1, RedLight.specular);

		glUniform3fv(kaUniform, 1, material_ambient);
		glUniform3fv(kdUniform, 1, material_diffuse);
		glUniform3fv(ksUniform, 1, material_specular);
		glUniform1f(shinynessUniform, material_shinyness);

		glUniform4fv(lightPoistion2Uniform, 1, BlueLight.position);
		glUniform3fv(la2Uniform, 1, BlueLight.ambient);
		glUniform3fv(ld2Uniform, 1, BlueLight.diffuse);
		glUniform3fv(ls2Uniform, 1, BlueLight.specular);
	}
	else
	{
		glUniform1i(isLKeyPressedUniform, 0);
	}

	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao_pyramid);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	glDrawArrays(GL_TRIANGLES, 0, 12);

	//// draw the scene
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	//glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0); // Unbinding

	// SwapBuffers(ghdc);
}

/**
https://macpaw.com/how-to/access-var-folder-mac
https://en.wikipedia.org/wiki/Pragma_once
https://stackoverflow.com/questions/24298144/duplicate-symbols-for-architecture-x86-64-under-xcode
https://github.com/microsoft/ChakraCore/issues/1492
https://github.com/Microsoft/ChakraCore/issues/2284
https://reviews.llvm.org/D36870
*/