#pragma once
#include "VarDeclarations.h"
// #include <stdio.h>
#import <Foundation/Foundation.h>

GLuint vao_sphere = -1;
GLuint vbo_position_sphere = -1;
GLuint vbo_light_sphere = -1;
GLuint gShaderProgramObject = 0;

GLuint mvUniform = -1; // model view
GLuint mvpUniform = -1;
GLuint projectionUniform = -1;
mat4 perspectiveProjectionMatrix;

GLfloat angleCube = 0.0f;
GLfloat ascendingCube = true;

bool gbShowLight = false;
bool gbAnimate = false;

GLuint ldUniform = -1;
GLuint kdUniform = -1;
GLuint lightPoistionUniform = -1;
GLuint isLKeyPressedUniform = -1;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gVbo_sphere_element = -1;

int gNumVertices = -1;
int gNumElements = -1;

int MyCount = 10;
FILE *gpFile = NULL;

int year = 0;
int day = 0;