#pragma once
#ifndef MYSPHERE_H
#define MYSPHERE_H

#include<math.h>
    // [760][9]
extern    short indices[760][9];
    // [382][3]
extern    float vertices[382][3];
    
    // [382][3]
extern     float normals[382][3];
 
    // [1][2]
extern     float textures[1][2];
    
    // variables
extern     int maxElements;
extern     int numElements;
extern     int numVertices;
    
// extern     float model_vertices[1146] = {};
// extern     float model_normals[1146] = {};
// extern     float model_textures[764] = {};
// extern     short model_elements[2280] = {};

    bool isFoundIdentical(float val1, float val2, float diff);
    
    void normalizeVector(float v[3]);
    
    void addTriangle(float single_vertex[3][3] , float single_normal[3][3] , float single_texture[3][2] );

    // code
    void processSphereData();
 
    void getSphereVertexData(float *spherePositionCoords, float *sphereNormalCoords, float *sphereTexCoords, unsigned short *sphereElements);
    
    int getNumberOfSphereVertices();
    
    int getNumberOfSphereElements();

#endif