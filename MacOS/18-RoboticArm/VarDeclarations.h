#pragma once
// #include <stdio.h> // file
// #include <GL/glew.h>
// #include "vmath.h"
// #include <Windows.h> // For HDC only

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import <Foundation/Foundation.h>

#import "mysphere.h"
#import "vmath.h"

// extern HDC ghdc; // Common context

extern int MyCount;
extern FILE *gpFile;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

extern GLuint vao_sphere;
extern GLuint vbo_position_sphere;
extern GLuint vbo_light_sphere;
extern GLuint gVbo_sphere_element;
extern GLuint gShaderProgramObject;

extern GLuint mvUniform; // model view
extern GLuint mvpUniform;
extern GLuint projectionUniform;
using namespace vmath;
extern mat4 perspectiveProjectionMatrix;

extern GLfloat angleCube;
extern GLfloat ascendingCube;

extern bool gbShowLight;
extern bool gbAnimate;

extern GLuint ldUniform;
extern GLuint kdUniform;
extern GLuint lightPoistionUniform;
extern GLuint isLKeyPressedUniform;

extern float sphere_vertices[1146];
extern float sphere_normals[1146];
extern float sphere_textures[764];
extern unsigned short sphere_elements[2280];


extern int gNumVertices;
extern int gNumElements;

extern int shoulder;
extern int elbow;
