#pragma once
#include "Logic.h"
// #include <GL/glew.h>
// #include <stdio.h> // file
// #include <Windows.h> // malloc free

#import <Foundation/Foundation.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

void incrCount(void)
{
	++MyCount;
}

int getCount(void)
{
	return MyCount;
}

void unmarkVaoVboList(void)
{
	for (int i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		vao_list[i] = -1;
		vbo_position_list[i] = -1;
		vbo_color_list[i] = -1;
		vbo_element_list[i] = -1;
	}
}

void uninitializePP(void)
{
	void uninitSPO(GLuint *theSpo);
	void uninitVAO(GLuint *theVao);
	void uninitVBO(GLuint *theVbo);

	// PP shader dtor
	// Safe Release
	for (int i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		uninitVBO(&vbo_element_list[i]);
		uninitVBO(&vbo_color_list[i]);
		uninitVBO(&vbo_position_list[i]);
		uninitVAO(&vao_list[i]);
	}

	uninitSPO(&genericShaderProgramObject);

	if (gpFile)
	{
		fprintf(gpFile, "\nClosing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void uninitVBO(GLuint *theVbo)
{
	if (*theVbo)
	{
		glDeleteBuffers(1, theVbo);
		*theVbo = 0;
	}
}

void uninitVAO(GLuint *theVao)
{
	if (*theVao)
	{
		glDeleteVertexArrays(1, theVao);
		*theVao = 0;
	}
}

void uninitSPO(GLuint *theSpo)
{
	if (*theSpo)
	{
		GLsizei shaderCount;
		GLsizei shaderNo;

		glUseProgram(*theSpo);

		// ask pgm how many shaders attached to you
		glGetProgramiv(*theSpo, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
		if (pShaders)
		{
			glGetAttachedShaders(*theSpo, shaderCount, &shaderCount, pShaders); // using same var

			for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
			{
				glDetachShader(*theSpo, pShaders[shaderNo]);
				glDeleteShader(pShaders[shaderNo]);
				pShaders[shaderNo] = 0;
			}
			free(pShaders);
		}

		glDeleteProgram(*theSpo); // Not actually deleting but changing machine state. Not shaikh chilli
		*theSpo = 0;
		glUseProgram(0);
	}
}

const char* GLenumToString(GLenum shaderType)
{
	switch (shaderType)
	{
	case GL_VERTEX_SHADER:
		return "Vertex Shader";
	case GL_FRAGMENT_SHADER:
		return "Fragment Shader";
	default:
		return "Wrong Shader";
	}
}

bool createShaders(enum ShaderModes shaderMode, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms)
{
	bool createVertexShader(GLuint *vertexShaderObjec, enum ShaderModes shaderMode);
	bool createFragmentShader(GLuint *fragmentShaderObject, enum ShaderModes shaderMode);
	bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms);

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	bool successful = false;

	if (!createVertexShader(&vertexShaderObject, shaderMode))
	{
		return false;
	}
	if (!createFragmentShader(&fragmentShaderObject, shaderMode))
	{
		return false;
	}
	return createShaderProgramAndLink(vertexShaderObject, fragmentShaderObject, gShaderProgramObject, shaderUniforms);
}

bool createVertexShader(GLuint *vertexShaderObject, enum ShaderModes shaderMode)
{
	bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);

	const GLchar *vertexShaderSourceCode_basic =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"	gl_Position = u_mvp_matrix * vPosition;" \
		"	out_color = vColor;" \
		"}";

	switch (shaderMode)
	{
	case BASIC:
		return createShader(vertexShaderObject, GL_VERTEX_SHADER, vertexShaderSourceCode_basic);
		break;

	default:
		return false;
		break;
	}
}

bool createFragmentShader(GLuint *fragmentShaderObject, enum ShaderModes shaderMode)
{
	bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);

	const GLchar *fragmentShaderSourceCode_basic =
		"#version 410 core" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec4 out_color;" \
		"void main(void)" \
		"{" \
		"	FragColor = out_color;" \
		"}";

	switch (shaderMode)
	{
	case BASIC:
		return createShader(fragmentShaderObject, GL_FRAGMENT_SHADER, fragmentShaderSourceCode_basic);
		break;

	default:
		return false;
		break;
	}
}

bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode)
{
	const char* GLenumToString(GLenum shaderType);

	*shaderObject = glCreateShader(typeOfShader);
	glShaderSource(*shaderObject, 1, (const GLchar**)&shaderSourceCode, NULL);
	glCompileShader(*shaderObject);

	// steps for catching errors
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(*shaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*shaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(*shaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\n%s: Compilation Error: %s", GLenumToString(typeOfShader), szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\n%s: failed to malloc szInfoLog...", GLenumToString(typeOfShader));
			}
		}
		else
		{
			fprintf(gpFile, "\n%s: Something went wrong, infoLogLength is zero...", GLenumToString(typeOfShader));
		}
		return false;
	}
	else
	{
		fprintf(gpFile, "\n%s compiled successfully.", GLenumToString(typeOfShader));
	}

	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	return true;
}

bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms)
{
	// step 1] create shader program obj
	*gShaderProgramObject = glCreateProgram();

	// step 2] Attach shaders
	glAttachShader(*gShaderProgramObject, vertexShaderObject);
	glAttachShader(*gShaderProgramObject, fragmentShaderObject);

	// Pre-Linking binding to vertex attribute
	glBindAttribLocation(*gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(*gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor"); // Worked even if was not bind

	// step 3] Link program
	glLinkProgram(*gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetProgramiv(*gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(*gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetProgramInfoLog(*gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "\nShader Program: Link Error: %s", szInfoLog);
				free(szInfoLog);
			}
			else
			{
				fprintf(gpFile, "\nShader Program: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fprintf(gpFile, "\nShader Program: Something went wrong, infoLogLength is zero...");
		}
		return false;
	}
	else
	{
		fprintf(gpFile, "\nShader program linked successfully.");
	}

	// Post-Linking retrieving uniform location
	shaderUniforms->mvpUniform = glGetUniformLocation(*gShaderProgramObject, "u_mvp_matrix");

	// reset
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	return true;
}

void bindVao(GLuint *theVao)
{
	glGenVertexArrays(1, theVao);
	glBindVertexArray(*theVao);
}

void unbindVao()
{
	glBindVertexArray(0);
}

void bindVbo(GLuint *theVbo)
{
	glGenBuffers(1, theVbo);
	glBindBuffer(GL_ARRAY_BUFFER, *theVbo);
}

void unbindVbo()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void bindVaoVbo(GLuint *theVao, GLuint *theVbo)
{
	glGenVertexArrays(1, theVao);
	glBindVertexArray(*theVao);

	glGenBuffers(1, theVbo);
	glBindBuffer(GL_ARRAY_BUFFER, *theVbo);
}

void unbindVboVao()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void fillBufferData(const float data[], int sizeofData, int noOfElementsInOneTuple, bool isStatic, int amc_attribute)
{
	if (isStatic)
	{
		glBufferData(GL_ARRAY_BUFFER, sizeofData, data, GL_STATIC_DRAW); // attachya atta oot
	}
	else
	{
		glBufferData(GL_ARRAY_BUFFER, sizeofData, NULL, GL_DYNAMIC_DRAW);
	}

	glVertexAttribPointer(amc_attribute,
		noOfElementsInOneTuple, // 3=xyx, 2=st
		GL_FLOAT,
		GL_FALSE, // isNormalized 0 to 1 NDC 
		0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
		NULL); // no stride therefore no offest

	glEnableVertexAttribArray(amc_attribute);
}

void initVAOs(void)
{
	// function declarations
	void unmarkVaoVboList(void);
	void initVaoGraph(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor, GLuint *theElementVbo);

	// code
	unmarkVaoVboList();
	initVaoGraph(&vao_list[GRAPH], GRAPH, &vbo_position_list[GRAPH], &vbo_color_list[GRAPH], &vbo_element_list[GRAPH]);
}

void initVaoGraph(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor, GLuint *theElementVbo)
{
	//GLfloat ratioVertices[((RATIO_COLS + 1) * 6) + ((RATIO_ROWS + 1) * 6)]; // 6 = 3 x,y,z * 2 start,end
	int sizeofVertices = sizeof(float) * ((LINES_TO_DRAW + 2) * 4 * NO_OF_COORDS_IN_3D_LINE);
	GeometryList[geometryListIdx].VerticesDynamic = (float*) malloc(sizeofVertices);

	int i = 0, j = 0;
	float stepRow = 1.0f / LINES_TO_DRAW;
	float stepCol = 1.0f / LINES_TO_DRAW;
	float xcoord = 1.0f;
	float ycoord = 1.0f;

	for (i = 0; i < LINES_TO_DRAW; i++)
	{
		GeometryList[geometryListIdx].VerticesDynamic[j++] = xcoord;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 1.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = xcoord;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = -1.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;

		GeometryList[geometryListIdx].VerticesDynamic[j++] = -xcoord;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 1.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = -xcoord;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = -1.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;

		xcoord -= stepCol;
	}

	for (i = 0; i < LINES_TO_DRAW; i++)
	{
		GeometryList[geometryListIdx].VerticesDynamic[j++] = -1.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = ycoord;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 1.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = ycoord;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;

		GeometryList[geometryListIdx].VerticesDynamic[j++] = -1.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = -ycoord;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 1.0f;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = -ycoord;
		GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;

		ycoord -= stepRow;
	}

	// X Axis
	GeometryList[geometryListIdx].VerticesDynamic[j++] = 1.0f;
	GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[j++] = -1.0f;
	GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;

	// Y Axis
	GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[j++] = 1.0f;
	GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[j++] = -1.0f;
	GeometryList[geometryListIdx].VerticesDynamic[j++] = 0.0f;

	GeometryList[geometryListIdx].ColorsDynamic = (float*)malloc(sizeofVertices);
	j = 0;
	for (i = 0; i < LINES_TO_DRAW * 4 * 2; i++)
	{
		GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;
		GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;
		GeometryList[geometryListIdx].ColorsDynamic[j++] = 1.0f;
	}

	// X Axis
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;

	GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;

	// Y Axis
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;

	GeometryList[geometryListIdx].ColorsDynamic[j++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;
	GeometryList[geometryListIdx].ColorsDynamic[j++] = 0.0f;

	bindVao(theVao);

	bindVbo(theVboPosition);
	//fillBufferData(ratioVertices, sizeof(ratioVertices), 3, true, AMC_ATTRIBUTE_POSITION);
	fillBufferData(GeometryList[geometryListIdx].VerticesDynamic, sizeofVertices, 3, true, AMC_ATTRIBUTE_POSITION);
	//glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.52f, 0.52f, 0.52f);
	unbindVbo();

	bindVbo(theVboColor);
	fillBufferData(GeometryList[geometryListIdx].ColorsDynamic, sizeofVertices, 3, true, AMC_ATTRIBUTE_COLOR); // 4 if alpha
	unbindVbo();

	/*glGenBuffers(1, theElementVbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *theElementVbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * GeometryList[geometryListIdx].NumElements, GeometryList[geometryListIdx].ElementsDynamic, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);*/

	unbindVao();
}
