#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> 

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end 

@interface GLView : NSOpenGLView
@end 

// Entry-point function 
int main(int argc, const char * argv[])
{ 
    // code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc]init]]; 
    [NSApp run]; 
    [pPool release]; 

    return(0);
}

// interface implementations
@implementation AppDelegate
{
    @private 
    NSWindow *window;
    GLView *glView;
} 

- (void) applicationDidFinishLaunching:(NSNotification *)aNotification
{ 
    // code
    // log file
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile = fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile == NULL)
    {
        printf("Can not create log file.\nExitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program started successfully.\n");
    
    // window 
    NSRect win_rect; 
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    // create simple window 
    window=[[NSWindow alloc] initWithContentRect:win_rect 
                styleMask:NSWindowStyleMaskTitled | 
                NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
                backing:NSBackingStoreBuffered 
                defer:NO];
                
    [window setTitle:@"macOS OGL Tweaked Smiley"];
    [window center]; 

    glView=[[GLView alloc]initWithFrame:win_rect];
    [window setContentView:glView];
    [window setDelegate:self]; 
    [window makeKeyAndOrderFront:self]; 
}

- (void) applicationWillTerminate:(NSNotification *)notification
{ 
    // code
    fprintf(gpFile, "\nTerminating program.\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

- (void)dealloc
{
    // code
    [glView release];
    [window release]; 
    [super dealloc]; 
}

@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_rect;
    GLuint vbo_position_rect;
    GLuint vbo_texture_rect;
    GLuint mvpUniform;
    
    GLuint texture_smiley;
    GLuint samplerUniform;

    vmath::mat4 perspectiveProjectionMatrix;

    int keyPressed;
}

-(id)initWithFrame:(NSRect)frame;
{ 
    // code
    self=[super initWithFrame: frame]; 

    if(self) 
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // Must specify the 4.1 core profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            // specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        }; // last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No valid OpenGL pixel format is available.\n Exitting...\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; // It automatically release older context, if present and sets newer
    }
    
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL info
    fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // *** VERTEX SHADER ***
    // create shader
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // step 2] write vertex shader code
    const GLchar *vertexShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec2 vTexCoord;" \
    "uniform mat4 u_mvp_matrix;" \
    "out vec2 out_texcoord;" \
    "void main(void)" \
    "{" \
    "	gl_Position = u_mvp_matrix * vPosition;" \
    "	out_texcoord = vTexCoord;" \
    "}";
    
    // step 3] specify above source code to vertex shader obj
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    // step 4] compile the vertex shader
    glCompileShader(vertexShaderObject);
    
    
    // steps for catching errors
    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written = 0;
                glGetShaderInfoLog(vertexShaderObject,
                                   iInfoLogLength,
                                   &written,
                                   szInfoLog);
                fprintf(gpFile, "\nVertex Shader: Compilation Error: %s", szInfoLog);
                free(szInfoLog);
            }
            else
            {
                fprintf(gpFile, "\nVertex Shader: failed to malloc szInfoLog...");
            }
        }
        else
        {
            fprintf(gpFile, "\nVertex Shader: Something went wrong, infoLogLength is zero...");
        }
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "\nVertex Shader compiled successfully.");
    }
    
    
    // reset flags
    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    
    // step 1] define fragment shader obj
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    // step 2] write fragment shader code
    const GLchar *fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "out vec4 FragColor;" \
    "in vec2 out_texcoord;" \
    "uniform sampler2D u_sampler;" \
    "void main(void)" \
    "{" \
    "	FragColor = texture(u_sampler, out_texcoord);" \
    "}";
    
    // step 3] specify above source code to fragment shader obj
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    // step 4] compile the fragment shader
    glCompileShader(fragmentShaderObject);
    
    
    // steps for catching errors
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written = 0;
                glGetShaderInfoLog(fragmentShaderObject,
                                   iInfoLogLength,
                                   &written,
                                   szInfoLog);
                fprintf(gpFile, "\nFragment Shader: Compilation Error: %s", szInfoLog);
                free(szInfoLog);
            }
            else
            {
                fprintf(gpFile, "\nFragment Shader: failed to malloc szInfoLog...");
            }
        }
        else
        {
            fprintf(gpFile, "\nFragment Shader: Something went wrong, infoLogLength is zero...");
        }
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "\nFragment Shader compiled successfully.");
    }
    
    // reset flags
    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    
    // create shader program obj
    // step 1] create
    shaderProgramObject = glCreateProgram();
    
    // step 2] Attach shaders
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    // Pre-Linking binding to vertex attribute
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");
    
    // step 3] Link program
    glLinkProgram(shaderProgramObject);
    
    GLint iProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written = 0;
                glGetProgramInfoLog(shaderProgramObject,
                                    iInfoLogLength,
                                    &written,
                                    szInfoLog);
                fprintf(gpFile, "\nShader Program: Link Error: %s", szInfoLog);
                free(szInfoLog);
            }
            else
            {
                fprintf(gpFile, "\nShader Program: failed to malloc szInfoLog...");
            }
        }
        else
        {
            fprintf(gpFile, "\nShader Program: Something went wrong, infoLogLength is zero...");
        }
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "\nShader program linked successfully.");
    }
    
    // Post-Linking retrieving uniform location
    mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
    samplerUniform = glGetUniformLocation(shaderProgramObject, "u_sampler");
    
    // load texture
    texture_smiley = [self loadTextureFromBMPFile:"Smiley.bmp"];

    // reset
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    
    //============== RECT ====================
	// Top right, Top left, Bottom left, bottom right;
    // NOTE: Mac image gets inverted, need to change coords
	const GLfloat quadVertices[] =
	{
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
	};

	glGenVertexArrays(1, &vao_rect);
	glBindVertexArray(vao_rect);

	glGenBuffers(1, &vbo_position_rect);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_rect);

	glBufferData(GL_ARRAY_BUFFER,
		sizeof(quadVertices),
		quadVertices,
		GL_STATIC_DRAW); // attachya atta oot

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // xyx
		GL_FLOAT,
		GL_FALSE, // isNormalized 0 to 1 NDC 
		0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
		NULL); // no stride therefore no offest

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    // // glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f); // Single uniform value to all vertices
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind

    // ==== Texture ====
	glGenBuffers(1, &vbo_texture_rect);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_rect);

	glBufferData(GL_ARRAY_BUFFER,
		4 * 2 * sizeof(GL_FLOAT),
		NULL,
		GL_DYNAMIC_DRAW); // attachya atta oot

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0,
		2,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind

	glBindVertexArray(0);


    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // glEnable(GL_CULL_FACE);
    glEnable(GL_TEXTURE_2D);
    
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue

    perspectiveProjectionMatrix = vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(GLuint)loadTextureFromBMPFile:(const char *)texFileName
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *textureFileNameWithPath = [NSString stringWithFormat:@"%@/%s", parentDirPath, texFileName];

    NSImage *bmpImage = [[NSImage alloc] initWithContentsOfFile:textureFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"Can't find %@", textureFileNameWithPath);
        return(0);
    }

    CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];

    int bmWidth = (int)CGImageGetWidth(cgImage);
    int bmHeight = (int)CGImageGetHeight(cgImage);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void *)CFDataGetBytePtr(imageData);

    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4 for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGBA,
			bmWidth,
			bmHeight,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			pixels); // _EXT try if not // verify 2-3

    // create mipmaps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D); // new addition

    CFRelease(imageData);
    return(bmpTexture);
}

-(void) reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect = [self bounds];
    
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    if (height == 0) {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    perspectiveProjectionMatrix = vmath::perspective(45.0f,
                                            (width / height),
                                             0.1f,
                                             100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    // code
    [self drawView];
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm
    
    // your code here
    // 9 steps
    // declaration of matrices
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
	vmath::mat4 translationMatrix;

    // initialize above 2 matrices to identity
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();

    // do necessary transformations like model scale, rotate, translate
    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
	modelViewMatrix = translationMatrix;

    // do necessary matrix multiplication
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    // In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum
    
    // send necessary matrices to shader in respective uniforms
    // display fn is dynamic, called in loop
    glUniformMatrix4fv(mvpUniform, // kashat kombaychay. globally declared used in display
                       1, // how many matrices
                       GL_FALSE, // transpose?
                       modelViewProjectionMatrix); // kashala chiktavaychay // verify
    // OpenGL/GLSL is column major, DirectX is row major
    
    // bind with vao - this will avoid many vbo repetitive calls in display
    glBindVertexArray(vao_rect);
    // IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
    // if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data
    
    // similarly bin with textures
    // ==== Work with texture now ABU ====
	glActiveTexture(GL_TEXTURE0); // matches to our AMC_ATTRIBUTE_TEXCOORD0
	// texture unit. 80 supported
    glBindTexture(GL_TEXTURE_2D, texture_smiley); 
    //glEnable(GL_TEXTURE_2D);
    glUniform1i(samplerUniform, 0); // GL_TEXTURE0 zeroth unit
    
    GLfloat rectTexCoord[8];

	switch (keyPressed)
	{
		case 1: // half
			rectTexCoord[0] = 0.50f; rectTexCoord[1] = 0.50f;
			rectTexCoord[2] = 0.0f; rectTexCoord[3] = 0.50f;
			rectTexCoord[4] = 0.0f; rectTexCoord[5] = 0.0f;
			rectTexCoord[6] = 0.50f; rectTexCoord[7] = 0.0f;
		break;

		case 2: // full
			rectTexCoord[0] = 1.0f; rectTexCoord[1] = 1.0f;
			rectTexCoord[2] = 0.0f; rectTexCoord[3] = 1.0f;
			rectTexCoord[4] = 0.0f; rectTexCoord[5] = 0.0f;
			rectTexCoord[6] = 1.0f; rectTexCoord[7] = 0.0f;
			break;

		case 3: // double
			rectTexCoord[0] = 2.0f; rectTexCoord[1] = 2.0f;
			rectTexCoord[2] = 0.0f; rectTexCoord[3] = 2.0f;
			rectTexCoord[4] = 0.0f; rectTexCoord[5] = 0.0f;
			rectTexCoord[6] = 2.0f; rectTexCoord[7] = 0.0f;
			break;

		case 4: // center
			rectTexCoord[0] = 0.5f; rectTexCoord[1] = 0.5f;
			rectTexCoord[2] = 0.5f; rectTexCoord[3] = 0.5f;
			rectTexCoord[4] = 0.5f; rectTexCoord[5] = 0.5f;
			rectTexCoord[6] = 0.5f; rectTexCoord[7] = 0.5f;
			break;

		default:
			rectTexCoord[0] = 0.0f; rectTexCoord[1] = 0.0f;
			rectTexCoord[2] = 0.0f; rectTexCoord[3] = 0.0f;
			rectTexCoord[4] = 0.0f; rectTexCoord[5] = 0.0f;
			rectTexCoord[6] = 0.0f; rectTexCoord[7] = 0.0f;
			break;
	}

	glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_rect);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectTexCoord), rectTexCoord, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

    // draw the scene
    glDrawArrays(GL_TRIANGLE_FAN,
                 0, // from which array element to start. You can put different geometries in single array-interleaved
                 4); // how many to draw? Ex: Triangle - 3 vertices, Quad 4 vert
    
    // unbind vao
    glBindVertexArray(0);
    
    glUseProgram(0); // Unbinding
    
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // // [self myUpdate];
}

// learning Update() seems predefined function, don't use with this name
- (void)myUpdate
{
	
}

-(BOOL)acceptsFirstResponder 
{ 
    // code 
    [[self window]makeFirstResponder:self]; 
    return(YES); 
}

-(void)keyDown :(NSEvent *)theEvent
{ 
    // code 
    int key=(int)[[theEvent characters] characterAtIndex:0]; 
    switch(key)
    {
        case 27: // Esc key
            [self release];
            [NSApp terminate:self];
            break; 
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self]; // repainting occurs automatically 
            break;

		case 49:
			keyPressed = 1;
			break;

		case 50:
			keyPressed = 2;
			break;

		case 51:
			keyPressed = 3;
			break;

		case 52:
			keyPressed = 4;
			break;

        default:
            keyPressed = 0;
            break;
    }
}

- (void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent 
{ 
    // code 
} 

-(void)rightMouseDown:(NSEvent *)theEvent 
{ 
    // code
}

- (void) dealloc
{ 
    // code
    // PP shader dtor
    // Safe Release
    // Don't declare shader objects globally, use locally in initialize n use as necessary
    if (vbo_texture_rect)
    {
        glDeleteBuffers(1, &vbo_texture_rect);
        vbo_texture_rect = 0;
    }
    if (vbo_position_rect)
    {
        glDeleteBuffers(1, &vbo_position_rect);
        vbo_position_rect = 0;
    }
    if (vao_rect)
    {
        glDeleteVertexArrays(1, &vao_rect);
        vao_rect = 0;
    }
    
    if (texture_smiley)
    {
        glDeleteTextures(1, &texture_smiley);
        texture_smiley = 0;
    }

    if (shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNo;
        
        glUseProgram(shaderProgramObject);
        
        // ask pgm how many shaders attached to you
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        
        GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
        if (pShaders)
        {
            glGetAttachedShaders(shaderProgramObject,
                                 shaderCount,
                                 &shaderCount, /// using same var
                                 pShaders);
            
            for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
            {
                glDetachShader(shaderProgramObject, pShaders[shaderNo]);
                glDeleteShader(pShaders[shaderNo]);
                pShaders[shaderNo] = 0;
            }
            free(pShaders);
        }
        
        glDeleteProgram(shaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
        shaderProgramObject = 0;
        glUseProgram(0);
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
} 

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
