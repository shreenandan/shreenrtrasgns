#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> 

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

// 'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

float light_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
float light_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
float light_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
float light_position[4] = { 100.0f,100.0f,100.0f,1.0f };

float material_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
float material_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
float material_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
float material_shininess = 128.0f;	//also try with 50.0f 

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end 

@interface GLView : NSOpenGLView
@end 

// Entry-point function 
int main(int argc, const char * argv[])
{ 
    // code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc]init]]; 
    [NSApp run]; 
    [pPool release]; 

    return(0);
}

// interface implementations
@implementation AppDelegate
{
    @private 
    NSWindow *window;
    GLView *glView;
} 

- (void) applicationDidFinishLaunching:(NSNotification *)aNotification
{ 
    // code
    // log file
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile = fopen(pszLogFileNameWithPath, "w");
    
    if(gpFile == NULL)
    {
        printf("Can not create log file.\nExitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program started successfully.\n");
    
    // window 
    NSRect win_rect; 
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

    // create simple window 
    window=[[NSWindow alloc] initWithContentRect:win_rect 
                styleMask:NSWindowStyleMaskTitled | 
                NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
                backing:NSBackingStoreBuffered 
                defer:NO];
                
    [window setTitle:@"macOS Interleaved"];
    [window center]; 

    glView=[[GLView alloc]initWithFrame:win_rect];
    [window setContentView:glView];
    [window setDelegate:self]; 
    [window makeKeyAndOrderFront:self]; 
}

- (void) applicationWillTerminate:(NSNotification *)notification
{ 
    // code
    fprintf(gpFile, "\nTerminating program.\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

- (void)dealloc
{
    // code
    [glView release];
    [window release]; 
    [super dealloc]; 
}

@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_pyramid;
    GLuint vbo_position_pyramid;
    GLuint vbo_texture_pyramid;
    GLuint vao_cube;
    GLuint vbo_position_cube;
    GLuint vbo_texture_cube;
    GLuint mvpUniform;
    
    GLuint pyramid_texture;
    GLuint cube_texture;
    GLuint samplerUniform;

    GLfloat angleRect;
    GLfloat angleTri;
    GLfloat ascendingTri;
    GLfloat ascendingRect;

    vmath::mat4 perspectiveProjectionMatrix;

    GLuint vbo_cube;
    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;
    GLuint laUniform;
    GLuint ldUniform;
    GLuint lsUniform;
    GLuint lightPositionUniform;
    GLuint kaUniform;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint materialShininessUniform;
    GLuint lKeyIsPressedUniform;
    GLuint texture_marble;
    bool bLight;
}

-(id)initWithFrame:(NSRect)frame;
{ 
    // code
    self=[super initWithFrame: frame]; 

    if(self) 
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // Must specify the 4.1 core profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            // specify the display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        }; // last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "No valid OpenGL pixel format is available.\n Exitting...\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; // It automatically release older context, if present and sets newer
    }
    
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // code
    // OpenGL info
    fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // *** VERTEX SHADER ***
    // create shader
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // step 2] write vertex shader code
    const GLchar *vertexShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec4 vColor;" \
    "in vec2 vTexcoord;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_m_matrix;" \
    "uniform mat4 u_v_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "uniform int u_lkeyispressed;" \
    "uniform vec4 u_light_position;" \
    "out vec3 t_norm;" \
    "out vec3 light_direction;" \
    "out vec3 viewer_vector;" \
    "out vec4 out_color;" \
    "out vec2 out_texcoord;" \
    "void main(void)" \
    "{" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec4 eye_coordinates=u_v_matrix * u_m_matrix * vPosition;" \
    "t_norm=mat3(u_v_matrix * u_m_matrix)*vNormal;" \
    "light_direction=vec3(u_light_position - eye_coordinates);" \
    "viewer_vector=normalize(vec3(-eye_coordinates.xyz));" \
    "}" \
    "gl_Position=u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
    "out_color=vColor;" \
    "out_texcoord =vTexcoord;" \
    "}";
    
    // step 3] specify above source code to vertex shader obj
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    
    // step 4] compile the vertex shader
    glCompileShader(vertexShaderObject);
    
    
    // steps for catching errors
    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written = 0;
                glGetShaderInfoLog(vertexShaderObject,
                                   iInfoLogLength,
                                   &written,
                                   szInfoLog);
                fprintf(gpFile, "\nVertex Shader: Compilation Error: %s", szInfoLog);
                free(szInfoLog);
            }
            else
            {
                fprintf(gpFile, "\nVertex Shader: failed to malloc szInfoLog...");
            }
        }
        else
        {
            fprintf(gpFile, "\nVertex Shader: Something went wrong, infoLogLength is zero...");
        }
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "\nVertex Shader compiled successfully.");
    }
    
    
    // reset flags
    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    
    // step 1] define fragment shader obj
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    // step 2] write fragment shader code
    const GLchar *fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_material_shininess;" \
    "in vec3 t_norm;" \
    "in vec3 light_direction;" \
    "in vec3 viewer_vector;" \
    "in vec2 out_texcoord;" \
    "uniform sampler2D u_sampler;" \
    "in vec4 out_color;" \
    "vec4 Texture;" \
    "out vec4 FragColor;" \
    "uniform int u_lkeyispressed;" \
    "void main(void)" \
    "{" \
    "vec3 fong_ads_light;" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec3 normalized_tnorm=normalize(t_norm);" \
    "vec3 normalized_lightdirection=normalize(light_direction);" \
    "vec3 normalized_viewervector=normalize(viewer_vector);" \
    "vec3 reflection_vector=reflect(-normalized_lightdirection,normalized_tnorm);" \
    "float tn_dot_ld=max(dot(normalized_lightdirection,normalized_tnorm),0.0);" \
    "vec3 ambient=u_la * u_ka;" \
    "vec3 diffuse=u_ld * u_kd * tn_dot_ld;" \
    "vec3 specular=u_ls * u_ks * pow(max(dot(reflection_vector,normalized_viewervector),0.0),u_material_shininess);" \
    "fong_ads_light= ambient + diffuse + specular;" \
    "}" \
    "else" \
    "{" \
    "fong_ads_light=vec3(1.0,1.0,1.0);" \
    "}" \
    "Texture=texture(u_sampler,out_texcoord);" \
    "FragColor=Texture * out_color * vec4(fong_ads_light,1.0);" \
    "}";
    
    // step 3] specify above source code to fragment shader obj
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    // step 4] compile the fragment shader
    glCompileShader(fragmentShaderObject);
    
    
    // steps for catching errors
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written = 0;
                glGetShaderInfoLog(fragmentShaderObject,
                                   iInfoLogLength,
                                   &written,
                                   szInfoLog);
                fprintf(gpFile, "\nFragment Shader: Compilation Error: %s", szInfoLog);
                free(szInfoLog);
            }
            else
            {
                fprintf(gpFile, "\nFragment Shader: failed to malloc szInfoLog...");
            }
        }
        else
        {
            fprintf(gpFile, "\nFragment Shader: Something went wrong, infoLogLength is zero...");
        }
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "\nFragment Shader compiled successfully.");
    }
    
    // reset flags
    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    
    // create shader program obj
    // step 1] create
    shaderProgramObject = glCreateProgram();
    
    // step 2] Attach shaders
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    // Pre-Linking binding to vertex attribute
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "vTexcoord");
    
    // step 3] Link program
    glLinkProgram(shaderProgramObject);
    
    GLint iProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written = 0;
                glGetProgramInfoLog(shaderProgramObject,
                                    iInfoLogLength,
                                    &written,
                                    szInfoLog);
                fprintf(gpFile, "\nShader Program: Link Error: %s", szInfoLog);
                free(szInfoLog);
            }
            else
            {
                fprintf(gpFile, "\nShader Program: failed to malloc szInfoLog...");
            }
        }
        else
        {
            fprintf(gpFile, "\nShader Program: Something went wrong, infoLogLength is zero...");
        }
        [self release];
        [NSApp terminate:self];
    }
    else
    {
        fprintf(gpFile, "\nShader program linked successfully.");
    }
    
    // Post-Linking retrieving uniform location
	modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_m_matrix");
	viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_v_matrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
	laUniform = glGetUniformLocation(shaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
	lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");
	kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
	lKeyIsPressedUniform = glGetUniformLocation(shaderProgramObject, "u_lkeyispressed");
	samplerUniform = glGetUniformLocation(shaderProgramObject, "u_sampler");

    // reset
    iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    

	const GLfloat cubeVCNT[] = 
    {
        1.0f, 1.0f, -1.0f, 1.0, 0.0, 0.0, 0.0f , 1.0f, 0.0f, 0.0f, 1.0f,
        -1.0f, 1.0f, -1.0f, 1.0, 0.0, 0.0, 0.0f , 1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0, 0.0, 0.0, 0.0f , 1.0f, 0.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 1.0, 0.0, 0.0, 0.0f , 1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 0.0, 1.0, 0.0, 0.0f , -1.0f, 0.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f, 0.0, 1.0, 0.0, 0.0f , -1.0f, 0.0f, 0.0f, 1.0f,
        -1.0f, -1.0f, -1.0f, 0.0, 1.0, 0.0, 0.0f , -1.0f, 0.0f, 0.0f, 0.0f,
        1.0f, -1.0f, -1.0f, 0.0, 1.0, 0.0, 0.0f , -1.0f, 0.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 0.0, 0.0, 1.0, 0.0f , 0.0f, 1.0f, 0.0f, 0.0f,
        -1.0f, 1.0f,1.0f, 0.0, 0.0, 1.0, 0.0f , 0.0f, 1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 0.0, 0.0, 1.0, 0.0f , 0.0f, 1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 0.0, 0.0, 1.0, 0.0f , 0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, -1.0f, -1.0f, 0.0, 1.0, 1.0, 0.0f , 0.0f, -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f,-1.0f, 0.0, 1.0, 1.0, 0.0f , 0.0f, -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f, 0.0, 1.0, 1.0, 0.0f , 0.0f, -1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, -1.0f, 0.0, 1.0, 1.0, 0.0f , 0.0f, -1.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0, 0.0, 1.0, -1.0f , 0.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, 1.0f,-1.0f, 1.0, 0.0, 1.0, -1.0f , 0.0f, 0.0f, 1.0f, 1.0f,
        -1.0f, -1.0f,-1.0f, 1.0, 0.0, 1.0, -1.0f , 0.0f, 0.0f, 0.0f, 1.0f,
        -1.0f, -1.0f, 1.0f, 1.0, 0.0, 1.0, -1.0f , 0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, -1.0f, 1.0, 1.0, 0.0, 1.0f , 0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 1.0, 1.0, 0.0, 1.0f , 0.0f, 0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f, 1.0, 1.0, 0.0, 1.0f , 0.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, -1.0f, 1.0, 1.0, 0.0, 1.0f , 0.0f, 0.0f, 0.0f, 1.0f
    };

	//create vao
	//for cube
	glGenVertexArrays(1, &vao_cube);
	glBindVertexArray(vao_cube);

	glGenBuffers(1, &vbo_cube);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube);
    
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVCNT), cubeVCNT, GL_STATIC_DRAW);
	
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), (void *)(0 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat)));
	
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(6 * sizeof(GLfloat)));
	
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void *)(9 * sizeof(GLfloat)));
	
    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
    glBindVertexArray(0);
    

    // load texture
    texture_marble = [self loadTextureFromBMPFile:"marble.bmp"];


    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // glEnable(GL_CULL_FACE);
    glEnable(GL_TEXTURE_2D);
    
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue

    perspectiveProjectionMatrix = vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(GLuint)loadTextureFromBMPFile:(const char *)texFileName
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *textureFileNameWithPath = [NSString stringWithFormat:@"%@/%s", parentDirPath, texFileName];

    NSImage *bmpImage = [[NSImage alloc] initWithContentsOfFile:textureFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"Can't find %@", textureFileNameWithPath);
        return(0);
    }

    CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];

    int bmWidth = (int)CGImageGetWidth(cgImage);
    int bmHeight = (int)CGImageGetHeight(cgImage);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void *)CFDataGetBytePtr(imageData);

    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4 for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGBA,
			bmWidth,
			bmHeight,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			pixels); // _EXT try if not // verify 2-3

    // create mipmaps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D); // new addition

    CFRelease(imageData);
    return(bmpTexture);
}

-(void) reshape
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect = [self bounds];
    
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    if (height == 0) {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    perspectiveProjectionMatrix = vmath::perspective(45.0f,
                                            (width / height),
                                             0.1f,
                                             100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    // code
    [self drawView];
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm
    
    // your code here
    // 9 steps
    // declaration of matrices
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    vmath::mat4 rotationMatrix;
	vmath::mat4 translationMatrix;
	vmath::mat4 projectionMatrix;

    // initialize above 2 matrices to identity
    modelMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	projectionMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();

    // do necessary transformations like model scale, rotate, translate
    translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    rotationMatrix = vmath::rotate(angleTri, angleTri, angleTri);
    modelMatrix = translationMatrix * rotationMatrix;
    
	projectionMatrix = perspectiveProjectionMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, projectionMatrix);

	//bind with texture
	glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_marble);
    glUniform1i(samplerUniform, 0);
    
	if (bLight == true)
	{
		glUniform1i(lKeyIsPressedUniform, 1);
		glUniform3fv(laUniform, 1, light_ambient);
		glUniform3fv(ldUniform, 1, light_diffuse);
		glUniform3fv(lsUniform, 1, light_specular);
		glUniform4fv(lightPositionUniform, 2, light_position);
		glUniform3fv(kaUniform, 1, material_ambient);
		glUniform3fv(kdUniform, 1, material_diffuse);
		glUniform3fv(ksUniform, 1, material_specular);
		glUniform1f(materialShininessUniform, material_shininess);
	}
	else
	{
		glUniform1i(lKeyIsPressedUniform, 0);
	}

	glBindVertexArray(vao_cube);

	glDrawArrays(GL_TRIANGLE_FAN,
		0,
		4); // how many to draw? Ex: Triangle - 3 vertices, Quad 4 vert
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindVertexArray(0);


    glUseProgram(0); // Unbinding
    
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    [self myUpdate];
}

// learning Update() seems predefined function, don't use with this name
- (void)myUpdate
{
	// Rotate vice versa
	if (ascendingRect)
	{
		angleRect += 0.5f;
		if (angleRect > 360.0f)
		{
			ascendingRect = false;
		}
	}
	else
	{
		angleRect -= 0.5f;
		if (angleRect < 0.0f)
		{
			ascendingRect = true;
		}
	}

	if (ascendingTri)
	{
		angleTri += 0.5f;
		if (angleTri > 360.0f)
		{
			ascendingTri = false;
		}
	}
	else
	{
		angleTri -= 0.5f;
		if (angleTri < 0.0f)
		{
			ascendingTri = true;
		}
	}
}

-(BOOL)acceptsFirstResponder 
{ 
    // code 
    [[self window]makeFirstResponder:self]; 
    return(YES); 
}

-(void)keyDown :(NSEvent *)theEvent
{ 
    // code 
    int key=(int)[[theEvent characters] characterAtIndex:0]; 
    switch(key)
    {
        case 27: // Esc key
            [self release];
            [NSApp terminate:self];
            break; 
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self]; // repainting occurs automatically 
            break;

        case 'L':
		case 'l':
			if (bLight == false) // YES or NO
			{
				bLight = true;
			}
			else
			{
				bLight = false;
			}
            break;

        default:
            break;
    }
}

- (void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent 
{ 
    // code 
} 

-(void)rightMouseDown:(NSEvent *)theEvent 
{ 
    // code
}

- (void) dealloc
{ 
    // code
    // PP shader dtor
    // Safe Release
    // Don't declare shader objects globally, use locally in initialize n use as necessary
    if (vbo_texture_pyramid)
    {
        glDeleteBuffers(1, &vbo_texture_pyramid);
        vbo_texture_pyramid = 0;
    }
    if (vbo_position_pyramid)
    {
        glDeleteBuffers(1, &vbo_position_pyramid);
        vbo_position_pyramid = 0;
    }
    if (vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }

    if (vbo_texture_cube)
    {
        glDeleteBuffers(1, &vbo_texture_cube);
        vbo_texture_cube = 0;
    }
    if (vbo_position_cube)
    {
        glDeleteBuffers(1, &vbo_position_cube);
        vbo_position_cube = 0;
    }
    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    
    if (cube_texture)
    {
        glDeleteTextures(1, &cube_texture);
        cube_texture = 0;
    }
    if (pyramid_texture)
    {
        glDeleteTextures(1, &pyramid_texture);
        pyramid_texture = 0;
    }

    if (shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNo;
        
        glUseProgram(shaderProgramObject);
        
        // ask pgm how many shaders attached to you
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        
        GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
        if (pShaders)
        {
            glGetAttachedShaders(shaderProgramObject,
                                 shaderCount,
                                 &shaderCount, /// using same var
                                 pShaders);
            
            for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
            {
                glDetachShader(shaderProgramObject, pShaders[shaderNo]);
                glDeleteShader(pShaders[shaderNo]);
                pShaders[shaderNo] = 0;
            }
            free(pShaders);
        }
        
        glDeleteProgram(shaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
        shaderProgramObject = 0;
        glUseProgram(0);
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
} 

@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
