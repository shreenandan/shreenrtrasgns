 // vertex shader 
var gVertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "precision mediump int;" +
    "\n" +
    "in vec4 vPosition;" +
    "uniform mat4 u_mvp_matrix;" +
    "in vec4 vColor;" +
    "out vec4 out_color;" +
    "void main(void)" +
    "{" +
    "   gl_Position = u_mvp_matrix * vPosition;" +
    "	out_color = vColor;" +
    "}";

// fragment shader 
 var gFragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "precision mediump int;" +
    "\n" +
    "out vec4 FragColor;" +
    "in vec4 out_color;" +
    "void main(void)" +
    "{" +
    "   FragColor = out_color;" +
    "}";
