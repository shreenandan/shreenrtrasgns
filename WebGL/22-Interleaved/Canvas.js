//Global vars
var canvas = null;
var gl = null; // WebGL Context
var bFullScreen = false;
var canvasOriginalWidth;
var canvasOriginalHeight;

const WebGLMacros = {
    //when whole 'WebGLMacros' is 'const', all inside it are automatically const
    VDG_ATTRIBUTE_VERTEX:0,
    VDG_ATTRIBUTE_COLOR:1,
    VDG_ATTRIBUTE_NORMAL:2,
    VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoCube;
var vboCube;
var vboTexture;

var cubeTexture = 0;
var uniformTexture0Sampler;

var lightAmbient = [0.0, 0.0, 0.0];
var lightDiffuse = [1.0, 1.0, 1.0];
var lightSpecular = [1.0, 1.0, 1.0];
var lightPosition = [100.0, 100.0, 100.0]; //, 1.0];

var materialAmbient = [0.0, 0.0, 0.0];
var materialDiffuse = [1.0, 1.0, 1.0];
var materialSpecular = [1.0, 1.0, 1.0];
var materialShininess = 128.0;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform, ldUniform, lsUniform, lightPositionUniform;
var kaUniform, kdUniform, ksUniform, materialShininessUniform;
var lKeyPressedUniform;

var bLKeyPressed = false;

var mvpUniform;
var angleRectangle = 0.0;

var perspectiveProjectionMatrix;

//To start animation : To have requestAnimationFrame() to be called cross-browser compatible
var requestAnimationFrame =
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

//To stop animation : To have cancelAnimationFrame() to be called cross-browser compatible
var cancelAnimationFrame =
    window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
    window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
    window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
    window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

//onLoad Function
function main() {
    //get Canvas Element
    canvas = document.getElementById("AMC");
    if (!canvas) {
        console.log("obataining canvas failed.");
    } else {
        console.log("Obtaining canvas succeded");
    }
    canvasOriginalWidth = canvas.width;
    canvasOriginalHeight = canvas.height;

    //register keyboard's keydown event
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    //initialize WebGL
    init();

    resize();
    draw();
}

function toggleFullScreen() {
    var fullScreenElement =
        document.fullscreenElement || document.webkitFullscreenElement ||
        document.mozFullScreenElement || document.msFullscreenElement || null;

    if (null == fullScreenElement) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullScreen) {
            canvas.webkitRequestFullScreen();
        } else if (canvas.msRequestFullScreen) {
            canvas.msRequestFullScreen();
        }
        bFullScreen = true;
    } else {//if already fullScreen
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozRequestFullScreen();
        else if (canvas.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (canvas.msRequestFullScreen) {
            document.msExitFullScreen();
        }
        bFullScreen = false;
    }
}

function init() {
    //Code
    //get WebGL2.0 Context
    gl = canvas.getContext("webgl2");
    if (gl == null) {
        console.log("failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertexShader
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec4 vColor;" +
        "in vec2 vTexcoord;" +
        "in vec3 vNormal;" +
        "uniform mat4 uModelMatrix;" +
        "uniform mat4 uViewMatrix;" +
        "uniform mat4 uProjectionMatrix;" +
        "uniform mediump int uLKeyPressed;" +
        "uniform vec4 uLightPosition;" +
        "out vec3 transformedNormals;" +
        "out vec3 lightDirection;" +
        "out vec3 viewerVector;" +
        "out vec4 outColor;" +
        "out vec2 outTexcoord;" +
        "void main(void)" +
        "{" +
        "if(uLKeyPressed == 1)" +
        "{" +
        "vec4 eyeCoordinates = uViewMatrix * uModelMatrix * vPosition;" +
        "transformedNormals = mat3(uViewMatrix * uModelMatrix) * vNormal;" +
        "lightDirection = vec3(uLightPosition - eyeCoordinates);" +
        "viewerVector = normalize(vec3(-eyeCoordinates.xyz));" +
        "}" +
        "gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vPosition;" +
        "outColor = vColor;" +
        "outTexcoord = vTexcoord;" +
        "}";
		
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        } 
    }
    
    //fragment Shader
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "uniform vec3 uLa;" +
        "uniform vec3 uLd;" +
        "uniform vec3 uLs;" +
        "uniform vec3 uKa;" +
        "uniform vec3 uKd;" +
        "uniform vec3 uKs;" +
        "uniform float uMaterialShininess;" +
        "in vec3 transformedNormals;" +
        "in vec3 lightDirection;" +
        "in vec3 viewerVector;" +
        "in vec2 outTexcoord;" +
        "uniform sampler2D uSampler;" +
        "in vec4 outColor;" +
        "vec4 Texture;" +
        "out vec4 FragColor;" +
        "uniform int uLKeyPressed;" +
        "void main(void)" +
        "{" +
        "vec3 phongADSColor;" +
        "if(uLKeyPressed == 1)" +
        "{" +
        "vec3 normalizedTransformedNormals = normalize(transformedNormals);" +
        "vec3 normalizedLightDirection = normalize(lightDirection);" +
        "vec3 normalizedViewerVector = normalize(viewerVector);" +
        "vec3 reflectionVector = reflect(-normalizedLightDirection ,normalizedTransformedNormals);" +
        "float tnDotld = max(dot(normalizedLightDirection, normalizedTransformedNormals), 0.0);" +
        "vec3 ambient = uLa * uKa;" +
        "vec3 diffuse = uLd * uKd * tnDotld;" +
        "vec3 specular = uLs * uKs * pow(max(dot(reflectionVector, normalizedViewerVector), 0.0), uMaterialShininess);" +
        "phongADSColor = ambient + diffuse + specular;" +
        "}" +
        "else" +
        "{" +
        "phongADSColor = vec3(1.0,1.0,1.0);" +
        "}" +
        "Texture = texture(uSampler,outTexcoord);" +
        "FragColor = Texture * outColor * vec4(phongADSColor,1.0);" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    //shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    //prelinkAttributeBinding
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexCoord");

    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    cubeTexture = gl.createTexture();
    cubeTexture.image = new Image();
    cubeTexture.image.src = "teapot.png";
    cubeTexture.image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA,  gl.RGBA, gl.UNSIGNED_BYTE, cubeTexture.image);

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    }

    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "uModelMatrix");
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "uViewMatrix");
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "uProjectionMatrix");
	
    lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "uLKeyPressed");
    laUniform = gl.getUniformLocation(shaderProgramObject, "uLa");
    ldUniform = gl.getUniformLocation(shaderProgramObject, "uLd");
    lsUniform = gl.getUniformLocation(shaderProgramObject, "uLs");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "uKd");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "uLightPosition");
	
    kaUniform = gl.getUniformLocation(shaderProgramObject, "uKa");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "uKd");
    ksUniform = gl.getUniformLocation(shaderProgramObject, "uKs");
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "uMaterialShininess");


    gl.bindVertexArray(null);

    //vertices, colors, shaderAttribs, vbo, vao initializations for Triangle
    var rectangleVertices = new Float32Array([
        1.0, 1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
        -1.0, 1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
        -1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0,
        1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
        1.0, -1.0, 1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 1.0,
        -1.0, -1.0, 1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 1.0,
        -1.0, -1.0, -1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0,
        1.0, -1.0, -1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0,
        1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
        -1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0,
        -1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
        1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0,
        1.0, -1.0, -1.0, 0.0, 1.0, 1.0, 0.0, 0.0, -1.0, 1.0, 0.0,
        -1.0, -1.0, -1.0, 0.0, 1.0, 1.0, 0.0, 0.0, -1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0, 0.0, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0,
        1.0, 1.0, -1.0, 0.0, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0,
        -1.0, 1.0, 1.0, 1.0, 0.0, 1.0, -1.0, 0.0, 0.0, 1.0, 0.0,
        -1.0, 1.0, -1.0, 1.0, 0.0, 1.0, -1.0, 0.0, 0.0, 1.0, 1.0,
        -1.0, -1.0, -1.0, 1.0, 0.0, 1.0, -1.0, 0.0, 0.0, 0.0, 1.0,
        -1.0, -1.0, 1.0, 1.0, 0.0, 1.0, -1.0, 0.0, 0.0, 0.0, 0.0,
        1.0, 1.0, -1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
        1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0,
        1.0, -1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0,
        1.0, -1.0, -1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0
    ]);
	
    vaoCube = gl.createVertexArray();
    gl.bindVertexArray(vaoCube);

    //for Cube position
    vboCubePosition = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboCubePosition);
    gl.bufferData(gl.ARRAY_BUFFER, rectangleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
        3,
        gl.FLOAT,
        false, 11 * 4, 0 * 4);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);

    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR,
        3,
        gl.FLOAT,
        false, 11 * 4, 3 * 4);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);

    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_NORMAL,
        3,
        gl.FLOAT,
        false, 11 * 4, 6 * 4);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_NORMAL);

    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
        2,
        gl.FLOAT,
        false, 11 * 4, 9 * 4);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

   
    //set Clear Color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);//Blue
    //Depth test will be always enabled
    gl.enable(gl.DEPTH_TEST);
    //We Will always cull back faces for better performance
    gl.enable(gl.CULL_FACE);
	
    //initialize prespectiveProjection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize(){
    //code
    if (bFullScreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    } else {
        canvas.width = canvasOriginalWidth;
        canvas.height = canvasOriginalHeight;
    }

    //set the viewPort to Match
    gl.viewport(0, 0, canvas.width, canvas.height);

    //perspective Projection => left, right, bottom, top, near, far
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100);
}

function draw() {
    //code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(shaderProgramObject);
	
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var projectionMatrix = mat4.create();

    mat4.identity(modelMatrix);
    mat4.identity(viewMatrix);
    mat4.identity(projectionMatrix);

    //for Rectangle
    mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -4.0]);
    mat4.rotateX(modelMatrix, modelMatrix, degToRad(angleRectangle));
    mat4.rotateY(modelMatrix, modelMatrix, degToRad(angleRectangle));
    mat4.rotateZ(modelMatrix, modelMatrix, degToRad(angleRectangle));
    //mat4.multiply(projectionMatrix, perspectiveProjectionMatrix, modelMatrix);

    gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

    if (bLKeyPressed == true) {
        gl.uniform1i(lKeyPressedUniform, 1);

        gl.uniform3fv(laUniform, lightAmbient);
        gl.uniform3fv(ldUniform, lightDiffuse);
        gl.uniform3fv(lsUniform, lightSpecular);
        gl.uniform4fv(lightPositionUniform, lightPosition);

        gl.uniform3fv(kaUniform, materialAmbient);
        gl.uniform3fv(kdUniform, materialDiffuse);
        gl.uniform3fv(ksUniform, materialSpecular);
        gl.uniform1f(materialShininessUniform, materialShininess);
    } else {
        gl.uniform1i(lKeyPressedUniform, 0);
    }

	// Work with ABU
    gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
    gl.uniform1i(uniformTexture0Sampler, 0);


    gl.bindVertexArray(vaoCube);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
    gl.bindVertexArray(null);
    
    gl.useProgram(null);

    angleRectangle = angleRectangle + 0.9;
    if (angleRectangle >= 360.0) {
        angleRectangle = 0.0;
    }
    
    // Animation loop
    requestAnimationFrame(draw, canvas);
}

function uninitialize() {
    //code
    if (vaoCube) {
        gl.deleteVertexArray(vaoRectangle);
        vaoRectangle = null;
    }
	
    if (vboCubePosition) {
        gl.deleteVertexBuffer(vboCubePosition);
        vboCubePosition = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject, fragmentShaderObject); 
            gl.deleteShader(fragmentShaderObject); 
            fragmentShaderObject=null;
        }
            
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject, vertexShaderObject); 
            gl.deleteShader(vertexShaderObject); 
            vertexShaderObject=null;
        }
        gl.deleteProgram(shaderProgramObject); 
        shaderProgramObject=null;
    }
}

function keyDown(event) {
    switch (event.keyCode) {
        case 27://Escape and uninitialize();
            uninitialize();
            window.close();
            break;

        case 70: // for F or f
            toggleFullScreen();
            break;

        case 76: // for L or l
            if (bLKeyPressed == false) {
                bLKeyPressed = true;
            } else {
                bLKeyPressed = false;
            }
            break;
    }
}


function mouseDown(event) {
   //code
}

function degToRad(degrees) {
    //code
    return (degrees * Math.PI / 180);
}
