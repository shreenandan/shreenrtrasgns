var MyCount = 10;

const WebGLMacros = // when whole 'WebGL Macros' is 'const', all inside it are automatically const' 
{
    VDG_ATTRIBUTE_VERTEX: 0,
    VDG_ATTRIBUTE_COLOR: 1,
    VDG_ATTRIBUTE_NORMAL: 2,
    VDG_ATTRIBUTE_TEXTURE0: 3
};

var perspectiveProjectionMatrix_scn;
var gbShowGraph = false;
const ShaderModes = { BASIC: 0 }; // not really required now in WebGL
// as Í'll be passing shaderSourceCode to function, have control over it from outside
// Reserved for future purpose

class ShaderUniforms
{
    constructor() { }

    mvpUniform = -1;
}
var GenericUniforms_scn = new ShaderUniforms();

const LINES_TO_DRAW = 20;
const RATIO_COLS = 16; //.0f
const RATIO_ROWS = 9; //.0f
const NO_OF_COORDS_IN_3D_LINE = 6;
const NO_OF_GEOMETRIES = 4;
const NO_OF_COORDS_IN_3D_VERTEX = 3;
const NO_OF_VERTEX_TRIANGLE = 3;
const NO_OF_VERTEX_RECTANGLE = 4;

var genericShaderProgramObject_scn = -1;

class Coord
{
    constructor() { }
	x;
	y;
	z;
};

class Triplet
{
    constructor() { }
	red;
	green;
	blue;
};

class Geometry
{
    constructor() { }

	// StartX, StartY, StartZ, EndX, EndY, EndZ
	Vertices =
	[
		0.0, 0.0, 0.0, 
		0.0, 0.0, 0.0 
    ];
    Colors =
	[
		0.0, 0.0, 0.0, 
		0.0, 0.0, 0.0 
    ];
    
	VerticesDynamic = null;
	ColorsDynamic = null;
	ElementsDynamic = null;
	
	IsComplete = false;
	InitialX = 0.0;
	StopMarker = 0.0;
	TranslationStep = 0.0;
	MovingX = 0.0;

	InitialY = 0.0;
	MovingY = 0.0;

	Alpha = 0.0;

	NumVertices = 0;
	NumElements = 0;
};

const LineVerticesIndex =
{ 
	START_X : 0, 
	START_Y : 1, 
	START_Z : 2, 
	END_X : 3, 
	END_Y : 4, 
	END_Z : 5
};

const LineColorsIndex =
{
	START_RED : 0,
	START_GREEN : 1,
	START_BLUE : 2,
	END_RED : 3,
	END_GREEN : 4,
	END_BLUE : 5
};

var GeometryList_scn = new Array(NO_OF_GEOMETRIES); //.fill(new Geometry(), 0, NO_OF_GEOMETRIES); // Isn't this memset..
var vao_list_scn = [];
var vbo_position_list_scn = [];
var vbo_color_list_scn = [];
var vbo_element_list_scn = [];

const Geometries =
{
	GRAPH : 0,
	TRIANGLE : 1,
	INCIRCLE : 2,
	STICK : 3
};

var gStepRow = 2.0 / RATIO_ROWS;
var gStepCol = 2.0 / RATIO_COLS;

var gfMagnitude = 0.5;

// NOTE: Very important, discarding fill as same object is referenced by all array members.
function initGeometryList()
{
	for(var idx = 0; idx < NO_OF_GEOMETRIES; idx++)
	{
		GeometryList_scn[idx] = new Geometry();
	}
}