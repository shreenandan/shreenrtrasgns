// global vars
var canvas = null;
var gl = null; // WebGL context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

// To start animation : To have requestAnimation Frame() to be called "cross-browser" compatible 
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimation Frame() to be called "cross browser" compatible 
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main() {
    // get <canvas> element 
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // print canvas width and height on console 
    console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);

    // register keyboard's keydown event handler 
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL 
    init();

    // start drawing here as warming-up 
    resize();
    draw();
}

function toggleFullscreen() {
    // code 
    var fullscreen_element = document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    // if not fullscreen 
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen = false;
    }
}

function init() {
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");

    if (gl == null) // failed to get context 
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader 
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "void main(void)" + 
        "{" +
        "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "void main(void)" + 
        "{" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // linking 
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // set clear color 
    gl.clearColor(0.0, 0.0, 1.0, 1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE); 
}

function resize() {
    // code 
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if(canvas.height == 0)
    {
        canvas.height = 1;
    }
    // set the viewport to match 
    gl.viewport(0, 0, canvas.width, canvas.height);
}

function draw() {
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);
    gl.useProgram(null);
    
    // animation loop 
    requestAnimationFrame(draw, canvas);
}

function uninitialize() 
{ 
    // code 
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject, fragmentShaderObject); 
            gl.deleteShader (fragmentShaderObject); 
            fragmentShaderObject=null;
        }
            
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject, vertexShaderObject); 
            gl.deleteShader(vertexShaderObject); 
            vertexShaderObject=null;
        }
        gl.deleteProgram(shaderProgramObject); 
        shaderProgramObject=null;
    }
}

function keyDown(event) {
    // code 
    switch (event.keyCode) {
        case 70: // for 'F' or 'f' 
            toggleFullscreen();
            break;

        case 27: // Escape 
            // uninitialize 
            uninitialize();
            // close our application's tab 
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
    }
}

function mouseDown() {
}
