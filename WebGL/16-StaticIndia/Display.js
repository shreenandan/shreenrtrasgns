
var rotationAngle = 0.0;

function updateOrg()
{
	if (GeometryList_scn[Geometries.STICK].MovingY <= 0.0)
	{
		if (rotationAngle <= 180.0)
			rotationAngle += 0.25;
	}
	else
		rotationAngle += 0.25;

	if (rotationAngle > 360.0)
		rotationAngle = 0.0;

	if (GeometryList_scn[Geometries.TRIANGLE].MovingX > 0.0)
        GeometryList_scn[Geometries.TRIANGLE].MovingX -= 0.0006;
	if (GeometryList_scn[Geometries.TRIANGLE].MovingY < 0.0)
        GeometryList_scn[Geometries.TRIANGLE].MovingY += 0.0003;

	if (GeometryList_scn[Geometries.STICK].MovingY > 0.0)
        GeometryList_scn[Geometries.STICK].MovingY -= GeometryList_scn[Geometries.STICK].TranslationStep;

	/*if (GeometryList[INCIRCLE].MovingX < 0.0f)
		GeometryList[INCIRCLE].MovingX += 0.0006f;
	if (GeometryList[INCIRCLE].MovingY < 0.0f)
		GeometryList[INCIRCLE].MovingY += 0.0003f;*/
}

function update()
{

}

function draw() {
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(genericShaderProgramObject_scn);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    var rotationMatrix = mat4.create();

    // vec3.set(modelViewMatrix, [0.0, 0.0, -3.0]);
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -2.0]);
    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
    gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);

    if (gbShowGraph)
    {
        drawGraph(Geometries.GRAPH);
    }
    
    drawStaticIndia();

    // mat4.rotateY(rotationMatrix, rotationMatrix, degToRad(rotationAngle));

    // // TRIANGLE
    // modelViewMatrix = mat4.create();
    // mat4.translate(modelViewMatrix, modelViewMatrix, [GeometryList_scn[Geometries.TRIANGLE].MovingX, GeometryList_scn[Geometries.TRIANGLE].MovingY, -2.0]);
    // mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);
    // mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
    // gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
    // drawGeometryWithMode(Geometries.TRIANGLE, gl.LINE_LOOP, 0, NO_OF_VERTEX_TRIANGLE);

	// // STICK
    // modelViewMatrix = mat4.create();
    // mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, GeometryList_scn[Geometries.STICK].MovingY, -2.0]);
    // mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
    // gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
    // drawGeometryWithMode(Geometries.STICK, gl.LINES, 0, 2); //NO_OF_COORDS_IN_3D_LINE);

	// // INCIRCLE
    // modelViewMatrix = mat4.create();
    // mat4.translate(modelViewMatrix, modelViewMatrix, [-GeometryList_scn[Geometries.TRIANGLE].MovingX + GeometryList_scn[Geometries.INCIRCLE].MovingX, GeometryList_scn[Geometries.TRIANGLE].MovingY + GeometryList_scn[Geometries.INCIRCLE].MovingY, -2.0]);
    // mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);
    // mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
    // gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
    // drawGeometryWithMode(Geometries.INCIRCLE, gl.LINE_LOOP, 0, GeometryList_scn[Geometries.INCIRCLE].NumVertices);

    gl.useProgram(null);

    update();

    // animation loop 
    requestAnimationFrame(draw, canvas);
}

function drawGraph(geometryListIdx)
{
	gl.lineWidth(1.0);

	// bind with vao - this will avoid many vbo repetitive calls in display
	gl.bindVertexArray(vao_list_scn[geometryListIdx]);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	gl.drawArrays(gl.LINES, 0, ((LINES_TO_DRAW + 2) * 4) * 2);
    gl.bindVertexArray(null);
}

function drawGeometryWithMode(geometryListIdx, mode, first, count)
{
	gl.bindVertexArray(vao_list_scn[geometryListIdx]);
	gl.drawArrays(mode, first, count);
	gl.bindVertexArray(null);
}

function degToRad(degrees) {
    // code 
    return (degrees * Math.PI / 180);
}



function drawStaticIndia()
{
	gl.lineWidth(5.0);

	gl.bindVertexArray(vao_staticindia);
	gl.drawArrays(gl.LINES, 0, 14 * 2);

	gl.bindVertexArray(null);
}
