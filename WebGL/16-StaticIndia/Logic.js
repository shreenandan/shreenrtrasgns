////////////////////////////////////
// TEST INTER MODULE FUNCTIONS
////////////////////////////////////

function incrCount()
{
	++MyCount;
}

function getCount()
{
	return MyCount;
}

////////////////////////////////////
// LOGIC COMMON FUNCTIONS
////////////////////////////////////

function GLenumToString(shaderType)
{
	switch (shaderType)
	{
	case gl.VERTEX_SHADER:
		return "Vertex Shader";
	case gl.FRAGMENT_SHADER:
		return "Fragment Shader";
	default:
		return "Wrong Shader";
	}
}

function makeShadersAndProgram(vsoSourceCode, fsoSourceCode){
    var vso = makeShader(gl.VERTEX_SHADER, vsoSourceCode);
    var fso = makeShader(gl.FRAGMENT_SHADER, fsoSourceCode);
    // debugger
    var pgm = makeShaderProgramAndLink(vso, fso);

    return {spo:pgm.spo, shUniforms:pgm.shUniforms, vso:vso, fso:fso};
}

function makeShader(type, soSourceCode){
    var so = gl.createShader(type);
    gl.shaderSource(so, soSourceCode);
    gl.compileShader(so);
    
    if (gl.getShaderParameter(so, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(so);
        if (error.length > 0) {
            alert(GLenumToString(type) + ": Compilation Error: ");
            alert(error);
            uninitialize();
        }
    }
    // debugger
    return so;
}

function makeShaderProgramAndLink(vso, fso){    
    // shader program 
    // debugger
    var shUniforms = new ShaderUniforms();
    var spo = gl.createProgram();
    gl.attachShader(spo, vso);
    gl.attachShader(spo, fso);

    // pre-link binding of shader program object with vertex shader attributes 
    gl.bindAttribLocation(spo, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(spo, WebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");

    // linking 
    gl.linkProgram(spo);
    if (!gl.getProgramParameter(spo, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(spo);
        if (error.length > 0) {
            alert("Shader Program: Link Error: ");
            alert(error);
            uninitialize();
        }
    }
    
    // get MVP uniform location 
    shUniforms.mvpUniform = gl.getUniformLocation(spo, "u_mvp_matrix");

    return {spo:spo, shUniforms:shUniforms};
}

///////////////////////////////////////////////
// LOGIC UN/BIND VAO, VBO, FILLDATA FUNCTIONS
///////////////////////////////////////////////

function bindVao(){
    var theVao = gl.createVertexArray();
    gl.bindVertexArray(theVao);

    return theVao;
}

function unbindVao(){
    gl.bindVertexArray(null);
}

function bindVbo(){
    var theVbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, theVbo);

    return theVbo;
}

function unbindVbo(){
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
}

function fillBufferData(data, noOfElementsInOneTuple, isStatic, amc_attribute){
    if (isStatic)
	{
        gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
    }
    else
    {
        gl.bufferData(gl.ARRAY_BUFFER, data, gl.DYNAMIC_DRAW);
    }
    gl.vertexAttribPointer(amc_attribute, noOfElementsInOneTuple, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(amc_attribute);
}

////////////////////////////////////
// LOGIC UNINITIALIZE FUNCTIONS
////////////////////////////////////

function unmarkVaoVboList()
{
	for (var i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		vao_list_scn[i] = -1;
		vbo_position_list_scn[i] = -1;
		vbo_color_list_scn[i] = -1;
		vbo_element_list_scn[i] = -1;
	}
}

function uninitializePP(){
    // debugger
    // PP shader dtor
	// Safe Release
	for (var i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		vbo_element_list_scn[i] = uninitVBO(vbo_element_list_scn[i]);
		vbo_color_list_scn[i] = uninitVBO(vbo_color_list_scn[i]);
		vbo_position_list_scn[i] = uninitVBO(vbo_position_list_scn[i]);
		vao_list_scn[i] = uninitVAO(vao_list_scn[i]); // Doubt ind of memory leak
	}
    // debugger

	uninitSPO({obj: genericShaderProgramObject_scn});
}

function uninitVAO(theVao){
    if(theVao && theVao != -1)
    {
        gl.deleteVertexArray(theVao); 
        theVao=null; 
    }
    return theVao;
}

function uninitVBO(theVbo){
    if(theVbo && theVbo != -1)
    {
        gl.deleteBuffer(theVbo); 
        theVbo=null; 
    }
    return theVbo;
}

// Texture not used, but future provision
function uninitTexture(theTex){
    if(theTex && theTex != -1)
    {
        gl.deleteTexture(theTex);
        theTex = null;
    }
    return theTex;
}

function uninitSPO(theSpo){
    if(theSpo.obj && theSpo.obj != -1)
    {
        var shaderCount;
		var shaderNo;

        gl.useProgram(theSpo.obj);
        
        shaderCount = gl.getProgramParameter(theSpo.obj, gl.ATTACHED_SHADERS);
        var shaderSequence = gl.getAttachedShaders(theSpo.obj);

        for(shaderNo = 0; shaderNo < shaderCount; shaderNo++)
        {
            gl.detachShader(theSpo.obj, shaderSequence[shaderNo]); 
            gl.deleteShader(shaderSequence[shaderNo]); 
            shaderSequence[shaderNo] = null;
        }

        gl.deleteProgram(theSpo.obj); 
        theSpo.obj=null;

        gl.useProgram(null);
    }
}

////////////////////////////////////
// LOGIC CUSTOM GEOMETRY FUNCTIONS
////////////////////////////////////

function initVAOs(){
    // code
    initGeometryList();
    unmarkVaoVboList();

    // vao handled
    graphObj = {
        geometryListIdx : Geometries.GRAPH
    };

    // initVaoGraph(Geometries.GRAPH); // graphObj
    // initVaoTriangle(Geometries.TRIANGLE);
    // initVaoIncircle(Geometries.INCIRCLE);
    // initVaoStick(Geometries.STICK);
	initVaoSI();
	
	// DOCUMENTATION LINK
	// https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/lineWidth
    console.log(gl.getParameter(gl.ALIASED_LINE_WIDTH_RANGE));
}

function initVaoGraph(geometryListIdx) //(geometryObj)
{
    // debugger
    var noOfVertices = ((LINES_TO_DRAW + 2) * 4 * NO_OF_COORDS_IN_3D_LINE);
    GeometryList_scn[geometryListIdx].VerticesDynamic = new Float32Array(noOfVertices).fill(0.0);
    // debugger
	var i = 0, j = 0;
	var stepRow = 1.0 / LINES_TO_DRAW;
	var stepCol = 1.0 / LINES_TO_DRAW;
	var xcoord = 1.0;
	var ycoord = 1.0;

	for (i = 0; i < LINES_TO_DRAW; i++)
	{
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		xcoord -= stepCol;
	}

	for (i = 0; i < LINES_TO_DRAW; i++)
	{
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		ycoord -= stepRow;
	}

	// X Axis
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	// Y Axis
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

    //// COLORS
    GeometryList_scn[geometryListIdx].ColorsDynamic = new Float32Array(noOfVertices).fill(0.0);
    j = 0;
	for (i = 0; i < LINES_TO_DRAW * 4 * 2; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	}

	// X Axis
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

	// Y Axis
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].VerticesDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].ColorsDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}

function initVaoTriangle(geometryListIdx)
{
    var noOfVertices = NO_OF_VERTEX_TRIANGLE * NO_OF_COORDS_IN_3D_VERTEX;
    GeometryList_scn[geometryListIdx].VerticesDynamic = new Float32Array(noOfVertices).fill(0.0);

	var j = 0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].VerticesDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.vertexAttrib3f(WebGLMacros.VDG_ATTRIBUTE_COLOR, 1.0, 1.0, 0.0);
    unbindVbo();

    unbindVao();

	GeometryList_scn[geometryListIdx].MovingX = 1.0;
	GeometryList_scn[geometryListIdx].MovingY = -0.50;
}

function initVaoIncircle(geometryListIdx)
{
	// code
	var sideA = 0.0, sideB = 0.0, sideC = 0.0;
	var perimeter = 0.0, halfPerimeter = 0.0;
	var incircleOx = 0.0, incircleOy = 0.0;
    var radius = 0.0;
    
	GeometryList_scn[geometryListIdx].TranslationStep = 0.1;
	GeometryList_scn[geometryListIdx].NumVertices = ((2.0 * Math.PI) / GeometryList_scn[geometryListIdx].TranslationStep) + 1;
	    
    var noOfVertices = GeometryList_scn[geometryListIdx].NumVertices * NO_OF_COORDS_IN_3D_VERTEX;
    GeometryList_scn[geometryListIdx].VerticesDynamic = new Float32Array(noOfVertices).fill(0.0);

    var ret_perimeter = getSidesAndPerimeterFromTriangle(gfMagnitude, sideA, sideB, sideC);
    perimeter = ret_perimeter.perimeter;
    sideA = ret_perimeter.sideA;
    sideB = ret_perimeter.sideB;
    sideC = ret_perimeter.sideC;
	halfPerimeter = perimeter / 2.0;

    // debugger
	var ret_incircle = getIncircleOrigin(perimeter, sideA, -gfMagnitude, -gfMagnitude, sideB, gfMagnitude, -gfMagnitude, sideC, 0, gfMagnitude, incircleOx, incircleOy);
    incircleOx = ret_incircle.incircleOx;
    // debugger
    incircleOy = ret_incircle.incircleOy;
    radius = getIncircleRadius(halfPerimeter, sideA, sideB, sideC);

	GeometryList_scn[geometryListIdx].MovingX = incircleOx;
	GeometryList_scn[geometryListIdx].MovingY = incircleOy;

	var j = 0;

	for (var angle = 0.0; angle <= 2.0 * Math.PI; angle += GeometryList_scn[geometryListIdx].TranslationStep)
	{
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = Math.cos(angle) * radius;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = Math.sin(angle) * radius;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	}

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].VerticesDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.vertexAttrib3f(WebGLMacros.VDG_ATTRIBUTE_COLOR, 1.0, 1.0, 0.0);
    unbindVbo();

    unbindVao();
    
    // console.log('after inner');
    // console.log(incircleOx);
    // console.log(incircleOy);
    // console.log(GeometryList_scn[Geometries.INCIRCLE].MovingY);
    // console.log(GeometryList_scn[geometryListIdx].MovingY);
    // // console.log(outerCircleOx);
    // // console.log(outerCircleOy);
}

function getIncircleRadius(halfPerimeter, sideA, sideB, sideC)
{
	var area = 0.0;
	area = (Math.sqrt(halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC)));
	return (area / halfPerimeter);
}

function getIncircleOrigin(perimeter, sideA, Ax, Ay, sideB, Bx, By, sideC, Cx, Cy, incircleOx, incircleOy)
{
	incircleOx = ((sideA * Ax) + (sideB * Bx) + (sideC * Cx)) / perimeter;
    incircleOy = ((sideA * Ay) + (sideB * By) + (sideC * Cy)) / perimeter;
    
    var ret_obj = 
    {
        incircleOx : incircleOx,
        incircleOy : incircleOy
    };
    // debugger

    return ret_obj;
}

function getSidesAndPerimeterFromTriangle(size, sideA, sideB, sideC)
{
	sideA = Math.sqrt(Math.pow((0.0 - size), 2) + Math.pow((size - (-size)), 2));
	sideB = Math.sqrt(Math.pow((0.0 - (-size)), 2) + Math.pow((size - (-size)), 2));
    sideC = Math.sqrt(Math.pow(((-size) - size), 2) + Math.pow(((-size) - (-size)), 2)); // Apex
    
    var ret_obj = 
    {
        perimeter : (sideA + sideB + sideC),
        sideA : sideA,
        sideB : sideB,
        sideC : sideC,
    };

    return ret_obj;
}

function initVaoStick(geometryListIdx)
{
    var noOfVertices = NO_OF_VERTEX_TRIANGLE * NO_OF_COORDS_IN_3D_VERTEX;
    GeometryList_scn[geometryListIdx].VerticesDynamic = new Float32Array(noOfVertices).fill(0.0);

    // midpoint = (x1+x2)/2, (y1+y2)/2
	// midpoint of side opposite to apex
	var midPointX = (-gfMagnitude + gfMagnitude) / 2.0;
	var midPointY = (-gfMagnitude + -gfMagnitude) / 2.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.START_X] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.START_Y] = gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.START_Z] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.END_X] = midPointX;
	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.END_Y] = midPointY;
	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.END_Z] = 0.0;
    // console.log(GeometryList_scn[geometryListIdx].Vertices.length);
    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].VerticesDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.vertexAttrib3f(WebGLMacros.VDG_ATTRIBUTE_COLOR, 1.0, 1.0, 0.0);
    unbindVbo();

    unbindVao();
    
	GeometryList_scn[geometryListIdx].MovingY = 0.50;
	GeometryList_scn[geometryListIdx].TranslationStep = 0.0003;
}

//////////////////////////////////


////////////////////////////////////
// LOGIC STATIC INDIA FUNCTIONS
////////////////////////////////////
function initVaoSI()
{
	// variable declarations
	var index = 0;
	var stepRow = 2.0 / RATIO_ROWS;
	var stepCol = 2.0 / RATIO_COLS;
	var xcoord = 1.0;
	var ycoord = 1.0;
	var theHeight = stepRow * 2.5;
	var IposX; // = -1.0f + (stepCol * 3.0f);
	var bigLetterWidth = stepCol * 1.5;
	var distanceInLetter = stepCol / 2.0;
    var p1 = new Coord(0,0,0), p2 = new Coord(0,0,0), p3 = new Coord(0,0,0);
    
	IposX = -1.0 + (stepCol * 4.75);// +distanceInLetter;

    debugger
    // var siVertices[85];
    var siVertices = Array.apply(null, Array(85)).map(function(val, i){ return 0.0; });

    debugger
	// I
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	
	// N
	IposX += distanceInLetter;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	IposX += bigLetterWidth;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	// D
	IposX += distanceInLetter;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX + bigLetterWidth; siVertices[index++] = theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX + bigLetterWidth; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	IposX += bigLetterWidth;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	// I
	IposX += distanceInLetter;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	// A
	IposX += distanceInLetter;
	p1.x = IposX, p1.y = -theHeight;
	IposX += (bigLetterWidth / 2.0);
	p2.x = IposX, p2.y = theHeight;
	IposX += (bigLetterWidth / 2.0);
	p3.x = IposX, p3.y = -theHeight;

	//drawTricolorInLetterA(p1.x, p1.y, p2.x, p2.y, p2.x, p2.y, p3.x, p3.y, siVertices, index);

	siVertices[index++] = p1.x; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	siVertices[index++] = p2.x; siVertices[index++] = theHeight; siVertices[index++] = 0;
	
	siVertices[index++] = p2.x; siVertices[index++] = theHeight; siVertices[index++] = 0;
    siVertices[index++] = p3.x; siVertices[index++] = -theHeight; siVertices[index++] = 0;
    
    /// TODO
	drawTricolorInLetterA(p1.x, p1.y, p2.x, p2.y, p2.x, p2.y, p3.x, p3.y, siVertices, index);

    var siColor =
    [
        // I
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// N
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// D
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,

		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// I
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// A
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue
    ];

    //// OGL
    vao_staticindia = bindVao();

    vbo_si_position = bindVbo();
    fillBufferData(new Float32Array(siVertices), 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();

    vbo_si_color = bindVbo();
    fillBufferData(new Float32Array(siColor), 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}

// step 1] find mid points of both sides
// step 2] can draw middle line joining both mid points
// step 3] find lengths of all three sides
// step 4] Upper: now we have lengths of all sides. find one angle. Inverse cos
//			cos A = (b2 + c2 - a2) /2bc		A = cos inverse(value)
// step 5] Lower: angle = 180 - upper. Inverse cos
// step 6].a find next point of side 1 nextS1A'(x + pos * cos(A), y + pos * sin(A))
// step 6].b find next point of side 2 nextS2A'(x + pos * cos(A), y + pos * sin(A))
// step 7] draw line from obtained points nextS1A' <--> nextS2A'
// step 8].a find prev point of side 1 prevS1A'(x + pos * cos(A), y + pos * sin(A))
// step 8].b find prev point of side 2 prevS2A'(x + pos * cos(A), y + pos * sin(A))
// step 9] draw line from obtained points prevS1A' <--> prevS2A'
function drawTricolorInLetterA(side1X1, side1Y1, side1X2, side1Y2,
	side2X1, side2Y1, side2X2, side2Y2, siVertices, index)
{
	// midpoint = (x1+x2)/2, (y1+y2)/2
	var side1MidPoint = new Coord(), side2MidPoint = new Coord();

	side1MidPoint.x = (side1X1 + side1X2) / 2.0;
	side1MidPoint.y = (side1Y1 + side1Y2) / 2.0;
	side1MidPoint.z = 0.0;

	side2MidPoint.x = (side2X1 + side2X2) / 2.0;
	side2MidPoint.y = (side2Y1 + side2Y2) / 2.0;
	side2MidPoint.z = 0.0;

	var lengthB = Math.sqrt(Math.pow((side1MidPoint.x - side1X2), 2) + Math.pow((side1MidPoint.y - side1Y2), 2));
	var lengthC = Math.sqrt(Math.pow((side1MidPoint.x - side2MidPoint.x), 2) + Math.pow((side1MidPoint.y - side2MidPoint.y), 2));
	var lengthA = Math.sqrt(Math.pow((side2MidPoint.x - side2X2), 2) + Math.pow((side2MidPoint.y - side2Y2), 2));

	var side1CosA = ((lengthB*lengthB) + (lengthC*lengthC) - (lengthA*lengthA)) / (2.0 *lengthB * lengthC);
	var side1AngleA = Math.cos(side1CosA) * (180.0 / Math.PI); // radian to degree
	var side1CosB = ((lengthA*lengthA) + (lengthC*lengthC) - (lengthB*lengthB)) / (2.0 *lengthA * lengthC);
	var side1AngleB = Math.cos(side1CosB) * (180.0 / Math.PI); // radian to degree
    
	var nextS1Ax = side1MidPoint.x + (0.002 * Math.cos(side1AngleA));
	var nextS1Ay = side1MidPoint.y + (0.035 * Math.sin(side1AngleA));
	var nextS2Ax = side2MidPoint.x - (0.002 * Math.cos(side1AngleB));
	var nextS2Ay = side2MidPoint.y + (0.035 * Math.sin(side1AngleB));
                                           
	var prevS1Ax = side1MidPoint.x - (0.002 * Math.cos(side1AngleA));
	var prevS1Ay = side1MidPoint.y - (0.035 * Math.sin(side1AngleA));
	var prevS2Ax = side2MidPoint.x + (0.002 * Math.cos(side1AngleB));
	var prevS2Ay = side2MidPoint.y - (0.035 * Math.sin(side1AngleB));

	siVertices[index++] = prevS1Ax; siVertices[index++] = prevS1Ay; siVertices[index++] = side1MidPoint.z;
	siVertices[index++] = prevS2Ax; siVertices[index++] = prevS2Ay; siVertices[index++] = side2MidPoint.z;

	siVertices[index++] = side1MidPoint.x; siVertices[index++] = side1MidPoint.y; siVertices[index++] = side1MidPoint.z;
	siVertices[index++] = side2MidPoint.x; siVertices[index++] = side2MidPoint.y; siVertices[index++] = side2MidPoint.z;

	siVertices[index++] = nextS1Ax; siVertices[index++] = nextS1Ay; siVertices[index++] = side1MidPoint.z;
	siVertices[index++] = nextS2Ax; siVertices[index++] = nextS2Ay; siVertices[index++] = side2MidPoint.z;
}
