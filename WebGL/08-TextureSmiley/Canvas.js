// global vars
var canvas = null;
var gl = null; // WebGL context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = // when whole 'WebGL Macros' is 'const', all inside it are automatically const' 
{
    VDG_ATTRIBUTE_VERTEX: 0,
    VDG_ATTRIBUTE_COLOR: 1,
    VDG_ATTRIBUTE_NORMAL: 2,
    VDG_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo;
var vbo_texture;

var mvpUniform;
var texture_smiley;
var samplerUniform;

var perspectiveProjectionMatrix;

// To start animation : To have requestAnimation Frame() to be called "cross-browser" compatible 
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimation Frame() to be called "cross browser" compatible 
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main() {
    // get <canvas> element 
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // print canvas width and height on console 
    console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);

    // register keyboard's keydown event handler 
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL 
    init();

    // start drawing here as warming-up 
    resize();
    draw();
}

function toggleFullscreen() {
    // code 
    var fullscreen_element = document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    // if not fullscreen 
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullscreen)
            canvas.mozrequestFullscreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullscreen)
            document.mozCancelFullscreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen = false;
    }
}

function init() {
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");

    if (gl == null) // failed to get context 
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader 
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "uniform mat4 u_mvp_matrix;" +
        "in vec2 vTexCoord;" +
		"out vec2 out_texcoord;" +
        "void main(void)" +
        "{" +
            "gl_Position = u_mvp_matrix * vPosition;" +
        "	out_texcoord = vTexCoord;" +
        "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "out vec4 FragColor;" +
        "in vec2 out_texcoord;" +
        "uniform highp sampler2D u_sampler;" +
        "void main(void)" +
        "{" +
            "FragColor = texture(u_sampler, out_texcoord);" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // pre-link binding of shader program object with vertex shader attributes 
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexCoord");

    // linking 
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // Load texture
    texture_smiley = gl.createTexture();
    texture_smiley.image = new Image();
    // texture_smiley.image.crossOrigin = "*";
    texture_smiley.image.src = "Smiley.png";
    // var theUrl = "Smiley.png";
    // requestCORSIfNotSameOrigin(texture_smiley.image, theUrl);
    // texture_smiley.image.src = theUrl;
    texture_smiley.image.onload = function()
    {
        gl.bindTexture(gl.TEXTURE_2D, texture_smiley);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_smiley.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    };

    // get MVP uniform location 
    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

    // *** vertices, colors, shader attribs, vbo, vao initializations *** 
    var quadVertices = new Float32Array([
        1.0, 1.0, 1.0,
		-1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0
    ]);
    var quadTexCoord = new Float32Array([
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0
    ]);

    vao = gl.createVertexArray();
    gl.bindVertexArray(vao);

    vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo);

    gl.bufferData(gl.ARRAY_BUFFER, quadVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
        3, // 3 is for X,Y,Z co-ordinates in our array 
        gl.FLOAT,
        false, 0, 0);

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_texture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
    gl.bufferData(gl.ARRAY_BUFFER, quadTexCoord, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
        2, // st
        gl.FLOAT,
        false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // set clear color 
    gl.clearColor(0.0, 0.0, 1.0, 1.0); // blue 
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE); 

    // initialize projection matrix 
    perspectiveProjectionMatrix = mat4.create();
}

function requestCORSIfNotSameOrigin(img, url) {
    if ((new URL(url)).origin !== window.location.origin) {
        img.crossOrigin = "";
    }
}

function resize() {
    // code 
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if(canvas.height == 0)
    {
        canvas.height = 1;
    }
    // set the viewport to match 
    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw() {
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    // vec3.set(modelViewMatrix, [0.0, 0.0, -3.0]);
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);
    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    // bind with texture
    gl.bindTexture(gl.TEXTURE_2D, texture_smiley);
    gl.uniform1i(samplerUniform, 0);

    gl.bindVertexArray(vao);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

    gl.bindVertexArray(null);
    gl.useProgram(null);

    // animation loop 
    requestAnimationFrame(draw, canvas);
}

function uninitialize() 
{ 
    // code 
    if(vao)
    {
        gl.deleteVertexArray(vao); 
        vao=null; 
    }
    if(vbo)
    {
        gl.deleteBuffer(vbo); 
        vbo=null; 
    }
    if(vbo_texture)
    {
        gl.deleteBuffer(vbo_texture);
        vbo_texture = null;
    }
    if(texture_smiley)
    {
        gl.deleteTexture(texture_smiley);
        texture_smiley = null;
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject, fragmentShaderObject); 
            gl.deleteShader(fragmentShaderObject); 
            fragmentShaderObject=null;
        }
            
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject, vertexShaderObject); 
            gl.deleteShader(vertexShaderObject); 
            vertexShaderObject=null;
        }
        gl.deleteProgram(shaderProgramObject); 
        shaderProgramObject=null;
    }
}

function keyDown(event) {
    // code 
    switch (event.keyCode) {
        case 70: // for 'F' or 'f' 
            toggleFullscreen();
            break;

        case 27: // Escape 
            // uninitialize 
            uninitialize();
            // close our application's tab 
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
    }
}

function mouseDown() {
}

// Obtaining Canvas Succeeded

// Canvas.js:59 Canvas Width : 800 And Canvas Height : 600
// Canvas.js:200 Uncaught DOMException: Failed to execute 'texImage2D' on 'WebGL2RenderingContext': The image element contains cross-origin data, and may not be loaded.
//     at Image.texture_smiley.image.onload (file:///C:/RTR/WebGL/PP/08-TextureSmiley/Canvas.js:200:12)
// texture_smiley.image.onload @ Canvas.js:200
// load (async)
// init @ Canvas.js:196
// main @ Canvas.js:67
// onload @ Canvas.html:9