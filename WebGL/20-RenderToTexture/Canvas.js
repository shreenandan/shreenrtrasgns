// global vars
var canvas = null;
var gl = null; // WebGL context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = // when whole 'WebGL Macros' is 'const', all inside it are automatically const' 
{
    VDG_ATTRIBUTE_VERTEX: 0,
    VDG_ATTRIBUTE_COLOR: 1,
    VDG_ATTRIBUTE_NORMAL: 2,
    VDG_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_pyramid;
var vbo_position_pyramid;
var vbo_color_pyramid;

var vao_cube;
var vbo_position_cube;
var vbo_texture_cube;
var vbo_texture_pyramid;

var texture_kundali = 0;
var texture_stone = 0;
var samplerUniform;

var mvpUniform;
var perspectiveProjectionMatrix;

var angleOfRotation = 0.0;

const TEX_WIDTH = 256;
const TEX_HEIGHT = 256;
var fbo;
var rbo;
var texture_myrender;
var gWidth;
var gHeight;

// To start animation : To have requestAnimation Frame() to be called "cross-browser" compatible 
var requestAnimationFrame =
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimation Frame() to be called "cross browser" compatible 
var cancelAnimationFrame =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main() {
    // get <canvas> element 
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // print canvas width and height on console 
    console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);

    // register keyboard's keydown event handler 
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL 
    init();

    // start drawing here as warming-up 
    resize();
    draw();
}

function toggleFullscreen() {
    // code 
    var fullscreen_element = document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    // if not fullscreen 
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen = false;
    }
}

function init() {
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");

    if (gl == null) // failed to get context 
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader 
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec2 vTexCoord;" +
        "uniform mat4 u_mvp_matrix;" +
        "out vec2 out_texcoord;" +
        "void main(void)" +
        "{" +
        "   gl_Position = u_mvp_matrix * vPosition;" +
        "	out_texcoord = vTexCoord;" +
        "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "out vec4 FragColor;" +
        "in vec2 out_texcoord;" +
        "uniform highp sampler2D u_sampler;" +
        "void main(void)" +
        "{" +
        "   FragColor = texture(u_sampler, out_texcoord);" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // pre-link binding of shader program object with vertex shader attributes 
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexCoord");

    // linking 
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }


    // get MVP uniform location 
    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
    samplerUniform = gl.getUniformLocation(shaderProgramObject, "u_sampler");

    // *** vertices, colors, shader attribs, vbo, vao initializations *** 
    var pyramidVertices = new Float32Array([
		0.0, 1.0, 0.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,

		0.0, 1.0, 0.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,

		0.0, 1.0, 0.0,
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,

		0.0, 1.0, 0.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0
    ]);

    var pyramidTexCoord = new Float32Array([
        0.5, 1.0,
		0.0, 0.0,
        1.0, 0.0,
        
		0.5, 1.0,
		1.0, 0.0,
        0.0, 0.0,
        
		0.5, 1.0,
		1.0, 0.0,
        0.0, 0.0,
        
		0.5, 1.0,
		0.0, 0.0,
		1.0, 0.0
    ]);

    vao_pyramid = gl.createVertexArray();
    gl.bindVertexArray(vao_pyramid);

    vbo_position_pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_pyramid);

    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
        3, // 3 is for X,Y,Z co-ordinates in our pyramidVertices array 
        gl.FLOAT,
        false, 0, 0);

    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_texture_pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidTexCoord, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //============== SQAURE ====================
    var cubeVertices = new Float32Array([
        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,

        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,

        1.0, 1.0, 1.0,
        -1.0, 1.0, 1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,

        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,

        1.0, 1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,

        -1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0
    ]);

    var quadTexCoord = new Float32Array([
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0
    ]);

    vao_cube = gl.createVertexArray();
    gl.bindVertexArray(vao_cube);

    vbo_position_cube = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_cube);
    gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_texture_cube = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_cube);
    gl.bufferData(gl.ARRAY_BUFFER, quadTexCoord, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);


    ///////////////////////////////////////
    // NOTE: Important initialization part
    initRenderTexture();
    initFramebuffer();
    ///////////////////////////////////////

    // Load texture
    texture_kundali = gl.createTexture();
    texture_kundali.image = new Image();
    // texture_kundali.image.crossOrigin = "*";
    texture_kundali.image.src = "Vijay_Kundali.png";
    texture_kundali.image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, texture_kundali);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_kundali.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    };

    texture_stone = gl.createTexture();
    texture_stone.image = new Image();
    // texture_stone.image.crossOrigin = "*";
    texture_stone.image.src = "Stone.png";
    texture_stone.image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, texture_stone);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_stone.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    };

    //glEnable(GL_TEXTURE_2D);

    // set clear color 
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue 
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    // We will always cull back faces for better performance
    // gl.enable(gl.CULL_FACE); 

    // initialize projection matrix 
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    // code 
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    // set the viewport to match 
    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);

    gWidth = canvas.width;
    gHeight = canvas.height;
}

function drawOrg() {
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    var translationMatrix = vec3.create();

    vec3.set(translationMatrix, -1.5, 0.0, -5.0);
    // mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation)); // fun order
    mat4.translate(modelViewMatrix, modelViewMatrix, translationMatrix);
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation));

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    
    // bind with texture
    gl.bindTexture(gl.TEXTURE_2D, texture_stone);
    gl.uniform1i(samplerUniform, 0);
    gl.bindVertexArray(vao_pyramid);
    gl.drawArrays(gl.TRIANGLES, 0, 12);
    gl.bindVertexArray(null);

    modelViewMatrix = mat4.create();
    mat4.identity(modelViewProjectionMatrix);

    mat4.translate(modelViewMatrix, modelViewMatrix, [1.5, 0.0, -5.0]);
    mat4.scale(modelViewMatrix, modelViewMatrix, [0.75, 0.75, 0.75]);
    // var temp = mat4.clone(modelViewMatrix);
    mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation));
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation));
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation));

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    
    // bind with texture
    gl.bindTexture(gl.TEXTURE_2D, texture_kundali);
    gl.uniform1i(samplerUniform, 0);

    gl.bindVertexArray(vao_cube);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

    gl.bindVertexArray(null);
    gl.useProgram(null);

    angleOfRotation = angleOfRotation + 0.50;
    if (angleOfRotation >= 360.0)
        angleOfRotation = 0.0;

    // animation loop 
    requestAnimationFrame(draw, canvas);
}

function uninitialize() {
    // code 
    if (vao_pyramid) {
        gl.deleteVertexArray(vao_pyramid);
        vao_pyramid = null;
    }
    if (vbo_position_pyramid) {
        gl.deleteBuffer(vbo_position_pyramid);
        vbo_position_pyramid = null;
    }
    if (vbo_color_pyramid) {
        gl.deleteBuffer(vbo_color_pyramid);
        vbo_color_pyramid = null;
    }

    if (vao_cube) {
        gl.deleteVertexArray(vao_cube);
        vao_cube = null;
    }
    if (vbo_position_cube) {
        gl.deleteBuffer(vbo_position_cube);
        vbo_position_cube = null;
    }
    if(vbo_texture_cube)
    {
        gl.deleteBuffer(vbo_texture_cube);
        vbo_texture_cube = null;
    }
    if(texture_kundali)
    {
        gl.deleteTexture(texture_kundali);
        texture_kundali = null;
    }
    if(vbo_texture_pyramid)
    {
        gl.deleteBuffer(vbo_texture_pyramid);
        vbo_texture_pyramid = null;
    }
    if(texture_stone)
    {
        gl.deleteTexture(texture_stone);
        texture_stone = null;
    }

    if (shaderProgramObject) {
        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}

function keyDown(event) {
    // code 
    switch (event.keyCode) {
        case 70: // for 'F' or 'f' 
            toggleFullscreen();
            break;

        case 27: // Escape 
            // uninitialize 
            uninitialize();
            // close our application's tab 
            window.close(); // may not work in Firefox but works in Safari and chrome
            break;
    }
}

function mouseDown() {
}

function degToRad(degrees) {
    // code 
    return (degrees * Math.PI / 180);
}


//////////////////////////////////////////////////

function initRenderTexture() {	  
	// https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Using_textures_in_WebGL

    texture_myrender = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture_myrender);

    // // Because images have to be download over the internet
	// // they might take a moment until they are ready.
	// // Until then put a single pixel in the texture so we can
	// // use it immediately. When the image has finished downloading
	// // we'll update the texture with the contents of the image.
	// const level = 0;
	// const internalFormat = gl.RGBA;
	// const width = 1;
	// const height = 1;
	// const border = 0;
	// const srcFormat = gl.RGBA;
	// const srcType = gl.UNSIGNED_BYTE;
	// const pixel = new Uint8Array([0, 0, 255, 255]);  // opaque blue
	// gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
				// width, height, border, srcFormat, srcType,
				// pixel);
	
	// const theImage = new Image();
	// theImage.onload = function() {
		
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    // // gl.texParameteri(gl.TEXTURE_2D, gl.GENERATE_MIPMAP, gl.TRUE); // auto mipmap
	gl.generateMipmap(gl.TEXTURE_2D);
	
    // // gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, theImage); // new Image());
	// Procedural texture refered CheckerBoard
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, TEX_WIDTH, TEX_HEIGHT, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

    // unbind
    gl.bindTexture(gl.TEXTURE_2D, null);
	// }
	
	// // theImage.src = texture_myrender;
}

function initFramebuffer()
{
    rbo = gl.createRenderbuffer();
    gl.bindRenderbuffer(gl.RENDERBUFFER, rbo);

    gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT24, TEX_WIDTH, TEX_HEIGHT); // DEPTH_COMPONENT16 also worked
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);


    fbo = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);

    // attach texture with FBO color attachment
    gl.framebufferTexture2D(gl.FRAMEBUFFER, // target
		gl.COLOR_ATTACHMENT0,
		gl.TEXTURE_2D,
		texture_myrender,
		0); // mipmap level

    // attach renderbuffer to depth attachment
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER,
		gl.DEPTH_ATTACHMENT,
		gl.RENDERBUFFER, // RBO target
		rbo);


    var status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
    if (status != gl.FRAMEBUFFER_COMPLETE)
    {
        console.log("ERROR: something went wrong in FrameBuffer...\n");
    }
}

function drawPyramid() {

    gl.useProgram(shaderProgramObject);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    var translationMatrix = vec3.create();

    vec3.set(translationMatrix, 0.0, 0.0, -4.0);
    // mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation)); // fun order
    mat4.translate(modelViewMatrix, modelViewMatrix, translationMatrix);
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation));

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    // bind with texture
    gl.bindTexture(gl.TEXTURE_2D, texture_stone);
    gl.uniform1i(samplerUniform, 0);
    gl.bindVertexArray(vao_pyramid);
    gl.drawArrays(gl.TRIANGLES, 0, 12);
    gl.bindVertexArray(null);

    gl.useProgram(null);
}

function draw() {
    // code
    gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);

    gl.clearColor(1.0, 0.90, 1.0, 1.0); // blue 
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.viewport(0, 0, TEX_WIDTH, TEX_HEIGHT);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(TEX_WIDTH) / parseFloat(TEX_HEIGHT), 0.1, 100.0);

    drawPyramid();

    gl.bindFramebuffer(gl.FRAMEBUFFER, null); // unbind

    // NOTE: This is important, now set buffered output as current texture
    gl.bindTexture(gl.TEXTURE_2D, texture_myrender);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.bindVertexArray(null);

    gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue 
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.viewport(0, 0, gWidth, gHeight);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(gWidth) / parseFloat(gHeight), 0.1, 100.0);


    gl.useProgram(shaderProgramObject);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    var translationMatrix = vec3.create();

    vec3.set(translationMatrix, 0.0, 0.0, -6.0);
    // mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation)); // fun order
    mat4.translate(modelViewMatrix, modelViewMatrix, translationMatrix);
    mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation));
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation));
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleOfRotation));

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);


    // bind with texture
    gl.bindTexture(gl.TEXTURE_2D, texture_myrender); // NOTE: IMP texture_stone);
    gl.uniform1i(samplerUniform, 0);

    gl.bindVertexArray(vao_cube);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

    gl.bindVertexArray(null);
    gl.useProgram(null);

    angleOfRotation = angleOfRotation + 0.50;
    if (angleOfRotation >= 360.0)
        angleOfRotation = 0.0;

    // animation loop 
    requestAnimationFrame(draw, canvas);
}




/*
gl.FRAMEBUFFER_COMPLETE
36053
gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT
36054
gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT
36055
gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS
36057
status
36057
*/