////////////////////////////////////
// TEST INTER MODULE FUNCTIONS
////////////////////////////////////

function incrCount()
{
	++MyCount;
}

function getCount()
{
	return MyCount;
}

////////////////////////////////////
// LOGIC COMMON FUNCTIONS
////////////////////////////////////

function GLenumToString(shaderType)
{
	switch (shaderType)
	{
	case gl.VERTEX_SHADER:
		return "Vertex Shader";
	case gl.FRAGMENT_SHADER:
		return "Fragment Shader";
	default:
		return "Wrong Shader";
	}
}

function makeShadersAndProgram(vsoSourceCode, fsoSourceCode){
    var vso = makeShader(gl.VERTEX_SHADER, vsoSourceCode);
    var fso = makeShader(gl.FRAGMENT_SHADER, fsoSourceCode);
    debugger
    var pgm = makeShaderProgramAndLink(vso, fso);

    return {spo:pgm.spo, shUniforms:pgm.shUniforms, vso:vso, fso:fso};
}

function makeShader(type, soSourceCode){
    var so = gl.createShader(type);
    gl.shaderSource(so, soSourceCode);
    gl.compileShader(so);
    
    if (gl.getShaderParameter(so, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(so);
        if (error.length > 0) {
            alert(GLenumToString(type) + ": Compilation Error: ");
            alert(error);
            uninitialize();
        }
    }
    debugger
    return so;
}

function makeShaderProgramAndLink(vso, fso){    
    // shader program 
    debugger
    var shUniforms = new ShaderUniforms();
    var spo = gl.createProgram();
    gl.attachShader(spo, vso);
    gl.attachShader(spo, fso);

    // pre-link binding of shader program object with vertex shader attributes 
    gl.bindAttribLocation(spo, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(spo, WebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");

    // linking 
    gl.linkProgram(spo);
    if (!gl.getProgramParameter(spo, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(spo);
        if (error.length > 0) {
            alert("Shader Program: Link Error: ");
            alert(error);
            uninitialize();
        }
    }
    
    // get MVP uniform location 
    shUniforms.mvpUniform = gl.getUniformLocation(spo, "u_mvp_matrix");

    return {spo:spo, shUniforms:shUniforms};
}

///////////////////////////////////////////////
// LOGIC UN/BIND VAO, VBO, FILLDATA FUNCTIONS
///////////////////////////////////////////////

function bindVao(){
    var theVao = gl.createVertexArray();
    gl.bindVertexArray(theVao);

    return theVao;
}

function unbindVao(){
    gl.bindVertexArray(null);
}

function bindVbo(){
    var theVbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, theVbo);

    return theVbo;
}

function unbindVbo(){
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
}

function fillBufferData(data, noOfElementsInOneTuple, isStatic, amc_attribute){
    if (isStatic)
	{
        gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
    }
    else
    {
        gl.bufferData(gl.ARRAY_BUFFER, data, gl.DYNAMIC_DRAW);
    }
    gl.vertexAttribPointer(amc_attribute, noOfElementsInOneTuple, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(amc_attribute);
}

////////////////////////////////////
// LOGIC UNINITIALIZE FUNCTIONS
////////////////////////////////////

function unmarkVaoVboList()
{
	for (var i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		vao_list_scn[i] = -1;
		vbo_position_list_scn[i] = -1;
		vbo_color_list_scn[i] = -1;
		vbo_element_list_scn[i] = -1;
	}
}

function uninitializePP(){
    debugger
    // PP shader dtor
	// Safe Release
	for (var i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		vbo_element_list_scn[i] = uninitVBO(vbo_element_list_scn[i]);
		vbo_color_list_scn[i] = uninitVBO(vbo_color_list_scn[i]);
		vbo_position_list_scn[i] = uninitVBO(vbo_position_list_scn[i]);
		vao_list_scn[i] = uninitVAO(vao_list_scn[i]); // Doubt ind of memory leak
	}
    debugger

	uninitSPO({obj: genericShaderProgramObject_scn});
}

function uninitVAO(theVao){
    if(theVao && theVao != -1)
    {
        gl.deleteVertexArray(theVao); 
        theVao=null; 
    }
    return theVao;
}

function uninitVBO(theVbo){
    if(theVbo && theVbo != -1)
    {
        gl.deleteBuffer(theVbo); 
        theVbo=null; 
    }
    return theVbo;
}

// Texture not used, but future provision
function uninitTexture(theTex){
    if(theTex && theTex != -1)
    {
        gl.deleteTexture(theTex);
        theTex = null;
    }
    return theTex;
}

function uninitSPO(theSpo){
    if(theSpo.obj && theSpo.obj != -1)
    {
        var shaderCount;
		var shaderNo;

        gl.useProgram(theSpo.obj);
        
        shaderCount = gl.getProgramParameter(theSpo.obj, gl.ATTACHED_SHADERS);
        var shaderSequence = gl.getAttachedShaders(theSpo.obj);

        for(shaderNo = 0; shaderNo < shaderCount; shaderNo++)
        {
            gl.detachShader(theSpo.obj, shaderSequence[shaderNo]); 
            gl.deleteShader(shaderSequence[shaderNo]); 
            shaderSequence[shaderNo] = null;
        }

        gl.deleteProgram(theSpo.obj); 
        theSpo.obj=null;

        gl.useProgram(null);
    }
}

////////////////////////////////////
// LOGIC CUSTOM GEOMETRY FUNCTIONS
////////////////////////////////////

function initVAOs(){
    // code
    unmarkVaoVboList();

    // vao handled
    graphObj = {
        geometryListIdx : Geometries.GRAPH
    };

    initVaoGraph(Geometries.GRAPH); // graphObj
}

function initVaoGraph(geometryListIdx) //(geometryObj)
{
    debugger
    var noOfVertices = ((LINES_TO_DRAW + 2) * 4 * NO_OF_COORDS_IN_3D_LINE);
    GeometryList_scn[geometryListIdx].VerticesDynamic = new Float32Array(noOfVertices).fill(0.0);
    debugger
	var i = 0, j = 0;
	var stepRow = 1.0 / LINES_TO_DRAW;
	var stepCol = 1.0 / LINES_TO_DRAW;
	var xcoord = 1.0;
	var ycoord = 1.0;

	for (i = 0; i < LINES_TO_DRAW; i++)
	{
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		xcoord -= stepCol;
	}

	for (i = 0; i < LINES_TO_DRAW; i++)
	{
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		ycoord -= stepRow;
	}

	// X Axis
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	// Y Axis
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

    //// COLORS
    GeometryList_scn[geometryListIdx].ColorsDynamic = new Float32Array(noOfVertices).fill(0.0);
    j = 0;
	for (i = 0; i < LINES_TO_DRAW * 4 * 2; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	}

	// X Axis
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

	// Y Axis
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].VerticesDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].ColorsDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}

//////////////////////////////////