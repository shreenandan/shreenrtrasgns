function draw() {
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(genericShaderProgramObject_scn);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    // vec3.set(modelViewMatrix, [0.0, 0.0, -3.0]);
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);
    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
    gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);

    drawGraph(Geometries.GRAPH);
    
    gl.useProgram(null);

    // animation loop 
    requestAnimationFrame(draw, canvas);
}

function drawGraph(geometryListIdx)
{
	gl.lineWidth(1.0);

	// bind with vao - this will avoid many vbo repetitive calls in display
	gl.bindVertexArray(vao_list_scn[geometryListIdx]);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	gl.drawArrays(gl.LINES, 0, ((LINES_TO_DRAW + 2) * 4) * 2);
    gl.bindVertexArray(null);
}
