// global vars
var canvas = null;
var gl = null; // WebGL context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = // when whole 'WebGL Macros' is 'const', all inside it are automatically const' 
{
    VDG_ATTRIBUTE_VERTEX: 0,
    VDG_ATTRIBUTE_COLOR: 1,
    VDG_ATTRIBUTE_NORMAL: 2,
    VDG_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;
var vertexShaderObject_pv;
var fragmentShaderObject_pv;
var shaderProgramObject_pv;

const DISTANCE_LIGHT = 1000.0;

var light_ambient= new Float32Array([
    0.0, 0.0, 0.0,
    0.0, 0.0, 0.0,
    0.0, 0.0, 0.0]);
var light_diffuse= new Float32Array([
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0]);
var light_specular= new Float32Array([
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0]);
var light_position= new Float32Array([
    0.0, DISTANCE_LIGHT, DISTANCE_LIGHT, 1.0,
	DISTANCE_LIGHT, 0.0, DISTANCE_LIGHT, 1.0,
	DISTANCE_LIGHT, DISTANCE_LIGHT, -DISTANCE_LIGHT, 1.0
    ]); 

var material_ambient= new Float32Array([0.0, 0.0, 0.0]);
var material_diffuse= new Float32Array([1.0, 1.0, 1.0]);
var material_specular= new Float32Array([1.0, 1.0, 1.0]);
var material_shininess= 128.0;

var sphere=null; 

// var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
// var laUniform, ldUniform, lsUniform, lightPositionUniform;
// var kaUniform, kdUniform, ksUniform, materialShininessUniform;
// var LKeyPressedUniform;
class ShaderUniforms
{
    constructor(){}
    modelMatrixUniform;
    viewMatrixUniform;
    projectionMatrixUniform;
    laUniform;
    ldUniform;
    lsUniform;
    lightPositionUniform;
    kaUniform;
    kdUniform;
    ksUniform;
    materialShininessUniform;
    LKeyPressedUniform;
}
var PerVertexUniforms = new ShaderUniforms();
var PerFragmentUniforms = new ShaderUniforms();

var angleRotation=0.0;
var blKeyPressed=false;
var bIsPerVertex=false;

var perspectiveProjectionMatrix;

// To start animation : To have requestAnimation Frame() to be called "cross-browser" compatible 
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimation Frame() to be called "cross browser" compatible 
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main() {
    // get <canvas> element 
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // print canvas width and height on console 
    console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);

    // register keyboard's keydown event handler 
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL 
    init();

    // start drawing here as warming-up 
    resize();
    draw();
}

function toggleFullscreen() {
    // code 
    var fullscreen_element = document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    // if not fullscreen 
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen = false;
    }
}

function init() {
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");

    if (gl == null) // failed to get context 
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader 
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "\n" +
        "in vec4 vPosition;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"in vec3 vNormal;" +
		"uniform int u_isLKeyPressed;" +
		"\n" +
		"uniform vec4 u_light_position[3];" +
		"\n" +
		"out vec3 tNorm[3];" +
		"out vec3 light_direction[3];" +
		"out vec3 viewer_vector[3];" +
		"\n" +
		"void main(void)" +
		"{" +
		"	if(u_isLKeyPressed == 1)" +
		"	{" +
		"		for(int loopIndex = 0; loopIndex < 3; loopIndex++)" +
		"		{" +
		"			vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
		"			tNorm[loopIndex] = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
		"			light_direction[loopIndex] = vec3(u_light_position[loopIndex] - eye_coordinates);" +
		"			viewer_vector[loopIndex] = vec3(-eye_coordinates).xyz;" +
		"		}" +
		"	}" +
		"\n" +
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
		"}";
        
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "\n" +
        "uniform int u_isLKeyPressed;" +
		"out vec4 FragColor;" +
		"\n" +
		"in vec3 tNorm[3];" +
		"in vec3 light_direction[3];" +
		"in vec3 viewer_vector[3];" +
		"\n" +
		"uniform vec3 u_la[3];" +
		"uniform vec3 u_ld[3];" +
		"uniform vec3 u_ls[3];" +
		"\n" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_shinyness;" +
		"\n" +
		"void main(void)" +
		"{" +
		"	vec3 phong_ads_light;" +
		"\n" +
		"	if(u_isLKeyPressed == 1)" +
		"	{" +
		"		phong_ads_light[0] = 0.0;" +
		"		phong_ads_light[1] = 0.0;" +
		"		phong_ads_light[2] = 0.0;" +
		"\n" +
		"		for(int loopIndex = 0; loopIndex < 3; loopIndex++)" +
		"		{" +
		"			vec3 normalized_tNorm = normalize(tNorm[loopIndex]);" +
		"			vec3 normalized_light_direction = normalize(light_direction[loopIndex]);" +
		"			vec3 normalized_viewer_vector = normalize(viewer_vector[loopIndex]);" +
		"\n" +
		"			vec3 reflection_vector = reflect(-normalized_light_direction, normalized_tNorm);" +
		"			float tnDotLightDir = max(dot(normalized_light_direction, normalized_tNorm), 0.0);" +
		"\n" +
		"			vec3 ambient = u_la[loopIndex] * u_ka;" +
		"			vec3 diffuse = u_ld[loopIndex] * u_kd * tnDotLightDir;" +
		"			vec3 specular = u_ls[loopIndex] * u_ks * pow( max( dot(reflection_vector, normalized_viewer_vector), 0.0f), u_shinyness);" +
		"			phong_ads_light = phong_ads_light + ambient + diffuse + specular;" +
		"		}" +
		"	}" +
		"	else" +
		"	{" +
		"		phong_ads_light = vec3(1.0, 1.0, 1.0);" +
		"	}" +
		"\n" +
		"	FragColor = vec4(phong_ads_light, 1.0);" +
		"}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // pre-link binding of shader program object with vertex shader attributes 
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

    // linking 
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get MVP uniform location 
    PerFragmentUniforms.modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
    PerFragmentUniforms.viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
    PerFragmentUniforms.projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");

    PerFragmentUniforms.LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_isLKeyPressed");

    PerFragmentUniforms.laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
    PerFragmentUniforms.ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
    PerFragmentUniforms.lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
    PerFragmentUniforms.lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");

    PerFragmentUniforms.kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
    PerFragmentUniforms.kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    PerFragmentUniforms.ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
    PerFragmentUniforms.materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_shinyness");

    initPerVertex();

    // *** vertices, colors, shader attribs, vbo, vao initializations *** 
    sphere = new Mesh();
    makeSphere(sphere, 1.0, 40, 40);

    // set clear color 
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE); 

    // initialize projection matrix 
    perspectiveProjectionMatrix = mat4.create();
}

function initPerVertex() {
    // code 
    // vertex shader 
    var vertexShaderSourceCode_pv =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "\n" +
        "in vec4 vPosition;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"in vec3 vNormal;" +
		"uniform int u_isLKeyPressed;" +
		"\n" +
		"uniform vec3 u_la[3];" +
		"uniform vec3 u_ld[3];" +
		"uniform vec3 u_ls[3];" +
		"\n" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_shinyness;" +
		"\n" +
		"uniform vec4 u_light_position[3];" +
		"out vec3 phong_ads_light;" +
		"\n" +
		"void main(void)" +
		"{" +
		"	if(u_isLKeyPressed == 1)" +
		"	{" +
		"		phong_ads_light[0] = 0.0;" +
		"		phong_ads_light[1] = 0.0;" +
		"		phong_ads_light[2] = 0.0;" +
				"\n" +
		"		for(int loopIndex = 0; loopIndex < 3; loopIndex++)" +
		"		{" +
		"			vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
		"			vec3 tNorm = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
		"			vec3 light_direction = normalize( vec3(u_light_position[loopIndex] - eye_coordinates) );" +
		"\n" +
		"			float tnDotLightDir = max(dot(light_direction, tNorm), 0.0);" +
		"			vec3 reflection_vector = reflect(-light_direction, tNorm);" +
		"			vec3 viewer_vector = normalize( vec3(-eye_coordinates).xyz );" +
		"\n" +
		"			vec3 ambient = u_la[loopIndex] * u_ka;" +
		"			vec3 diffuse = u_ld[loopIndex] * u_kd * tnDotLightDir;" +
		"			vec3 specular = u_ls[loopIndex] * u_ks * pow( max( dot(reflection_vector, viewer_vector), 0.0f), u_shinyness);" +
		"\n" +
		"			phong_ads_light = phong_ads_light + ambient + diffuse + specular;" +
		"		}" +
		"	}" +
		"	else" +
		"	{" +
		"		phong_ads_light = vec3(1.0, 1.0, 1.0);" +
		"	}" +
		"\n" +
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
		"}";

		//"		vec3 specular = u_ls * u_ks * pow( max( dot(reflection_vector, viewer_vector), 0.0f), u_shinyness);" +
		//"		vec3 specular = u_ls * u_ks * pow( max( normalize(dot(reflection_vector, viewer_vector)), 0.0f), u_shinyness);" +
		//"		vec3 source = vec3(u_light_position - eye_coordinates).xyz;" +
        
    vertexShaderObject_pv = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_pv, vertexShaderSourceCode_pv);
    gl.compileShader(vertexShaderObject_pv);

    if(gl.getShaderParameter(vertexShaderObject_pv,gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject_pv);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 
    var fragmentShaderSourceCode_pv =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "\n" +
		"uniform int u_isLKeyPressed;" +
        "in vec3 phong_ads_light;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"	FragColor = vec4(phong_ads_light, 1.0);" +
		"}";

    fragmentShaderObject_pv = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_pv, fragmentShaderSourceCode_pv);
    gl.compileShader(fragmentShaderObject_pv);

    if (gl.getShaderParameter(fragmentShaderObject_pv, gl.COMPILE_STATUS) == false) {
        error = gl.getShaderInfoLog(fragmentShaderObject_pv);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObject_pv = gl.createProgram();
    gl.attachShader(shaderProgramObject_pv, vertexShaderObject_pv);
    gl.attachShader(shaderProgramObject_pv, fragmentShaderObject_pv);

    // pre-link binding of shader program object with vertex shader attributes 
    gl.bindAttribLocation(shaderProgramObject_pv, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_pv, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

    // linking 
    gl.linkProgram(shaderProgramObject_pv);
    if (!gl.getProgramParameter(shaderProgramObject_pv, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject_pv);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get MVP uniform location 
    PerVertexUniforms.modelMatrixUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_model_matrix");
    PerVertexUniforms.viewMatrixUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_view_matrix");
    PerVertexUniforms.projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_projection_matrix");

    PerVertexUniforms.LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_isLKeyPressed");

    PerVertexUniforms.laUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_la");
    PerVertexUniforms.ldUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_ld");
    PerVertexUniforms.lsUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_ls");
    PerVertexUniforms.lightPositionUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_light_position");

    PerVertexUniforms.kaUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_ka");
    PerVertexUniforms.kdUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_kd");
    PerVertexUniforms.ksUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_ks");
    PerVertexUniforms.materialShininessUniform = gl.getUniformLocation(shaderProgramObject_pv, "u_shinyness");
}

function resize() {
    // code 
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if(canvas.height == 0)
    {
        canvas.height = 1;
    }
    // set the viewport to match 
    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw() {
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    var selectedUniforms;
    
    if(bIsPerVertex == false)
    {
        gl.useProgram(shaderProgramObject);
        selectedUniforms = PerFragmentUniforms;
    }
    else
    {
        gl.useProgram(shaderProgramObject_pv);
        selectedUniforms = PerVertexUniforms;
    }

    if(blKeyPressed==true)
    {
        gl.uniform1i(selectedUniforms.LKeyPressedUniform, 1);

        // setting light properties
        gl.uniform3fv(selectedUniforms.laUniform, light_ambient); // diffuse intensity of light 
        gl.uniform3fv(selectedUniforms.ldUniform, light_diffuse);
        gl.uniform3fv(selectedUniforms.lsUniform, light_specular);
        
        // setting material properties 
        gl.uniform3fv(selectedUniforms.kaUniform, material_ambient); // diffuse intensity of light 
        gl.uniform3fv(selectedUniforms.kdUniform, material_diffuse);
        gl.uniform3fv(selectedUniforms.ksUniform, material_specular);
        gl.uniform1f(selectedUniforms.materialShininessUniform, material_shininess); // diffuse reflectivity of material 
        
		light_position[1] = DISTANCE_LIGHT * Math.cos(degToRad(angleRotation));
		light_position[4] = DISTANCE_LIGHT * Math.cos(degToRad(angleRotation));
		light_position[8] = DISTANCE_LIGHT * Math.cos(degToRad(angleRotation));

		light_position[2] = DISTANCE_LIGHT * Math.sin(degToRad(angleRotation));
		light_position[6] = DISTANCE_LIGHT * Math.sin(degToRad(angleRotation));
		light_position[9] = DISTANCE_LIGHT * Math.sin(degToRad(angleRotation));
        gl.uniform4fv(selectedUniforms.lightPositionUniform, light_position); // light position
        
    }
    else
    {
        gl.uniform1i(selectedUniforms.LKeyPressedUniform, 0);
    }

    // sphere = new Mesh();
    // makeSphere(sphere, 2.0, 30, 30);

    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var translationMatrix = vec3.create();

    vec3.set(translationMatrix, 0, 0, -3.0);
    mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -4.0]); // translationMatrix);

    // mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleRotation));
    //mat4.rotateY(modelMatrix, modelMatrix, degToRad(angleRotation));
    // mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleRotation));

    gl.uniformMatrix4fv(selectedUniforms.modelMatrixUniform, false, modelMatrix);
    gl.uniformMatrix4fv(selectedUniforms.viewMatrixUniform, false, viewMatrix);
    gl.uniformMatrix4fv(selectedUniforms.projectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphere.draw();

    gl.useProgram(null);

    update();

    // animation loop 
    requestAnimationFrame(draw, canvas);
}

function update(){
    angleRotation = angleRotation + 0.50;
    if(angleRotation >= 360.0)
        angleRotation = 0.0;
}

function uninitialize() 
{ 
    // code 
    if(sphere)
    {
        sphere.deallocate(); 
        sphere=null; 
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject, fragmentShaderObject); 
            gl.deleteShader (fragmentShaderObject); 
            fragmentShaderObject=null;
        }
            
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject, vertexShaderObject); 
            gl.deleteShader(vertexShaderObject); 
            vertexShaderObject=null;
        }
        gl.deleteProgram(shaderProgramObject); 
        shaderProgramObject=null;
    }
    
    if(shaderProgramObject_pv)
    {
        if(fragmentShaderObject_pv)
        {
            gl.detachShader(shaderProgramObject_pv, fragmentShaderObject_pv); 
            gl.deleteShader (fragmentShaderObject_pv); 
            fragmentShaderObject_pv=null;
        }
            
        if(vertexShaderObject_pv)
        {
            gl.detachShader(shaderProgramObject_pv, vertexShaderObject_pv); 
            gl.deleteShader(vertexShaderObject_pv); 
            vertexShaderObject_pv=null;
        }
        gl.deleteProgram(shaderProgramObject_pv); 
        shaderProgramObject_pv=null;
    }
}

function keyDown(event) {
    // code 
    switch (event.keyCode) {
        //27: // Why not Escape?
        /*Request for fullscreen was denied because Element.requestFullscreen() 
        was not called from inside a short running user-generated event handler. */ 
        case 72: // H //27: // Escape 
            toggleFullscreen();
            break;

        case 81: // Q 
            // uninitialize 
            uninitialize();
            // close our application's tab 
            window.close(); // may not work in Firefox but works in Safari and chrome
			window.location.replace("about:blank"); // removes history from session, so no navigation on back button
            break;

        case 76: // for 'l' or 'l' 
            if(blKeyPressed==false) 
                blKeyPressed=true; 
            else 
                blKeyPressed=false; 
            break;
            
        case 86: // for 'v' 
            bIsPerVertex=true;
            // alert(bIsPerVertex);
            break;
            
        case 70: // for 'f'
            bIsPerVertex=false;
            // alert(bIsPerVertex);
            break;
    }
}

function mouseDown() {
}

function degToRad(degrees){
    // code 
    return(degrees * Math.PI / 180);
} 



// 17:09:37.022 Navigated to file:///D:/Learning/OGL/WebGL/05-Triangle-Ortho/05-Triangle-Ortho.html
// 17:09:36.970 Obtaining Canvas Succeeded
// 05-Triangle-Ortho.js:47:9
// 17:09:36.971 Canvas Width : 800 And Canvas Height : 600
// 05-Triangle-Ortho.js:53:5
// 17:10:01.233 Request for fullscreen was denied because Element.requestFullscreen() was not called from inside a short running user-generated event handler.
// 05-Triangle-Ortho.js:81:12
