// global vars
var canvas = null;
var gl = null; // WebGL context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = // when whole 'WebGL Macros' is 'const', all inside it are automatically const' 
{
    VDG_ATTRIBUTE_VERTEX: 0,
    VDG_ATTRIBUTE_COLOR: 1,
    VDG_ATTRIBUTE_NORMAL: 2,
    VDG_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var light_ambient= new Float32Array([0.0,0.0,0.0]);
var light_diffuse= new Float32Array([1.0,1.0,1.0]);
var light_specular= new Float32Array([1.0,1.0,1.0]);
var light_position= new Float32Array([100.0,100.0,100.0,1.0]); 

var material_ambient= new Float32Array([0.0,0.0,0.0]);
var material_diffuse= new Float32Array([1.0,1.0,1.0]);
var material_specular= new Float32Array([1.0,1.0,1.0]);
var material_shininess= 50.0;

var sphere=null; 

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform, ldUniform, lsUniform, lightPositionUniform;
var kaUniform, kdUniform, ksUniform, materialShininessUniform;
var LKeyPressedUniform;

var angleRotation=0.0;
var blKeyPressed=false; 

var perspectiveProjectionMatrix;

// To start animation : To have requestAnimation Frame() to be called "cross-browser" compatible 
var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

// To stop animation : To have cancelAnimation Frame() to be called "cross browser" compatible 
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main() {
    // get <canvas> element 
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // print canvas width and height on console 
    console.log("Canvas Width : " + canvas.width + " And Canvas Height : " + canvas.height);

    // register keyboard's keydown event handler 
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL 
    init();

    // start drawing here as warming-up 
    resize();
    draw();
}

function toggleFullscreen() {
    // code 
    var fullscreen_element = document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    // if not fullscreen 
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen = false;
    }
}

function init() {
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");

    if (gl == null) // failed to get context 
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader 
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "\n" +
        "in vec4 vPosition;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"in vec3 vNormal;" +
		"uniform int u_isLKeyPressed;" +
		"\n" +
		"uniform vec4 u_light_position;" +
		"\n" +
		"out vec3 tNorm;" +
		"out vec3 light_direction;" +
		"out vec3 viewer_vector;" +
		"\n" +
		"void main(void)" +
		"{" +
		"	if(u_isLKeyPressed == 1)" +
		"	{" +
		"		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
		"		tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
		"		light_direction = vec3(u_light_position - eye_coordinates);" +
		"		viewer_vector = vec3(-eye_coordinates).xyz;" +
		"	}" +
		"\n" +
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
		"}";

		//"		vec3 specular = u_ls * u_ks * pow( max( dot(reflection_vector, viewer_vector), 0.0f), u_shinyness);" +
		//"		vec3 specular = u_ls * u_ks * pow( max( normalize(dot(reflection_vector, viewer_vector)), 0.0f), u_shinyness);" +
		//"		vec3 source = vec3(u_light_position - eye_coordinates).xyz;" +
        
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "\n" +
        "in vec3 phong_ads_light;" +
		"uniform int u_isLKeyPressed;" +
		"out vec4 FragColor;" +
		"\n" +
		"in vec3 tNorm;" +
		"in vec3 light_direction;" +
		"in vec3 viewer_vector;" +
		"\n" +
		"uniform vec3 u_la;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_ls;" +
		"\n" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_shinyness;" +
		"\n" +
		"void main(void)" +
		"{" +
		"	vec3 phong_ads_light;" +
		"	if(u_isLKeyPressed == 1)" +
		"	{" +
		"		vec3 normalized_tNorm = normalize(tNorm);" +
		"		vec3 normalized_light_direction = normalize(light_direction);" +
		"		vec3 normalized_viewer_vector = normalize(viewer_vector);" +
		"\n" +
		"		vec3 reflection_vector = reflect(-normalized_light_direction, normalized_tNorm);" +
		"		float tnDotLightDir = max(dot(normalized_light_direction, normalized_tNorm), 0.0);" +
		"\n" +
		"		vec3 ambient = u_la * u_ka;" +
		"		vec3 diffuse = u_ld * u_kd * tnDotLightDir;" +
		"		vec3 specular = u_ls * u_ks * pow( max( dot(reflection_vector, normalized_viewer_vector), 0.0f), u_shinyness);" +
		"		phong_ads_light = ambient + diffuse + specular;" +
		"	}" +
		"	else" +
		"	{" +
		"		phong_ads_light = vec3(1.0, 1.0, 1.0);" +
		"	}" +
		"\n" +
		"	FragColor = vec4(phong_ads_light, 1.0);" +
		"}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // pre-link binding of shader program object with vertex shader attributes 
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

    // linking 
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get MVP uniform location 
    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");

    LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_isLKeyPressed");

    laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
    ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
    lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");

    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_shinyness");

    // *** vertices, colors, shader attribs, vbo, vao initializations *** 
    sphere = new Mesh();
    makeSphere(sphere, 1.0, 40, 40);

    // set clear color 
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    // We will always cull back faces for better performance
    gl.enable(gl.CULL_FACE); 

    // initialize projection matrix 
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    // code 
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if(canvas.height == 0)
    {
        canvas.height = 1;
    }
    // set the viewport to match 
    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

function draw() {
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    if(blKeyPressed==true)
    {
        gl.uniform1i(LKeyPressedUniform, 1);

        // setting light properties
        gl.uniform3fv(laUniform, light_ambient); // diffuse intensity of light 
        gl.uniform3fv(ldUniform, light_diffuse);
        gl.uniform3fv(lsUniform, light_specular);
        gl.uniform4fv(lightPositionUniform, light_position); // light position
        
        // setting material properties 
        gl.uniform3fv(kaUniform, material_ambient); // diffuse intensity of light 
        gl.uniform3fv(kdUniform, material_diffuse);
        gl.uniform3fv(ksUniform, material_specular);
        gl.uniform1f(materialShininessUniform, material_shininess); // diffuse reflectivity of material 
        
    }
    else
    {
        gl.uniform1i(LKeyPressedUniform, 0);
    }

    // sphere = new Mesh();
    // makeSphere(sphere, 2.0, 30, 30);

    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var translationMatrix = vec3.create();

    vec3.set(translationMatrix, 0, 0, -3.0);
    mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -5.0]); // translationMatrix);

    // mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleRotation));
    // mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleRotation));
    // mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleRotation));

    gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphere.draw();

    gl.useProgram(null);

    // angleRotation = angleRotation + 0.50;
    // if(angleRotation >= 360.0)
    //     angleRotation = 0.0;
    
    // animation loop 
    requestAnimationFrame(draw, canvas);
}

function uninitialize() 
{ 
    // code 
    if(sphere)
    {
        sphere.deallocate(); 
        sphere=null; 
    }

    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject, fragmentShaderObject); 
            gl.deleteShader (fragmentShaderObject); 
            fragmentShaderObject=null;
        }
            
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject, vertexShaderObject); 
            gl.deleteShader(vertexShaderObject); 
            vertexShaderObject=null;
        }
        gl.deleteProgram(shaderProgramObject); 
        shaderProgramObject=null;
    }
}

function keyDown(event) {
    // code 
    switch (event.keyCode) {
        case 70: // for 'F' or 'f' 
            toggleFullscreen();
            break;

        case 27: // Escape 
            // uninitialize 
            uninitialize();
            // close our application's tab 
            window.close(); // may not work in Firefox but works in Safari and chrome
			window.location.replace("about:blank"); // removes history from session, so no navigation on back button
            break;

        case 76: // for 'l' or 'l' 
            if(blKeyPressed==false) 
                blKeyPressed=true; 
            else 
                blKeyPressed=false; 
            break;
    }
}

function mouseDown() {
}

function degToRad(degrees){
    // code 
    return(degrees * Math.PI / 180);
} 



// 17:09:37.022 Navigated to file:///D:/Learning/OGL/WebGL/05-Triangle-Ortho/05-Triangle-Ortho.html
// 17:09:36.970 Obtaining Canvas Succeeded
// 05-Triangle-Ortho.js:47:9
// 17:09:36.971 Canvas Width : 800 And Canvas Height : 600
// 05-Triangle-Ortho.js:53:5
// 17:10:01.233 Request for fullscreen was denied because Element.requestFullscreen() was not called from inside a short running user-generated event handler.
// 05-Triangle-Ortho.js:81:12
