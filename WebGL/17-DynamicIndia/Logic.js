////////////////////////////////////
// TEST INTER MODULE FUNCTIONS
////////////////////////////////////

function incrCount()
{
	++MyCount;
}

function getCount()
{
	return MyCount;
}

////////////////////////////////////
// LOGIC COMMON FUNCTIONS
////////////////////////////////////

function GLenumToString(shaderType)
{
	switch (shaderType)
	{
	case gl.VERTEX_SHADER:
		return "Vertex Shader";
	case gl.FRAGMENT_SHADER:
		return "Fragment Shader";
	default:
		return "Wrong Shader";
	}
}

function makeShadersAndProgram(vsoSourceCode, fsoSourceCode){
    var vso = makeShader(gl.VERTEX_SHADER, vsoSourceCode);
    var fso = makeShader(gl.FRAGMENT_SHADER, fsoSourceCode);
    // debugger
    var pgm = makeShaderProgramAndLink(vso, fso);

    return {spo:pgm.spo, shUniforms:pgm.shUniforms, vso:vso, fso:fso};
}

function makeShader(type, soSourceCode){
    var so = gl.createShader(type);
    gl.shaderSource(so, soSourceCode);
    gl.compileShader(so);
    
    if (gl.getShaderParameter(so, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(so);
        if (error.length > 0) {
            alert(GLenumToString(type) + ": Compilation Error: ");
            alert(error);
            uninitialize();
        }
    }
    // debugger
    return so;
}

function makeShaderProgramAndLink(vso, fso){    
    // shader program 
    // debugger
    var shUniforms = new ShaderUniforms();
    var spo = gl.createProgram();
    gl.attachShader(spo, vso);
    gl.attachShader(spo, fso);

    // pre-link binding of shader program object with vertex shader attributes 
    gl.bindAttribLocation(spo, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(spo, WebGLMacros.VDG_ATTRIBUTE_COLOR, "vColor");

    // linking 
    gl.linkProgram(spo);
    if (!gl.getProgramParameter(spo, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(spo);
        if (error.length > 0) {
            alert("Shader Program: Link Error: ");
            alert(error);
            uninitialize();
        }
    }
    
    // get MVP uniform location 
    shUniforms.mvpUniform = gl.getUniformLocation(spo, "u_mvp_matrix");

    return {spo:spo, shUniforms:shUniforms};
}

///////////////////////////////////////////////
// LOGIC UN/BIND VAO, VBO, FILLDATA FUNCTIONS
///////////////////////////////////////////////

function bindVao(){
    var theVao = gl.createVertexArray();
    gl.bindVertexArray(theVao);

    return theVao;
}

function unbindVao(){
    gl.bindVertexArray(null);
}

function bindVbo(){
    var theVbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, theVbo);

    return theVbo;
}

function unbindVbo(){
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
}

function fillBufferData(data, noOfElementsInOneTuple, isStatic, amc_attribute){
    if (isStatic)
	{
        gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
    }
    else
    {
        gl.bufferData(gl.ARRAY_BUFFER, data, gl.DYNAMIC_DRAW);
    }
    gl.vertexAttribPointer(amc_attribute, noOfElementsInOneTuple, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(amc_attribute);
}

////////////////////////////////////
// LOGIC UNINITIALIZE FUNCTIONS
////////////////////////////////////

function unmarkVaoVboList()
{
	for (var i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		vao_list_scn[i] = -1;
		vbo_position_list_scn[i] = -1;
		vbo_color_list_scn[i] = -1;
		vbo_element_list_scn[i] = -1;
	}
}

function uninitializePP(){
    // debugger
    // PP shader dtor
	// Safe Release
	for (var i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		vbo_element_list_scn[i] = uninitVBO(vbo_element_list_scn[i]);
		vbo_color_list_scn[i] = uninitVBO(vbo_color_list_scn[i]);
		vbo_position_list_scn[i] = uninitVBO(vbo_position_list_scn[i]);
		vao_list_scn[i] = uninitVAO(vao_list_scn[i]); // Doubt ind of memory leak
	}
    // debugger

	uninitSPO({obj: genericShaderProgramObject_scn});
}

function uninitVAO(theVao){
    if(theVao && theVao != -1)
    {
        gl.deleteVertexArray(theVao); 
        theVao=null; 
    }
    return theVao;
}

function uninitVBO(theVbo){
    if(theVbo && theVbo != -1)
    {
        gl.deleteBuffer(theVbo); 
        theVbo=null; 
    }
    return theVbo;
}

// Texture not used, but future provision
function uninitTexture(theTex){
    if(theTex && theTex != -1)
    {
        gl.deleteTexture(theTex);
        theTex = null;
    }
    return theTex;
}

function uninitSPO(theSpo){
    if(theSpo.obj && theSpo.obj != -1)
    {
        var shaderCount;
		var shaderNo;

        gl.useProgram(theSpo.obj);
        
        shaderCount = gl.getProgramParameter(theSpo.obj, gl.ATTACHED_SHADERS);
        var shaderSequence = gl.getAttachedShaders(theSpo.obj);

        for(shaderNo = 0; shaderNo < shaderCount; shaderNo++)
        {
            gl.detachShader(theSpo.obj, shaderSequence[shaderNo]); 
            gl.deleteShader(shaderSequence[shaderNo]); 
            shaderSequence[shaderNo] = null;
        }

        gl.deleteProgram(theSpo.obj); 
        theSpo.obj=null;

        gl.useProgram(null);
    }
}

////////////////////////////////////
// LOGIC CUSTOM GEOMETRY FUNCTIONS
////////////////////////////////////

function initVAOs(){
    // code
    initGeometryList();
    unmarkVaoVboList();

    // vao handled
    graphObj = {
        geometryListIdx : Geometries.GRAPH
    };

    // initVaoGraph(Geometries.GRAPH); // graphObj
    // initVaoTriangle(Geometries.TRIANGLE);
    // initVaoIncircle(Geometries.INCIRCLE);
    // initVaoStick(Geometries.STICK);
    initVaoSI();
	console.log(gl.getParameter(gl.ALIASED_LINE_WIDTH_RANGE));
	
	initVaoI1(Geometries.LETTER_I1);
	initVaoN(Geometries.LETTER_N);
	initVaoA(Geometries.LETTER_A);
	initVaoI2(Geometries.LETTER_I2);
	initVaoD(Geometries.LETTER_D);

	initVaoPlane(Geometries.PLANE_A);
	initVaoPlane(Geometries.PLANE_B);
	initVaoPlane(Geometries.PLANE_C);

	initVaoTricolor(Geometries.LETTER_A_TRICOLOR);
	
	GeometryList_scn[Geometries.PLANE_A].InitialX = 0.0;
	GeometryList_scn[Geometries.PLANE_A].MovingX = GeometryList_scn[Geometries.PLANE_A].InitialX;

	GeometryList_scn[Geometries.PLANE_C].InitialX = 0.0;
	GeometryList_scn[Geometries.PLANE_C].MovingX = GeometryList_scn[Geometries.PLANE_C].InitialX;

	GeometryList_scn[Geometries.PLANE_B].InitialX = -2.88;
	GeometryList_scn[Geometries.PLANE_B].MovingX = GeometryList_scn[Geometries.PLANE_B].InitialX;
	GeometryList_scn[Geometries.PLANE_B].StopMarker = 2.56;
	GeometryList_scn[Geometries.PLANE_B].TranslationStep = 0.000098;
}

function initVaoGraph(geometryListIdx) //(geometryObj)
{
    // debugger
    var noOfVertices = ((LINES_TO_DRAW + 2) * 4 * NO_OF_COORDS_IN_3D_LINE);
    GeometryList_scn[geometryListIdx].VerticesDynamic = new Float32Array(noOfVertices).fill(0.0);
    // debugger
	var i = 0, j = 0;
	var stepRow = 1.0 / LINES_TO_DRAW;
	var stepCol = 1.0 / LINES_TO_DRAW;
	var xcoord = 1.0;
	var ycoord = 1.0;

	for (i = 0; i < LINES_TO_DRAW; i++)
	{
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -xcoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		xcoord -= stepCol;
	}

	for (i = 0; i < LINES_TO_DRAW; i++)
	{
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -ycoord;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

		ycoord -= stepRow;
	}

	// X Axis
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	// Y Axis
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -1.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

    //// COLORS
    GeometryList_scn[geometryListIdx].ColorsDynamic = new Float32Array(noOfVertices).fill(0.0);
    j = 0;
	for (i = 0; i < LINES_TO_DRAW * 4 * 2; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
		GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	}

	// X Axis
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

	// Y Axis
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 1.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].ColorsDynamic[j++] = 0.0;

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].VerticesDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].ColorsDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}

function initVaoTriangle(geometryListIdx)
{
    var noOfVertices = NO_OF_VERTEX_TRIANGLE * NO_OF_COORDS_IN_3D_VERTEX;
    GeometryList_scn[geometryListIdx].VerticesDynamic = new Float32Array(noOfVertices).fill(0.0);

	var j = 0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = -gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].VerticesDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.vertexAttrib3f(WebGLMacros.VDG_ATTRIBUTE_COLOR, 1.0, 1.0, 0.0);
    unbindVbo();

    unbindVao();

	GeometryList_scn[geometryListIdx].MovingX = 1.0;
	GeometryList_scn[geometryListIdx].MovingY = -0.50;
}

function initVaoIncircle(geometryListIdx)
{
	// code
	var sideA = 0.0, sideB = 0.0, sideC = 0.0;
	var perimeter = 0.0, halfPerimeter = 0.0;
	var incircleOx = 0.0, incircleOy = 0.0;
    var radius = 0.0;
    
	GeometryList_scn[geometryListIdx].TranslationStep = 0.1;
	GeometryList_scn[geometryListIdx].NumVertices = ((2.0 * Math.PI) / GeometryList_scn[geometryListIdx].TranslationStep) + 1;
	    
    var noOfVertices = GeometryList_scn[geometryListIdx].NumVertices * NO_OF_COORDS_IN_3D_VERTEX;
    GeometryList_scn[geometryListIdx].VerticesDynamic = new Float32Array(noOfVertices).fill(0.0);

    var ret_perimeter = getSidesAndPerimeterFromTriangle(gfMagnitude, sideA, sideB, sideC);
    perimeter = ret_perimeter.perimeter;
    sideA = ret_perimeter.sideA;
    sideB = ret_perimeter.sideB;
    sideC = ret_perimeter.sideC;
	halfPerimeter = perimeter / 2.0;

    // debugger
	var ret_incircle = getIncircleOrigin(perimeter, sideA, -gfMagnitude, -gfMagnitude, sideB, gfMagnitude, -gfMagnitude, sideC, 0, gfMagnitude, incircleOx, incircleOy);
    incircleOx = ret_incircle.incircleOx;
    // debugger
    incircleOy = ret_incircle.incircleOy;
    radius = getIncircleRadius(halfPerimeter, sideA, sideB, sideC);

	GeometryList_scn[geometryListIdx].MovingX = incircleOx;
	GeometryList_scn[geometryListIdx].MovingY = incircleOy;

	var j = 0;

	for (var angle = 0.0; angle <= 2.0 * Math.PI; angle += GeometryList_scn[geometryListIdx].TranslationStep)
	{
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = Math.cos(angle) * radius;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = Math.sin(angle) * radius;
		GeometryList_scn[geometryListIdx].VerticesDynamic[j++] = 0.0;
	}

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].VerticesDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.vertexAttrib3f(WebGLMacros.VDG_ATTRIBUTE_COLOR, 1.0, 1.0, 0.0);
    unbindVbo();

    unbindVao();
    
    // console.log('after inner');
    // console.log(incircleOx);
    // console.log(incircleOy);
    // console.log(GeometryList_scn[Geometries.INCIRCLE].MovingY);
    // console.log(GeometryList_scn[geometryListIdx].MovingY);
    // // console.log(outerCircleOx);
    // // console.log(outerCircleOy);
}

function getIncircleRadius(halfPerimeter, sideA, sideB, sideC)
{
	var area = 0.0;
	area = (Math.sqrt(halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC)));
	return (area / halfPerimeter);
}

function getIncircleOrigin(perimeter, sideA, Ax, Ay, sideB, Bx, By, sideC, Cx, Cy, incircleOx, incircleOy)
{
	incircleOx = ((sideA * Ax) + (sideB * Bx) + (sideC * Cx)) / perimeter;
    incircleOy = ((sideA * Ay) + (sideB * By) + (sideC * Cy)) / perimeter;
    
    var ret_obj = 
    {
        incircleOx : incircleOx,
        incircleOy : incircleOy
    };
    // debugger

    return ret_obj;
}

function getSidesAndPerimeterFromTriangle(size, sideA, sideB, sideC)
{
	sideA = Math.sqrt(Math.pow((0.0 - size), 2) + Math.pow((size - (-size)), 2));
	sideB = Math.sqrt(Math.pow((0.0 - (-size)), 2) + Math.pow((size - (-size)), 2));
    sideC = Math.sqrt(Math.pow(((-size) - size), 2) + Math.pow(((-size) - (-size)), 2)); // Apex
    
    var ret_obj = 
    {
        perimeter : (sideA + sideB + sideC),
        sideA : sideA,
        sideB : sideB,
        sideC : sideC,
    };

    return ret_obj;
}

function initVaoStick(geometryListIdx)
{
    var noOfVertices = NO_OF_VERTEX_TRIANGLE * NO_OF_COORDS_IN_3D_VERTEX;
    GeometryList_scn[geometryListIdx].VerticesDynamic = new Float32Array(noOfVertices).fill(0.0);

    // midpoint = (x1+x2)/2, (y1+y2)/2
	// midpoint of side opposite to apex
	var midPointX = (-gfMagnitude + gfMagnitude) / 2.0;
	var midPointY = (-gfMagnitude + -gfMagnitude) / 2.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.START_X] = 0.0;
	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.START_Y] = gfMagnitude;
	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.START_Z] = 0.0;

	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.END_X] = midPointX;
	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.END_Y] = midPointY;
	GeometryList_scn[geometryListIdx].VerticesDynamic[LineVerticesIndex.END_Z] = 0.0;
    // console.log(GeometryList_scn[geometryListIdx].Vertices.length);
    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(GeometryList_scn[geometryListIdx].VerticesDynamic, 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.vertexAttrib3f(WebGLMacros.VDG_ATTRIBUTE_COLOR, 1.0, 1.0, 0.0);
    unbindVbo();

    unbindVao();
    
	GeometryList_scn[geometryListIdx].MovingY = 0.50;
	GeometryList_scn[geometryListIdx].TranslationStep = 0.0003;
}

//////////////////////////////////


////////////////////////////////////
// LOGIC STATIC INDIA FUNCTIONS
////////////////////////////////////
function initVaoSI()
{
	// variable declarations
	var index = 0;
	var stepRow = 2.0 / RATIO_ROWS;
	var stepCol = 2.0 / RATIO_COLS;
	var xcoord = 1.0;
	var ycoord = 1.0;
	var theHeight = stepRow * 2.5;
	var IposX; // = -1.0f + (stepCol * 3.0f);
	var bigLetterWidth = stepCol * 1.5;
	var distanceInLetter = stepCol / 2.0;
    var p1 = new Coord(0,0,0), p2 = new Coord(0,0,0), p3 = new Coord(0,0,0);
    
	IposX = -1.0 + (stepCol * 4.75);// +distanceInLetter;

    debugger
    // var siVertices[85];
    var siVertices = Array.apply(null, Array(85)).map(function(val, i){ return 0.0; });

    debugger
	// I
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	
	// N
	IposX += distanceInLetter;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	IposX += bigLetterWidth;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	// D
	IposX += distanceInLetter;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX + bigLetterWidth; siVertices[index++] = theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX + bigLetterWidth; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	IposX += bigLetterWidth;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	// I
	IposX += distanceInLetter;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	// A
	IposX += distanceInLetter;
	p1.x = IposX, p1.y = -theHeight;
	IposX += (bigLetterWidth / 2.0);
	p2.x = IposX, p2.y = theHeight;
	IposX += (bigLetterWidth / 2.0);
	p3.x = IposX, p3.y = -theHeight;

	//drawTricolorInLetterA(p1.x, p1.y, p2.x, p2.y, p2.x, p2.y, p3.x, p3.y, siVertices, index);

	siVertices[index++] = p1.x; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	siVertices[index++] = p2.x; siVertices[index++] = theHeight; siVertices[index++] = 0;
	
	siVertices[index++] = p2.x; siVertices[index++] = theHeight; siVertices[index++] = 0;
    siVertices[index++] = p3.x; siVertices[index++] = -theHeight; siVertices[index++] = 0;
    
    /// TODO
	drawTricolorInLetterA(p1.x, p1.y, p2.x, p2.y, p2.x, p2.y, p3.x, p3.y, siVertices, index);

    var siColor =
    [
        // I
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// N
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// D
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,

		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// I
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// A
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue
    ];

    //// OGL
    vao_staticindia = bindVao();

    vbo_si_position = bindVbo();
    fillBufferData(new Float32Array(siVertices), 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();

    vbo_si_color = bindVbo();
    fillBufferData(new Float32Array(siColor), 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}

// step 1] find mid points of both sides
// step 2] can draw middle line joining both mid points
// step 3] find lengths of all three sides
// step 4] Upper: now we have lengths of all sides. find one angle. Inverse cos
//			cos A = (b2 + c2 - a2) /2bc		A = cos inverse(value)
// step 5] Lower: angle = 180 - upper. Inverse cos
// step 6].a find next point of side 1 nextS1A'(x + pos * cos(A), y + pos * sin(A))
// step 6].b find next point of side 2 nextS2A'(x + pos * cos(A), y + pos * sin(A))
// step 7] draw line from obtained points nextS1A' <--> nextS2A'
// step 8].a find prev point of side 1 prevS1A'(x + pos * cos(A), y + pos * sin(A))
// step 8].b find prev point of side 2 prevS2A'(x + pos * cos(A), y + pos * sin(A))
// step 9] draw line from obtained points prevS1A' <--> prevS2A'
function drawTricolorInLetterA(side1X1, side1Y1, side1X2, side1Y2,
	side2X1, side2Y1, side2X2, side2Y2, siVertices, index)
{
	// midpoint = (x1+x2)/2, (y1+y2)/2
	var side1MidPoint = new Coord(), side2MidPoint = new Coord();

	side1MidPoint.x = (side1X1 + side1X2) / 2.0;
	side1MidPoint.y = (side1Y1 + side1Y2) / 2.0;
	side1MidPoint.z = 0.0;

	side2MidPoint.x = (side2X1 + side2X2) / 2.0;
	side2MidPoint.y = (side2Y1 + side2Y2) / 2.0;
	side2MidPoint.z = 0.0;

	var lengthB = Math.sqrt(Math.pow((side1MidPoint.x - side1X2), 2) + Math.pow((side1MidPoint.y - side1Y2), 2));
	var lengthC = Math.sqrt(Math.pow((side1MidPoint.x - side2MidPoint.x), 2) + Math.pow((side1MidPoint.y - side2MidPoint.y), 2));
	var lengthA = Math.sqrt(Math.pow((side2MidPoint.x - side2X2), 2) + Math.pow((side2MidPoint.y - side2Y2), 2));

	var side1CosA = ((lengthB*lengthB) + (lengthC*lengthC) - (lengthA*lengthA)) / (2.0 *lengthB * lengthC);
	var side1AngleA = Math.cos(side1CosA) * (180.0 / Math.PI); // radian to degree
	var side1CosB = ((lengthA*lengthA) + (lengthC*lengthC) - (lengthB*lengthB)) / (2.0 *lengthA * lengthC);
	var side1AngleB = Math.cos(side1CosB) * (180.0 / Math.PI); // radian to degree
    
	var nextS1Ax = side1MidPoint.x + (0.002 * Math.cos(side1AngleA));
	var nextS1Ay = side1MidPoint.y + (0.035 * Math.sin(side1AngleA));
	var nextS2Ax = side2MidPoint.x - (0.002 * Math.cos(side1AngleB));
	var nextS2Ay = side2MidPoint.y + (0.035 * Math.sin(side1AngleB));
                                           
	var prevS1Ax = side1MidPoint.x - (0.002 * Math.cos(side1AngleA));
	var prevS1Ay = side1MidPoint.y - (0.035 * Math.sin(side1AngleA));
	var prevS2Ax = side2MidPoint.x + (0.002 * Math.cos(side1AngleB));
	var prevS2Ay = side2MidPoint.y - (0.035 * Math.sin(side1AngleB));

	siVertices[index++] = prevS1Ax; siVertices[index++] = prevS1Ay; siVertices[index++] = side1MidPoint.z;
	siVertices[index++] = prevS2Ax; siVertices[index++] = prevS2Ay; siVertices[index++] = side2MidPoint.z;

	siVertices[index++] = side1MidPoint.x; siVertices[index++] = side1MidPoint.y; siVertices[index++] = side1MidPoint.z;
	siVertices[index++] = side2MidPoint.x; siVertices[index++] = side2MidPoint.y; siVertices[index++] = side2MidPoint.z;

	siVertices[index++] = nextS1Ax; siVertices[index++] = nextS1Ay; siVertices[index++] = side1MidPoint.z;
	siVertices[index++] = nextS2Ax; siVertices[index++] = nextS2Ay; siVertices[index++] = side2MidPoint.z;
}

function initVaoI1(geometryListIdx){

	GeometryList_scn[geometryListIdx].InitialX = -2.95;
	GeometryList_scn[geometryListIdx].MovingX = GeometryList_scn[geometryListIdx].InitialX;
	GeometryList_scn[geometryListIdx].StopMarker = -1.0 + (gStepCol);
	GeometryList_scn[geometryListIdx].TranslationStep = 0.005;

	// GeometryList_scn[geometryListIdx].Vertices = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].Vertices[0] = -1.0;
	GeometryList_scn[geometryListIdx].Vertices[1] = gLetterHeight;
	GeometryList_scn[geometryListIdx].Vertices[2] = 0.0;
	GeometryList_scn[geometryListIdx].Vertices[3] = -1.0;
	GeometryList_scn[geometryListIdx].Vertices[4] = -gLetterHeight;
	GeometryList_scn[geometryListIdx].Vertices[5] = 0.0;

	// GeometryList_scn[geometryListIdx].Colors = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].Colors[0] = gDeepSaffron.red;
	GeometryList_scn[geometryListIdx].Colors[1] = gDeepSaffron.green;
	GeometryList_scn[geometryListIdx].Colors[2] = gDeepSaffron.blue;
	GeometryList_scn[geometryListIdx].Colors[3] = gIndiaGreen.red;
	GeometryList_scn[geometryListIdx].Colors[4] = gIndiaGreen.green;
	GeometryList_scn[geometryListIdx].Colors[5] = gIndiaGreen.blue;

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].Vertices), 3, false, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].Colors), 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}

function initVaoN(geometryListIdx){

	GeometryList_scn[geometryListIdx].InitialY = 1.24;
	GeometryList_scn[geometryListIdx].MovingY = GeometryList_scn[geometryListIdx].InitialY;
	GeometryList_scn[geometryListIdx].StopMarker = -gLetterHeight;
	GeometryList_scn[geometryListIdx].TranslationStep = 0.005;
	var theX = -1.0 + (gStepCol * 3.0);

	// GeometryList_scn[geometryListIdx].Vertices = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].VerticesDynamic = [];

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialY + (gLetterHeight * 2.0));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialY);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialY + (gLetterHeight * 2.0));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX+ gBigLetterWidth);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialY);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX+ gBigLetterWidth);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialY + (gLetterHeight * 2.0));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX+ gBigLetterWidth);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialY);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// GeometryList_scn[geometryListIdx].Colors = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].ColorsDynamic = [];

	for (var i = 0; i < NO_OF_LINES_IN_LETTER_N; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	}

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].VerticesDynamic), 3, false, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].ColorsDynamic), 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}

function initVaoA(geometryListIdx){

	GeometryList_scn[geometryListIdx].InitialX = 2.2;
	GeometryList_scn[geometryListIdx].MovingX = GeometryList_scn[geometryListIdx].InitialX;
	GeometryList_scn[geometryListIdx].StopMarker = -1.0 + (gStepCol * 13.0);
	GeometryList_scn[geometryListIdx].TranslationStep = 0.005;
	var theX = -1.0 + (gStepCol * 3.0);

	// GeometryList_scn[geometryListIdx].Vertices = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].VerticesDynamic = [];

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialX + (gBigLetterWidth / 2.0));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(gLetterHeight);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialX);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gLetterHeight);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialX + (gBigLetterWidth / 2.0));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(gLetterHeight);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(GeometryList_scn[geometryListIdx].InitialX + gBigLetterWidth);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gLetterHeight);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// GeometryList_scn[geometryListIdx].Colors = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].ColorsDynamic = [];

	for (var i = 0; i < NO_OF_LINES_IN_LETTER_A; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	}

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].VerticesDynamic), 3, false, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].ColorsDynamic), 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}

function initVaoI2(geometryListIdx){

	var theX = -1.0 + (gStepCol * 11.0);
	GeometryList_scn[geometryListIdx].InitialY = -1.24;
	GeometryList_scn[geometryListIdx].MovingY = GeometryList_scn[geometryListIdx].InitialY;
	GeometryList_scn[geometryListIdx].StopMarker = gLetterHeight;
	GeometryList_scn[geometryListIdx].TranslationStep = 0.005;

	// GeometryList_scn[geometryListIdx].Vertices = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].Vertices[0] = theX;
	GeometryList_scn[geometryListIdx].Vertices[1] = GeometryList_scn[geometryListIdx].InitialY;
	GeometryList_scn[geometryListIdx].Vertices[2] = 0.0;
	GeometryList_scn[geometryListIdx].Vertices[3] = theX;
	GeometryList_scn[geometryListIdx].Vertices[4] = GeometryList_scn[geometryListIdx].InitialY - (gLetterHeight * 2.0);
	GeometryList_scn[geometryListIdx].Vertices[5] = 0.0;

	// GeometryList_scn[geometryListIdx].Colors = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].Colors[0] = gDeepSaffron.red;
	GeometryList_scn[geometryListIdx].Colors[1] = gDeepSaffron.green;
	GeometryList_scn[geometryListIdx].Colors[2] = gDeepSaffron.blue;
	GeometryList_scn[geometryListIdx].Colors[3] = gIndiaGreen.red;
	GeometryList_scn[geometryListIdx].Colors[4] = gIndiaGreen.green;
	GeometryList_scn[geometryListIdx].Colors[5] = gIndiaGreen.blue;

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].Vertices), 3, false, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].Colors), 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}

function initVaoD(geometryListIdx){

	GeometryList_scn[geometryListIdx].Alpha = 0.0; //0.0;
	GeometryList_scn[geometryListIdx].TranslationStep = 0.002;
	var theX = -1.0 + (gStepCol * 7.0);

	// GeometryList_scn[geometryListIdx].Vertices = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].VerticesDynamic = [];

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(gLetterHeight);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gLetterHeight);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(gLetterHeight);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX + gBigLetterWidth);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(gLetterHeight - (gLetterHeight/3.0));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gLetterHeight);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX + gBigLetterWidth);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gLetterHeight + (gLetterHeight/3.0));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX + gBigLetterWidth);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(gLetterHeight - (gLetterHeight/3.0));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(theX + gBigLetterWidth);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gLetterHeight + (gLetterHeight/3.0));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// GeometryList_scn[geometryListIdx].Colors = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].ColorsDynamic = [];

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);
	
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].VerticesDynamic), 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].ColorsDynamic), 4, false, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

    unbindVao();
}


function initVaoPlane(geometryListIdx){

	var unitMagnitude = gStepCol * 1.25;
	var half = (unitMagnitude / 2.0);
	var percent10 = (unitMagnitude * 0.1);
	var percent15 = (unitMagnitude * 0.15);
	var percent20 = (unitMagnitude * 0.2);
	var percent25 = (unitMagnitude * 0.25);
	var percent30 = (unitMagnitude * 0.3);
	var percent35 = (unitMagnitude * 0.35);
	var percent40 = (unitMagnitude * 0.4);
	var tempIdx = 0;

	// GeometryList_scn[geometryListIdx].Vertices = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].VerticesDynamic = [];
	
	// airoplaneTail
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gUnitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gUnitMagnitude - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(half);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gUnitMagnitude + percent30);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(half);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gUnitMagnitude + percent40);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gUnitMagnitude + percent40);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gUnitMagnitude + percent30);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-half);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-gUnitMagnitude - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-half);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// airoplaneMiddle
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude + percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude + percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent25);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent25);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// airoplaneHead
	// 11
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent25);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 12
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(unitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 13
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0 + percent30);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(unitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 14
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0 + half);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent30);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 15
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(unitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent35);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 16
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(unitMagnitude + percent40);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 17
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(unitMagnitude + percent40);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 18
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(unitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent35);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 19
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0 + half);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent30);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 20
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0 + percent30);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 21
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 22
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent25);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// exhaustSaffron
	// 23
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 24
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 25
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 26
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 27
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// exhaustWhite
	// 28
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 29
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 30
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 31
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-(percent15 - percent10));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 32
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-(percent15 - percent10));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// exhaustGreen
	// 33
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 34
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 35
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 36
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-(percent15 - percent10));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 37
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-(percent15 - percent10));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// IAF
	var side1MidPointx = (percent40 + percent20 + percent30 + percent15) / 2.0;
	var midPointy = (percent20 + (-percent20)) / 2.0;
	var side2MidPointx = (percent40 + percent20 + percent15 + percent40 + percent20) / 2.0;
	var tempX1 = 0.0, tempX2 = 0.0, tempY = 0.0, reduceBy = 0.1;

	// 38
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent40);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 39
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1 = percent40);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 40
	tempX1 = tempX1 - (percent10 * 0.6);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 41
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// A
	// 42
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1 = percent30 + percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent25);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 43
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX2 = percent40 + percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 44
	tempX2 = tempX2 + (tempX2 * 0.1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX2);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 45
	tempX1 = tempX1 + (tempX1 * 0.1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent25);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 46
	tempX2 = percent40 + percent20 + percent15;
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX2);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 47
	tempX2 = tempX2 + (tempX2 * 0.1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX2);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 48
	tempX1 = side1MidPointx;
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(side1MidPointx);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(midPointy);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 49
	tempX2 = side2MidPointx;
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(side2MidPointx);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(midPointy);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 50
	tempY = midPointy - 0.01;
	tempX2 = tempX2 + (percent10 * 0.1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX2);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempY);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 51
	tempX1 = tempX1 - (percent10 * 0.1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempY);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// F
	// 52
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent10 + percent40 * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 53
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1 = percent10 + percent40 * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 54
	tempX1 = tempX1 + (percent10 * 0.5);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 55
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 56
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent30 + percent40 * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 57
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent30 + percent40 * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent20 - 0.01);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 58
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent20 - 0.01);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 59
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 60
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-0.01);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 61
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1 + percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-0.01);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 62
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(tempX1 + percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);


	
	// element vbo
	tempIdx = 0;
	GeometryList_scn[geometryListIdx].ElementsDynamic = [];

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(1);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(0);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(2);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(2);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(0);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(3);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(3);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(0);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(4);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(4);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(0);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(5);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(5);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(0);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(6);

	// airoplaneMiddle
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(8);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(7);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(9);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(9);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(7);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(10);

	// airoplaneHead
	// 7
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(11);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(13);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(12);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(11);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(14);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(13);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(11);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(19);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(14);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(11);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(22);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(19);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(22);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(20);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(19);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(20);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(22);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(21);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(14);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(18);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(15);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(18);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(14);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(19);

	GeometryList_scn[geometryListIdx].ElementsDynamic.push(15);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(17);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(16);

	// 16
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(17);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(15);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(18);

	// exhaustSaffron
	// 17
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(23);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(24);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(25);

	// 18
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(25);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(26);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(27);

	// 19
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(23);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(25);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(27);

	// exhaustWhite
	// 20
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(28);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(29);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(30);

	// 21
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(30);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(31);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(32);

	// 22
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(28);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(30);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(32);

	// exhaustGreen
	// 23
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(34);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(33);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(35);

	// 24
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(36);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(35);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(37);

	// 25
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(35);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(33);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(37);

	// IAF
	// 26
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(39);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(38);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(40);

	// 27
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(41);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(40);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(38);

	// A
	// 28
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(43);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(42);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(44);

	// 29
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(44);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(43);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(45);

	// 30
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(43);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(46);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(44);

	// 31
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(44);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(46);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(47);

	// 32
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(49);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(48);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(50);

	// 33
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(51);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(50);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(48);

	// F
	// 34
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(52);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(53);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(54);

	// 35
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(55);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(52);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(54);

	// 36
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(56);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(55);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(57);

	// 37
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(55);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(58);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(57);

	// 38
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(59);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(60);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(61);

	// 39
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(62);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(59);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(61);

	GeometryList_scn[geometryListIdx].NumElements = tempIdx; // / 3; // TODO



	// GeometryList_scn[geometryListIdx].Colors = Array.apply(null, Array(6)).map(function(val, i){ return 0.0; });
	GeometryList_scn[geometryListIdx].ColorsDynamic = [];

	var i = 0;
	for (; i < 23; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(0.7294117647058824);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(0.8862745098039216);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(0.9333333333333333);
	}
	// exhaustSaffron
	for (; i < 28; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(0.59765625);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(0.19921875);
	}
	// exhaustWhite
	for (; i < 33; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	}
	// exhaustGreen
	for (; i < 38; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(0.07421875);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(0.53125);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(0.03125);
	}
	// IAF
	for (; i < NO_OF_LINES_IN_PLANE; i++)
	{
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
		GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	}


    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].VerticesDynamic), 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].ColorsDynamic), 3, true, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

	var theVboElement = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, theVboElement);
	vbo_element_list_scn[geometryListIdx] = theVboElement;
	// VERIFY Int16 or Int32 or Int8
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(GeometryList_scn[geometryListIdx].ElementsDynamic), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    unbindVao();
}

function initVaoTricolor(geometryListIdx){

	var unitMagnitude = gStepCol * 1.25;
	var half = (unitMagnitude / 2.0);
	var percent10 = (unitMagnitude * 0.1);
	var percent15 = (unitMagnitude * 0.15);
	var percent20 = (unitMagnitude * 0.2);
	var percent25 = (unitMagnitude * 0.25);
	var percent30 = (unitMagnitude * 0.3);
	var percent35 = (unitMagnitude * 0.35);
	var percent40 = (unitMagnitude * 0.4);
	var tempIdx = 0;

	// NOTE: Had to change EBO orders swap 1 and 2 of each triplet

	GeometryList_scn[geometryListIdx].Alpha = 0.0;
	GeometryList_scn[geometryListIdx].TranslationStep = 0.002;

	GeometryList_scn[geometryListIdx].VerticesDynamic = [];
	// 0
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 1
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 2
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 3
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 4
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 5
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(percent15 - percent10);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 6
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-(percent15 - percent10));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 7
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-(percent15 - percent10));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 8
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-(percent15 - percent10));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 9
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-(percent15 - percent10));
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 10
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude * 2.0);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// 11
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-unitMagnitude - percent20);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(-percent15);
	GeometryList_scn[geometryListIdx].VerticesDynamic.push(0.0);

	// GeometryList_scn[geometryListIdx].NumVertices = tempIdx / 3;

	// element vbo
	tempIdx = 0;
	GeometryList_scn[geometryListIdx].ElementsDynamic = [];
	
	// 0
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(0);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(1);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(2);

	// 1
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(3);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(0);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(2);

	// 2
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(4);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(5);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(6);

	// 3
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(7);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(4);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(6);

	// 4
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(8);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(9);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(10);

	// 5
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(11);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(8);
	GeometryList_scn[geometryListIdx].ElementsDynamic.push(10);

	tempIdx = 0;
	GeometryList_scn[geometryListIdx].ColorsDynamic = [];
	
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gDeepSaffron.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	// white
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(1.0);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	// 
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.red);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.green);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(gIndiaGreen.blue);
	GeometryList_scn[geometryListIdx].ColorsDynamic.push(GeometryList_scn[geometryListIdx].Alpha);

    //// OGL
    vao_list_scn[geometryListIdx] = bindVao();

    vbo_position_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].VerticesDynamic), 3, true, WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    unbindVbo();
    
    vbo_color_list_scn[geometryListIdx] = bindVbo();
    fillBufferData(new Float32Array(GeometryList_scn[geometryListIdx].ColorsDynamic), 4, false, WebGLMacros.VDG_ATTRIBUTE_COLOR);
    unbindVbo();

	var indexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
	vbo_element_list_scn[geometryListIdx] = indexBuffer;
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Int32Array(GeometryList_scn[geometryListIdx].ElementsDynamic), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    unbindVao();
}
