
var rotationAngle = 0.0;

function updateOrg()
{
	if (GeometryList_scn[Geometries.STICK].MovingY <= 0.0)
	{
		if (rotationAngle <= 180.0)
			rotationAngle += 0.25;
	}
	else
		rotationAngle += 0.25;

	if (rotationAngle > 360.0)
		rotationAngle = 0.0;

	if (GeometryList_scn[Geometries.TRIANGLE].MovingX > 0.0)
        GeometryList_scn[Geometries.TRIANGLE].MovingX -= 0.0006;
	if (GeometryList_scn[Geometries.TRIANGLE].MovingY < 0.0)
        GeometryList_scn[Geometries.TRIANGLE].MovingY += 0.0003;

	if (GeometryList_scn[Geometries.STICK].MovingY > 0.0)
        GeometryList_scn[Geometries.STICK].MovingY -= GeometryList_scn[Geometries.STICK].TranslationStep;

	/*if (GeometryList[INCIRCLE].MovingX < 0.0f)
		GeometryList[INCIRCLE].MovingX += 0.0006f;
	if (GeometryList[INCIRCLE].MovingY < 0.0f)
		GeometryList[INCIRCLE].MovingY += 0.0003f;*/
}

function update()
{

}

// NOTE: This is very important to have global variables
// as JS does not have local static variables, killer.
var angleA = Math.PI - ((Math.PI/2) / 4.0);
var angleA2 = Math.PI/2;
var angleC = Math.PI + (Math.PI/2);
var angleC2 = Math.PI + ((Math.PI/2) / 4.0);

function draw() {
	// variable declarations
	var theX;
	var theY;
	var radius = 1.24;
	var tempRotF = 0.0;

    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(genericShaderProgramObject_scn);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    var rotationMatrix = mat4.create();

    // vec3.set(modelViewMatrix, [0.0, 0.0, -3.0]);
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -2.0]);
    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
    gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);

    if (gbShowGraph)
    {
        drawGraph(Geometries.GRAPH);
    }
    
    // drawStaticIndia();
	// drawDynamicIndiaLetters();
	if(drawDynamicIndiaLetters())
	{
		mat4.identity(modelViewMatrix);
		mat4.translate(modelViewMatrix, modelViewMatrix, [GeometryList_scn[Geometries.PLANE_B].MovingX, 0.0, -2.0]);
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
		gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
		
		drawPlane(Geometries.PLANE_B);

		// Upper plane Up to Down
		if (GeometryList_scn[Geometries.PLANE_A].MovingX <= (1.24 + 0.8))
		{
			theX = Math.cos(angleA) * radius;
			theY = Math.sin(angleA) * radius;
			tempRotF = (angleA + (Math.PI/2)); // Should be in radians for rotateZ() // * (180.0 / Math.PI);

			mat4.identity(modelViewMatrix);
			mat4.translate(modelViewMatrix, modelViewMatrix, [theX - 2.2 + 1.24 + GeometryList_scn[Geometries.PLANE_A].MovingX, theY + 1.24, -2.0]);
			mat4.rotateZ(modelViewMatrix, modelViewMatrix, tempRotF);
			mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
			gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
			
			drawPlane(Geometries.PLANE_A);
			
			if (angleA < Math.PI + (Math.PI/2.0))
			{
				angleA += 0.005;
			}
			else
			{
				GeometryList_scn[Geometries.PLANE_A].MovingX += 0.0049;
			}
		}
		else if (GeometryList_scn[Geometries.PLANE_A].MovingX > (1.24 + 0.8))
		{
			theX = Math.cos(angleA2) * radius;
			theY = Math.sin(angleA2) * radius;
			tempRotF = (angleA2 - (Math.PI/2)); // Should be in radians for rotateZ() // * (180.0 / Math.PI);

			mat4.identity(modelViewMatrix);
			mat4.translate(modelViewMatrix, modelViewMatrix, [theX - 2.2 + 1.24 + GeometryList_scn[Geometries.PLANE_A].MovingX, theY - 1.24, -2.0]);
			mat4.rotateZ(modelViewMatrix, modelViewMatrix, tempRotF);
			mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
			gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
			
			drawPlane(Geometries.PLANE_A);
			
			if (angleA2 > -(Math.PI/4.0))
			{
				angleA2 -= 0.005;
			}
		}
		
		// Lower Plane Down to Up
		if (GeometryList_scn[Geometries.PLANE_C].MovingX <= (1.24 + 0.8))
		{
			theX = Math.cos(angleC2) * radius;
			theY = Math.sin(angleC2) * radius;
			tempRotF = (angleC2 - (Math.PI/2)); // Should be in radians for rotateZ() // * (180.0 / Math.PI);

			mat4.identity(modelViewMatrix);
			mat4.translate(modelViewMatrix, modelViewMatrix, [theX - 2.2 + 1.24 + GeometryList_scn[Geometries.PLANE_C].MovingX, theY - 1.24, -2.0]);
			mat4.rotateZ(modelViewMatrix, modelViewMatrix, tempRotF);
			mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
			gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
			
			drawPlane(Geometries.PLANE_C);
			
			if (angleC2 > (Math.PI/2.0))
			{
				angleC2 -= 0.005;
			}
			else
			{
				GeometryList_scn[Geometries.PLANE_C].MovingX += 0.0049;
			}
		}
		else if (GeometryList_scn[Geometries.PLANE_C].MovingX > (1.24 + 0.8))
		{
			theX = Math.cos(angleC) * radius;
			theY = Math.sin(angleC) * radius;
			tempRotF = (angleC + (Math.PI/2)); // Should be in radians for rotateZ() // * (180.0 / Math.PI);

			mat4.identity(modelViewMatrix);
			mat4.translate(modelViewMatrix, modelViewMatrix, [theX - 2.2 + 1.24 + GeometryList_scn[Geometries.PLANE_C].MovingX, theY + 1.24, -2.0]);
			mat4.rotateZ(modelViewMatrix, modelViewMatrix, tempRotF);
			mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
			gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
			
			drawPlane(Geometries.PLANE_C);
			
			if (angleC < (Math.PI * 2.0) + (Math.PI/4))
			{
				angleC += 0.005;
			}
		}

		// Tricolor fade in
		if (GeometryList_scn[Geometries.PLANE_B].MovingX > 0.9)
		{
			mat4.identity(modelViewMatrix);
			mat4.translate(modelViewMatrix, modelViewMatrix, [gStepCol * 7.750, 0.0, -2.0]);
			mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
			gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
			
			drawTricolor();
		}

		// update
		if (GeometryList_scn[Geometries.PLANE_B].MovingX < -2.2 + 1.24)
		{
			GeometryList_scn[Geometries.PLANE_B].MovingX += 0.0049;
		}
		else if (GeometryList_scn[Geometries.PLANE_B].MovingX < 1.24 + 0.8)
		{
			GeometryList_scn[Geometries.PLANE_B].MovingX += 0.0049;
		}
		else if (GeometryList_scn[Geometries.PLANE_B].MovingX < 2.56)
		{
			GeometryList_scn[Geometries.PLANE_B].MovingX += 0.005;
		}
	}

    // mat4.rotateY(rotationMatrix, rotationMatrix, degToRad(rotationAngle));

    // // TRIANGLE
    // modelViewMatrix = mat4.create();
    // mat4.translate(modelViewMatrix, modelViewMatrix, [GeometryList_scn[Geometries.TRIANGLE].MovingX, GeometryList_scn[Geometries.TRIANGLE].MovingY, -2.0]);
    // mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);
    // mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
    // gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
    // drawGeometryWithMode(Geometries.TRIANGLE, gl.LINE_LOOP, 0, NO_OF_VERTEX_TRIANGLE);

	// // STICK
    // modelViewMatrix = mat4.create();
    // mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, GeometryList_scn[Geometries.STICK].MovingY, -2.0]);
    // mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
    // gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
    // drawGeometryWithMode(Geometries.STICK, gl.LINES, 0, 2); //NO_OF_COORDS_IN_3D_LINE);

	// // INCIRCLE
    // modelViewMatrix = mat4.create();
    // mat4.translate(modelViewMatrix, modelViewMatrix, [-GeometryList_scn[Geometries.TRIANGLE].MovingX + GeometryList_scn[Geometries.INCIRCLE].MovingX, GeometryList_scn[Geometries.TRIANGLE].MovingY + GeometryList_scn[Geometries.INCIRCLE].MovingY, -2.0]);
    // mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);
    // mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_scn, modelViewMatrix);
    // gl.uniformMatrix4fv(GenericUniforms_scn.mvpUniform, false, modelViewProjectionMatrix);
    // drawGeometryWithMode(Geometries.INCIRCLE, gl.LINE_LOOP, 0, GeometryList_scn[Geometries.INCIRCLE].NumVertices);

    gl.useProgram(null);

    update();

    // animation loop 
    requestAnimationFrame(draw, canvas);
}

function drawGraph(geometryListIdx)
{
	gl.lineWidth(1.0);

	// bind with vao - this will avoid many vbo repetitive calls in display
	gl.bindVertexArray(vao_list_scn[geometryListIdx]);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	gl.drawArrays(gl.LINES, 0, ((LINES_TO_DRAW + 2) * 4) * 2);
    gl.bindVertexArray(null);
}

function drawGeometryWithMode(geometryListIdx, mode, first, count)
{
	gl.bindVertexArray(vao_list_scn[geometryListIdx]);
	gl.drawArrays(mode, first, count);
	gl.bindVertexArray(null);
}

function degToRad(degrees) {
    // code 
    return (degrees * Math.PI / 180);
}

///////////////////////////////

function drawStaticIndia()
{
	gl.lineWidth(5.0);

	gl.bindVertexArray(vao_staticindia);
	gl.drawArrays(gl.LINES, 0, 14 * 2);

	gl.bindVertexArray(null);
}

function drawDynamicIndiaLetters()
{
	var isPlacedI1 = false;
	var isPlacedN = false;
	var isPlacedD = false;
	var isPlacedI2 = false;
	var isPlacedA = false;

	isPlacedI1 = drawLetterI1();
	if(isPlacedI1)
    {
		isPlacedA = drawLetterA();
		if(isPlacedA)
		{
			isPlacedN = drawLetterN();
			if(isPlacedN)
			{
				isPlacedI2 = drawLetterI2();
				if(isPlacedI2)
				{
					isPlacedD = drawLetterD();
				}
			}
		}
	}

	return isPlacedD;
}

function drawLetterI1()
{
	//glLineWidth(5.0f);
	gl.bindVertexArray(vao_list_scn[Geometries.LETTER_I1]);

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_list_scn[Geometries.LETTER_I1]);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(GeometryList_scn[Geometries.LETTER_I1].Vertices), gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	if (GeometryList_scn[Geometries.LETTER_I1].MovingX < GeometryList_scn[Geometries.LETTER_I1].StopMarker)
	{
		GeometryList_scn[Geometries.LETTER_I1].MovingX += GeometryList_scn[Geometries.LETTER_I1].TranslationStep;

		GeometryList_scn[Geometries.LETTER_I1].Vertices[0] = GeometryList_scn[Geometries.LETTER_I1].MovingX;
		GeometryList_scn[Geometries.LETTER_I1].Vertices[3] = GeometryList_scn[Geometries.LETTER_I1].MovingX;

		return false;
	}
	return true;
}

function drawLetterN()
{
	//glLineWidth(5.0f);
	var tempY = 0.0;
	gl.bindVertexArray(vao_list_scn[Geometries.LETTER_N]);

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_list_scn[Geometries.LETTER_N]);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(GeometryList_scn[Geometries.LETTER_N].VerticesDynamic), gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

	if (GeometryList_scn[Geometries.LETTER_N].MovingY > GeometryList_scn[Geometries.LETTER_N].StopMarker)
	{
		GeometryList_scn[Geometries.LETTER_N].MovingY -= GeometryList_scn[Geometries.LETTER_N].TranslationStep;
		tempY = GeometryList_scn[Geometries.LETTER_N].MovingY + (gLetterHeight * 2.0);

		GeometryList_scn[Geometries.LETTER_N].VerticesDynamic[1] = tempY;
		GeometryList_scn[Geometries.LETTER_N].VerticesDynamic[4] = GeometryList_scn[Geometries.LETTER_N].MovingY;

		GeometryList_scn[Geometries.LETTER_N].VerticesDynamic[7] = tempY;
		GeometryList_scn[Geometries.LETTER_N].VerticesDynamic[10] = GeometryList_scn[Geometries.LETTER_N].MovingY;

		GeometryList_scn[Geometries.LETTER_N].VerticesDynamic[13] = tempY;
		GeometryList_scn[Geometries.LETTER_N].VerticesDynamic[16] = GeometryList_scn[Geometries.LETTER_N].MovingY;

		return false;
	}
	return true;
}

function drawLetterA()
{
	//glLineWidth(5.0f);
	var tempX = 0.0;
	gl.bindVertexArray(vao_list_scn[Geometries.LETTER_A]);

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_list_scn[Geometries.LETTER_A]);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(GeometryList_scn[Geometries.LETTER_A].VerticesDynamic), gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.drawArrays(gl.LINES, 0, 4);

	gl.bindVertexArray(null);

	if (GeometryList_scn[Geometries.LETTER_A].MovingX > GeometryList_scn[Geometries.LETTER_A].StopMarker)
	{
		GeometryList_scn[Geometries.LETTER_A].MovingX -= GeometryList_scn[Geometries.LETTER_A].TranslationStep;
		tempX = GeometryList_scn[Geometries.LETTER_A].MovingX + (gBigLetterWidth / 2.0);

		GeometryList_scn[Geometries.LETTER_A].VerticesDynamic[0] = tempX;
		GeometryList_scn[Geometries.LETTER_A].VerticesDynamic[3] = GeometryList_scn[Geometries.LETTER_A].MovingX;

		GeometryList_scn[Geometries.LETTER_A].VerticesDynamic[6] = tempX;
		GeometryList_scn[Geometries.LETTER_A].VerticesDynamic[9] = GeometryList_scn[Geometries.LETTER_A].MovingX + gBigLetterWidth;

		return false;
	}
	return true;
}

function drawLetterI2()
{
	//glLineWidth(5.0f);
	gl.bindVertexArray(vao_list_scn[Geometries.LETTER_I2]);

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_list_scn[Geometries.LETTER_I2]);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(GeometryList_scn[Geometries.LETTER_I2].Vertices), gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	if (GeometryList_scn[Geometries.LETTER_I2].MovingY < GeometryList_scn[Geometries.LETTER_I2].StopMarker)
	{
		GeometryList_scn[Geometries.LETTER_I2].MovingY += GeometryList_scn[Geometries.LETTER_I2].TranslationStep;

		GeometryList_scn[Geometries.LETTER_I2].Vertices[1] = GeometryList_scn[Geometries.LETTER_I2].MovingY;
		GeometryList_scn[Geometries.LETTER_I2].Vertices[4] = GeometryList_scn[Geometries.LETTER_I2].MovingY - (gLetterHeight * 2.0);

		return false;
	}
	return true;
}

function drawLetterD()
{
	gl.enable(gl.BLEND);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

	//glLineWidth(5.0f);
	gl.bindVertexArray(vao_list_scn[Geometries.LETTER_D]);

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_list_scn[Geometries.LETTER_D]);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(GeometryList_scn[Geometries.LETTER_D].ColorsDynamic), gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.drawArrays(gl.LINES, 0, 8);

	gl.bindVertexArray(null);

	gl.disable(gl.BLEND);

	if (GeometryList_scn[Geometries.LETTER_D].Alpha < 1.0)
	{
		GeometryList_scn[Geometries.LETTER_D].Alpha += GeometryList_scn[Geometries.LETTER_D].TranslationStep;

		GeometryList_scn[Geometries.LETTER_D].ColorsDynamic[3] = GeometryList_scn[Geometries.LETTER_D].Alpha;
		GeometryList_scn[Geometries.LETTER_D].ColorsDynamic[7] = GeometryList_scn[Geometries.LETTER_D].Alpha;

		GeometryList_scn[Geometries.LETTER_D].ColorsDynamic[11] = GeometryList_scn[Geometries.LETTER_D].Alpha;
		GeometryList_scn[Geometries.LETTER_D].ColorsDynamic[15] = GeometryList_scn[Geometries.LETTER_D].Alpha;

		GeometryList_scn[Geometries.LETTER_D].ColorsDynamic[19] = GeometryList_scn[Geometries.LETTER_D].Alpha;
		GeometryList_scn[Geometries.LETTER_D].ColorsDynamic[23] = GeometryList_scn[Geometries.LETTER_D].Alpha;

		GeometryList_scn[Geometries.LETTER_D].ColorsDynamic[27] = GeometryList_scn[Geometries.LETTER_D].Alpha;
		GeometryList_scn[Geometries.LETTER_D].ColorsDynamic[31] = GeometryList_scn[Geometries.LETTER_D].Alpha;

		return false;
	}
	return true;
}

function drawPlane(geometryListIdx)
{
	//glLineWidth(5.0f);
	gl.bindVertexArray(vao_list_scn[geometryListIdx]);

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vbo_element_list_scn[geometryListIdx]);
	gl.drawElements(gl.TRIANGLES, 39*3, gl.UNSIGNED_SHORT, 0); //117
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

	gl.bindVertexArray(null);
}

function drawTricolor()
{
	gl.enable(gl.BLEND);
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

	//glLineWidth(5.0f);
	gl.bindVertexArray(vao_list_scn[Geometries.LETTER_A_TRICOLOR]);

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_list_scn[Geometries.LETTER_A_TRICOLOR]);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(GeometryList_scn[Geometries.LETTER_A_TRICOLOR].ColorsDynamic), gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vbo_element_list_scn[Geometries.LETTER_A_TRICOLOR]);
	gl.drawElements(gl.TRIANGLES, 6*3, gl.UNSIGNED_INT, 0); //117
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.disable(gl.BLEND);
	
	if (GeometryList_scn[Geometries.LETTER_A_TRICOLOR].Alpha < 1.0)
	{
		GeometryList_scn[Geometries.LETTER_A_TRICOLOR].Alpha += GeometryList_scn[Geometries.LETTER_A_TRICOLOR].TranslationStep;

		for (var tempdx = 0; tempdx < NO_OF_VERTICES_IN_LETTER_A_TRICOLOR; tempdx++)
		{
			GeometryList_scn[Geometries.LETTER_A_TRICOLOR].ColorsDynamic[tempdx*4+3] = GeometryList_scn[Geometries.LETTER_A_TRICOLOR].Alpha;
		}
		return false;
	}
	return true;
}
