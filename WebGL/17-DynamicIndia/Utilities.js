class AudioUtility {
    constructor(audioElemId) {
        this._audioElement = document.getElementById(audioElemId);
    }
    
    playAudio() {
        this._audioElement.play();
    }

    pauseAudio() {
        this._audioElement.pause();
    }
}

// class FileUtility {
//     constructor() {
//         this._fileName = "";
//         this._fileContents = "";
//         // Taking care of the browser-specific prefixes.
//         window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
//         window.directoryEntry = window.directoryEntry || window.webkitDirectoryEntry;
//     }

//     get fname() {
//         return this._fileName;
//     }

//     set fname(value) {
//         // can use regex for validation
//         this._fileName = value;
//     }

//     getData() {
//         var fr = new FileReader();
//         fr.onload = function (progressEvent) {
//             this._fileContents = this.result;
//             console.log(this._fileContents);
//             console.log(this.result);
//         }
//         debugger
//         window.requestFileSystem(window.TEMPORARY, 5 * 1024 * 1024 /*5MB*/, onFs, onError);
//         // var fsd = new FileSystemDirectoryEntry();
//         var theFile = new File(["foo"], this.fname);
//         fr.readAsText(theFile);
//         return this._fileContents;
//     }
// }

// document.getElementById("idFile").addEventListener('change',
//     function () {
//         console.log(this.files[0]);
//     });

// function onError(err) {
//     console.log(err);
//     alert(err);
// }

// function onFs(fs) {
//     console.log(this);
//     fs.root.getDirectory('Documents', { create: false }, function (directoryEntry) {
//         //directoryEntry.isFile === false
//         //directoryEntry.isDirectory === true
//         //directoryEntry.name === 'Documents'
//         //directoryEntry.fullPath === '/Documents'
//         console.log(this);
//     }, onError);
// }
