package edu.shreenandan.interleaved;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

// asa view jyacha surface OGL support karato
// x -> Extension, J2ME, GL10: OGL 10th extension, for basic features of OGL
// 3 For OGL Buffers

public class GLESView extends GLSurfaceView
        implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private GestureDetector gestureDetector;
    private final Context context;

    // java neither has uint nor GLuint
    private int fragmentShaderObject;
    private int vertexShaderObject;
    private int shaderProgramObject;

    // As java does not have address operator we declare array of object and pass name of array.
    private int[] vao_cube = new int[1];
    private int[] vbo_cube = new int[1];
    private int[] texture_kundali = new int[1];

    private float[] lightAmbient = new float[] {0.0f, 0.0f, 0.0f};
    private float[] lightDiffuse = new float[] {1.0f, 1.0f, 1.0f};
    private float[] lightSpecular = new float[] {1.0f, 1.0f, 1.0f};
    private float[] lightPosition = new float[] {100.0f, 100.0f, 100.0f, 1.0f};

    private float[] materialAmbient = new float[] {0.0f, 0.0f, 0.0f};
    private float[] materialDiffuse = new float[] {1.0f, 1.0f, 1.0f};
    private float[] materialSpecular = new float[] {1.0f, 1.0f, 1.0f};
    private float materialShininess = 128.0f;


    private int samplerUniform;
    private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
    private int laUniform, ldUniform, lsUniform;
    private int kaUniform, kdUniform, ksUniform, lightPositionUniform, materialShininessUniform;
    private int lKeyPressedUniform;

    private float[] perspectiveProjectionMatrix = new float[16];

    private float angleCube = 0.0f;

    boolean bLight=false;


    // constructor
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;
        // not useful now, will be helpful in texture

        // param 2: Listener; param 3: Handler, null = no other handler, this listner class obj himself is handler; param 4: always false
        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

        // call 3 functions of GLES
        setEGLContextClientVersion(3); // 3.x
        // will give highest supported to 3, ie 3.2

        setRenderer(this); // onDrawFrame() - display()
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE // other is RENDERMODE_CONTINUOUSLY
        // https://developer.android.com/training/graphics/opengl/motion.html?
        // https://developer.android.com/reference/android/opengl/GLSurfaceView
    }

    // Handling 'onTouchEvent' is the most important,
    // Because it triggers all gesture and tap events
    @Override
    public boolean onTouchEvent(MotionEvent me)
    {
        // below line is not useful in this pgm
        // useful in case of phone's keyboard actions
        int eventaction = me.getAction();

        // if not my event, then delegate to parent
        if(!gestureDetector.onTouchEvent(me))
        {
            super.onTouchEvent(me);
        }

        return(true);
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    // it is more reliable than onDoubleTapEvent
    @Override
    public boolean onDoubleTap(MotionEvent me)
    {
        return(true);
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    // less reliable, start, leave touch points
    @Override
    public boolean onDoubleTapEvent(MotionEvent me)
    {
        // do not write code here because handled in 'onDoubleTap'
        return(true);
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent me)
    {
        if(bLight==false)
        {
            bLight=true;
        }
        else
        {
            bLight=false;
        }
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public boolean onDown(MotionEvent me)
    {
        // do not write code here because handled in 'onSingleTapConfirmed'
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY)
    {
        // fling means swipe
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public void onLongPress(MotionEvent me)
    {
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY)
    {
        uninitialize();
        System.exit(0);
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public void onShowPress(MotionEvent me)
    {
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent me)
    {
        return(true);
    }


    // implement 3 functions of GLSurfaceView.Renderer
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_VENDOR);
        System.out.println("RTR: GLES32.GL_VENDOR: " + version);

        version = gl.glGetString(GLES32.GL_RENDERER);
        System.out.println("RTR: GLES32.GL_RENDERER: " + version);

        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
        update();
    }

    // our custom methods
    private void initialize()
    {
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "in vec4 vPosition;" +
                        "in vec4 vColor;" +
                        "in vec2 vTexcoord;" +
                        "in vec3 vNormal;" +
                        "uniform mat4 uModelMatrix;" +
                        "uniform mat4 uViewMatrix;" +
                        "uniform mat4 uProjectionMatrix;" +
                        "uniform mediump int uLKeyPressed;" +
                        "uniform vec4 uLightPosition;" +
                        "out vec3 transformedNormals;" +
                        "out vec3 lightDirection;" +
                        "out vec3 viewerVector;" +
                        "out vec4 outColor;" +
                        "out vec2 outTexcoord;" +
                        "void main(void)" +
                        "{" +
                        "if(uLKeyPressed == 1)" +
                        "{" +
                        "vec4 eyeCoordinates = uViewMatrix * uModelMatrix * vPosition;" +
                        "transformedNormals = mat3(uViewMatrix * uModelMatrix) * vNormal;" +
                        "lightDirection = vec3(uLightPosition - eyeCoordinates);" +
                        "viewerVector = normalize(vec3(-eyeCoordinates.xyz));" +
                        "}" +
                        "gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vPosition;" +
                        "outColor = vColor;" +
                        "outTexcoord = vTexcoord;" +
                        "}"
        );
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR: Error: Vertex shader compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Vertex Shader: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "precision highp float;" +
                        "uniform vec3 uLa;" +
                        "uniform vec3 uLd;" +
                        "uniform vec3 uLs;" +
                        "uniform vec3 uKa;" +
                        "uniform vec3 uKd;" +
                        "uniform vec3 uKs;" +
                        "uniform float uMaterialShininess;" +
                        "in vec3 transformedNormals;" +
                        "in vec3 lightDirection;" +
                        "in vec3 viewerVector;" +
                        "in vec2 outTexcoord;" +
                        "uniform sampler2D uSampler;" +
                        "in vec4 outColor;" +
                        "vec4 Texture;" +
                        "out vec4 FragColor;" +
                        "uniform int uLKeyPressed;" +
                        "void main(void)" +
                        "{" +
                        "vec3 phongADSColor;" +
                        "if(uLKeyPressed == 1)" +
                        "{" +
                        "vec3 normalizedTransformedNormals = normalize(transformedNormals);" +
                        "vec3 normalizedLightDirection = normalize(lightDirection);" +
                        "vec3 normalizedViewerVector = normalize(viewerVector);" +
                        "vec3 reflectionVector = reflect(-normalizedLightDirection ,normalizedTransformedNormals);" +
                        "float tnDotld = max(dot(normalizedLightDirection, normalizedTransformedNormals), 0.0);" +
                        "vec3 ambient = uLa * uKa;" +
                        "vec3 diffuse = uLd * uKd * tnDotld;" +
                        "vec3 specular = uLs * uKs * pow(max(dot(reflectionVector, normalizedViewerVector), 0.0), uMaterialShininess);" +
                        "phongADSColor = ambient + diffuse + specular;" +
                        "}" +
                        "else" +
                        "{" +
                        "phongADSColor = vec3(1.0, 1.0, 1.0);" +
                        "}" +
                        "Texture = texture(uSampler, outTexcoord);" +
                        "FragColor = Texture * outColor * vec4(phongADSColor, 1.0);" +
                        "}"
        );
        // "	FragColor = vec4(1.0, 1.0, 1.0, 1.0);//texture(u_sampler, out_texcoord);" +
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Error: Fragment shader compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Fragment Shader: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("RTR: Error: Shader Program compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Shader Program: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        //samplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uSampler");
        modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uModelMatrix");
        viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uViewMatrix");
        projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uProjectionMatrix");
        
		lKeyPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uLKeyPressed");
		laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uLa");
        ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uLd");
        lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uLs");
        
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uKa");
        kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uKd");
        ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uKs");
        materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uMaterialShininess");

        lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uLightPosition");

        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;


        final float[] cubePCNT = new float[]
		{
			1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
			-1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
			-1.0f, 1.0f,1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
			1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
			-1.0f, -1.0f,-1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,
			1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
			-1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			-1.0f, 1.0f,-1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			-1.0f, -1.0f,-1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
			1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
			1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f
		};

        GLES32.glGenVertexArrays(1, vao_cube, 0);
        GLES32.glBindVertexArray(vao_cube[0]);

        GLES32.glGenBuffers(1, vbo_cube, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubePCNT.length * 4); // float
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
        positionBuffer.put(cubePCNT);
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubePCNT.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT , false, 11*4, 0*4);
        // 3: xyx, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT , false, 11*4, 3*4);
        // 3: xyx, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT , false, 11*4, 6*4);
        // 3: xyx, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, 2, GLES32.GL_FLOAT , false, 11*4, 9*4);
        // 3: xyx, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

        GLES32.glBindVertexArray(0);

        texture_kundali[0] = loadGLTexture(R.raw.teapot); // Physical file name without extension
        GLES32.glEnable(GLES32.GL_TEXTURE_2D);

        //GLES32.glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
        GLES32.glEnable(GLES32.GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        System.out.println("RTR: initialize() successful.");
    }

    private void resize(int width, int height)
    {
        if(height == 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0,
                45.0f,
                (float)width/(float)height,
                0.1f,
                100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm

        float[] modelMatrix = new float[16];
        float[] viewMatrix = new float[16];
        float[] projectionMatrix = new float[16];
        float[] roationMatrix = new float[16];
        float[] translationMatrix = new float[16];
        float[] scaleMatrix = new float[16];

        // ================================================

        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(viewMatrix, 0);
        Matrix.setIdentityM(projectionMatrix, 0);
        Matrix.setIdentityM(roationMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(scaleMatrix, 0);

        Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -4.0f);
        Matrix.rotateM(roationMatrix, 0, angleCube, 1.0f, 1.0f, 1.0f);
        Matrix.scaleM(scaleMatrix,0,0.85f,0.85f,0.85f);

        Matrix.multiplyMM(modelMatrix,0, modelMatrix,0, translationMatrix,0);
        Matrix.multiplyMM(modelMatrix,0, modelMatrix,0, roationMatrix,0);
        Matrix.multiplyMM(modelMatrix,0, modelMatrix,0, scaleMatrix,0);
        Matrix.multiplyMM(projectionMatrix,0, projectionMatrix,0, perspectiveProjectionMatrix,0);

        GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
        GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
        GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, projectionMatrix, 0);

        // ==== Work with texture now ABU ====
        GLES32.glActiveTexture(GLES32.GL_TEXTURE0); // matches to our AMC_ATTRIBUTE_TEXCOORD0
        // texture unit. 80 supported
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_kundali[0]);
        GLES32.glUniform1i(samplerUniform, 0); // GL_TEXTURE0 zeroth unit

        if(bLight)
		{
            GLES32.glUniform1i(lKeyPressedUniform, 1);
            GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);

            GLES32.glUniform3fv(laUniform, 1, lightAmbient, 0);
            GLES32.glUniform3fv(ldUniform, 1, lightDiffuse, 0);
            GLES32.glUniform3fv(lsUniform, 1, lightSpecular, 0);

            GLES32.glUniform3fv(kaUniform, 1, materialAmbient, 0);
            GLES32.glUniform3fv(kdUniform, 1, materialDiffuse, 0);
            GLES32.glUniform3fv(ksUniform, 1, materialSpecular, 0);
            GLES32.glUniform1f(materialShininessUniform, materialShininess);
        }
		else
		{
            GLES32.glUniform1i(lKeyPressedUniform, 0);
        }


        GLES32.glBindVertexArray(vao_cube[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);

        requestRender(); // fn of Base Class like swapBuffers
    }

    private void uninitialize()
    {
        if (vbo_cube[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_cube, 0);
            vbo_cube[0] = 0;
        }
        if (vao_cube[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_cube, 0);
            vao_cube[0] = 0;
        }

        if (shaderProgramObject != 0)
        {
            int[] shaderCount = new int[1];
            int shaderNo;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];
            GLES32.glGetAttachedShaders(shaderProgramObject,
                    shaderCount[0],
                    shaderCount, 0,
                    shaders, 0);

            for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++)
            {
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNo]);
                GLES32.glDeleteShader(shaders[shaderNo]);
                shaders[shaderNo] = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
            shaderProgramObject = 0;
            GLES32.glUseProgram(0);
        }

    }

    private int loadGLTexture(int imageFileResourceId)
    {
        int[] texture = new int[1];
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceId, options);

        GLES32.glGenTextures(1, texture, 0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);

        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 4); //RGBA
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0); //glTextImage2D mipmap level, 345 789, border level

        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D); // new addition
        // no explicit unbind in FFP, must in PP
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

        System.out.println("RTR: loadGLTexture ID = " + texture[0]);
        return texture[0];
    }

    void update()
    {
        // Rotate vice versa
            angleCube += 1.0f;
            if (angleCube > 360.0f)
            {
                angleCube = 0.0f;
            }
    }

}


/**
Microsoft Windows [Version 10.0.17763.1098]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Users\shree>cd C:\RTR\Android\PP\20-Interleaved

C:\RTR\Android\PP\20-Interleaved>gradlew clean
Starting a Gradle Daemon, 1 incompatible and 1 stopped Daemons could not be reused, use --status for details

BUILD SUCCESSFUL in 16s
2 actionable tasks: 2 executed
C:\RTR\Android\PP\20-Interleaved>gradlew build

> Task :app:lint
Ran lint on variant debug: 8 issues found
Ran lint on variant release: 8 issues found
Wrote HTML report to file:///C:/RTR/Android/PP/20-Interleaved/app/build/reports/lint-results.html
Wrote XML report to file:///C:/RTR/Android/PP/20-Interleaved/app/build/reports/lint-results.xml

Deprecated Gradle features were used in this build, making it incompatible with Gradle 6.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/5.1.1/userguide/command_line_interface.html#sec:command_line_warnings

BUILD SUCCESSFUL in 39s
51 actionable tasks: 51 executed
<-------------> 0% WAITING
> IDLE
> IDLE
> IDLE
> IDLE
C:\RTR\Android\PP\20-Interleaved>adb -d install -r app\build\outputs\apk\debug\app-debug.apk
* daemon not running; starting now at tcp:5037
* daemon started successfully
Performing Streamed Install
adb: failed to install app\build\outputs\apk\debug\app-debug.apk:
Exception occurred while executing:
android.os.ParcelableException: java.io.IOException: Requested internal only, but not enough space
        at android.util.ExceptionUtils.wrap(ExceptionUtils.java:34)
        at com.android.server.pm.PackageInstallerService.createSession(PackageInstallerService.java:596)
        at com.android.server.pm.PackageManagerShellCommand.doCreateSession(PackageManagerShellCommand.java:1350)
        at com.android.server.pm.PackageManagerShellCommand.runInstall(PackageManagerShellCommand.java:199)
        at com.
C:\RTR\Android\PP\20-Interleaved>adb -d install -r app\build\outputs\apk\debug\app-debug.apk
Performing Streamed Install
adb: failed to install app\build\outputs\apk\debug\app-debug.apk:
Exception occurred while executing:
android.os.ParcelableException: java.io.IOException: Requested internal only, but not enough space
        at android.util.ExceptionUtils.wrap(ExceptionUtils.java:34)
        at com.android.server.pm.PackageInstallerService.createSession(PackageInstallerService.java:596)
        at com.android.server.pm.PackageManagerShellCommand.doCreateSession(PackageManagerShellCommand.java:1350)
        at com.android.server.pm.PackageManagerShellCommand.runInstall(PackageManagerShellCommand.java:199)
        at com.
C:\RTR\Android\PP\20-Interleaved>adb -d install -r app\build\outputs\apk\debug\app-debug.apk
Performing Streamed Install
adb: failed to install app\build\outputs\apk\debug\app-debug.apk:
C:\RTR\Android\PP\20-Interleaved>adb -d install -r app\build\outputs\apk\debug\app-debug.apk
Performing Streamed Install
Success

C:\RTR\Android\PP\20-Interleaved>
**/