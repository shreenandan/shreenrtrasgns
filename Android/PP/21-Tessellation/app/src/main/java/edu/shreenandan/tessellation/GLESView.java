package edu.shreenandan.tessellation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Build;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;

import androidx.annotation.RequiresApi;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

// asa view jyacha surface OGL support karato
// x -> Extension, J2ME, GL10: OGL 10th extension, for basic features of OGL
// 3 For OGL Buffers

public class GLESView extends GLSurfaceView
        implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private GestureDetector gestureDetector;
    private final Context context;

    // java neither has uint nor GLuint
    private int fragmentShaderObject;
    private int vertexShaderObject;
    private int tessellationControlShaderObject;
    private int tessellationEvaluationShaderObject;
    private int shaderProgramObject;

    // As java does not have address operator we declare array of object and pass name of array.
    private int[] vao = new int[1];
    private int[] vboPosition = new int[1];
    private int[] vboColor = new int[1];
    int numberOfLineSegments;
    //  private int[] texture_smiley = new int[1];


    private int mvpUniform, numberOfSegmentsUniform, numberOfStripsUniform, lineColorUniform;
    private float[] perspectiveProjectionMatrix = new float[16];

    private float angleCube = 0.0f;
    private float anglePyramid = 0.0f;
    private boolean ascendingPyramid = true;
    private boolean ascendingCube = true;

    // constructor
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;
        // not useful now, will be helpful in texture

        // param 2: Listener; param 3: Handler, null = no other handler, this listner class obj himself is handler; param 4: always false
        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

        // call 3 functions of GLES
        setEGLContextClientVersion(3); // 3.x
        // will give highest supported to 3, ie 3.2

        setRenderer(this); // onDrawFrame() - display()
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE // other is RENDERMODE_CONTINUOUSLY
        // https://developer.android.com/training/graphics/opengl/motion.html?
        // https://developer.android.com/reference/android/opengl/GLSurfaceView
    }

    // Handling 'onTouchEvent' is the most important,
    // Because it triggers all gesture and tap events
    @Override
    public boolean onTouchEvent(MotionEvent me)
    {
        // below line is not useful in this pgm
        // useful in case of phone's keyboard actions
        int eventaction = me.getAction();

        // if not my event, then delegate to parent
        if(!gestureDetector.onTouchEvent(me))
        {
            super.onTouchEvent(me);
        }

        return(true);
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    // it is more reliable than onDoubleTapEvent
    @Override
    public boolean onDoubleTap(MotionEvent me)
    {
        numberOfLineSegments--;
        if (numberOfLineSegments <= 0)
        {
            numberOfLineSegments = 1;
        }
        return(true);
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    // less reliable, start, leave touch points
    @Override
    public boolean onDoubleTapEvent(MotionEvent me)
    {
        // do not write code here because handled in 'onDoubleTap'
        return(true);
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent me)
    {
        numberOfLineSegments++;
        if (numberOfLineSegments >= 50)
        {
            numberOfLineSegments = 50;
        }
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public boolean onDown(MotionEvent me)
    {
        // do not write code here because handled in 'onSingleTapConfirmed'
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY)
    {
        // fling means swipe
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public void onLongPress(MotionEvent me)
    {
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY)
    {
        uninitialize();
        System.exit(0);
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public void onShowPress(MotionEvent me)
    {
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent me)
    {
        return(true);
    }


    // implement 3 functions of GLSurfaceView.Renderer
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_VENDOR);
        System.out.println("RTR: GLES32.GL_VENDOR: " + version);

        version = gl.glGetString(GLES32.GL_RENDERER);
        System.out.println("RTR: GLES32.GL_RENDERER: " + version);

        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
        update();
    }

    // our custom methods
    private void initialize()
    {
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
                        "#version 320 es" +
                        "\n" +
                        "in vec2 vPosition;" +
                        "void main (void)" +
                        "{" +
						"gl_PointSize = 5.0;" +
                        "gl_Position = vec4(vPosition, 0.0, 1.0);" +
                        "}"
        );
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR: Error: Vertex shader compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Vertex Shader: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;


        //Tessellation Control Shader
        tessellationControlShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_CONTROL_SHADER);
        final String tessellationControlShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "layout(vertices = 4)out;"+
                        "uniform int numberOfSegments;"+
                        "uniform int numberOfStrips;"+
                        "void main (void)" +
                        "{" +
                        "gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" +
                        "gl_TessLevelOuter[0] = float(numberOfStrips);"+
                        "gl_TessLevelOuter[1] = float(numberOfSegments);"+
                        "}"

        );
        GLES32.glShaderSource(tessellationControlShaderObject, tessellationControlShaderSourceCode);
        GLES32.glCompileShader(tessellationControlShaderObject);

        // Error checking
        GLES32.glGetShaderiv(tessellationControlShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(tessellationControlShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(tessellationControlShaderObject);
                System.out.println("RTR: Error: tessellationControlShader compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: tessellationControlShader: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: tessellationControlShader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;


        //Tessellation Evaluation Shader
        tessellationEvaluationShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_EVALUATION_SHADER);
        final String tessellationEvaluationShaderSourceCode = String.format(
                        "#version 320 es" +
                        "\n" +
                        "layout(isolines)in;" +
                        "uniform mat4 uMVPMatrix;" +
                        "void main (void)" +
                        "{" +
                        "float u = gl_TessCoord.x;" +
                        "vec3 p0 = gl_in[0].gl_Position.xyz;" +
                        "vec3 p1 = gl_in[1].gl_Position.xyz;" +
                        "vec3 p2 = gl_in[2].gl_Position.xyz;" +
                        "vec3 p3 = gl_in[3].gl_Position.xyz;" +
                        "float u1 = (1.0 -u);" +
                        "float u2 = u * u;" +
                        "float b3 = u2 * u;" +
                        "float b2 = 3.0 * u2 * u1;" +
                        "float b1 = 3.0 * u * u1 * u1;" +
                        "float b0 = u1 * u1 * u1;"+
                        "vec3 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;" +
                        "gl_Position = uMVPMatrix * vec4(p, 1.0);" +
						"gl_PointSize = 5.0;" +
                        "}"

        );
        GLES32.glShaderSource(tessellationEvaluationShaderObject, tessellationEvaluationShaderSourceCode);
        GLES32.glCompileShader(tessellationEvaluationShaderObject);

        // Error checking
        GLES32.glGetShaderiv(tessellationEvaluationShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(tessellationEvaluationShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(tessellationEvaluationShaderObject);
                System.out.println("RTR: Error: tessellationEvaluationShader compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: tessellationEvaluationShader: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: tessellationEvaluationShader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;


        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode = String.format(
                        "#version 320 es" +
                        "\n" +
                        "precision highp float;" +
                        "uniform vec4 lineColor;" +
                        "out vec4 FragColor;" +
                        "void main (void)" +
                        "{" +
                        "FragColor = lineColor;" +
                        "}"
        );
        // "	FragColor = vec4(1.0, 1.0, 1.0, 1.0);//texture(u_sampler, out_texcoord);" +
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Error: Fragment shader compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Fragment Shader: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, tessellationControlShaderObject);
        GLES32.glAttachShader(shaderProgramObject, tessellationEvaluationShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        //GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("RTR: Error: Shader Program compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Shader Program: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uMVPMatrix");
        numberOfSegmentsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "numberOfSegments");
        numberOfStripsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "numberOfStrips");
        lineColorUniform = GLES32.glGetUniformLocation(shaderProgramObject, "lineColor");

        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        // Generation
        //============== CUBE ====================
        // Top right, Top left, Bottom left, bottom right;
        // Top, Bottom, Front, Back, Right, Left
        final float[] vertices = new float[]
		{
			-1.0f, -1.0f, -0.5f,  1.0f, 0.5f,  -1.0f, 1.0f, 1.0f
		};

        GLES32.glGenVertexArrays(1, vao, 0);
        GLES32.glBindVertexArray(vao[0]);

        GLES32.glGenBuffers(1, vboPosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4); // float
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
        positionBuffer.put(vertices);
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, vertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 2, GLES32.GL_FLOAT, false, 0, 0);
        // 3: xyx, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind

        // GLES32.glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
        GLES32.glEnable(GLES32.GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		// GLES32.glEnable(GLES32.GL_PROGRAM_POINT_SIZE);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        System.out.println("RTR: initialize() successful.");
    }

    private void resize(int width, int height)
    {
        if(height == 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0,
                45.0f,
                (float)width/(float)height,
                0.1f,
                100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        float yellowColor[] = {1.0f,1.0f,0.0f,1.0f};
        float greenColor[] = {0.0f,1.0f,0.0f,1.0f}; //for 0 to 25
        float redColor[] = {1.0f,0.0f,0.0f,1.0f}; //above 25

        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -4.0f);
        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
        GLES32.glUniform1i(numberOfSegmentsUniform, numberOfLineSegments);
        GLES32.glUniform1i(numberOfStripsUniform, 1);
        //GLES32.glUniform4fv(lineColorUniform, 1, yellowColor, 0);

        if(numberOfLineSegments >=0 && numberOfLineSegments <=16)
        {
            GLES32.glUniform4fv(lineColorUniform, 1,redColor,0);
        }
        else if(numberOfLineSegments > 16 && numberOfLineSegments <= 32)
        {
            GLES32.glUniform4fv(lineColorUniform, 1,yellowColor,0);
        }
        else if(numberOfLineSegments > 32 && numberOfLineSegments <=50)
        {
            GLES32.glUniform4fv(lineColorUniform, 1,greenColor,0);
        }

        GLES32.glBindVertexArray(vao[0]);
        GLES32.glPatchParameteri(GLES32.GL_PATCH_VERTICES, 4); // this step is imp to give propper shape to curve
        GLES32.glDrawArrays(GLES32.GL_PATCHES, 0, 4);

        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);

        requestRender(); // fn of Base Class like swapBuffers
    }

    private void uninitialize()
    {
        if (vboColor[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboColor, 0);
            vboColor[0] = 0;
        }
        if (vboPosition[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboPosition, 0);
            vboPosition[0] = 0;
        }
        if (vao[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao, 0);
            vao[0] = 0;
        }

        if (shaderProgramObject != 0)
        {
            int[] shaderCount = new int[1];
            int shaderNo;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];
            GLES32.glGetAttachedShaders(shaderProgramObject,
                    shaderCount[0],
                    shaderCount, 0,
                    shaders, 0);

            for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++)
            {
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNo]);
                GLES32.glDeleteShader(shaders[shaderNo]);
                shaders[shaderNo] = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
            shaderProgramObject = 0;
            GLES32.glUseProgram(0);
        }

    }

    void update()
    {

    }

}

/*
c:\RTR\Android\PP\21-Tessellation>gradlew clean

BUILD SUCCESSFUL in 3s
2 actionable tasks: 2 executed
c:\RTR\Android\PP\21-Tessellation>gradlew build

> Task :app:lint
Ran lint on variant debug: 8 issues found
Ran lint on variant release: 8 issues found
Wrote HTML report to file:///C:/RTR/Android/PP/21-Tessellation/app/build/reports/lint-results.html
Wrote XML report to file:///C:/RTR/Android/PP/21-Tessellation/app/build/reports/lint-results.xml

Deprecated Gradle features were used in this build, making it incompatible with Gradle 6.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/5.1.1/userguide/command_line_interface.html#sec:command_line_warnings

BUILD SUCCESSFUL in 15s
51 actionable tasks: 50 executed, 1 up-to-date
c:\RTR\Android\PP\21-Tessellation>adb -d install -r app\build\outputs\apk\debug\app-debug.apk
Performing Streamed Install
Success

c:\RTR\Android\PP\21-Tessellation>adb logcat | findstr /i rtr:
04-18 23:00:11.798 10973 10973 I System.out: RTR: in create
04-18 23:00:11.883 10973 10996 I System.out: RTR: GL10.GL_VERSION: OpenGL ES 3.2 V@269.0 (GIT@33f3a03, I26dffed9a4) (Date:04/17/18)
04-18 23:00:11.884 10973 10996 I System.out: RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: OpenGL ES GLSL ES 3.20
04-18 23:00:11.884 10973 10996 I System.out: RTR: GLES32.GL_VENDOR: Qualcomm
04-18 23:00:11.884 10973 10996 I System.out: RTR: GLES32.GL_RENDERER: Adreno (TM) 530
04-18 23:00:11.890 10973 10996 I System.out: RTR: Vertex Shader compiled successfully.
04-18 23:00:11.892 10973 10996 I System.out: RTR: Error: tessellationControlShader compilation log: ERROR: 0:2: 'gl_PointSize' :  no such field in structure
04-18 23:00:14.569 11029 11029 I System.out: RTR: in create
04-18 23:00:14.856 11029 11029 I System.out: RTR: in create
04-18 23:00:14.957 11029 11051 I System.out: RTR: GL10.GL_VERSION: OpenGL ES 3.2 V@269.0 (GIT@33f3a03, I26dffed9a4) (Date:04/17/18)
04-18 23:00:14.958 11029 11051 I System.out: RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: OpenGL ES GLSL ES 3.20
04-18 23:00:14.958 11029 11051 I System.out: RTR: GLES32.GL_VENDOR: Qualcomm
04-18 23:00:14.958 11029 11051 I System.out: RTR: GLES32.GL_RENDERER: Adreno (TM) 530
04-18 23:00:14.967 11029 11051 I System.out: RTR: Vertex Shader compiled successfully.
04-18 23:00:14.968 11029 11051 I System.out: RTR: Fragment Shader compiled successfully.
04-18 23:00:14.969 11029 11051 I System.out: RTR: Shader Program compiled successfully.
04-18 23:00:15.002 11029 11051 I System.out: RTR: loadGLTexture ID = 2
04-18 23:00:15.012 11029 11051 I System.out: RTR: loadGLTexture ID = 3
04-18 23:00:15.012 11029 11051 I System.out: RTR: initialize() successful.
04-18 23:00:20.343 11074 11074 I System.out: RTR: in create
04-18 23:00:20.611 11074 11074 I System.out: RTR: in create
04-18 23:00:20.711 11074 11096 I System.out: RTR: GL10.GL_VERSION: OpenGL ES 3.2 V@269.0 (GIT@33f3a03, I26dffed9a4) (Date:04/17/18)
04-18 23:00:20.711 11074 11096 I System.out: RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: OpenGL ES GLSL ES 3.20
04-18 23:00:20.711 11074 11096 I System.out: RTR: GLES32.GL_VENDOR: Qualcomm
04-18 23:00:20.711 11074 11096 I System.out: RTR: GLES32.GL_RENDERER: Adreno (TM) 530
04-18 23:00:20.718 11074 11096 I System.out: RTR: Vertex Shader compiled successfully.
04-18 23:00:20.720 11074 11096 I System.out: RTR: Error: tessellationControlShader compilation log: ERROR: 0:2: 'gl_PointSize' :  no such field in structure
04-18 23:02:48.916 11381 11381 I System.out: RTR: in create
04-18 23:02:49.281 11381 11381 I System.out: RTR: in create
04-18 23:02:49.394 11381 11403 I System.out: RTR: GL10.GL_VERSION: OpenGL ES 3.2 V@269.0 (GIT@33f3a03, I26dffed9a4) (Date:04/17/18)
04-18 23:02:49.394 11381 11403 I System.out: RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: OpenGL ES GLSL ES 3.20
04-18 23:02:49.394 11381 11403 I System.out: RTR: GLES32.GL_VENDOR: Qualcomm
04-18 23:02:49.394 11381 11403 I System.out: RTR: GLES32.GL_RENDERER: Adreno (TM) 530
04-18 23:02:49.401 11381 11403 I System.out: RTR: Vertex Shader compiled successfully.
04-18 23:02:49.405 11381 11403 I System.out: RTR: tessellationControlShader compiled successfully.
04-18 23:02:49.412 11381 11403 I System.out: RTR: tessellationEvaluationShader compiled successfully.
04-18 23:02:49.415 11381 11403 I System.out: RTR: Fragment Shader compiled successfully.
04-18 23:02:49.442 11381 11403 I System.out: RTR: Shader Program compiled successfully.
04-18 23:02:49.444 11381 11403 I System.out: RTR: initialize() successful.

c:\RTR\Android\PP\21-Tessellation>
*/