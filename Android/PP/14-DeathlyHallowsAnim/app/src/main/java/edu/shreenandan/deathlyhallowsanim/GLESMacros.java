package edu.shreenandan.deathlyhallowsanim;

public class GLESMacros
{
    public static final int AMC_ATTRIBUTE_POSITION = 0;
    public static final int AMC_ATTRIBUTE_COLOR = 1;
    public static final int AMC_ATTRIBUTE_NORMAL = 2;
    public static final int AMC_ATTRIBUTE_TEXCOORD0 = 3;
}
