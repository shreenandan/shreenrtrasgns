package edu.shreenandan.deathlyhallowsanim;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

// asa view jyacha surface OGL support karato
import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // opengl ver 3.2
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
// x -> Extension, J2ME, GL10: OGL 10th extension, for basic features of OGL

// 3 For OGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView
implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	public class Triplet
	{
		public float x = 0.0f;
		public float y = 0.0f;
		public float z = 0.0f;
		
		Triplet() {}
		
		Triplet(float X, float Y, float Z)
		{
			x = X;
			y = Y;
			z = Z;
		}		
	}
	
	private Triplet translationForLine = new Triplet(0.0f, 0.50f, 0.0f);
	private Triplet translationForCircle = new Triplet(-1.0f, -0.50f, 0.0f);
	private Triplet translationForTriangle = new Triplet(1.0f, -0.50f, 0.0f);
	private float rotationAngle = 0.0f;
	
	private GestureDetector gestureDetector;
	private final Context context;
	
	// java neither has uint nor GLuint
	private int fragmentShaderObject;
	private int vertexShaderObject;
	private int shaderProgramObject;
	
	// As java does not have address operator we declare array of object and pass name of array.
	private int[] vao_graph = new int[1];
	private int[] vbo_graph_position = new int[1];
	
	private int mvpUniform;
	private float[] perspectiveProjectionMatrix = new float[16];
	
	private final int linesToDraw = 20;
	private FloatBuffer positionBuffer;
	final float[] lineXAxis = new float[]
	{
		-1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f
	};
	final float[] lineYAxis = new float[]
	{
		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f
	};
	float[] lineCoords = new float[]
	{
		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f
	};
	
	private final float magnitude = 0.5f;
	ByteBuffer byteBuffer;
	private boolean showGraph = false;
	float[] genericTriangleCoords = new float[]
	{
		0.0f, magnitude, 0.0f,
		-magnitude, -magnitude, 0.0f,
		magnitude, -magnitude, 0.0f
	};
	private int[] vao_triangle = new int[1];
	private int[] vbo_triangle_position = new int[1];

	private int[] vao_rectangle = new int[1];
	private int[] vbo_rectangle_position = new int[1];
	float[] genericRectangleCoords = new float[]
	{
		magnitude, magnitude, 0.0f,
		-magnitude, magnitude, 0.0f,
		-magnitude, -magnitude, 0.0f,
		magnitude, -magnitude, 0.0f
	};
	
	private int[] vao_incircle = new int[1];
	private int[] vbo_incircle_position = new int[1];
	private float sideA = 0.0f, sideB = 0.0f, sideC = 0.0f;
	private float perimeter = 0.0f, halfPerimeter = 0.0f;
	private float incircleOx = 0.0f, incircleOy = 0.0f;
	private float radius = 0.0f;
	private float outerCircleOx = 0.0f, outerCircleOy = 0.0f;
	
	private int[] vao_outercircle = new int[1];
	private int[] vbo_outercircle_position = new int[1];
	
	private int arrLengthIncircle;
	private int arrLengthOuterCircle;
	
	private int[] vao_centerline = new int[1];
	private int[] vbo_centerline_position = new int[1];
	
	// constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		// not useful now, will be helpful in texture
		
		// param 2: Listener; param 3: Handler, null = no other handler, this listner class obj himself is handler; param 4: always false
		gestureDetector = new GestureDetector(drawingContext, this, null, false);		
		gestureDetector.setOnDoubleTapListener(this);

		// call 3 functions of GLES
		setEGLContextClientVersion(3); // 3.x
		// will give highest supported to 3, ie 3.2

		setRenderer(this); // onDrawFrame() - display()
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE
	}
	
	// Handling 'onTouchEvent' is the most important,
	// Because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent me)
	{
		// below line is not useful in this pgm
		// useful in case of phone's keyboard actions
		int eventaction = me.getAction();
		
		// if not my event, then delegate to parent
		if(!gestureDetector.onTouchEvent(me))
		{
			super.onTouchEvent(me);
		}
		
		return(true);
	}
	
	// abstract method from OnDoubleTapListener, so must be implemented
	// it is more reliable than onDoubleTapEvent
	@Override
	public boolean onDoubleTap(MotionEvent me)
	{
		showGraph = !showGraph;
		return(true);
	}
	
	// abstract method from OnDoubleTapListener, so must be implemented
	// less reliable, start, leave touch points
	@Override
	public boolean onDoubleTapEvent(MotionEvent me)
	{
		// do not write code here because handled in 'onDoubleTap'
		return(true);
	}
	
	// abstract method from OnDoubleTapListener, so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent me)
	{
		return(true);
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public boolean onDown(MotionEvent me)
	{
		// do not write code here because handled in 'onSingleTapConfirmed'
		return(true);
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY)
	{
		// fling means swipe
		return(true);
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public void onLongPress(MotionEvent me)
	{
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public void onShowPress(MotionEvent me)
	{
	}
	
	// abstract method from OnDoubleTapListener, so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent me)
	{
		return(true);
	}
	

	// implement 3 functions of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: GL10.GL_VERSION: " + version);

		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

		version = gl.glGetString(GLES32.GL_VENDOR);
		System.out.println("RTR: GLES32.GL_VENDOR: " + version);
		
		version = gl.glGetString(GLES32.GL_RENDERER);
		System.out.println("RTR: GLES32.GL_RENDERER: " + version);

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
		update();
	}


	// our custom methods
	private void initialize()
	{		
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"uniform mat4 u_mvp_matrix;" +
			"in vec4 vColor;" + 
			"out vec4 out_color;" + 
			"void main(void)" +
			"{" +
			"	gl_Position = u_mvp_matrix * vPosition;" +
			"	out_color = vColor;" + 
			"}"
		);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		
		// Error checking
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: Error: Vertex shader compilation log: " + szInfoLog);
			}
			else
			{
				System.out.println("RTR: Vertex Shader: Something went wrong, infoLogLength is zero...");
			}
			uninitialize();
			System.exit(0);
		}
		else
		{
			System.out.println("RTR: Vertex Shader compiled successfully.");
		}
		
		// reset flags
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"out vec4 FragColor;" +
			"in vec4 out_color;" +
			"void main(void)" +
			"{" +
			"	FragColor = out_color;" +
			"}"
		);
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		
		// Error checking		
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: Error: Fragment shader compilation log: " + szInfoLog);
			}
			else
			{
				System.out.println("RTR: Fragment Shader: Something went wrong, infoLogLength is zero...");
			}
			uninitialize();
			System.exit(0);
		}
		else
		{
			System.out.println("RTR: Fragment Shader compiled successfully.");
		}
		
		// reset flags
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		shaderProgramObject = GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		// Pre Link Attribute Binding
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
		GLES32.glLinkProgram(shaderProgramObject);
		
		// Error checking
		int[] iProgramLinkStatus = new int[1];
		
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("RTR: Error: Shader Program compilation log: " + szInfoLog);				
			}
			else
			{
				System.out.println("RTR: Shader Program: Something went wrong, infoLogLength is zero...");
			}
			uninitialize();
			System.exit(0);
		}
		else
		{
			System.out.println("RTR: Shader Program compiled successfully.");
		}
		
		// Post Link Uniform Location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		
		// reset flags
		iProgramLinkStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		
		// Generation
		GLES32.glGenVertexArrays(1, vao_graph, 0);
		GLES32.glBindVertexArray(vao_graph[0]);
		
		GLES32.glGenBuffers(1, vbo_graph_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graph_position[0]);
		
		// convert the array which we pass to glBufferData()
		// Unlike Win, XWin. In C,C++ array name is iteself pointer
		byteBuffer = ByteBuffer.allocateDirect(lineXAxis.length * 4); // float
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(lineXAxis);
		positionBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineXAxis.length * 4, null, GLES32.GL_DYNAMIC_DRAW); 
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		// 3: xyx, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 1.0f, 0.0f, 0.0f); // NOTE: Constant color for all
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind
		GLES32.glBindVertexArray(0);
		
		initVaoTriangle();
		initIncircle();
		initCenterLine();
		
		// GLES32.glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
		GLES32.glEnable(GLES32.GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		System.out.println("RTR: initialize() successful.");
	}

	private void resize(int width, int height)
	{
		if(height == 0)
		{
			height = 1;
		}
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 
			45.0f, 
			(float)width/(float)height, 
			0.1f, 
			100.0f);
	}

	private void initVaoTriangle()
	{
		bindVaoVbo(vao_triangle, vbo_triangle_position);
		// fillBufferDataWithSameColor(genericTriangleCoords, 3, true, GLESMacros.AMC_ATTRIBUTE_POSITION, 1.0f, 1.0f, 0.0f);
		fillBufferData(genericTriangleCoords, 3, true, GLESMacros.AMC_ATTRIBUTE_POSITION);
		unbindVboVao();
	}
	
	private void initVaoRectangle()
	{
		bindVaoVbo(vao_rectangle, vbo_rectangle_position);
		fillBufferData(genericRectangleCoords, 3, true, GLESMacros.AMC_ATTRIBUTE_POSITION);
		unbindVboVao();
	}
	
	private void initIncircle()
	{
		float[] sides = new float[]{sideA, sideB, sideC};
		float[] incircleOxy = new float[]{incircleOx, incircleOy};
		System.out.println("RTR: Before sideA:" + sideA + ", sideB:" + sideB + ", sideC:" + sideC);
		perimeter = getSidesAndPerimeterFromTriangle(magnitude, sides);
		halfPerimeter = perimeter / 2.0f;
		
		sideA = sides[0];
		sideB = sides[1];
		sideC = sides[2];
		System.out.println("RTR: After sideA:" + sideA + ", sideB:" + sideB + ", sideC:" + sideC);
		System.out.println("RTR: perimeter:" + perimeter);
		System.out.println("RTR: halfPerimeter:" + halfPerimeter);
		
		getIncircleOrigin(perimeter, sideA, -magnitude, -magnitude, sideB, magnitude, -magnitude, sideC, 0, magnitude, incircleOxy);
		radius = getIncircleRadius(halfPerimeter, sideA, sideB, sideC);
		
		incircleOx = incircleOxy[0];
		incircleOy = incircleOxy[1];
		System.out.println("RTR: After incircleOx:" + incircleOx + ", incircleOy:" + incircleOy);
		System.out.println("RTR: radius:" + radius);
		
		arrLengthIncircle = (int) Math.round(((3.142 * 2) / 0.001f) + 1.0f);
		float[] incircleCoords = new float[arrLengthIncircle * 3];
		
		int i = 0;
		for (float angle = 0.0f; angle <= 2.0f * 3.142; angle += 0.001f)
		{
			incircleCoords[i++] = (float)(Math.cos(angle) * radius);
			incircleCoords[i++] = (float)(Math.sin(angle) * radius);
			incircleCoords[i++] = 0.0f;
		}
		
		bindVaoVbo(vao_incircle, vbo_incircle_position);
		fillBufferData(incircleCoords, 3, true, GLESMacros.AMC_ATTRIBUTE_POSITION);
		unbindVboVao();
	}
	
	float getSidesAndPerimeterFromTriangle(float size, float sides[])
	{
		sides[0] = (float)Math.sqrt(Math.pow((0.0f - size), 2) + Math.pow((size - (-size)), 2));
		sides[1] = (float)Math.sqrt(Math.pow((0.0f - (-size)), 2) + Math.pow((size - (-size)), 2));
		sides[2] = (float)Math.sqrt(Math.pow(((-size) - size), 2) + Math.pow(((-size) - (-size)), 2)); // Apex
		System.out.println("RTR: within sideA:" + sides[0] + ", sideB:" + sides[1] + ", sideC:" + sides[2]);
		return (sides[0] + sides[1] + sides[2]);
	}
	
	void getIncircleOrigin(float perimeter, float sideA, float Ax, float Ay, float sideB, float Bx, float By, float sideC, float Cx, float Cy, float[] incircleOxy)
	{
		incircleOxy[0] = ((sideA * Ax) + (sideB * Bx) + (sideC * Cx)) / perimeter;
		incircleOxy[1] = ((sideA * Ay) + (sideB * By) + (sideC * Cy)) / perimeter;
		System.out.println("RTR: within incircleOx:" + incircleOxy[0] + ", incircleOy:" + incircleOxy[1]);
	}

	float getIncircleRadius(float halfPerimeter, float sideA, float sideB, float sideC)
	{
		float area = 0.0f;
		area = (float)(Math.sqrt(halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC)));
		System.out.println("RTR: area:" + area);
		System.out.println("RTR: (area / halfPerimeter):" + (area / halfPerimeter));
		System.out.println("RTR: (float)(area / halfPerimeter):" + (float)(area / halfPerimeter));
		return (area / halfPerimeter);
	}
	
	private void initOuterCircle()
	{
		float[] outerCircleOxy = new float[]{outerCircleOx, outerCircleOy};
		
		getOuterCircleOrigin(-magnitude, -magnitude, magnitude, -magnitude, magnitude, magnitude, outerCircleOxy); // or use Pythagoras theorem
		
		outerCircleOx = outerCircleOxy[0];
		outerCircleOy = outerCircleOxy[1];
		radius = (float) Math.sqrt(Math.pow((outerCircleOx - magnitude), 2) + Math.pow((outerCircleOy - (magnitude)), 2)); // or use Pythagoras theorem
		
		System.out.println("RTR: After outerCircleOx:" + outerCircleOxy[0] + ", outerCircleOy:" + outerCircleOxy[1]);
		System.out.println("RTR: radius:" + radius);
		
		arrLengthOuterCircle = (int) Math.round(((3.142 * 2) / 0.001f) + 1.0f);
		float[] circleCoords = new float[arrLengthOuterCircle * 3];
		
		int i = 0;
		for (float angle = 0.0f; angle <= 2.0f * 3.142; angle += 0.001f)
		{
			circleCoords[i++] = (float)(Math.cos(angle) * radius);
			circleCoords[i++] = (float)(Math.sin(angle) * radius);
			circleCoords[i++] = 0.0f;
		}
		
		bindVaoVbo(vao_outercircle, vbo_outercircle_position);
		fillBufferData(circleCoords, 3, true, GLESMacros.AMC_ATTRIBUTE_POSITION);
		unbindVboVao();
	}
	
	void getOuterCircleOrigin(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float[] outerCircleOxy)
	{
		// Origin of the circle passing through 3 points
		float theD = 2.0f * ((Ax*(By - Cy)) + (Bx * (Cy - Ay)) + (Cx * (Ay - By)));
		outerCircleOxy[0] = ((((Ax*Ax) + (Ay*Ay)) * (By - Cy)) + (((Bx*Bx) + (By*By)) * (Cy - Ay)) + (((Cx*Cx) + (Cy*Cy)) * (Ay - By))) / theD;
		outerCircleOxy[1] = ((((Ax*Ax) + (Ay*Ay)) * (Cy - By)) + (((Bx*Bx) + (By*By)) * (Ay - Cy)) + (((Cx*Cx) + (Cy*Cy)) * (By - Ay))) / theD;
	}
	
	private void initCenterLine()
	{
		// midpoint = (x1+x2)/2, (y1+y2)/2
		// midpoint of side opposite to apex
		float midPointX = (-magnitude + magnitude) / 2.0f;
		float midPointY = (-magnitude + -magnitude) / 2.0f;
		
		lineCoords[0] = 0.0f; lineCoords[1] = magnitude;
		lineCoords[3] = midPointX; lineCoords[4] = midPointY;
		
		bindVaoVbo(vao_centerline, vbo_centerline_position);
		fillBufferData(lineCoords, 3, true, GLESMacros.AMC_ATTRIBUTE_POSITION);
		unbindVboVao();
	}
	
	private void bindVao(int[] theVao)
	{
		GLES32.glGenVertexArrays(1, theVao, 0);
		GLES32.glBindVertexArray(theVao[0]);
	}
	private void unbindVao()
	{
		GLES32.glBindVertexArray(0);
	}
	
	private void bindVbo(int[] theVbo)
	{
		GLES32.glGenBuffers(1, theVbo, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, theVbo[0]);
	}
	private void unbindVbo()
	{
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
	}

	private void bindVaoVbo(int[] theVao, int[] theVbo)
	{
		GLES32.glGenVertexArrays(1, theVao, 0);
		GLES32.glBindVertexArray(theVao[0]);
		
		GLES32.glGenBuffers(1, theVbo, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, theVbo[0]);
	}
	private void unbindVboVao()
	{
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);
	}

	private void fillBufferDataWithSameColor(float[] data, int noOfElementsInOneTuple, boolean isStatic, int attribTypeGLESMacro, float red, float green, float blue)
	{		
		// verify 1] pass by reference 2] common array variables
		byteBuffer = ByteBuffer.allocateDirect(data.length * 4); // float
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(data);
		positionBuffer.position(0);
		
		if(isStatic)
		{
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, data.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
		}
		else
		{
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, data.length * 4, null, GLES32.GL_DYNAMIC_DRAW);
		}
		GLES32.glVertexAttribPointer(attribTypeGLESMacro, noOfElementsInOneTuple, GLES32.GL_FLOAT, false, 0, 0);
		// 3: xyx 2: st, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
		GLES32.glEnableVertexAttribArray(attribTypeGLESMacro);
		
		// GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, red, green, blue); // NOTE: Constant color for all
		// GLES32.glEnableVertexAttrib(GLESMacros.AMC_ATTRIBUTE_COLOR);	
	}

	private void fillBufferData(float[] data, int noOfElementsInOneTuple, boolean isStatic, int attribTypeGLESMacro)
	{		
		// verify 1] pass by reference 2] common array variables
		byteBuffer = ByteBuffer.allocateDirect(data.length * 4); // float
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(data);
		positionBuffer.position(0);
		
		if(isStatic)
		{
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, data.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
		}
		else
		{
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, data.length * 4, null, GLES32.GL_DYNAMIC_DRAW);
		}
		GLES32.glVertexAttribPointer(attribTypeGLESMacro, noOfElementsInOneTuple, GLES32.GL_FLOAT, false, 0, 0);
		// 3: xyx 2: st, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
		GLES32.glEnableVertexAttribArray(attribTypeGLESMacro);
	}
	
	private void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		float[] rotationMatrix = new float[16];
		float[] translationMatrix = new float[16];

		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);

		Matrix.translateM(translationMatrix, 0, translationForTriangle.x, translationForTriangle.y, translationForTriangle.z + -2.5f);
		Matrix.setRotateM(rotationMatrix, 0, rotationAngle, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0, translationMatrix, 0, rotationMatrix, 0);
		
		Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		
		if(showGraph)
		{
			drawGraph();
		}
		drawTriangle(1.0f, 1.0f, 0.0f);
		
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		
		Matrix.translateM(modelViewMatrix, 0, translationForLine.x, translationForLine.y, translationForLine.z + -2.5f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		drawCenterLine(1.0f, 1.0f, 0.0f);		
		
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		
		Matrix.translateM(translationMatrix, 0, incircleOx + translationForCircle.x, incircleOy + translationForCircle.y, -2.5f + translationForCircle.z);
		Matrix.setRotateM(rotationMatrix, 0, rotationAngle, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0, translationMatrix, 0, rotationMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		drawCircle(vao_incircle, arrLengthIncircle, 1.0f, 1.0f, 0.0f);
		
		GLES32.glUseProgram(0);
		
		requestRender(); // fn of Base Class like swapBuffers
	}
	
	private void update()
	{
		if (translationForLine.y <= 0.0f)
		{
			if (rotationAngle <= 180.0f)
				rotationAngle += 0.25f;
		}
		else
			rotationAngle += 0.25f;

		if (rotationAngle > 360.0f)
			rotationAngle = 0.0f;

		if (translationForTriangle.x > 0.0f)
			translationForTriangle.x -= 0.0006f;
		if (translationForTriangle.y < 0.0f)
			translationForTriangle.y += 0.0003f;
		
		if (translationForLine.y > 0.0f)
			translationForLine.y -= 0.0003f; //0.0003f;
		
		if (translationForCircle.x < 0.0f)
			translationForCircle.x += 0.0006f;
		if (translationForCircle.y < 0.0f)
			translationForCircle.y += 0.0003f;
	}
	
	private void drawGraph()
	{
		int i = 0;
		float step = 1.0f / linesToDraw;
		float xcoord = step;
		float ycoord = step;
		
		GLES32.glBindVertexArray(vao_graph[0]);
		GLES32.glLineWidth(5.0f);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graph_position[0]);
		
		positionBuffer.put(lineXAxis);
		positionBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineXAxis.length * 4, positionBuffer, GLES32.GL_DYNAMIC_DRAW); 
		GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 0.0f, 1.0f, 0.0f);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		
		positionBuffer.put(lineYAxis);
		positionBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineYAxis.length * 4, positionBuffer, GLES32.GL_DYNAMIC_DRAW); 
		GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 1.0f, 0.0f, 0.0f);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glLineWidth(1.0f);
		
		for (i = 0; i < linesToDraw; i++)
		{
			lineCoords[0] = -xcoord; lineCoords[1] = 1.0f;
			lineCoords[3] = -xcoord; lineCoords[4] = -1.0f;
			
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graph_position[0]);
			
			positionBuffer.put(lineCoords);
			positionBuffer.position(0);
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineCoords.length * 4, positionBuffer, GLES32.GL_DYNAMIC_DRAW); 
			GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
			
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
			
			lineCoords[0] = xcoord; lineCoords[1] = 1.0f;
			lineCoords[3] = xcoord; lineCoords[4] = -1.0f;
			
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graph_position[0]);
			
			positionBuffer.put(lineCoords);
			positionBuffer.position(0);
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineCoords.length * 4, positionBuffer, GLES32.GL_DYNAMIC_DRAW); 
			GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
			
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
			xcoord += step;
		}
		
		for (i = 0; i < linesToDraw; i++)
		{
			lineCoords[0] = -1.0f; lineCoords[1] = ycoord;
			lineCoords[3] = 1.0f; lineCoords[4] = ycoord;
			
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graph_position[0]);
			
			positionBuffer.put(lineCoords);
			positionBuffer.position(0);
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineCoords.length * 4, positionBuffer, GLES32.GL_DYNAMIC_DRAW); 
			GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
			
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
			
			lineCoords[0] = -1.0f; lineCoords[1] = -ycoord;
			lineCoords[3] = 1.0f; lineCoords[4] = -ycoord;
			
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graph_position[0]);
			
			positionBuffer.put(lineCoords);
			positionBuffer.position(0);
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, lineCoords.length * 4, positionBuffer, GLES32.GL_DYNAMIC_DRAW); 
			GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
			
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
			ycoord += step;
		}
	
		GLES32.glBindVertexArray(0);
	}

	private void uninitialize()
	{
		if (vbo_graph_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_graph_position, 0);
			vbo_graph_position[0] = 0;
		}
		if (vao_graph[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_graph, 0);
			vao_graph[0] = 0;
		}

		if (vbo_triangle_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_triangle_position, 0);
			vbo_triangle_position[0] = 0;
		}
		if (vao_triangle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_triangle, 0);
			vao_triangle[0] = 0;
		}

		if (vbo_rectangle_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_rectangle_position, 0);
			vbo_rectangle_position[0] = 0;
		}
		if (vao_rectangle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_rectangle, 0);
			vao_rectangle[0] = 0;
		}

		if (vbo_incircle_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_incircle_position, 0);
			vbo_incircle_position[0] = 0;
		}
		if (vao_incircle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_incircle, 0);
			vao_incircle[0] = 0;
		}

		if (vbo_outercircle_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_outercircle_position, 0);
			vbo_outercircle_position[0] = 0;
		}
		if (vao_outercircle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_outercircle, 0);
			vao_outercircle[0] = 0;
		}
		
		if (vbo_centerline_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_centerline_position, 0);
			vbo_centerline_position[0] = 0;
		}
		if (vao_centerline[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_centerline, 0);
			vao_centerline[0] = 0;
		}
		
		if (shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNo;

			GLES32.glUseProgram(shaderProgramObject);
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

			int[] shaders = new int[shaderCount[0]];
			// GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
			GLES32.glGetAttachedShaders(shaderProgramObject,
				shaderCount[0],
				shaderCount, 0, 
				shaders, 0);

			for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++)
			{
				GLES32.glDetachShader(shaderProgramObject, shaders[shaderNo]);
				GLES32.glDeleteShader(shaders[shaderNo]);
				shaders[shaderNo] = 0;
			}

			GLES32.glDeleteProgram(shaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}

	}
	
	void drawTriangle(float red, float green, float blue)
	{		
		GLES32.glBindVertexArray(vao_triangle[0]);
		GLES32.glLineWidth(2.0f);
		GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, red, green, blue);
		GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 3);
		GLES32.glBindVertexArray(0);		
	}
	
	void drawRectangle(float red, float green, float blue)
	{		
		GLES32.glBindVertexArray(vao_rectangle[0]);
		GLES32.glLineWidth(2.0f);
		GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, red, green, blue);
		GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 4);
		GLES32.glBindVertexArray(0);		
	}
	
	void drawCircle(int[] theVao, int arrLength, float red, float green, float blue)
	{		
		GLES32.glBindVertexArray(theVao[0]);
		// GLES32.glPointSize(2.0f);
		GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, red, green, blue);
		GLES32.glDrawArrays(GLES32.GL_POINTS, 0, arrLength);
		GLES32.glBindVertexArray(0);		
	}
	
	void drawCenterLine(float red, float green, float blue)
	{		
		GLES32.glBindVertexArray(vao_centerline[0]);
		GLES32.glLineWidth(2.0f);
		GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, red, green, blue);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);		
	}
	
}

/*
Microsoft Windows [Version 10.0.17763.678]
(c) 2018 Microsoft Corporation. All rights reserved.




C:\RTR\Android\PP\14-DeathlyHallowsAnim>gradlew.bat clean

BUILD SUCCESSFUL in 3s
2 actionable tasks: 2 executed
C:\RTR\Android\PP\14-DeathlyHallowsAnim>gradlew.bat build

> Task :app:lint
Ran lint on variant debug: 6 issues found
Ran lint on variant release: 6 issues found
Wrote HTML report to file:///C:/RTR/Android/PP/14-DeathlyHallowsAnim/app/build/reports/lint-results.html
Wrote XML report to file:///C:/RTR/Android/PP/14-DeathlyHallowsAnim/app/build/reports/lint-results.xml

Deprecated Gradle features were used in this build, making it incompatible with Gradle 6.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/5.1.1/userguide/command_line_interface.html#sec:command_line_warnings

BUILD SUCCESSFUL in 19s
51 actionable tasks: 50 executed, 1 up-to-date
C:\RTR\Android\PP\14-DeathlyHallowsAnim>adb -d install -r app\build\outputs\apk\debug\app-debug.apk
Performing Streamed Install
Success

C:\RTR\Android\PP\14-DeathlyHallowsAnim>adb logcat | findstr /i rtr:
09-14 19:12:36.821 29213 29213 I System.out: RTR: in create
09-14 19:12:36.932 29213 29235 I System.out: RTR: GL10.GL_VERSION: OpenGL ES 3.2 V@269.0 (GIT@33f3a03, I26dffed9a4) (Date:04/17/18)
09-14 19:12:36.932 29213 29235 I System.out: RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: OpenGL ES GLSL ES 3.20
09-14 19:12:36.932 29213 29235 I System.out: RTR: GLES32.GL_VENDOR: Qualcomm
09-14 19:12:36.932 29213 29235 I System.out: RTR: GLES32.GL_RENDERER: Adreno (TM) 530
09-14 19:12:36.938 29213 29235 I System.out: RTR: Vertex Shader compiled successfully.
09-14 19:12:36.943 29213 29235 I System.out: RTR: Fragment Shader compiled successfully.
09-14 19:12:36.958 29213 29235 I System.out: RTR: Shader Program compiled successfully.
09-14 19:12:36.961 29213 29235 I System.out: RTR: Before sideA:0.0, sideB:0.0, sideC:0.0
09-14 19:12:36.962 29213 29235 I System.out: RTR: within sideA:1.118034, sideB:1.118034, sideC:1.0
09-14 19:12:36.962 29213 29235 I System.out: RTR: After sideA:1.118034, sideB:1.118034, sideC:1.0
09-14 19:12:36.962 29213 29235 I System.out: RTR: perimeter:3.236068
09-14 19:12:36.963 29213 29235 I System.out: RTR: halfPerimeter:1.618034
09-14 19:12:36.963 29213 29235 I System.out: RTR: within incircleOx:0.0, incircleOy:-0.19098301
09-14 19:12:36.963 29213 29235 I System.out: RTR: area:0.5
09-14 19:12:36.963 29213 29235 I System.out: RTR: (area / halfPerimeter):0.309017
09-14 19:12:36.963 29213 29235 I System.out: RTR: (float)(area / halfPerimeter):0.309017
09-14 19:12:36.964 29213 29235 I System.out: RTR: After incircleOx:0.0, incircleOy:-0.19098301
09-14 19:12:36.964 29213 29235 I System.out: RTR: radius:0.309017
09-14 19:12:36.988 29213 29235 I System.out: RTR: initialize() successful.
09-14 19:29:55.707 30719 30719 I System.out: RTR: in create
09-14 19:29:56.173 30719 30719 I System.out: RTR: in create
09-14 19:29:56.327 30719 30780 I System.out: RTR: GL10.GL_VERSION: OpenGL ES 3.2 V@269.0 (GIT@33f3a03, I26dffed9a4) (Date:04/17/18)
09-14 19:29:56.327 30719 30780 I System.out: RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: OpenGL ES GLSL ES 3.20
09-14 19:29:56.327 30719 30780 I System.out: RTR: GLES32.GL_VENDOR: Qualcomm
09-14 19:29:56.328 30719 30780 I System.out: RTR: GLES32.GL_RENDERER: Adreno (TM) 530
09-14 19:29:56.346 30719 30780 I System.out: RTR: Vertex Shader compiled successfully.
09-14 19:29:56.351 30719 30780 I System.out: RTR: Fragment Shader compiled successfully.
09-14 19:29:56.366 30719 30780 I System.out: RTR: Shader Program compiled successfully.
09-14 19:29:56.370 30719 30780 I System.out: RTR: Before sideA:0.0, sideB:0.0, sideC:0.0
09-14 19:29:56.370 30719 30780 I System.out: RTR: within sideA:1.118034, sideB:1.118034, sideC:1.0
09-14 19:29:56.371 30719 30780 I System.out: RTR: After sideA:1.118034, sideB:1.118034, sideC:1.0
09-14 19:29:56.371 30719 30780 I System.out: RTR: perimeter:3.236068
09-14 19:29:56.371 30719 30780 I System.out: RTR: halfPerimeter:1.618034
09-14 19:29:56.371 30719 30780 I System.out: RTR: within incircleOx:0.0, incircleOy:-0.19098301
09-14 19:29:56.371 30719 30780 I System.out: RTR: area:0.5
09-14 19:29:56.371 30719 30780 I System.out: RTR: (area / halfPerimeter):0.309017
09-14 19:29:56.372 30719 30780 I System.out: RTR: (float)(area / halfPerimeter):0.309017
09-14 19:29:56.372 30719 30780 I System.out: RTR: After incircleOx:0.0, incircleOy:-0.19098301
09-14 19:29:56.372 30719 30780 I System.out: RTR: radius:0.309017
09-14 19:29:56.403 30719 30780 I System.out: RTR: initialize() successful.
^C^C
C:\RTR\Android\PP\14-DeathlyHallowsAnim>

*/