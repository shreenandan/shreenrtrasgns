package edu.shreenandan.geometry;

import android.content.Context;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

// asa view jyacha surface OGL support karato
// x -> Extension, J2ME, GL10: OGL 10th extension, for basic features of OGL
// 3 For OGL Buffers

public class GLESView extends GLSurfaceView
        implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private GestureDetector gestureDetector;
    private final Context context;

    // java neither has uint nor GLuint
    private int fragmentShaderObject;
    private int geometryShaderObject;
    private int vertexShaderObject;
    private int shaderProgramObject;

    // As java does not have address operator we declare array of object and pass name of array.
    private int[] vao = new int[1];
    private int[] vbo = new int[1];

    private int mvpUniform;
    private float[] perspectiveProjectionMatrix = new float[16];

    // constructor
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;
        // not useful now, will be helpful in texture

        // param 2: Listener; param 3: Handler, null = no other handler, this listner class obj himself is handler; param 4: always false
        gestureDetector = new GestureDetector(drawingContext, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

        // call 3 functions of GLES
        setEGLContextClientVersion(3); // 3.x
        // will give highest supported to 3, ie 3.2

        setRenderer(this); // onDrawFrame() - display()
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE
    }

    // Handling 'onTouchEvent' is the most important,
    // Because it triggers all gesture and tap events
    @Override
    public boolean onTouchEvent(MotionEvent me)
    {
        // below line is not useful in this pgm
        // useful in case of phone's keyboard actions
        int eventaction = me.getAction();

        // if not my event, then delegate to parent
        if(!gestureDetector.onTouchEvent(me))
        {
            super.onTouchEvent(me);
        }

        return(true);
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    // it is more reliable than onDoubleTapEvent
    @Override
    public boolean onDoubleTap(MotionEvent me)
    {
        return(true);
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    // less reliable, start, leave touch points
    @Override
    public boolean onDoubleTapEvent(MotionEvent me)
    {
        // do not write code here because handled in 'onDoubleTap'
        return(true);
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent me)
    {
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public boolean onDown(MotionEvent me)
    {
        // do not write code here because handled in 'onSingleTapConfirmed'
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY)
    {
        // fling means swipe
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public void onLongPress(MotionEvent me)
    {
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY)
    {
        uninitialize();
        System.exit(0);
        return(true);
    }

    // abstract method from OnGestureListener, so must be implemented
    @Override
    public void onShowPress(MotionEvent me)
    {
    }

    // abstract method from OnDoubleTapListener, so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent me)
    {
        return(true);
    }


    // implement 3 functions of GLSurfaceView.Renderer
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: GL10.GL_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

        version = gl.glGetString(GLES32.GL_VENDOR);
        System.out.println("RTR: GLES32.GL_VENDOR: " + version);

        version = gl.glGetString(GLES32.GL_RENDERER);
        System.out.println("RTR: GLES32.GL_RENDERER: " + version);

        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    // our custom methods
    private void initialize()
    {
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "in vec4 vPosition;" +
                        "uniform mat4 u_mvp_matrix;" +
                        "void main (void)" +
                        "{" +
                        "gl_Position = u_mvp_matrix * vPosition;" +
                        "}"
        );
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR: Error: Vertex shader compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Vertex Shader: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Vertex Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;


        //Geometry Shader
        geometryShaderObject = GLES32.glCreateShader(GLES32.GL_GEOMETRY_SHADER);
        final String geometryShaderSourceCode = String.format(
                        "#version 320 es" +
                        "\n" +
                        "out vec4 out_color;" +
                        "layout(triangles)in;" +
                        "layout(triangle_strip, max_vertices = 9)out;" +
                        "uniform mat4 u_mvp_matrix;" +
                        "void main (void)" +
                        "{" +
                            "for(int vertex = 0; vertex < 3; vertex++)" +
                            "{" +

                                "if(vertex == 0)" +
                                "{" +
                                "out_color = vec4(1.0, 0.0, 0.0, 0.0);" +
                                "}" +
                                "else if(vertex == 1)" +
                                "{" +
                                "out_color = vec4(0.0, 1.0, 0.0, 0.0);" +
                                "}" +
                                "else if(vertex == 2)" +
                                "{" +
                                "out_color = vec4(0.0, 0.0, 1.0, 0.0);" +
                                "}" +

                                "gl_Position = u_mvp_matrix * (gl_in[vertex].gl_Position + vec4(0.0, 1.0, 0.0, 0.0));" +
                                "EmitVertex();" +

                                "gl_Position = u_mvp_matrix * (gl_in[vertex].gl_Position + vec4(-1.0, -1.0, 0.0, 0.0));" +
                                "EmitVertex();" +

                                "gl_Position = u_mvp_matrix * (gl_in[vertex].gl_Position + vec4(1.0, -1.0, 0.0, 0.0));" +
                                "EmitVertex();" +

                                "EndPrimitive();" +
                            "}" +
                        "}"
        );
        GLES32.glShaderSource(geometryShaderObject, geometryShaderSourceCode);
        GLES32.glCompileShader(geometryShaderObject);

        // Error checking
        iShaderCompileStatus = new int[1];
        iInfoLogLength = new int[1];
        szInfoLog = null;

        GLES32.glGetShaderiv(geometryShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(geometryShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(geometryShaderObject);
                System.out.println("RTR: Error: Geometry shader compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Geometry Shader: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Geometry Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "precision highp float;" +
                        "in vec4 out_color;" +
                        "out vec4 FragColor;" +
                        "void main (void)" +
                        "{" +
                        "FragColor = out_color;" +
                        "}"
        );
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
        if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Error: Fragment shader compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Fragment Shader: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Fragment Shader compiled successfully.");
        }

        // reset flags
        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, geometryShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre Link Attribute Binding
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("RTR: Error: Shader Program compilation log: " + szInfoLog);
            }
            else
            {
                System.out.println("RTR: Shader Program: Something went wrong, infoLogLength is zero...");
            }
            uninitialize();
            System.exit(0);
        }
        else
        {
            System.out.println("RTR: Shader Program compiled successfully.");
        }

        // Post Link Uniform Location
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

        // reset flags
        iProgramLinkStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        final float[] triangleVertices = new float[]
		{
			0.0f,1.0f,1.0f,//apex
			-1.0f,-1.0f,0.0f,//left bottom
			1.0f,-1.0f,0.0f//right bottom
		};

        // Generation
        GLES32.glGenVertexArrays(1, vao, 0);
        GLES32.glBindVertexArray(vao[0]);

        GLES32.glGenBuffers(1, vbo, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);

        // convert the array which we pass to glBufferData()
        // Unlike Win, XWin. In C,C++ array name is iteself pointer
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4); // float
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
        positionBuffer.put(triangleVertices);
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
        // 3: xyx, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind
        GLES32.glBindVertexArray(0);

        // GLES32.glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
        GLES32.glEnable(GLES32.GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    }

    private void resize(int width, int height)
    {
        if(height == 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0,
                45.0f,
                (float)width/(float)height,
                0.1f,
                100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -5.5f);

        Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        GLES32.glBindVertexArray(vao[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);

        GLES32.glBindVertexArray(0);
        GLES32.glUseProgram(0);

        requestRender(); // fn of Base Class like swapBuffers
    }

    private void uninitialize()
    {
        if (vbo[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo, 0);
            vbo[0] = 0;
        }
        if (vao[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao, 0);
            vao[0] = 0;
        }

        if (shaderProgramObject != 0)
        {
            int[] shaderCount = new int[1];
            int shaderNo;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];
            // GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
            GLES32.glGetAttachedShaders(shaderProgramObject,
                    shaderCount[0],
                    shaderCount, 0,
                    shaders, 0);

            for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++)
            {
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNo]);
                GLES32.glDeleteShader(shaders[shaderNo]);
                shaders[shaderNo] = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
            shaderProgramObject = 0;
            GLES32.glUseProgram(0);
        }

    }

}


/*

c:\RTR\Android\PP\22-Geometry>gradlew clean
Starting a Gradle Daemon (subsequent builds will be faster)

BUILD SUCCESSFUL in 10s
2 actionable tasks: 2 executed
c:\RTR\Android\PP\22-Geometry>gradlew build

> Task :app:lint
Ran lint on variant release: 8 issues found
Ran lint on variant debug: 8 issues found
Wrote HTML report to file:///C:/RTR/Android/PP/22-Geometry/app/build/reports/lint-results.html
Wrote XML report to file:///C:/RTR/Android/PP/22-Geometry/app/build/reports/lint-results.xml

Deprecated Gradle features were used in this build, making it incompatible with Gradle 6.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/5.1.1/userguide/command_line_interface.html#sec:command_line_warnings

BUILD SUCCESSFUL in 36s
51 actionable tasks: 51 executed
c:\RTR\Android\PP\22-Geometry>adb -d install -r app\build\outputs\apk\debug\app-debug.apk
Performing Streamed Install
adb: failed to install app\build\outputs\apk\debug\app-debug.apk: Failure [INSTALL_FAILED_USER_RESTRICTED: Install canceled by user]

c:\RTR\Android\PP\22-Geometry>adb -d install -r app\build\outputs\apk\debug\app-debug.apk
Performing Streamed Install
Success

c:\RTR\Android\PP\22-Geometry>

*/