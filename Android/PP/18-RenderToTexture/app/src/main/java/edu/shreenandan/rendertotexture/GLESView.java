package edu.shreenandan.rendertotexture;

import android.content.Context;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

// asa view jyacha surface OGL support karato
import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // opengl ver 3.2
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
// x -> Extension, J2ME, GL10: OGL 10th extension, for basic features of OGL

// 3 For OGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils; // for texImage2d()

public class GLESView extends GLSurfaceView
implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	// java neither has uint nor GLuint
	private int fragmentShaderObject;
	private int vertexShaderObject;
	private int shaderProgramObject;
	
	// As java does not have address operator we declare array of object and pass name of array.
	private int[] vao_cube = new int[1];
	private int[] vbo_position_cube = new int[1];
	private int[] vbo_texture_cube = new int[1];
	private int[] texture_kundali = new int[1];
	
	private int[] vao_pyramid = new int[1];
	private int[] vbo_position_pyramid = new int[1];
	private int[] vbo_texture_pyramid = new int[1];
	private int[] texture_pyramid = new int[1];
	
	private int[] fbo = new int[1];
	private int[] rbo = new int[1];
	private int[] texture_myrender = new int[1];
	private int gWidth;
	private int gHeight;
	private final int TEX_WIDTH = 256;
	private final int TEX_HEIGHT = 256;
	
	private int samplerUniform;
	
	private int mvpUniform;
	private float[] perspectiveProjectionMatrix = new float[16];
	
	private float angleCube = 0.0f;
	private float anglePyramid = 0.0f;
	private boolean ascendingPyramid = true;
	private boolean ascendingCube = true;

	// constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		// not useful now, will be helpful in texture
		
		// param 2: Listener; param 3: Handler, null = no other handler, this listner class obj himself is handler; param 4: always false
		gestureDetector = new GestureDetector(drawingContext, this, null, false);		
		gestureDetector.setOnDoubleTapListener(this);

		// call 3 functions of GLES
		setEGLContextClientVersion(3); // 3.x
		// will give highest supported to 3, ie 3.2

		setRenderer(this); // onDrawFrame() - display()
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // NOTE // other is RENDERMODE_CONTINUOUSLY
		// https://developer.android.com/training/graphics/opengl/motion.html?
		// https://developer.android.com/reference/android/opengl/GLSurfaceView
	}
	
	// Handling 'onTouchEvent' is the most important,
	// Because it triggers all gesture and tap events
	@Override
	public boolean onTouchEvent(MotionEvent me)
	{
		// below line is not useful in this pgm
		// useful in case of phone's keyboard actions
		int eventaction = me.getAction();
		
		// if not my event, then delegate to parent
		if(!gestureDetector.onTouchEvent(me))
		{
			super.onTouchEvent(me);
		}
		
		return(true);
	}
	
	// abstract method from OnDoubleTapListener, so must be implemented
	// it is more reliable than onDoubleTapEvent
	@Override
	public boolean onDoubleTap(MotionEvent me)
	{
		return(true);
	}
	
	// abstract method from OnDoubleTapListener, so must be implemented
	// less reliable, start, leave touch points
	@Override
	public boolean onDoubleTapEvent(MotionEvent me)
	{
		// do not write code here because handled in 'onDoubleTap'
		return(true);
	}
	
	// abstract method from OnDoubleTapListener, so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent me)
	{
		return(true);
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public boolean onDown(MotionEvent me)
	{
		// do not write code here because handled in 'onSingleTapConfirmed'
		return(true);
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY)
	{
		// fling means swipe
		return(true);
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public void onLongPress(MotionEvent me)
	{
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public boolean onScroll(MotionEvent me1, MotionEvent me2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	// abstract method from OnGestureListener, so must be implemented
	@Override
	public void onShowPress(MotionEvent me)
	{
	}
	
	// abstract method from OnDoubleTapListener, so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent me)
	{
		return(true);
	}
	

	// implement 3 functions of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: GL10.GL_VERSION: " + version);

		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: " + version);

		version = gl.glGetString(GLES32.GL_VENDOR);
		System.out.println("RTR: GLES32.GL_VENDOR: " + version);
		
		version = gl.glGetString(GLES32.GL_RENDERER);
		System.out.println("RTR: GLES32.GL_RENDERER: " + version);

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
		update();
	}

	// our custom methods
	private void initialize()
	{		
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"uniform mat4 u_mvp_matrix;" +
			"in vec2 vTexCoord;" + 
			"out vec2 out_texcoord;" + 
			"void main(void)" +
			"{" +
			"	gl_Position = u_mvp_matrix * vPosition;" +
			"	out_texcoord = vTexCoord;" + 
			"}"
		);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		
		// Error checking
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: Error: Vertex shader compilation log: " + szInfoLog);
			}
			else
			{
				System.out.println("RTR: Vertex Shader: Something went wrong, infoLogLength is zero...");
			}
			uninitialize();
			System.exit(0);
		}
		else
		{
			System.out.println("RTR: Vertex Shader compiled successfully.");
		}
		
		// reset flags
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"out vec4 FragColor;" +
			"in vec2 out_texcoord;" + 
			"uniform sampler2D u_sampler;" + 
			"void main(void)" +
			"{" +
			"	FragColor = texture(u_sampler, out_texcoord);" +
			"}"
		);
		// "	FragColor = vec4(1.0, 1.0, 1.0, 1.0);//texture(u_sampler, out_texcoord);" +
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		
		// Error checking
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0); // zeroth
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: Error: Fragment shader compilation log: " + szInfoLog);
			}
			else
			{
				System.out.println("RTR: Fragment Shader: Something went wrong, infoLogLength is zero...");
			}
			uninitialize();
			System.exit(0);
		}
		else
		{
			System.out.println("RTR: Fragment Shader compiled successfully.");
		}
		
		// reset flags
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		shaderProgramObject = GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		// Pre Link Attribute Binding
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");
		GLES32.glLinkProgram(shaderProgramObject);
		
		// Error checking
		int[] iProgramLinkStatus = new int[1];
		
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
		if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("RTR: Error: Shader Program compilation log: " + szInfoLog);				
			}
			else
			{
				System.out.println("RTR: Shader Program: Something went wrong, infoLogLength is zero...");
			}
			uninitialize();
			System.exit(0);
		}
		else
		{
			System.out.println("RTR: Shader Program compiled successfully.");
		}
		
		// Post Link Uniform Location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		samplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_sampler");
		
		// reset flags
		iProgramLinkStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
						
		// Generation
		//============== CUBE ====================
		// Top right, Top left, Bottom left, bottom right;
		// Top, Bottom, Front, Back, Right, Left
		final float[] cubeVertices = new float[]
		{
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,
			
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};
		
		final float[] cubeTexCoord = new float[]
		{
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f
		};
		
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);
		
		GLES32.glGenBuffers(1, vbo_position_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_cube[0]);
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4); // float
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(cubeVertices);
		positionBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW); 
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		// 3: xyx, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind
		
		// === Texture ===
		GLES32.glGenBuffers(1, vbo_texture_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_texture_cube[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(cubeTexCoord.length * 4); // float
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(cubeTexCoord);
		positionBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexCoord.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW); 
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, 2, GLES32.GL_FLOAT, false, 0, 0);
		// 2: st, false: non normalized 0 to 1, 0: no stride/dhanga, 0: no offset
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // unbind
		
		GLES32.glBindVertexArray(0);
		
		
		//============== PYRAMID ====================
		// Apex, Left, Right; Front, Right, Back, Left
		final float[] pyramidVertices = new float[]
		{
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};
		
		final float[] pyramidTexCoord = new float[]
		{
			0.5f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,

			0.5f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,

			0.5f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,

			0.5f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f
		};
		
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);
		GLES32.glBindVertexArray(vao_pyramid[0]);
		
		GLES32.glGenBuffers(1, vbo_position_pyramid, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_pyramid[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(pyramidVertices);
		positionBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// === Texture ===
		GLES32.glGenBuffers(1, vbo_texture_pyramid, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_texture_pyramid[0]);
		
		byteBuffer = ByteBuffer.allocateDirect(pyramidTexCoord.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(pyramidTexCoord);
		positionBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidTexCoord.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, 2, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		

		///////////////////////////////////////
		// NOTE: Important initialization part
		initRenderTexture();
		initFramebuffer();
		///////////////////////////////////////

		
		texture_kundali[0] = loadGLTexture(R.raw.vijaykundali); // Physical file name without extension
		texture_pyramid[0] = loadGLTexture(R.raw.stone);
		GLES32.glEnable(GLES32.GL_TEXTURE_2D);
		
		// GLES32.glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
		GLES32.glEnable(GLES32.GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

		GLES32.glClearColor(0.20f, 0.20f, 0.20f, 1.0f);
		
		System.out.println("RTR: initialize() successful.");
	}

	private void resize(int width, int height)
	{
		if(height == 0)
		{
			height = 1;
		}
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 
			45.0f, 
			(float)width/(float)height, 
			0.1f, 
			100.0f);
			
		gWidth = width;
		gHeight = height;
	}

	private void displayOrg()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		float[] rotationMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelViewMatrix, 0, -1.5f, 0.0f, -6.0f);
		Matrix.setRotateM(rotationMatrix, 0, anglePyramid, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0); // no transpose
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_pyramid[0]);
		GLES32.glUniform1i(samplerUniform, 0);
		
		GLES32.glBindVertexArray(vao_pyramid[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		GLES32.glBindVertexArray(0);
		
		// ================================================
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(scaleMatrix, 0);
		
		Matrix.translateM(modelViewMatrix, 0, 1.5f, 0.0f, -6.0f);
		Matrix.setRotateM(rotationMatrix, 0, angleCube, 1.0f, 1.0f, 1.0f);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, 0.75f, 0.75f, 0.75f);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, scaleMatrix, 0);
		
		Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		
		// ==== Work with texture now ABU ====
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0); // matches to our AMC_ATTRIBUTE_TEXCOORD0
		// texture unit. 80 supported
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_kundali[0]);
		GLES32.glUniform1i(samplerUniform, 0); // GL_TEXTURE0 zeroth unit

		GLES32.glBindVertexArray(vao_cube[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
		
		requestRender(); // fn of Base Class like swapBuffers
	}

	private void uninitialize()
	{
		if (vbo_texture_pyramid[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_texture_pyramid, 0);
			vbo_texture_pyramid[0] = 0;
		}
		if (vbo_position_pyramid[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_position_pyramid, 0);
			vbo_position_pyramid[0] = 0;
		}
		if (vao_pyramid[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
			vao_pyramid[0] = 0;
		}
		
		if (vbo_texture_cube[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_texture_cube, 0);
			vbo_texture_cube[0] = 0;
		}
		if (vbo_position_cube[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_position_cube, 0);
			vbo_position_cube[0] = 0;
		}
		if (vao_cube[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_cube, 0);
			vao_cube[0] = 0;
		}
		
		if (shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int shaderNo;

			GLES32.glUseProgram(shaderProgramObject);
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

			int[] shaders = new int[shaderCount[0]];
			GLES32.glGetAttachedShaders(shaderProgramObject,
				shaderCount[0],
				shaderCount, 0, 
				shaders, 0);

			for (shaderNo = 0; shaderNo < shaderCount[0]; shaderNo++)
			{
				GLES32.glDetachShader(shaderProgramObject, shaders[shaderNo]);
				GLES32.glDeleteShader(shaders[shaderNo]);
				shaders[shaderNo] = 0;
			}

			GLES32.glDeleteProgram(shaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}

	}
	
	private int loadGLTexture(int imageFileResourceId)
	{
		int[] texture = new int[1];
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceId, options);
		
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 4); //RGBA
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
		
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0); //glTextImage2D mipmap level, 345 789, border level
		
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D); // new addition
		// no explicit unbind in FFP, must in PP
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		System.out.println("RTR: loadGLTexture ID = " + texture[0]);
		return texture[0];
	}

	void update()
	{
		// Rotate vice versa	
		if (ascendingPyramid)
		{
			anglePyramid += 3.0f;
			if (anglePyramid > 360.0f)
			{
				ascendingPyramid = false;
			}
		}
		else
		{
			anglePyramid -= 3.0f;
			if (anglePyramid < 0.0f)
			{
				ascendingPyramid = true;
			}
		}

		// Rotate vice versa	
		if (ascendingCube)
		{
			angleCube += 0.50f;
			if (angleCube > 360.0f)
			{
				ascendingCube = false;
			}
		}
		else
		{
			angleCube -= 0.5f;
			if (angleCube < 0.0f)
			{
				ascendingCube = true;
			}
		}
	}

	
	//////////////////////////////////////////////////

	private void initRenderTexture()
	{
		Bitmap bitmap = Bitmap.createBitmap(TEX_WIDTH, TEX_HEIGHT, Bitmap.Config.ARGB_8888);
		
		GLES32.glGenTextures(1, texture_myrender, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_myrender[0]);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_CLAMP_TO_EDGE);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_CLAMP_TO_EDGE);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		// GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_GENERATE_MIPMAP, GLES32.GL_TRUE); // auto mipmap
		
		// GLES32.glTexImage2D(GLES32.GL_TEXTURE_2D, 0, GLES32.GL_RGBA8, TEX_WIDTH, TEX_HEIGHT, 0, GLES32.GL_RGBA, GLES32.GL_UNSIGNED_BYTE, NULL);
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);

		// unbind
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
	}

	private void initFramebuffer()
	{
		GLES32.glGenRenderbuffers(1, rbo, 0);
		GLES32.glBindRenderbuffer(GLES32.GL_RENDERBUFFER, rbo[0]);

		GLES32.glRenderbufferStorage(GLES32.GL_RENDERBUFFER, GLES32.GL_DEPTH_COMPONENT, TEX_WIDTH, TEX_HEIGHT);
		GLES32.glBindRenderbuffer(GLES32.GL_RENDERBUFFER, 0);


		GLES32.glGenFramebuffers(1, fbo, 0);
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, fbo[0]);

		// attach texture with FBO color attachment
		GLES32.glFramebufferTexture2D(GLES32.GL_FRAMEBUFFER, // target
			GLES32.GL_COLOR_ATTACHMENT0,
			GLES32.GL_TEXTURE_2D,
			texture_myrender[0],
			0); // mipmap level

		// attach renderbuffer to depth attachment
		GLES32.glFramebufferRenderbuffer(GLES32.GL_FRAMEBUFFER,
			GLES32.GL_DEPTH_ATTACHMENT,
			GLES32.GL_RENDERBUFFER, // RBO target
			rbo[0]);


		int status = GLES32.glCheckFramebufferStatus(GLES32.GL_FRAMEBUFFER);
		if (status != GLES32.GL_FRAMEBUFFER_COMPLETE)
		{
			System.out.println("RTR: ERROR: something went wrong in FrameBuffer...");
		}
	}
	
	private void drawPyramid()
	{
		GLES32.glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		float[] rotationMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -4.0f);
		Matrix.setRotateM(rotationMatrix, 0, anglePyramid, 0.0f, 1.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0); // no transpose
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_pyramid[0]);
		GLES32.glUniform1i(samplerUniform, 0);
		
		GLES32.glBindVertexArray(vao_pyramid[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
	}

	private void display()
	{
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, fbo[0]);
		
		GLES32.glClearColor(1.0f, 0.90f, 1.0f, 1.0f);
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glViewport(0, 0, TEX_WIDTH, TEX_HEIGHT);		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 
			45.0f, 
			(float)TEX_WIDTH/(float)TEX_HEIGHT, 
			0.1f, 
			100.0f);
		drawPyramid();
		
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);
		
		
		//////
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_myrender[0]);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glViewport(0, 0, gWidth, gHeight);		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 
			45.0f, 
			(float)gWidth/(float)gHeight, 
			0.1f, 
			100.0f);
		
		
		GLES32.glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm
		
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		float[] rotationMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		// This also works together, but we don't want this
		// Matrix.translateM(modelViewMatrix, 0, -1.5f, 0.0f, -6.0f);
		// Matrix.setRotateM(rotationMatrix, 0, anglePyramid, 0.0f, 1.0f, 0.0f);
		// Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0);
		// Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		// GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0); // no transpose
		
		// GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		// GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_pyramid[0]);
		// GLES32.glUniform1i(samplerUniform, 0);
		
		// GLES32.glBindVertexArray(vao_pyramid[0]);
		// GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		// GLES32.glBindVertexArray(0);
		
		// ================================================
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -4.20f);
		Matrix.setRotateM(rotationMatrix, 0, angleCube, 1.0f, 1.0f, 1.0f);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, rotationMatrix, 0);
		
		Matrix.multiplyMM(modelViewProjectionMatrix,0, perspectiveProjectionMatrix,0, modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		
		// ==== Work with texture now ABU ====
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0); // matches to our AMC_ATTRIBUTE_TEXCOORD0
		// texture unit. 80 supported
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_myrender[0]); // NOTE: IMP texture_kundali
		GLES32.glUniform1i(samplerUniform, 0); // GL_TEXTURE0 zeroth unit

		GLES32.glBindVertexArray(vao_cube[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
		
		requestRender(); // fn of Base Class like swapBuffers
	}

}

/*

C:\RTR\Android\PP\08-TexturePyramidKundali>gradlew clean

BUILD SUCCESSFUL in 2s
2 actionable tasks: 2 executed
C:\RTR\Android\PP\08-TexturePyramidKundali>gradlew build

> Task :app:lint
Ran lint on variant release: 7 issues found
Ran lint on variant debug: 7 issues found
Wrote HTML report to file:///C:/RTR/Android/PP/08-TexturePyramidKundali/app/build/reports/lint-results.html
Wrote XML report to file:///C:/RTR/Android/PP/08-TexturePyramidKundali/app/build/reports/lint-results.xml

Deprecated Gradle features were used in this build, making it incompatible with Gradle 6.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/5.1.1/userguide/command_line_interface.html#sec:command_line_warnings

BUILD SUCCESSFUL in 22s
51 actionable tasks: 50 executed, 1 up-to-date
C:\RTR\Android\PP\08-TexturePyramidKundali>adb -d install -r app\build\outputs\apk\debug\app-debug.apk
Performing Streamed Install
Success

C:\RTR\Android\PP\08-TexturePyramidKundali>adb logcat | findstr /i rtr:
09-08 21:21:31.426 27212 27212 I System.out: RTR: in create
09-08 21:21:31.748 27212 27212 I System.out: RTR: in create
09-08 21:21:31.851 27212 27234 I System.out: RTR: GL10.GL_VERSION: OpenGL ES 3.2 V@269.0 (GIT@33f3a03, I26dffed9a4) (Date:04/17/18)
09-08 21:21:31.851 27212 27234 I System.out: RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: OpenGL ES GLSL ES 3.20
09-08 21:21:31.851 27212 27234 I System.out: RTR: GLES32.GL_VENDOR: Qualcomm
09-08 21:21:31.852 27212 27234 I System.out: RTR: GLES32.GL_RENDERER: Adreno (TM) 530
09-08 21:21:31.860 27212 27234 I System.out: RTR: Vertex Shader compiled successfully.
09-08 21:21:31.865 27212 27234 I System.out: RTR: Fragment Shader compiled successfully.
09-08 21:21:31.882 27212 27234 I System.out: RTR: Shader Program compiled successfully.
09-08 21:21:31.888 27212 27234 I System.out: RTR: loadGLTexture ID = 1
09-08 21:21:31.895 27212 27234 I System.out: RTR: loadGLTexture ID = 2
09-08 21:21:31.895 27212 27234 I System.out: RTR: initialize() successful.
09-08 21:27:33.596 28327 28327 I System.out: RTR: in create
09-08 21:27:33.939 28327 28327 I System.out: RTR: in create
09-08 21:27:34.043 28327 28350 I System.out: RTR: GL10.GL_VERSION: OpenGL ES 3.2 V@269.0 (GIT@33f3a03, I26dffed9a4) (Date:04/17/18)
09-08 21:27:34.043 28327 28350 I System.out: RTR: GLES32.GL_SHADING_LANGUAGE_VERSION: OpenGL ES GLSL ES 3.20
09-08 21:27:34.043 28327 28350 I System.out: RTR: GLES32.GL_VENDOR: Qualcomm
09-08 21:27:34.043 28327 28350 I System.out: RTR: GLES32.GL_RENDERER: Adreno (TM) 530
09-08 21:27:34.050 28327 28350 I System.out: RTR: Vertex Shader compiled successfully.
09-08 21:27:34.053 28327 28350 I System.out: RTR: Fragment Shader compiled successfully.
09-08 21:27:34.068 28327 28350 I System.out: RTR: Shader Program compiled successfully.
09-08 21:27:34.074 28327 28350 I System.out: RTR: loadGLTexture ID = 1
09-08 21:27:34.081 28327 28350 I System.out: RTR: loadGLTexture ID = 2
09-08 21:27:34.082 28327 28350 I System.out: RTR: initialize() successful.

C:\RTR\Android\PP\08-TexturePyramidKundali>

*/