package edu.shreenandan.ortho;
/*
change 2] change directory name win_hello
change 3] update package names in all .java files
change 4] in strings.xml provide new application name/title
*/

/*
This application will do
a> Fullscreen
b> Forced landscape orientation
c> blue background
d> orthographic projection
*/

// default applications given by android
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

// added by us
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.view.View; // for navigation flag

// Native/Non-Blocking Input Output
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class MainActivity extends AppCompatActivity {
	
	private GLESView glesView;

	// java neither has unint nor GLuint
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	// as java does not have address operator, we declare array of obj and pass name of array
	private int[] vao = new int[1];
	private int[] vbo = new int[1];

	private int mvpUniform; // model view projection
	private float[] orthographicProjectionMatrix = new float[16];
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
		System.out.println("RTR: in create");
        super.onCreate(savedInstanceState);
		
		// get rid of title bar
		this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// hide navigation bar
		this
			.getWindow()
			.getDecorView()
			.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		
		// make fullscreen
		this
			.getWindow()
			.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
				
		// do forced landscape orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		// set background color
		this
			.getWindow()
			.getDecorView()
			.setBackgroundColor(Color.BLACK);
			
		// define our own view
		glesView = new GLESView(this); // pass activity
				
		// now set this view as our main view
		setContentView(glesView);
		
    }//end of method
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

}
