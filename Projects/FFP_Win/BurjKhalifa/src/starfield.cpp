/*
   starfield.c
   Nate Robins, 1997

   An example of starfields in OpenGL.

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if defined (__APPLE__) || defined (MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


/* #define SCREEN_SAVER_MODE */

FILE *gpFile = NULL;

enum Dirs {
	SCROLL_LEFT = 1,
	SCROLL_RIGHT,
	SCROLL_UP,
	SCROLL_DOWN
};
Dirs type = SCROLL_RIGHT;


typedef struct _star {
	float x, y;
	float vx, vy;
} star;


star* stars = NULL;
int num_stars = 150;


void
reshape(int width, int height)
{
	int i = 0;

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, 0, height, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor3ub(255, 255, 255);

	for (i = 0; i < num_stars; i++) {
		stars[i].x = rand() % width;
		stars[i].y = rand() % height;
		stars[i].vx = rand() / (float)RAND_MAX * 5 + 2;
		stars[i].vy = 0;
	fprintf(gpFile, "\n%f %f %f %f", stars[i].x, stars[i].y, stars[i].vx, stars[i].vy);
	}

}

	static float delay = 10.0f;
	float theX = 0.0f;

void
display(void)
{
	int i, x1 =0, x2=2;
	float temp = 0, z = 0.0f;
	int ww = glutGet(GLUT_WINDOW_WIDTH);
	glClear(GL_COLOR_BUFFER_BIT);

	glLineWidth(3.0f);

	for (i = 0; i < num_stars; i++) {
		//stars[i].x += stars[i].vx;
		//stars[i].x = (int)theX % ww;
		temp = stars[i].x + stars[i].vx;
		if (stars[i].x < ww) {
			glBegin(GL_LINE_STRIP);
			//glColor3ub(255, 0, 0);
			glColor3ub(185, 122, 87);
			//glVertex2i(stars[i].x - stars[i].vx, stars[i].y);
			x1 = (int)(theX + temp - stars[i].vx) % ww;
			x2 = (int)(theX + temp) % ww;
			z = i % 5 == 0 ? 0.45f : i % 4 == 0 ? 0.35f : i % 3 == 0 ? 0.25f : i % 2 == 0 ? 0.15f : 0;
			//glVertex2i(x1, stars[i].y);
			glVertex3f(x1, stars[i].y, z);
			//glColor3ub(255, 255, 255);
			//glVertex2i(stars[i].x, stars[i].y);
			//glVertex2i(x1 > x2 ? x1 : x2, stars[i].y);
			x2 = x1 > x2 ? x1 : x2;
			glVertex3f(x2, stars[i].y, z);
			
			fprintf(gpFile, "\n%d %d %.8f %.8f", x1, x2, stars[i].y, z);

			glEnd();
		}
		else {
			//stars[i].x = 0;
			temp = 0;
		}
	}

	glutSwapBuffers();
}

void
idle(void)
{
	delay -= 1.0f;
	if (delay < 0.0f)
	{
		delay = 0.50f;
		theX += 0.1f;
	}
	if (theX >= glutGet(GLUT_WINDOW_WIDTH))
		theX = 0.0f;

	glutPostRedisplay();
}

void
bail(int code)
{
	free(stars);
	exit(code);
}

#ifdef SCREEN_SAVER_MODE
void
ss_keyboard(char key, int x, int y)
{
	bail(0);
}

void
ss_mouse(int button, int state, int x, int y)
{
	bail(0);
}

void
ss_passive(int x, int y)
{
	static int been_here = 0;

	/* for some reason, GLUT sends an initial passive motion callback
	   when a window is initialized, so this would immediately
	   terminate the program.  to get around this, see if we've been
	   here before. (actually if we've been here twice.) */

	if (been_here > 1)
		bail(0);
	been_here++;
}

#else

void
keyboard(unsigned char key, int x, int y)
{
	static int old_x = 50;
	static int old_y = 50;
	static int old_width = 320;
	static int old_height = 320;

	switch (key) {
	case 27:
		bail(0);
		break;
	case 'w':
		glutPositionWindow(old_x, old_y);
		glutReshapeWindow(old_width, old_height);
		break;
	case 'f':
		if (glutGet(GLUT_WINDOW_WIDTH) < glutGet(GLUT_SCREEN_WIDTH)) {
			old_x = glutGet(GLUT_WINDOW_X);
			old_y = glutGet(GLUT_WINDOW_Y);
			old_width = glutGet(GLUT_WINDOW_WIDTH);
			old_height = glutGet(GLUT_WINDOW_HEIGHT);
			glutFullScreen();
		}
		break;
	}
}

#endif

int
main(int argc, char** argv)
{
	// code
	if (fopen_s(&gpFile, "log_star.txt", "w") != 0)
	{
		//glutMessageBox(NULL, "Log file can not be created...", TEXT("ERROR"), MB_OK);
		exit(0); // Abortive but 0 and 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(320, 320);
	glutInit(&argc, argv);

	glutCreateWindow("Starfield");
	//glClearColor(0, 0.2f, 0.5f, 1.0f);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
#ifdef SCREEN_SAVER_MODE
	glutPassiveMotionFunc(ss_passive);
	glutKeyboardFunc(ss_keyboard);
	glutMouseFunc(ss_mouse);
	glutSetCursor(GLUT_CURSOR_NONE);
	glutFullScreen();
#else
	glutKeyboardFunc(keyboard);
#endif

	if (argc > 1) {
		if (strcmp(argv[1], "-h") == 0) {
			fprintf(stderr, "%s [stars]\n", argv[0]);
			exit(0);
		}
		sscanf_s(argv[1], "%d", &num_stars);
	}

	stars = (star*)malloc(sizeof(star) * num_stars);

	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}
