/*
	Assignment: Draw Rotating 3D Cube in perspective.
	Date: 25 Mar 2019
 */

 // Headers
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h> // For perspective change 3]
#include <stdio.h>
#include "Resource.h"
#include <stdlib.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib") // For perspective change 3]

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define LINES_TO_DRAW_GRAPH 20

#define TERRAIN_WIDTH 2200
#define TERRAIN_HEIGHT 1300
#define TERRAIN_SCALE 20
#define TERRAIN_ROWS TERRAIN_HEIGHT/TERRAIN_SCALE
#define TERRAIN_COLS TERRAIN_WIDTH/TERRAIN_SCALE
#define TERRAIN_SCALE_01 1.0f/TERRAIN_SCALE

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
bool bIsFullScreen = false;
DWORD dwStyle; // global default initialized to zero
HWND gHwnd;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
MONITORINFO mi = { sizeof(MONITORINFO) };

HDC ghdc = NULL; // Common context
HGLRC ghrc = NULL; // Super context, OpenGL Rendering Context
bool gbActiveWindow = false;
FILE *gpFile = NULL;

// variable declarations
GLfloat angleCube = 0.0f;
bool ascendingPyramid = false;
bool gbShowGraph = false;
GLfloat cameraPosZ = 0.0f;
bool ascendingCameraZ = false;
GLfloat TerrainZ[TERRAIN_ROWS * 2][TERRAIN_COLS * 2];
float flying = 0.0f;

GLfloat LightAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f }; // Lights step 2]
GLfloat LightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Lights step 2]
GLfloat LightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f }; // Lights step 2]

GLuint texture_stone;
GLuint texture_kundali;
bool gbWireframe = false;
GLfloat TerrainRotation = 90.0f;
GLfloat DayColor = 0.0001f;
GLboolean IsNight = false;
GLboolean IsDay = true;
GLfloat NightColor = 0.0001f;
GLboolean AscendingDay = true;
GLboolean AscendingNight = true;

GLfloat CameraPosX = 0.0f;
GLboolean CameraPosXAscending = true;
GLfloat CameraUpZ = 1.0f;
GLboolean CameraUpZAscending = true;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void); // Double Buffer change 1]
	void update();

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("TowerTimeLapse");

	bool bDone = false;
	int iRet = 0;

	// code
	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, "Log file can not be created...", TEXT("ERROR"), MB_OK);
		exit(0); // Abortive but 0 and 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Tower Time Lapse"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		// own always on top, all others clipped
		// high performance rendering, WS_VISIBLE convention not compulsion
		// use in case of external painting. Makes your window visible even if ShowWindow() not called
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;

	// Removed -> UpdateWindow(hwnd);
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == 0)
	{
		fprintf(gpFile, "initialize() successful.\n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop code
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// Here call update()
				update(); // 3D change 8] New update approach introduced with game loop
			}
			// Here call display()
			display(); // Double Buffer change 2]
			// no call to unitialize()
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void resize(int width, int height);
	void uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		// In earlier past version this was LOWORD(wParam), now switch(wParam) only.
		// Becuase now ther is no info in HIWORD. Earlier 'scanrate' was stored in HIWORD.
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd); // NOTE: Request OS send WM_DESTROY message to my window.
			break;

		case 0x46: // 'f' or 'F'
			ToggleFullScreen();
			break;

		case 'G':
			gbShowGraph = !gbShowGraph;
			break;

		case 'W':
			gbWireframe = !gbWireframe;
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_RBUTTONDOWN:
		break;

	case WM_DESTROY:
		uninitialize(); // NOTE
		PostQuitMessage(0);
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); // lParam contains resized window width, height
		break;

		// Double Buffer change 3] WM_PAINT removed, new added
	case WM_ERASEBKGND:
		return (0); // Don't go to DefWindowProc, a) bcoz it posts WM_PAINT, which we don't want
		// b) use my display() when WM_APINT is not there

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

		}

		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}

void MakeFullScreen(void)
{
	dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

	if (dwStyle & WS_OVERLAPPEDWINDOW)
	{
		if (GetWindowPlacement(gHwnd, &wpPrev) &&
			GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
		{
			SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

			SetWindowPos(gHwnd,
				HWND_TOP,
				mi.rcMonitor.left,
				mi.rcMonitor.top,
				mi.rcMonitor.right - mi.rcMonitor.left,
				mi.rcMonitor.bottom - mi.rcMonitor.top,
				SWP_NOZORDER | SWP_FRAMECHANGED);
		}
	}

	ShowCursor(FALSE);
	bIsFullScreen = true;
}

// function implementations
int initialize(void)
{
	// function declarations
	void resize(int width, int height);
	BOOL loadTexture(GLuint *texture, TCHAR imageResourceId[]);
	void calculateTerrainZ(void);
	void calculateTerrainZSmooth(void);
	void MakeFullScreen(void);
	void initDust(int width, int height);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// step 1] initialize form descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; // NOTE: Hard coded, remember lecture
	// Windows OS stopped OpenGL support from OpenGL v1.5 bcoz of DirectX
	// Above 1.5+ versions are only bcoz of device driver support
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Double Buffer change 6]
	pfd.iPixelType = PFD_TYPE_RGBA; // NOTE
	pfd.cColorBits = 32; // NOTE: Can have variable bits for each
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; // 3D change 1]

	// step 2]
	ghdc = GetDC(gHwnd);

	// step 3] give form to OS
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	// return index is always 1 based. So 1 to 38 and not 0 to 37.
	// if 0 gets returned then it is failure

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	// NOTE: BRIDGING APIs, remember all platforms

	// step 4]
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return -3;
	}

	// step 5]
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	// convention 1 as per Red book // 3D change 5]
	glShadeModel(GL_SMOOTH); // Remember lecture, light, interpolation, flat

	// NOTE: This provides existence, releases order
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Checks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when rendering starts, display().
	//	"OpenGL is a state machine."

	glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
	glEnable(GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
	glDepthFunc(GL_LEQUAL); // 3D change 4.2] Less than or Equal to

	// convention 2 as per Red book // 3D change 6]
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // 3 modes: GL_NICEST, GL_FASTEST, GL_DONT_CARE

	// Lights step 3]
	//glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	//glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	//glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	//glEnable(GL_LIGHT0); // could not see if not enabled
	////glEnable(GL_LIGHTING); // lights

	calculateTerrainZSmooth();

	// convention 2 as per Red book // 3D change 6]
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // 3 modes: GL_NICEST, GL_FASTEST, GL_DONT_CARE

	//glEnable(GL_TEXTURE_2D);
	BOOL ret = loadTexture(&texture_stone, MAKEINTRESOURCE(IDBITMAP_STONE));
	//ret = loadTexture(&texture_kundali, MAKEINTRESOURCE(IDBITMAP_KUNDALI));

	//http://cse.csusb.edu/tongyu/courses/cs520/notes/texture.php
	//automatic texture coordinates generation
	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR); //GL_OBJECT_PLANE GL_OBJECT_LINEAR
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR); //GL_OBJECT_LINEAR
	glEnable(GL_TEXTURE_GEN_S);
	glEnable(GL_TEXTURE_GEN_T);
	//glEnable(GL_TEXTURE_2D);

	MakeFullScreen(); // NOTE

	// warm up call to resize, convention and not compulsion
	RECT rc;
	GetClientRect(gHwnd, &rc);
	int ww = rc.right - rc.left;
	int wh = rc.bottom - rc.top;

	initDust(ww, wh);

	resize(ww, wh);
	//resize(WIN_WIDTH, WIN_HEIGHT); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()


	return 0;
}

void uninitialize(void)
{
	/* Check whether fullscreen or not and
	if it is then restore to normal size
	and then proceed for uninitialization.
	Dots per inch problem, resolution disturbed.
	*/
	if (bIsFullScreen == true)
	{
		// verify
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	glDeleteTextures(1, &texture_stone);
	//glDeleteTextures(1, &texture_kundali);

	if (gpFile)
	{
		fprintf(gpFile, "Closing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void resize(int width, int height)
{
	// Perspective change 1]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

	// Perspective change 2] NOTE:Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Perspective change 3]
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void drawQuad(void)
{
	glBegin(GL_QUADS);

	// top
	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, 1.0f, 1.0f);

	// bottom
	//glColor3f(0.45f, 0.45f, 0.45f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	//glColor3f(0.55f, 0.55f, 0.55f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// front
	//glColor3f(0.0f, 0.0f, 1.0f);
	//glColor3ub(208, 212, 125);
	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	//glColor3f(0.0f, 0.9f, 0.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// back
	//glColor3f(0.0f, 1.0f, 1.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// right
	//glColor3f(1.0f, 0.0f, 1.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// left
	//glColor3f(1.0f, 1.0f, 0.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();
}

void drawMengerL1(void)
{
	glLoadIdentity();
	//glPushMatrix();
	//glTranslatef(-2.0f, -2.0f, angleCube); // Z negative = inside/away from you
	gluLookAt(0.0f, 0.0f, cameraPosZ,
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f);

	//glLoadIdentity();
	glTranslatef(-2.0f, -2.0f, 0);
	glRotatef(angleCube, 1.0f, 1.0f, 1.0f);
	drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 0.0f, -2.0f); drawQuad();
	glTranslatef(-4.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 0.0f, -2.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();

	//glLoadIdentity();
	//glPopMatrix();
	glTranslatef(-4.0f, 2.0f, 4.0f); drawQuad();
	glTranslatef(4.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(0.0f, 0.0f, -4.0f); drawQuad();
	glTranslatef(-4.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 2.0f, 4.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 0.0f, -2.0f); drawQuad();
	glTranslatef(-4.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 0.0f, -2.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
}




static float delayDust = 10.0f;
float theXDust = 0.0f;
typedef struct _star {
	float x, y;
	float vx, vy;
} star;
star* stars = NULL;
int num_dust = 150;

void initDust(int width, int height)
{
	width = 1024;
	height = 768;

	stars = (star*)malloc(sizeof(star) * num_dust);

	for (int i = 0; i < num_dust; i++) {
		/*stars[i].x = (rand() % width) * 0.00001f;
		stars[i].y = (rand() % height) * 0.00001f;
		stars[i].vx = ((rand() / (float)RAND_MAX * 5 + 2) /width) * 0.01f;
		stars[i].vy = 0;*/
		stars[i].x = (rand() % width) * 0.003f;
		stars[i].y = (rand() % height) * 0.003f;
		//stars[i].vx = 0.03f;
		//stars[i].vy = 0;
		fprintf(gpFile, "\n%f %f %f %f", stars[i].x, stars[i].y, stars[i].vx, stars[i].vy);
	}
}

void drawDust(void)
{
	int i; float x1 = 0, x2 = 2;
	float temp = 0, z = 0.0f;
	/*RECT rc;
	GetClientRect(gHwnd, &rc);*/
	int ww = 1;// 024; // rc.right - rc.left;

	glLineWidth(3.0f);

	for (i = 0; i < num_dust; i++) {
		z = 0;
		glBegin(GL_LINE_STRIP);
		glColor3ub(185, 122, 87);
		z += i % 5 == 0 ? 0.045f : i % 4 == 0 ? 0.035f : i % 3 == 0 ? 0.025f : i % 2 == 0 ? 0.015f : 0;
		//glVertex2i(x1, stars[i].y);
		glVertex3f(stars[i].x, stars[i].y, z);
		//glColor3ub(255, 255, 255);
		glVertex3f(stars[i].x + 0.003f, stars[i].y, z);
		glEnd();
	}

	//for (i = 0; i < num_dust; i++) {
	//	//stars[i].x += stars[i].vx;
	//	//stars[i].x = (int)theX % ww;
	//	temp = stars[i].x + stars[i].vx;
	//	if (stars[i].x < ww) {
	//		z = 0.0f;
	//		glBegin(GL_LINE_STRIP);
	//		//glColor3ub(255, 0, 0);
	//		glColor3ub(185, 122, 87);
	//		//glVertex2i(stars[i].x - stars[i].vx, stars[i].y);
	//		x1 = ((int)(theXDust + temp - stars[i].vx) % ww) * 0.1f;
	//		x2 = ((int)(theXDust + temp) % ww) * 0.1f;
	//		z += i % 5 == 0 ? 0.45f : i % 4 == 0 ? 0.35f : i % 3 == 0 ? 0.25f : i % 2 == 0 ? 0.15f : 0;
	//		//glVertex2i(x1, stars[i].y);
	//		glVertex3f(x1, stars[i].y, z);
	//		//glColor3ub(255, 255, 255);
	//		//glVertex2i(stars[i].x, stars[i].y);
	//		//glVertex2i(x1 > x2 ? x1 : x2, stars[i].y);
	//		glVertex3f(x1 > x2 ? x1 : x2, stars[i].y, z);
	//		glEnd();
	//	}
	//	else {
	//		//stars[i].x = 0;
	//		temp = 0;
	//	}
	//}

}

void updateDust(void)
{
	RECT rc;
	GetClientRect(gHwnd, &rc);
	int ww = 1024; // rc.right - rc.left;

	delayDust -= 1.0f;
	if (delayDust < 0.0f)
	{
		delayDust = 0.50f;
		theXDust += 0.1f;
	}
	if (theXDust >= 1.0)
		theXDust = -1.0f;
}



void display(void)
{
	void update();
	void drawGraph(void);
	void drawQuad(void);
	void drawMengerL1(void);
	void drawTerrain(void);
	void drawBack(void);
	void drawTower(float thexOrg, float theyOrg);
	void drawDust(void);
	void initDust(int, int);

	//initDust(1024,768);

	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]
	// Double Buffer change 7] glFlush(); removed

	glMatrixMode(GL_MODELVIEW);
	//drawMengerL1();

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.30f);
	drawDust();
	//glTranslatef(0.0f, 0.0f, -7.0f);

	////drawTower();

	////glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	////drawTower();

	////glPushMatrix();
	////gluLookAt(0, -2.25f, 0.50f, 0, 1.2f, 0, 0, 0, 1);
	////gluLookAt(CameraPosX, CameraUpZ, 1, 0, 1.2f, 0, 0, 0, 1); 
	//// 6th param - khali var
	//// 7th param - tilt inclination
	//// 8 - incline with plane
	//
	////glPopMatrix();
	////glScalef(0.5f, 0.5f, 0.5f);

	//glPushMatrix();
	//glPushMatrix();
	//glRotatef(-75.0f, 1.0f, 0.0f, 0.0f);
	//drawTerrain();

	////glRotatef(75.0f, 1.0f, 0.0f, 0.0f);
	////drawDust();

	//glPopMatrix();
	/*glTranslatef(0.0f, -0.20f, 0.0f);
	drawTower(2.5f);
	glTranslatef(0.0f, 0.20f, 0.0f);
	drawTower(2.2f);
	glTranslatef(0.0f, 0.20f, 0.0f);
	drawTower(2.0f);*/

	/*glTranslatef(0.0f, -0.2f, 0.0f);
	glRotatef(1.0f, 0.0f, 1.0f, 0.0f);
	for (float theY = -0.2f, theX = 2.2f; theY < 1.5f; theY +=0.1f, theX -= 0.1f)
	{
		glTranslatef(0.0f, 0.1f, 0.0f);
		drawTower(theX, 0.1f);
	}

	glColor4f(1, 1, 1, 1);
	glPopMatrix();*/

	//glLoadIdentity();
	//glTranslatef(0.0f, 1.0f, -13.0f);
	//glPopMatrix();

	//glTranslatef(0.0f, (TERRAIN_ROWS * (1.0f / TERRAIN_SCALE)), 1.0f);
	//glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	//drawBack();
	//glColor4f(1, 1, 1, 1);

	if (gbShowGraph)
	{
		//glLoadIdentity();
		drawGraph();
	}

	SwapBuffers(ghdc);
	//update(); // 3D change 8] New update approach introduced with game loop
}

// 3D change 8] New update approach introduced with game loop
void update()
{
	void updateDust(void);
	updateDust();

	// Rotate vice versa	
	/*if (ascendingPyramid)
	{
		angleCube += 0.025f;
		if (angleCube > 360.0f)
		{
			ascendingPyramid = false;
		}
	}
	else
	{
		angleCube -= 0.025f;
		if (angleCube < 0.0f)
		{
			ascendingPyramid = true;
		}
	}*/

	if (CameraPosXAscending)
	{
		CameraPosX += 0.0005f;
		if (CameraPosX > 7.0f)
		{
			CameraPosXAscending = false;
			CameraUpZAscending = false;
		}
	}
	else
	{
		CameraPosX -= 0.0005f;
		if (CameraPosX < -7.0f)
		{
			CameraPosXAscending = true;
			CameraUpZAscending = true;
		}
	}

	CameraUpZ = CameraUpZAscending == true ? 1.0f : -1.0f;

	angleCube += 0.05f;
	if (angleCube > 360.0f)
		angleCube = 0.0f;

	cameraPosZ += 0.02f;
	if (cameraPosZ > 50.0f)
		cameraPosZ = 0.0f;

	if (TerrainRotation > 70.0f)
		TerrainRotation -= 0.001f;

	if (IsDay)
	{
		if (AscendingDay)
		{
			DayColor += 0.00009f;
			if (DayColor >= 1.25f)
				AscendingDay = false;
		}
		else
		{
			DayColor -= 0.00009f;

			if (DayColor < 0.3f)
				IsNight = true;

			if (DayColor <= 0.0f)
			{
				AscendingDay = true;

				DayColor = 0.0001f;
				IsDay = false;
			}
		}
	}
	/*if (DayColor >= 1.0f)
	{
		IsNight = true;
		DayColor = 0.0001f;
		IsDay = false;
	}*/

	if (IsNight)
	{
		if (AscendingNight)
		{
			NightColor += 0.0002f;
			if (NightColor >= 1.0f)
				AscendingNight = false;
		}
		else
		{
			NightColor -= 0.0002f;

			if (NightColor <= 0.2f)
				IsDay = true;

			if (NightColor <= 0.0f)
			{
				AscendingNight = true;

				IsNight = false;
				NightColor = 0.0001f;
			}
		}
	}
	/*if (NightColor >= 1.0f)
	{
		IsNight = false;
		NightColor = 0.0001f;
		IsDay = true;
	}*/

	/*if (ascendingCameraZ)
	{
		cameraPosZ += 0.01f;
		if (cameraPosZ > 20.0f)
		{
			ascendingCameraZ = false;
		}
	}
	else
	{
		cameraPosZ -= 0.01f;
		if (cameraPosZ < -2.0f)
		{
			ascendingCameraZ = true;
		}
	}*/

	//flying += 0.02f;
	//for (int i = 0; i < TERRAIN_ROWS * 2; i++)
	//{
	//	float xoff = flying;// 0.02f;
	//	for (int j = 0; j < TERRAIN_COLS * 2; j++)
	//	{
	//		/*if (j%2 == 0)
	//			TerrainZ[i][j] = 4.0f / (GLfloat)rand();
	//		else
	//			TerrainZ[i][j] = -4.0f / (GLfloat)rand();*/

	//		if ((((i & 0x4) == 0) ^ ((j & 0x4) == 0))) // * 255 == 255)
	//			TerrainZ[i][j] = xoff; //4.0f / (GLfloat)rand();
	//		else
	//			TerrainZ[i][j] = -xoff;//-4.0f / (GLfloat)rand();
	//		//xoff += 0.002;
	//	}
	//	//yoff += 0.02;
	//}
	//
	//if (flying > 0.010f)
	//{
	//	flying = 0.0f;
	//}
}

void drawGraph(void)
{
	// variable declarations
	int i = 0;
	float step = 1.0f / LINES_TO_DRAW_GRAPH;
	float xcoord = step; // 0.0f;
	float ycoord = step; // 0.0f;
	float zcoord = step; // 0.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-xcoord, 1.0f);
		glVertex2f(-xcoord, -1.0f);
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord += step;
	}

	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		glVertex2f(-1.0f, -ycoord);
		glVertex2f(1.0f, -ycoord);
		ycoord += step;
	}

	glColor3f(1.0f, 1.0f, 0.0f);
	for (i = 0; i < LINES_TO_DRAW_GRAPH - 5; i++)
	{
		glVertex3f(zcoord, 0.0f, -1.0f);
		glVertex3f(zcoord, 0.0f, 1.0f);
		glVertex3f(-zcoord, 0.0f, -1.0f);
		glVertex3f(-zcoord, 0.0f, 1.0f);
		zcoord += step;
	}

	glEnd();
}

void calculateTerrainZ(void)
{
	bool isDown = false;

	//float yoff = 0.0f;
	flying += 0.02f;
	for (int i = 0; i < TERRAIN_ROWS * 2; i++)
	{
		float xoffOrg = 0.01f; //flying;
		float xoff = xoffOrg; //flying;
		for (int j = 0; j < TERRAIN_COLS * 2; j++)
		{
			/*if (j%2 == 0)
				TerrainZ[i][j] = 4.0f / (GLfloat)rand();
			else
				TerrainZ[i][j] = -4.0f / (GLfloat)rand();*/

				// Hex 0x12, 0x8
				//if ((((i & 0x8) == 0) ^ ((j & 0xA) == 0))) // * 255 == 255)
				//	TerrainZ[i][j] = xoff; //4.0f / (GLfloat)rand();
				//else
				//	TerrainZ[i][j] = -xoff;//-4.0f / (GLfloat)rand();

				//xoff += 0.005f;
				//if (xoff > (3.0f * 0.005) + 0.01f)
					//xoff = 0.01f;

			TerrainZ[i][j] = ((i & 16) ^ (j & 16)) ? xoff : -xoff;
			isDown = j & 8;

			if (isDown)
				xoff -= 0.005f;
			else
				xoff += 0.005f;

			if (j % 16 == 0)
				xoff = xoffOrg;// 0.01f;

			/*if(j & 0x4)
				xoff -= 0.005f;
			if (j & 0x8)
				xoff = 0.01f;*/
		}
		//yoff += 0.02;
	}

}

void calculateTerrainZSmooth(void)
{
	bool isDown = false;
	float xoffOrg = 0.0f; //0.01f//flying;
	float xoffStep = 0.001f;

	//float yoff = 0.0f;
	flying += 0.02f;
	for (int i = 0; i < TERRAIN_ROWS * 2; i++)
		//for (int i = 0; i < 32; i++)
	{
		float xoff = xoffOrg; //flying;
		float smoothFact = 1.0f;
		float smoothFactVary = 0.1f;

		for (int j = 0; j < TERRAIN_COLS * 2; j++)
			//for (int j = 0; j < 32; j++)
		{
			/*if (j%2 == 0)
				TerrainZ[i][j] = 4.0f / (GLfloat)rand();
			else
				TerrainZ[i][j] = -4.0f / (GLfloat)rand();*/

				// Hex 0x12, 0x8
				//if ((((i & 0x8) == 0) ^ ((j & 0xA) == 0))) // * 255 == 255)
				//	TerrainZ[i][j] = xoff; //4.0f / (GLfloat)rand();
				//else
				//	TerrainZ[i][j] = -xoff;//-4.0f / (GLfloat)rand();

				//xoff += 0.005f;
				//if (xoff > (3.0f * 0.005) + 0.01f)
					//xoff = 0.01f;
			/*if (i % 8 == 0)
				TerrainZ[i][j] = xoff;
			else*/
			{
				TerrainZ[i][j] = ((i & 16) ^ (j & 16)) ? xoff : -xoff;
				isDown = j & 8;

				smoothFactVary = ((j + 1) % 8) ? 0.1f : 0.05f;
				//smoothFactVary = ((j&4) ^ (j&4)) ? 0.1f : 0.05f;

				if (isDown) {
					xoff -= xoffStep * smoothFact;
					smoothFact += smoothFactVary;
				}
				else {
					xoff += xoffStep * smoothFact;
					smoothFact -= smoothFactVary;
				}

				if ((j + 1) % 16 == 0)
				{
					xoff = xoffOrg;// 0.01f;
					smoothFact = 1.0f;
					smoothFactVary = 0.1f;
				}
			}

			/*if(j & 0x4)
				xoff -= 0.005f;
			if (j & 0x8)
				xoff = 0.01f;*/
		}
		//yoff += 0.02;

		isDown = i & 8;
		if (isDown)
			xoffStep -= 0.004f;
		else
			xoffStep += 0.004f;
		//xoffStep
	}

}

void drawTerrain(void)
{
	int x, y;
	float step = -1.0f / TERRAIN_SCALE;
	int i = 0, j = 0, vertexNo = 1;

	glTranslatef(0.0f, -0.35f, 0.0f);
	//glRotatef(-TerrainRotation, 1.0f, 0.0f, 0.0f);
	////glTranslatef(0.0f, -0.35f + (TERRAIN_ROWS / 2.0f * step), 0.0f);
	//glTranslatef(0.0f, -0.35f, 0.0f);
	////glTranslatef(0.0f, -0.35f + 2.5f, 0.0f);
	////glRotatef(-(TerrainRotation-90.0f), 1.0f, 0.0f, 0.0f);
	//glPushMatrix();
	////glTranslatef(0.0f, -(TERRAIN_ROWS/2.0f * step), 0.0f);
	////glTranslatef(0.0f, -(2.5f), 0.0f);
	////glRotatef((90.0f), 1.0f, 0.0f, 0.0f);
	//glPushMatrix();
	//glPopMatrix();

	glEnable(GL_TEXTURE_2D);

	if (gbWireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//137, 78, 10 Dark Brown
	//249, 208, 129 light
	//253, 227, 190 pinkish
	for (y = -TERRAIN_ROWS, i = 0; y < TERRAIN_ROWS - 1; y++, i++)
		//for (y = -16, i = 0; y < 16 - 1; y++, i++)
	{
		glBindTexture(GL_TEXTURE_2D, texture_stone);
		glBegin(GL_QUAD_STRIP);
		//glColor3f(1.0f, 0.0f, 0.0f);
		vertexNo = 1;
		for (x = -TERRAIN_COLS, j = 0; x < TERRAIN_COLS; x++, j++)
			//for (x = -16, j = 0; x < 16; x++, j++)
		{
			//glNormal3f(0.0f, 0.0f, 1.0f);
			//glTexCoord2f(0.0f, 0.0f);
			//if (x % 2 == 0)
			//glColor3ub(233, 150, 122);
			//else
			//	glColor3ub(163, 40, 40);
			//glVertex3f(x * step, y * step, TerrainZ[i][j]); //0.2f/(GLfloat)rand()
			////glTexCoord2f(0.5f, 1.0f);
			////glColor3ub(255, 127, 80);
			//glColor3ub(255, 160, 122); // 255,99,71)
			////glColor3ub(255, 0,0); // 255,99,71)
			//glVertex3f(x * step, (y + 1) * step, TerrainZ[i + 1][j]); //-0.2f/(GLfloat)rand()

			/*if (vertexNo % 3 == 0)
				glColor3ub(137, 78, 10);
			else if (vertexNo % 2 == 0)
				glColor3ub(253, 227, 190);
			else
				glColor3ub(249, 208, 129);
			glVertex3f(x * step, y * step, TerrainZ[i][j]); //0.2f/(GLfloat)rand()
			vertexNo++;

			if (vertexNo % 3 == 0)
				glColor3ub(137, 78, 10);
			else if (vertexNo % 2 == 0)
				glColor3ub(253, 227, 190);
			else
				glColor3ub(249, 208, 129);
			glVertex3f(x * step, (y + 1) * step, TerrainZ[i + 1][j]);
			vertexNo++;*/


			//if (x % 2 == 0)
			//	glColor3ub(158, 105, 43);
			//else
			//	glColor3ub(243, 202, 127); // 255,99,71)
			//glVertex3f(x * step, y * step, TerrainZ[i][j]); //0.2f/(GLfloat)rand()
			//glColor3ub(253, 227, 190);
			//glVertex3f(x * step, (y + 1) * step, TerrainZ[i + 1][j]);


			/*if (i + 1 % 2 == 0)
			{
				if (j + 1 % 2 == 0)
					glTexCoord2f(1, 1);
				else
					glTexCoord2f(0, 1);
			}
			else
			{
				if (j + 1 % 2 == 0)
					glTexCoord2f(0, 0);
				else
					glTexCoord2f(1, 0);
			}*/
			/*if (i + 1 % 2 == 1)
			{
				if (j + 1 % 2 == 0)
					glTexCoord2f(0.8f, 1);
				else
					glTexCoord2f(1, 1);
			}
			else
			{
				if (j + 1 % 2 == 0)
					glTexCoord2f(0.8f, 0.8f);
				else
					glTexCoord2f(1, 0.8f);
			}*/
			glVertex3f(x * step, y * step, TerrainZ[i][j]); //0.2f/(GLfloat)rand()

			/*if (i + 2 % 2 == 0)
			{
				if (j + 1 % 2 == 0)
					glTexCoord2f(1, 1);
				else
					glTexCoord2f(0, 1);
			}
			else
			{
				if (j + 1 % 2 == 0)
					glTexCoord2f(0, 0);
				else
					glTexCoord2f(1, 0);
			}*/
			/*if (i + 1 % 2 == 1)
			{
				if (j + 1 % 2 == 0)
					glTexCoord2f(0.8f, 0.8f);
				else
					glTexCoord2f(1, 0.8f);
			}
			else
			{
				if (j + 1 % 2 == 0)
					glTexCoord2f(0.8f, 1);
				else
					glTexCoord2f(1, 1);
			}*/
			glVertex3f(x * step, (y + 1) * step, TerrainZ[i + 1][j]);


			//vertexNo++;
			//if (vertexNo > 3)
			//{
			//	vertexNo = 1;
			//}
			//if(vertexNo == 1)
			//	glTexCoord2f(0.50f, 1.0f);
			//else if (vertexNo == 2)
			//	glTexCoord2f(0.20f, 0.70f);
			//else if (vertexNo == 3)
			//	glTexCoord2f(0.80f, 0.70f);
			//glVertex3f(x * step, y * step, TerrainZ[i][j]); //0.2f/(GLfloat)rand()

			//vertexNo++;
			//if (vertexNo > 3)
			//{
			//	vertexNo = 1;
			//}
			//if (vertexNo == 1)
			//	glTexCoord2f(0.50f, 1.0f);
			//else if (vertexNo == 2)
			//	glTexCoord2f(0.20f, 0.70f);
			//else if (vertexNo == 3)
			//	glTexCoord2f(0.80f, 0.70f);
			//glVertex3f(x * step, (y + 1) * step, TerrainZ[i + 1][j]);
		}

		glEnd();
	}

	/*glBindTexture(GL_TEXTURE_2D, texture_stone);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-TERRAIN_COLS * step, -TERRAIN_ROWS * step, 0);
	glTexCoord2f(0.0f, 8.0f);
	glVertex3f(-TERRAIN_COLS * step, TERRAIN_ROWS * step, 0);
	glTexCoord2f(8.0f, 8.0f);
	glVertex3f(TERRAIN_COLS * step, TERRAIN_ROWS * step, 0);
	glTexCoord2f(8.0f, 0.0f);
	glVertex3f(TERRAIN_COLS * step, -TERRAIN_ROWS * step, 0);
	glEnd();*/


	if (gbWireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glDisable(GL_TEXTURE_2D);

}

BOOL loadTexture(GLuint *texture, TCHAR imageResourceId[])
{
	// variable declaration
	HBITMAP hBitmap = NULL;
	BITMAP bmp; // structure, so no NULL
	BOOL bStatus = FALSE;

	// conversion of image resource to image data
	hBitmap = (HBITMAP)LoadImage(
		GetModuleHandle(NULL),
		imageResourceId,
		IMAGE_BITMAP,
		0, 0,
		LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		bStatus = TRUE;

		// getting bitmap from data
		GetObject(hBitmap, sizeof(BITMAP), &bmp); // Verify
		// convention and not compulsion
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4); // state machine, gap of 4 b/w 2 rows RGBA

		glGenTextures(1, texture); // empty in, filled out. Memory from graphics card
		glBindTexture(GL_TEXTURE_2D, *texture); //

		// original 2 lines
		/*glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);*/

		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); //mix with light GL_MODULATE GL_REPLACE
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		// fill data
		gluBuild2DMipmaps(GL_TEXTURE_2D,
			3,
			bmp.bmWidth,
			bmp.bmHeight,
			GL_BGR_EXT,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		DeleteObject(hBitmap);
	}

	return bStatus;
}

void drawBack(void)
{
	float scaleX = 5.5f;
	float scaleY = 1.0f;
	GLfloat theZ = 0.0f; //1.0f

	if (IsDay)
	{
		glBegin(GL_QUADS);
		// front
		glColor3f(DayColor, 0.0f, 0.0f);
		//glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f * scaleX, 1.0f * scaleY, theZ);
		//glTexCoord2f(1.0f, 0.0f);
		glColor3f(DayColor, 0.0f, 0.0f);
		glVertex3f(-1.0f * scaleX, 1.0f * scaleY, theZ);
		//glTexCoord2f(1.0f, 1.0f);
		glColor3f(DayColor, DayColor, 0.0f);
		glVertex3f(-1.0f * scaleX, -1.0f * scaleY, theZ);
		//glTexCoord2f(0.0f, 1.0f);
		glColor3f(DayColor, DayColor, 0.0f);
		glVertex3f(1.0f * scaleX, -1.0f * scaleY, theZ);

		glEnd();
	}
	if (IsNight)
	{
		glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, NightColor);
		glVertex3f(1.0f * scaleX, 1.0f * scaleY, theZ);
		glColor3f(0.0f, 0.0f, NightColor);
		glVertex3f(-1.0f * scaleX, 1.0f * scaleY, theZ);
		glColor3f(0.0f, NightColor, NightColor);
		glVertex3f(-1.0f * scaleX, -1.0f * scaleY, theZ);
		glColor3f(0.0f, NightColor, NightColor);
		glVertex3f(1.0f * scaleX, -1.0f * scaleY, theZ);
		glEnd();
	}
}

void drawTower(float thexOrg, float theyOrg)
{
	glLineWidth(1);
	glColor4f(0.5f, 0.5f, 0.5f, 1);

	GLfloat y1 = 0.0f;
	GLfloat y2 = theyOrg;
	GLfloat xOrg = thexOrg;
	GLfloat x = xOrg;
	//GLfloat x2 = 1.0f;
	GLfloat z = 0.0f;
	//GLfloat z2 = 0.0f;

	/*
	Cyan + Yellow
	Blue + Yellow
	Green + Yellow
	GreyDark + Yellow
	GreyDark + Red
	colorRed: colorRedMedium + GreyDark
	GreyDark + Yellow + Cyan
	*/

	float factX = 0.1f;
	float factZ = 1.0f;
	GLfloat colorWhiteLight[] = { 0.80f, 0.80f, 0.80f };
	GLfloat colorCyanMedium[] = { 0.50f, 1.0f, 1.0f };
	GLfloat colorRed[] = { 1.0f,0,0 };
	GLfloat colorBlue[] = { 0,0,0.5f };
	GLfloat colorGreen[] = { 0,1,0 };
	GLfloat colorYellow[] = { 1.0f,1.0f,0 };
	GLfloat colorGreyDark[] = { 0.25f, 0.25f, 0.25f };
	GLfloat colorGrey[] = { 0.5f, 0.5f, 0.5f };
	GLfloat colorGreyLight[] = { 0.75f, 0.75f, 0.75f };
	bool isSecond = false;
	int i = 0;
	int numVertices = 0;
	float floor[1500][3];

	memset(floor, 0, sizeof(floor));

	if (gbWireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBegin(GL_TRIANGLE_STRIP);

	//for (int i = 0; i < 10; i++)
	while (x > 0.0f)
	{
		//glColor3fv(isSecond ? colorBlue : colorRed);
		//glColor3fv(isSecond ? colorRed: colorRedMedium);
		//glColor3fv(isSecond ? colorRedMedium: colorYellow);
		glColor3fv(isSecond ? colorCyanMedium : colorWhiteLight);
		glVertex3f(x, y1, z);
		//glColor3fv(isSecond ? colorGreyDark : colorYellow);
		glColor3fv(colorGreyDark);
		glVertex3f(x, y2, z);

		floor[numVertices][0] = x;
		floor[numVertices][1] = y2;
		floor[numVertices][2] = z;

		x -= (0.1f *factX);
		z += (0.1f *factZ);

		factX += (0.1f);
		factZ -= (0.1f);

		isSecond = !isSecond;
		++i;
		if (i > 5)
		{
			i = 0;
			factX = 0.1f;
			factZ = 1.0f;
		}
		++numVertices;
	}

	/*if (x != 0.0f)
	{*/
	glColor3fv(isSecond ? colorBlue : colorRed);
	glVertex3f(0, y1, z);
	glColor3fv(isSecond ? colorGreen : colorYellow);
	//glColor3fv(isSecond ? colorBlue : colorRed);
	glVertex3f(0, y2, z);

	floor[numVertices][0] = 0;
	floor[numVertices][1] = y2;
	floor[numVertices][2] = z;
	++numVertices;
	//}

	glEnd();

	glColor3fv(colorGreyLight);
	glBegin(GL_POLYGON);
	glVertex3f(0, y2, 0);
	for (int j = 0; j < numVertices; j++)
	{
		glVertex3f(floor[j][0], floor[j][1], floor[j][2]);
	}
	glEnd();

	if (gbWireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}


