/*
	Assignment: Draw Rotating 3D Cube in perspective.
	Date: 25 Mar 2019
 */

 // Headers
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h> // For perspective change 3]
#include <stdio.h>
#include "Resource.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib") // For perspective change 3]

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define LINES_TO_DRAW_GRAPH 20

#define TERRAIN_WIDTH 1700
#define TERRAIN_HEIGHT 800
#define TERRAIN_SCALE 20
#define TERRAIN_ROWS TERRAIN_HEIGHT/TERRAIN_SCALE
#define TERRAIN_COLS TERRAIN_WIDTH/TERRAIN_SCALE
#define TERRAIN_SCALE_01 1.0f/TERRAIN_SCALE

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
bool bIsFullScreen = false;
DWORD dwStyle; // global default initialized to zero
HWND gHwnd;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
MONITORINFO mi = { sizeof(MONITORINFO) };

HDC ghdc = NULL; // Common context
HGLRC ghrc = NULL; // Super context, OpenGL Rendering Context
bool gbActiveWindow = false;
FILE *gpFile = NULL;

// variable declarations
GLfloat angleCube = 0.0f;
bool ascendingPyramid = false;
bool gbShowGraph = false;
GLfloat cameraPosZ = 0.0f;
bool ascendingCameraZ = false;
GLfloat TerrainZ[TERRAIN_ROWS * 2][TERRAIN_COLS * 2];
float flying = 0.0f;

GLfloat LightAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f }; // Lights step 2]
GLfloat LightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Lights step 2]
GLfloat LightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f }; // Lights step 2]

GLuint texture_stone;
GLuint texture_kundali;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void); // Double Buffer change 1]
	void update();

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Rotate3DCube");

	bool bDone = false;
	int iRet = 0;

	// code
	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, "Log file can not be created...", TEXT("ERROR"), MB_OK);
		exit(0); // Abortive but 0 and 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Rotate 3D Cube: Native FFP Perspective"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		// own always on top, all others clipped
		// high performance rendering, WS_VISIBLE convention not compulsion
		// use in case of external painting. Makes your window visible even if ShowWindow() not called
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;

	// Removed -> UpdateWindow(hwnd);
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == 0)
	{
		fprintf(gpFile, "initialize() successful.\n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop code
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// Here call update()
				update(); // 3D change 8] New update approach introduced with game loop
			}
			// Here call display()
			display(); // Double Buffer change 2]
			// no call to unitialize()
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void resize(int width, int height);
	void uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		// In earlier past version this was LOWORD(wParam), now switch(wParam) only.
		// Becuase now ther is no info in HIWORD. Earlier 'scanrate' was stored in HIWORD.
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd); // NOTE: Request OS send WM_DESTROY message to my window.
			break;

		case 0x46: // 'f' or 'F'
			ToggleFullScreen();
			break;

		case 'G':
			gbShowGraph = !gbShowGraph;
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_RBUTTONDOWN:
		break;

	case WM_DESTROY:
		uninitialize(); // NOTE
		PostQuitMessage(0);
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); // lParam contains resized window width, height
		break;

		// Double Buffer change 3] WM_PAINT removed, new added
	case WM_ERASEBKGND:
		return (0); // Don't go to DefWindowProc, a) bcoz it posts WM_PAINT, which we don't want
		// b) use my display() when WM_APINT is not there

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

		}

		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}

// function implementations
int initialize(void)
{
	// function declarations
	void resize(int width, int height);
	BOOL loadTexture(GLuint *texture, TCHAR imageResourceId[]);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// step 1] initialize form descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; // NOTE: Hard coded, remember lecture
	// Windows OS stopped OpenGL support from OpenGL v1.5 bcoz of DirectX
	// Above 1.5+ versions are only bcoz of device driver support
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Double Buffer change 6]
	pfd.iPixelType = PFD_TYPE_RGBA; // NOTE
	pfd.cColorBits = 32; // NOTE: Can have variable bits for each
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; // 3D change 1]

	// step 2]
	ghdc = GetDC(gHwnd);

	// step 3] give form to OS
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	// return index is always 1 based. So 1 to 38 and not 0 to 37.
	// if 0 gets returned then it is failure

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	// NOTE: BRIDGING APIs, remember all platforms

	// step 4]
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return -3;
	}

	// step 5]
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	// convention 1 as per Red book // 3D change 5]
	glShadeModel(GL_SMOOTH); // Remember lecture, light, interpolation, flat

	// NOTE: This provides existence, releases order
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Checks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when rendering starts, display().
	//	"OpenGL is a state machine."

	glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
	glEnable(GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
	glDepthFunc(GL_LEQUAL); // 3D change 4.2] Less than or Equal to

	// convention 2 as per Red book // 3D change 6]
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // 3 modes: GL_NICEST, GL_FASTEST, GL_DONT_CARE

	// Lights step 3]
	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	glEnable(GL_LIGHT0); // could not see if not enabled
	//glEnable(GL_LIGHTING); // lights

	//float yoff = 0.0f;
	flying += 0.02f;
	for (int i = 0; i < TERRAIN_ROWS * 2; i++)
	{
		float xoff = 0.01f; //flying;
		for (int j = 0; j < TERRAIN_COLS * 2; j++)
		{
			/*if (j%2 == 0)
				TerrainZ[i][j] = 4.0f / (GLfloat)rand();
			else
				TerrainZ[i][j] = -4.0f / (GLfloat)rand();*/

			if ((((i & 0x4) == 0) ^ ((j & 0x4) == 0))) // * 255 == 255)
				TerrainZ[i][j] = xoff; //4.0f / (GLfloat)rand();
			else
				TerrainZ[i][j] = -xoff;//-4.0f / (GLfloat)rand();
			
			xoff += 0.005f;
			if (xoff > (2.0f * 0.005) + 0.01f)
				xoff = 0.01f;
		}
		//yoff += 0.02;
	}
	// convention 2 as per Red book // 3D change 6]
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // 3 modes: GL_NICEST, GL_FASTEST, GL_DONT_CARE

	glEnable(GL_TEXTURE_2D);
	BOOL ret = loadTexture(&texture_stone, MAKEINTRESOURCE(IDBITMAP_STONE));
	ret = loadTexture(&texture_kundali, MAKEINTRESOURCE(IDBITMAP_KUNDALI));


	// warm up call to resize, convention and not compulsion
	resize(WIN_WIDTH, WIN_HEIGHT); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()

	return 0;
}

void uninitialize(void)
{
	/* Check whether fullscreen or not and
	if it is then restore to normal size
	and then proceed for uninitialization.
	Dots per inch problem, resolution disturbed.
	*/
	if (bIsFullScreen == true)
	{
		// verify
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	glDeleteTextures(1, &texture_stone);
	glDeleteTextures(1, &texture_kundali);

	if (gpFile)
	{
		fprintf(gpFile, "Closing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void resize(int width, int height)
{
	// Perspective change 1]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

	// Perspective change 2] NOTE:Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Perspective change 3]
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void drawQuad(void)
{
	glBegin(GL_QUADS);

	// top
	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, 1.0f, 1.0f);

	// bottom
	//glColor3f(0.45f, 0.45f, 0.45f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	//glColor3f(0.55f, 0.55f, 0.55f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// front
	//glColor3f(0.0f, 0.0f, 1.0f);
	//glColor3ub(208, 212, 125);
	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	//glColor3f(0.0f, 0.9f, 0.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// back
	//glColor3f(0.0f, 1.0f, 1.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// right
	//glColor3f(1.0f, 0.0f, 1.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// left
	//glColor3f(1.0f, 1.0f, 0.0f);
	glColor3ub(138, 137, 194);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glColor3ub(138, 137, 255);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();
}

void drawMengerL1(void)
{
	glLoadIdentity();
	//glPushMatrix();
	//glTranslatef(-2.0f, -2.0f, angleCube); // Z negative = inside/away from you
	gluLookAt(0.0f, 0.0f, cameraPosZ,
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f);

	//glLoadIdentity();
	glTranslatef(-2.0f, -2.0f, 0);
	glRotatef(angleCube, 1.0f, 1.0f, 1.0f);
	drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 0.0f, -2.0f); drawQuad();
	glTranslatef(-4.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 0.0f, -2.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();

	//glLoadIdentity();
	//glPopMatrix();
	glTranslatef(-4.0f, 2.0f, 4.0f); drawQuad();
	glTranslatef(4.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(0.0f, 0.0f, -4.0f); drawQuad();
	glTranslatef(-4.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 2.0f, 4.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 0.0f, -2.0f); drawQuad();
	glTranslatef(-4.0f, 0.0f, 0.0f); drawQuad();

	glTranslatef(0.0f, 0.0f, -2.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
	glTranslatef(2.0f, 0.0f, 0.0f); drawQuad();
}

void display(void)
{
	void update();
	void drawGraph(void);
	void drawQuad(void);
	void drawMengerL1(void);
	void drawTerrain(void);
	void drawBack(void);

	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]
	// Double Buffer change 7] glFlush(); removed

	glMatrixMode(GL_MODELVIEW);
	//drawMengerL1();

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);
	drawTerrain();

	glLoadIdentity();
	glTranslatef(0.0f, 1.0f, -7.0f);
	drawBack();

	if (gbShowGraph)
		drawGraph();

	SwapBuffers(ghdc);
	update(); // 3D change 8] New update approach introduced with game loop
}

// 3D change 8] New update approach introduced with game loop
void update()
{
	// Rotate vice versa	
	/*if (ascendingPyramid)
	{
		angleCube += 0.025f;
		if (angleCube > 360.0f)
		{
			ascendingPyramid = false;
		}
	}
	else
	{
		angleCube -= 0.025f;
		if (angleCube < 0.0f)
		{
			ascendingPyramid = true;
		}
	}*/

	angleCube += 0.05f;
	if (angleCube > 360.0f)
		angleCube = 0.0f;

	cameraPosZ += 0.02f;
	if (cameraPosZ > 50.0f)
		cameraPosZ = 0.0f;

	/*if (ascendingCameraZ)
	{
		cameraPosZ += 0.01f;
		if (cameraPosZ > 20.0f)
		{
			ascendingCameraZ = false;
		}
	}
	else
	{
		cameraPosZ -= 0.01f;
		if (cameraPosZ < -2.0f)
		{
			ascendingCameraZ = true;
		}
	}*/

	//flying += 0.02f;
	//for (int i = 0; i < TERRAIN_ROWS * 2; i++)
	//{
	//	float xoff = flying;// 0.02f;
	//	for (int j = 0; j < TERRAIN_COLS * 2; j++)
	//	{
	//		/*if (j%2 == 0)
	//			TerrainZ[i][j] = 4.0f / (GLfloat)rand();
	//		else
	//			TerrainZ[i][j] = -4.0f / (GLfloat)rand();*/

	//		if ((((i & 0x4) == 0) ^ ((j & 0x4) == 0))) // * 255 == 255)
	//			TerrainZ[i][j] = xoff; //4.0f / (GLfloat)rand();
	//		else
	//			TerrainZ[i][j] = -xoff;//-4.0f / (GLfloat)rand();
	//		//xoff += 0.002;
	//	}
	//	//yoff += 0.02;
	//}
	//
	//if (flying > 0.010f)
	//{
	//	flying = 0.0f;
	//}
}

void drawGraph(void)
{
	// variable declarations
	int i = 0;
	float step = 1.0f / LINES_TO_DRAW_GRAPH;
	float xcoord = step; // 0.0f;
	float ycoord = step; // 0.0f;
	float zcoord = step; // 0.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-xcoord, 1.0f);
		glVertex2f(-xcoord, -1.0f);
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord += step;
	}

	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		glVertex2f(-1.0f, -ycoord);
		glVertex2f(1.0f, -ycoord);
		ycoord += step;
	}

	glColor3f(1.0f, 1.0f, 0.0f);
	for (i = 0; i < LINES_TO_DRAW_GRAPH - 5; i++)
	{
		glVertex3f(zcoord, 0.0f, -1.0f);
		glVertex3f(zcoord, 0.0f, 1.0f);
		glVertex3f(-zcoord, 0.0f, -1.0f);
		glVertex3f(-zcoord, 0.0f, 1.0f);
		zcoord += step;
	}

	glEnd();
}

void drawTerrain(void)
{
	int x, y;
	float step = -1.0f / TERRAIN_SCALE;
	int i = 0, j = 0;

	glTranslatef(0.0f, -0.35f, 0.0f);
	glRotatef(-70.0f, 1.0f, 0.0f, 0.0f);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	for (y = -TERRAIN_ROWS, i = 0; y < TERRAIN_ROWS - 1; y++, i++)
	{
		glBindTexture(GL_TEXTURE_2D, texture_stone);
		glBegin(GL_TRIANGLE_STRIP);
		//glColor3f(1.0f, 0.0f, 0.0f);
		for (x = -TERRAIN_COLS, j = 0; x < TERRAIN_COLS; x++, j++)
		{
			//glNormal3f(0.0f, 0.0f, 1.0f);
			glTexCoord2f(0.0f, 0.0f); 
			glVertex3f(x * step, y * step, TerrainZ[i][j]); //0.2f/(GLfloat)rand()
			glTexCoord2f(0.5f, 1.0f);
			glVertex3f(x * step, (y + 1) * step, TerrainZ[i + 1][j]); //-0.2f/(GLfloat)rand()
		}

		glEnd();
	}

	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

BOOL loadTexture(GLuint *texture, TCHAR imageResourceId[])
{
	// variable declaration
	HBITMAP hBitmap = NULL;
	BITMAP bmp; // structure, so no NULL
	BOOL bStatus = FALSE;

	// conversion of image resource to image data
	hBitmap = (HBITMAP)LoadImage(
		GetModuleHandle(NULL),
		imageResourceId,
		IMAGE_BITMAP,
		0, 0,
		LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		bStatus = TRUE;

		// getting bitmap from data
		GetObject(hBitmap, sizeof(BITMAP), &bmp); // Verify
		// convention and not compulsion
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4); // state machine, gap of 4 b/w 2 rows RGBA

		glGenTextures(1, texture); // empty in, filled out. Memory from graphics card
		glBindTexture(GL_TEXTURE_2D, *texture); //

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// fill data
		gluBuild2DMipmaps(GL_TEXTURE_2D,
			3,
			bmp.bmWidth,
			bmp.bmHeight,
			GL_BGR_EXT,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		DeleteObject(hBitmap);
	}

	return bStatus;
}

void drawBack(void)
{
	float scaleX = 4.5f;
	float scaleY = 1.0f;

	glBegin(GL_QUADS);
	// front
	//glColor3f(0.0f, 0.0f, 1.0f);
	//glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f * scaleX, 1.0f * scaleY, 1.0f);
	//glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f * scaleX, 1.0f * scaleY, 1.0f);
	//glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f * scaleX, -1.0f * scaleY, 1.0f);
	//glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f * scaleX, -1.0f * scaleY, 1.0f);

	glEnd();
}
