//
//  AppDelegate.h
//  HelloWindow
//
//  Created by Nishchal Nandanwar on 19/12/19.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

