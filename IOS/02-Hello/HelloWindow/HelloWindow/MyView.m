//
//  MyView.m
//  HelloWindow
//
//  Created by Nishchal Nandanwar on 19/12/19.
//
//

#import "MyView.h"

@implementation MyView
{
    NSString *centralText;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frameRect;
{
    // code
    self=[super initWithFrame:frameRect];
    
    if(self)
    {
        // initialization code here
        
        [self setBackgroundColor:[UIColor whiteColor]];
        
        centralText=@"Hello World !!!";
        
        // gesture recognition
        // tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer =
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:1];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long press
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    
    return(self);
}

- (void)drawRect:(CGRect)rect
{
    // black background
    UIColor *fillcolor=[UIColor blackColor];
    [fillcolor set];
    UIRectFill(rect);
    
    // dictionary with kvc
    NSDictionary *dictionaryForTextAttributes=[NSDictionary
                                               dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Helvetica" size:24], NSFontAttributeName,
                                               [UIColor greenColor], NSForegroundColorAttributeName,
                                               nil];
    
    CGSize textSize=[centralText sizeWithAttributes:dictionaryForTextAttributes];
    
    CGPoint point;
    point.x=(rect.size.width/2)-(textSize.width/2);
    point.y=(rect.size.height/2)-(textSize.height/2)+12;
    
    [centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
}

-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    ////
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    // code
    centralText = @"'singleTap' event";
    [self setNeedsDisplay]; //repaint
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    // code
    centralText = @"'DoubleTap' event";
    [self setNeedsDisplay]; //repaint
}

-(void) onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    // code
    centralText = @"'LongPress' event";
    [self setNeedsDisplay]; //repaint
}

- (void) dealloc
{
    // code
    
    [super dealloc];
}

@end
