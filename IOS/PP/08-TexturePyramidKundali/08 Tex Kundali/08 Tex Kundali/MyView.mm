//
//  MyView.m
//  BlueWnd
//
//  Created by Nishchal Nandanwar on 11/01/20.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "MyView.h"

#import "vmath.h"

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0
};

@implementation MyView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_pyramid;
    GLuint vbo_position_pyramid;
    GLuint vbo_texture_pyramid;
    GLuint vao_cube;
    GLuint vbo_position_cube;
    GLuint vbo_texture_cube;
    GLuint mvpUniform;
    
    GLuint pyramid_texture;
    GLuint cube_texture;
    GLuint samplerUniform;
    
    GLfloat angleRect;
    GLfloat angleTri;
    GLfloat ascendingTri;
    GLfloat ascendingRect;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(CGRect)frameRect;
{
    // code
    self=[super initWithFrame:frameRect];
    
    if(self)
    {
        // initialization code here
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                              kEAGLDrawablePropertyRetainedBacking,
                              kEAGLColorFormatRGBA8,
                              kEAGLDrawablePropertyColorFormat,
                              nil];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            [self release];
            return (nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        // LOGIC
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to create complete framebuffer object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return (nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s \n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60; // default since iOS 8.2
        
        
        ///////////////////////////////////////
        
        // *** VERTEX SHADER ***
        // create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // step 2] write vertex shader code
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec4 vPosition;" \
        "in vec2 vTexCoord;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec2 out_texcoord;" \
        "void main(void)" \
        "{" \
        "	gl_Position = u_mvp_matrix * vPosition;" \
        "	out_texcoord = vTexCoord;" \
        "}";
        
        // step 3] specify above source code to vertex shader obj
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
        
        // step 4] compile the vertex shader
        glCompileShader(vertexShaderObject);
        
        
        // steps for catching errors
        GLint iShaderCompileStatus = 0;
        GLint iInfoLogLength = 0;
        GLchar *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written = 0;
                    glGetShaderInfoLog(vertexShaderObject,
                                       iInfoLogLength,
                                       &written,
                                       szInfoLog);
                    printf("\nVertex Shader: Compilation Error: %s", szInfoLog);
                    free(szInfoLog);
                }
                else
                {
                    printf("\nVertex Shader: failed to malloc szInfoLog...");
                }
            }
            else
            {
                printf("\nVertex Shader: Something went wrong, infoLogLength is zero...");
            }
            [self release];
        }
        else
        {
            printf("\nVertex Shader compiled successfully.");
        }
        
        
        // reset flags
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        
        // step 1] define fragment shader obj
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // step 2] write fragment shader code
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "out vec4 FragColor;" \
        "in vec2 out_texcoord;" \
        "uniform sampler2D u_sampler;" \
        "void main(void)" \
        "{" \
        "	FragColor = texture(u_sampler, out_texcoord);" \
        "}";
        
        // step 3] specify above source code to fragment shader obj
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
        
        // step 4] compile the fragment shader
        glCompileShader(fragmentShaderObject);
        
        
        // steps for catching errors
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        if (iShaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written = 0;
                    glGetShaderInfoLog(fragmentShaderObject,
                                       iInfoLogLength,
                                       &written,
                                       szInfoLog);
                    printf("\nFragment Shader: Compilation Error: %s", szInfoLog);
                    free(szInfoLog);
                }
                else
                {
                    printf("\nFragment Shader: failed to malloc szInfoLog...");
                }
            }
            else
            {
                printf("\nFragment Shader: Something went wrong, infoLogLength is zero...");
            }
            [self release];
        }
        else
        {
            printf("\nFragment Shader compiled successfully.");
        }
        
        // reset flags
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        
        // create shader program obj
        // step 1] create
        shaderProgramObject = glCreateProgram();
        
        // step 2] Attach shaders
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // Pre-Linking binding to vertex attribute
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");
        
        // step 3] Link program
        glLinkProgram(shaderProgramObject);
        
        GLint iProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        if (iProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (GLchar*)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written = 0;
                    glGetProgramInfoLog(shaderProgramObject,
                                        iInfoLogLength,
                                        &written,
                                        szInfoLog);
                    printf("\nShader Program: Link Error: %s", szInfoLog);
                    free(szInfoLog);
                }
                else
                {
                    printf("\nShader Program: failed to malloc szInfoLog...");
                }
            }
            else
            {
                printf("\nShader Program: Something went wrong, infoLogLength is zero...");
            }
            [self release];
        }
        else
        {
            printf("\nShader program linked successfully.");
        }
        
        // Post-Linking retrieving uniform location
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        samplerUniform = glGetUniformLocation(shaderProgramObject, "u_sampler");
        
        
        // reset
        iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        
        // load texture
        pyramid_texture = [self loadTextureFromBMPFile:@"Stone" :@"bmp"];
        cube_texture = [self loadTextureFromBMPFile:@"Vijay_Kundali" :@"bmp"];

        
        
        //============== PYRAMID ====================
        // Apex, Left, Right; Front, Right, Back, Left
        const GLfloat pyramidVertices[] =
        {
            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            
            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            
            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            
            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f
        };
        const GLfloat pyramidTexcoords[] =
        {
            0.5f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
            
            0.5f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            
            0.5f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            
            0.5f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f
        };
        
        glGenVertexArrays(1, &vao_pyramid);
        glBindVertexArray(vao_pyramid);
        
        glGenBuffers(1, &vbo_position_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
        
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(pyramidVertices),
                     pyramidVertices,
                     GL_STATIC_DRAW); // attachya atta oot
        
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
                              3, // xyx
                              GL_FLOAT,
                              GL_FALSE, // isNormalized 0 to 1 NDC
                              0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
                              NULL); // no stride therefore no offest
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
        
        // ==== Texture ====
        glGenBuffers(1, &vbo_texture_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_pyramid);
        
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(pyramidTexcoords),
                     pyramidTexcoords,
                     GL_STATIC_DRAW); // attachya atta oot
        
        glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0,
                              2,
                              GL_FLOAT,
                              GL_FALSE,
                              0,
                              NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
        
        glBindVertexArray(0);
        
        //============== CUBE ====================
        // Top right, Top left, Bottom left, bottom right;
        // Top, Bottom, Front, Back, Right, Left
        GLfloat cubeVertices[] =
        {
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            
            1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            
            1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            
            -1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f
        };
        
        const GLfloat cubeTexcoords[] =
        {
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            
            1.0f, 1.0f,
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
            
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
            
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
            0.0f, 0.0f,
            
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
            0.0f, 0.0f,
            
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f
        };
        
        // for(int i=0; i<72; i++)
        // {
        //     if(cubeVertices[i] < 0.0f)
        //     {
        //         cubeVertices[i] = cubeVertices[i] + 0.25f;
        //     }
        //     else if(cubeVertices[i] > 0.0f)
        //     {
        //         cubeVertices[i] = cubeVertices[i] - 0.25f;
        //     }
        //     else
        //     {
        //         cubeVertices[i] = cubeVertices[i];
        //     }
        // }
        
        glGenVertexArrays(1, &vao_cube);
        glBindVertexArray(vao_cube);
        
        glGenBuffers(1, &vbo_position_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_cube);
        
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(cubeVertices),
                     cubeVertices,
                     GL_STATIC_DRAW); // attachya atta oot
        
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
                              3, // xyx
                              GL_FLOAT,
                              GL_FALSE, // isNormalized 0 to 1 NDC 
                              0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
                              NULL); // no stride therefore no offest
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        
        glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f); // Single uniform value to all vertices
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
        
        // ==== Texture ====
        glGenBuffers(1, &vbo_texture_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_cube);
        
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(cubeTexcoords),
                     cubeTexcoords,
                     GL_STATIC_DRAW); // attachya atta oot
        
        glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0,
                              2,
                              GL_FLOAT,
                              GL_FALSE,
                              0,
                              NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
        
        glBindVertexArray(0);
        
        
        //glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glEnable(GL_CULL_FACE);
        glEnable(GL_TEXTURE_2D);
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        
        perspectiveProjectionMatrix = vmath::mat4::identity();

        ///////////////////////////////////////
        
        
        // gesture recognition
        // tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer =
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector (onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:1];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long press
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    
    return(self);
}

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath = [[NSBundle mainBundle] pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage = [[UIImage alloc] initWithContentsOfFile:textureFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"Can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = bmpImage.CGImage;
    
    int bmWidth = (int)CGImageGetWidth(cgImage);
    int bmHeight = (int)CGImageGetHeight(cgImage);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4 for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 bmWidth,
                 bmHeight,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels); // _EXT try if not // verify 2-3
    
    // create mipmaps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D); // new addition
    
    CFRelease(imageData);
    return(bmpTexture);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    // code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    ///////////////////////////////////////
    
    glUseProgram(shaderProgramObject); // Binding shader pgm to OpenGL pgm
    
    // your code here
    // 9 steps
    // declaration of matrices
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 translationMatrix;
    vmath::mat4 scaleMatrix;
    
    // initialize above 2 matrices to identity
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    scaleMatrix = vmath::mat4::identity();
    
    // do necessary transformations like model scale, rotate, translate
    translationMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
    rotationMatrix = vmath::rotate(angleTri, 0.0f, 1.0f, 0.0f);
    modelViewMatrix = translationMatrix * rotationMatrix;
    
    // do necessary matrix multiplication
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    // In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum
    
    // send necessary matrices to shader in respective uniforms
    // display fn is dynamic, called in loop
    glUniformMatrix4fv(mvpUniform, // kashat kombaychay. globally declared used in display
                       1, // how many matrices
                       GL_FALSE, // transpose?
                       modelViewProjectionMatrix); // kashala chiktavaychay // verify
    // OpenGL/GLSL is column major, DirectX is row major
    
    // bind with vao - this will avoid many vbo repetitive calls in display
    glBindVertexArray(vao_pyramid);
    // IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
    // if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data
    
    // similarly bin with textures, if any
    glBindTexture(GL_TEXTURE_2D, pyramid_texture);
    //glEnable(GL_TEXTURE_2D);
    
    // draw the scene
    glDrawArrays(GL_TRIANGLES,
                 0, // from which array element to start. You can put different geometries in single array-interleaved
                 12); // how many to draw? Ex: Triangle - 3 vertices, Quad 4 vert
    
    // unbind vao
    glBindVertexArray(0);
    
    //============== SQAURE ====================
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    
    translationMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
    scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
    rotationMatrix = vmath::rotate(angleTri, angleTri, angleTri);
    
    // modelViewMatrix = modelViewMatrix * translationMatrix;
    // modelViewMatrix = modelViewMatrix * rotationMatrix;
    // modelViewMatrix = modelViewMatrix * scaleMatrix;
    
    modelViewMatrix = translationMatrix * scaleMatrix;
    // modelViewMatrix = modelViewMatrix * vmath::rotate(angleTri, 0.0f, 0.0f);
    // modelViewMatrix = modelViewMatrix * vmath::rotate(0.0f, angleTri, 0.0f);
    // modelViewMatrix = modelViewMatrix * vmath::rotate(0.0f, 0.0f, angleTri);
    // modelViewMatrix = modelViewMatrix * vmath::rotate(angleTri, 1.0f, 0.0f, 0.0f);
    // modelViewMatrix = modelViewMatrix * vmath::rotate(angleTri, 0.0f, 1.0f, 0.0f);
    // modelViewMatrix = modelViewMatrix * vmath::rotate(angleTri, 0.0f, 0.0f, 1.0f);
    // modelViewMatrix = modelViewMatrix * vmath::rotate(angleTri, 1.0f, 1.0f, 1.0f); // most wiered
    modelViewMatrix = modelViewMatrix * vmath::rotate(angleTri, angleTri, angleTri);
    
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform,
                       1,
                       GL_FALSE,
                       modelViewProjectionMatrix);
    
    glBindTexture(GL_TEXTURE_2D, cube_texture);
    glBindVertexArray(vao_cube);
    
    glDrawArrays(GL_TRIANGLE_FAN,
                 0,
                 4); // how many to draw? Ex: Triangle - 3 vertices, Quad 4 vert
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    
    glBindVertexArray(0);
    
    
    glUseProgram(0); // Unbinding
    
    ///////////////////////////////////////
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    [self myUpdate];
}

- (void)myUpdate
{
    // Rotate vice versa
    if (ascendingRect)
    {
        angleRect += 0.5f;
        if (angleRect > 360.0f)
        {
            ascendingRect = false;
        }
    }
    else
    {
        angleRect -= 0.5f;
        if (angleRect < 0.0f)
        {
            ascendingRect = true;
        }
    }
    
    if (ascendingTri)
    {
        angleTri += 0.5f;
        if (angleTri > 360.0f)
        {
            ascendingTri = false;
        }
    }
    else
    {
        angleTri -= 0.5f;
        if (angleTri < 0.0f)
        {
            ascendingTri = true;
        }
    }
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    glViewport(0, 0, width, height);
    
    ///////////////////////////////////////
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f,
                                                     (fwidth / fheight),
                                                     0.1f,
                                                     100.0f);
    ///////////////////////////////////////
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to create complete framebuffer object %x \n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void) startAnimation
{
    printf("within startAnimation\n");
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void) stopAnimation
{
    printf("within stopAnimation\n");
    if(!isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    ////
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    // code
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    // code
}

-(void) onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    // code
}

- (void) dealloc
{
    // code
    // PP shader dtor
    // Safe Release
    // Don't declare shader objects globally, use locally in initialize n use as necessary
    if (vbo_texture_pyramid)
    {
        glDeleteBuffers(1, &vbo_texture_pyramid);
        vbo_texture_pyramid = 0;
    }
    if (vbo_position_pyramid)
    {
        glDeleteBuffers(1, &vbo_position_pyramid);
        vbo_position_pyramid = 0;
    }
    if (vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }
    
    if (vbo_texture_cube)
    {
        glDeleteBuffers(1, &vbo_texture_cube);
        vbo_texture_cube = 0;
    }
    if (vbo_position_cube)
    {
        glDeleteBuffers(1, &vbo_position_cube);
        vbo_position_cube = 0;
    }
    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    
    if (cube_texture)
    {
        glDeleteTextures(1, &cube_texture);
        cube_texture = 0;
    }
    if (pyramid_texture)
    {
        glDeleteTextures(1, &pyramid_texture);
        pyramid_texture = 0;
    }
    
    if (shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNo;
        
        glUseProgram(shaderProgramObject);
        
        // ask pgm how many shaders attached to you
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        
        GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
        if (pShaders)
        {
            glGetAttachedShaders(shaderProgramObject,
                                 shaderCount,
                                 &shaderCount, /// using same var
                                 pShaders);
            
            for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
            {
                glDetachShader(shaderProgramObject, pShaders[shaderNo]);
                glDeleteShader(pShaders[shaderNo]);
                pShaders[shaderNo] = 0;
            }
            free(pShaders);
        }
        
        glDeleteProgram(shaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
        shaderProgramObject = 0;
        glUseProgram(0);
    }

    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}

@end
