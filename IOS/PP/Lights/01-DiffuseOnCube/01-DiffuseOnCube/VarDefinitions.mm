#pragma once
#import "VarDeclarations.h"

int MyCount = 10;

GLuint vao_cube = 0;
GLuint vbo_position_cube = 0;
GLuint vbo_light_cube = 0;
GLuint gShaderProgramObject = 0;

GLuint mvUniform = -1; // model view
GLuint projectionUniform = -1;
mat4 perspectiveProjectionMatrix;

GLfloat angleCube = 0.0f;
GLfloat ascendingCube = true;

bool gbShowLight = false;
bool gbAnimate = false;

GLuint ldUniform = -1;
GLuint kdUniform = -1;
GLuint lightPoistionUniform = -1;
GLuint isLKeyPressedUniform = -1;
