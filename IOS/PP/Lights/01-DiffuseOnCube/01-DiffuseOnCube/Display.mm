#import "Logic.h"

void update(void)
{
    // Rotate vice versa
    if (ascendingCube)
    {
        angleCube += 0.1f;
        if (angleCube > 360.0f)
        {
            ascendingCube = false;
        }
    }
    else
    {
        angleCube -= 0.1f;
        if (angleCube < 0.0f)
        {
            ascendingCube = true;
        }
    }
}

void display(void)
{
    // NOTE: This makes existence functional and executes given order
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]
    
    // Use Pass Through Shader Program
    glUseProgram(gShaderProgramObject); // Binding shader pgm to OpenGL pgm
    
    // your code here
    // 9 steps
    // declaration of matrices
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
    mat4 rotationMatrix;
    mat4 translationMatrix;
    
    // initialize above 2 matrices to identity
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    rotationMatrix = mat4::identity();
    translationMatrix = mat4::identity();
    
    // do necessary transformations like model scale, rotate, translate
    // here in this pgm no transformation, but in later pgms
    rotationMatrix = rotate(0.0f, angleCube, 0.0f);
    translationMatrix = translate(0.0f, 0.0f, -6.0f);
    modelViewMatrix = translationMatrix * rotationMatrix;
    
    // do necessary matrix multiplication
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    // In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum
    
    // send necessary matrices to shader in respective uniforms
    // display fn is dynamic, called in loop
    glUniformMatrix4fv(mvUniform, // kashat kombaychay. globally declared used in display
                       1, // how many matrices
                       GL_FALSE, // transpose?
                       modelViewMatrix); // kashala chiktavaychay // verify
    // OpenGL/GLSL is column major, DirectX is row major
    glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
    
    if (gbShowLight)
    {
        glUniform1i(isLKeyPressedUniform, 1);
        glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f);
        glUniform3f(kdUniform, 0.50f, 0.50f, 0.50f);
        glUniform4f(lightPoistionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
    }
    else
    {
        glUniform1i(isLKeyPressedUniform, 0);
    }
    // bind with vao - this will avoid many vbo repetitive calls in display
    glBindVertexArray(vao_cube);
    // IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
    // if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data
    
    // similarly bin with textures, if any
    // glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);
    
    // draw the scene
    glDrawArrays(GL_TRIANGLE_FAN,
                 0, // from which array element to start. You can put different geometries in single array-interleaved
                 4); // how many to draw? Ex: Triangle - 3 vertices, Quad 4 vert
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    
    // unbind vao
    glBindVertexArray(0);
    
    glUseProgram(0); // Unbinding
    
    // SwapBuffers(ghdc);
}
