#pragma once
#import "Logic.h"

#import <UIKit/UIKit.h>

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

void incrCount(void)
{
	++MyCount;
}

int getCount(void)
{
	return MyCount;
}

void uninitializePP(void)
{
	void uninitSPO(GLuint *theSpo);
	void uninitVAO(GLuint *theVao);
	void uninitVBO(GLuint *theVbo);

	// PP shader dtor
	// Safe Release
    uninitVBO(&vbo_light_sphere_scn);
    uninitVBO(&vbo_position_sphere_scn);
    uninitVAO(&vao_sphere_scn);
    
    uninitSPO(&gShaderProgramObject_pf_scn);
    uninitSPO(&gShaderProgramObject_pv_scn);
    
    printf("\nuninitializePP");
}

void uninitVBO(GLuint *theVbo)
{
	if (*theVbo)
	{
		glDeleteBuffers(1, theVbo);
		*theVbo = 0;
	}
}

void uninitVAO(GLuint *theVao)
{
	if (*theVao)
	{
		glDeleteVertexArrays(1, theVao);
		*theVao = 0;
	}
}

void uninitSPO(GLuint *theSpo)
{
	if (*theSpo)
	{
		GLsizei shaderCount;
		GLsizei shaderNo;

		glUseProgram(*theSpo);

		// ask pgm how many shaders attached to you
		glGetProgramiv(*theSpo, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
		if (pShaders)
		{
			glGetAttachedShaders(*theSpo, shaderCount, &shaderCount, pShaders); // using same var

			for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
			{
				glDetachShader(*theSpo, pShaders[shaderNo]);
				glDeleteShader(pShaders[shaderNo]);
				pShaders[shaderNo] = 0;
			}
			free(pShaders);
		}

		glDeleteProgram(*theSpo); // Not actually deleting but changing machine state. Not shaikh chilli
		*theSpo = 0;
		glUseProgram(0);
	}
}

const char* GLenumToString(GLenum shaderType)
{
	switch (shaderType)
	{
	case GL_VERTEX_SHADER:
		return "Vertex Shader";
	case GL_FRAGMENT_SHADER:
		return "Fragment Shader";
	default:
		return "Wrong Shader";
	}
}

bool createShaders(enum ShaderModes shaderMode, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms)
{
    bool createVertexShader(GLuint *vertexShaderObject, enum ShaderModes shaderMode);
    bool createFragmentShader(GLuint *fragmentShaderObject, enum ShaderModes shaderMode);
    bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms);
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    
    if (!createVertexShader(&vertexShaderObject, shaderMode))
    {
        return false;
    }
    if (!createFragmentShader(&fragmentShaderObject, shaderMode))
    {
        return false;
    }
    return createShaderProgramAndLink(vertexShaderObject, fragmentShaderObject, gShaderProgramObject, shaderUniforms);
}

bool createVertexShader(GLuint *vertexShaderObject, enum ShaderModes shaderMode)
{
    bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);
    
    const GLchar *vertexShaderSourceCode_pv =
    "#version 300 es" \
    "\n" \
    "precision highp float;" \
    "precision mediump int;" \
    "\n" \
    "in vec4 vPosition;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "in vec3 vNormal;" \
    "uniform int u_isLKeyPressed;" \
    "\n" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    "\n" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_shinyness;" \
    "\n" \
    "uniform vec4 u_light_position;" \
    "out vec3 phong_ads_light;" \
    "\n" \
    "void main(void)" \
    "{" \
    "	if(u_isLKeyPressed == 1)" \
    "	{" \
    "		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
    "		vec3 tNorm = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
    "		vec3 light_direction = normalize( vec3(u_light_position - eye_coordinates) );" \
    "\n" \
    "		float tnDotLightDir = max(dot(light_direction, tNorm), 0.0);" \
    "		vec3 reflection_vector = reflect(-light_direction, tNorm);" \
    "		vec3 viewer_vector = normalize( vec3(-eye_coordinates).xyz );" \
    "\n" \
    "		vec3 ambient = u_la * u_ka;" \
    "		vec3 diffuse = u_ld * u_kd * tnDotLightDir;" \
    "		vec3 specular = u_ls * u_ks * pow( max( dot(reflection_vector, viewer_vector), 0.0f), u_shinyness);" \
    "\n" \
    "		phong_ads_light = ambient + diffuse + specular;" \
    "		" \
    "	}" \
    "	else" \
    "	{" \
    "		phong_ads_light = vec3(1.0, 1.0, 1.0);" \
    "	}" \
    "\n" \
    "	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    
    //"		vec3 specular = u_ls * u_ks * pow( max( dot(reflection_vector, viewer_vector), 0.0f), u_shinyness);" \
    //"		vec3 specular = u_ls * u_ks * pow( max( normalize(dot(reflection_vector, viewer_vector)), 0.0f), u_shinyness);" \
    //"		vec3 source = vec3(u_light_position - eye_coordinates).xyz;" \
    
    
    const GLchar *vertexShaderSourceCode_pf =
    "#version 300 es" \
    "\n" \
    "precision highp float;" \
    "precision mediump int;" \
    "in vec4 vPosition;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "in vec3 vNormal;" \
    "uniform int u_isLKeyPressed;" \
    "\n" \
    "uniform vec4 u_light_position;" \
    "\n" \
    "out vec3 tNorm;" \
    "out vec3 light_direction;" \
    "out vec3 viewer_vector;" \
    "\n" \
    "void main(void)" \
    "{" \
    "	if(u_isLKeyPressed == 1)" \
    "	{" \
    "		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
    "		tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
    "		light_direction = vec3(u_light_position - eye_coordinates);" \
    "		viewer_vector = vec3(-eye_coordinates).xyz;" \
    "	}" \
    "\n" \
    "	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    
    //"		vec3 specular = u_ls * u_ks * pow( max( dot(reflection_vector, viewer_vector), 0.0f), u_shinyness);" \
    //"		vec3 specular = u_ls * u_ks * pow( max( normalize(dot(reflection_vector, viewer_vector)), 0.0f), u_shinyness);" \
    //"		vec3 source = vec3(u_light_position - eye_coordinates).xyz;" \
    
    switch (shaderMode)
    {
        case PER_VERTEX:
            return createShader(vertexShaderObject, GL_VERTEX_SHADER, vertexShaderSourceCode_pv);
            break;
            
        case PER_FRAGMENT:
            return createShader(vertexShaderObject, GL_VERTEX_SHADER, vertexShaderSourceCode_pf);
            break;
            
        default:
            return false;
            break;
    }
}

bool createFragmentShader(GLuint *fragmentShaderObject, enum ShaderModes shaderMode)
{
    bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);
    
    const GLchar *fragmentShaderSourceCode_pv =
    "#version 300 es" \
    "\n" \
    "precision highp float;" \
    "precision mediump int;" \
    "\n" \
    "in vec3 phong_ads_light;" \
    "uniform int u_isLKeyPressed;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "	FragColor = vec4(phong_ads_light, 1.0);" \
    "}";
    
    const GLchar *fragmentShaderSourceCode_pf =
    "#version 300 es" \
    "\n" \
    "precision highp float;" \
    "precision mediump int;" \
    "in vec3 phong_ads_light;" \
    "uniform int u_isLKeyPressed;" \
    "out vec4 FragColor;" \
    "\n" \
    "in vec3 tNorm;" \
    "in vec3 light_direction;" \
    "in vec3 viewer_vector;" \
    "\n" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    "\n" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_shinyness;" \
    "\n" \
    "void main(void)" \
    "{" \
    "	vec3 phong_ads_light;" \
    "	if(u_isLKeyPressed == 1)" \
    "	{" \
    "		vec3 normalized_tNorm = normalize(tNorm);" \
    "		vec3 normalized_light_direction = normalize(light_direction);" \
    "		vec3 normalized_viewer_vector = normalize(viewer_vector);" \
    "\n" \
    "		vec3 reflection_vector = reflect(-normalized_light_direction, normalized_tNorm);" \
    "		float tnDotLightDir = max(dot(normalized_light_direction, normalized_tNorm), 0.0);" \
    "\n" \
    "		vec3 ambient = u_la * u_ka;" \
    "		vec3 diffuse = u_ld * u_kd * tnDotLightDir;" \
    "		vec3 specular = u_ls * u_ks * pow( max( dot(reflection_vector, normalized_viewer_vector), 0.0f), u_shinyness);" \
    "		phong_ads_light = ambient + diffuse + specular;" \
    "	}" \
    "	else" \
    "	{" \
    "		phong_ads_light = vec3(1.0, 1.0, 1.0);" \
    "	}" \
    "\n" \
    "	FragColor = vec4(phong_ads_light, 1.0);" \
    "}";
    
    switch (shaderMode)
    {
        case PER_VERTEX:
            return createShader(fragmentShaderObject, GL_FRAGMENT_SHADER, fragmentShaderSourceCode_pv);
            break;
            
        case PER_FRAGMENT:
            return createShader(fragmentShaderObject, GL_FRAGMENT_SHADER, fragmentShaderSourceCode_pf);
            break;
            
        default:
            return false;
            break;
    }
}

bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode)
{
	const char* GLenumToString(GLenum shaderType);

	*shaderObject = glCreateShader(typeOfShader);
	glShaderSource(*shaderObject, 1, (const GLchar**)&shaderSourceCode, NULL);
	glCompileShader(*shaderObject);

	// steps for catching errors
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(*shaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*shaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(*shaderObject, iInfoLogLength, &written, szInfoLog);
				printf("\n%s: Compilation Error: %s", GLenumToString(typeOfShader), szInfoLog);
				free(szInfoLog);
			}
			else
			{
				printf("\n%s: failed to malloc szInfoLog...", GLenumToString(typeOfShader));
			}
		}
		else
		{
			printf("\n%s: Something went wrong, infoLogLength is zero...", GLenumToString(typeOfShader));
		}
		return false;
	}
	else
	{
		printf("\n%s compiled successfully.", GLenumToString(typeOfShader));
	}

	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	return true;
}

bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms)
{
    // step 1] create shader program obj
    *gShaderProgramObject = glCreateProgram();
    
    // step 2] Attach shaders
    glAttachShader(*gShaderProgramObject, vertexShaderObject);
    glAttachShader(*gShaderProgramObject, fragmentShaderObject);
    
    // Pre-Linking binding to vertex attribute
    glBindAttribLocation(*gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(*gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
    
    // step 3] Link program
    glLinkProgram(*gShaderProgramObject);
    
    GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetProgramiv(*gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(*gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetProgramInfoLog(*gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				printf("\nShader Program: Link Error: %s", szInfoLog);
				free(szInfoLog);
			}
			else
			{
				printf("\nShader Program: failed to malloc szInfoLog...");
			}
		}
		else
		{
			printf("\nShader Program: Something went wrong, infoLogLength is zero...");
		}
		return false;
	}
	else
	{
		printf("\nShader program linked successfully.");
	}

    // Post-Linking retrieving uniform location
    shaderUniforms->modelUniform = glGetUniformLocation(*gShaderProgramObject, "u_model_matrix");
    shaderUniforms->viewUniform = glGetUniformLocation(*gShaderProgramObject, "u_view_matrix");
    shaderUniforms->projectionUniform = glGetUniformLocation(*gShaderProgramObject, "u_projection_matrix");
    shaderUniforms->isLKeyPressedUniform = glGetUniformLocation(*gShaderProgramObject, "u_isLKeyPressed");
    
    shaderUniforms->laUniform = glGetUniformLocation(*gShaderProgramObject, "u_la");
    shaderUniforms->ldUniform = glGetUniformLocation(*gShaderProgramObject, "u_ld");
    shaderUniforms->lsUniform = glGetUniformLocation(*gShaderProgramObject, "u_ls");
    
    shaderUniforms->kaUniform = glGetUniformLocation(*gShaderProgramObject, "u_ka");
    shaderUniforms->kdUniform = glGetUniformLocation(*gShaderProgramObject, "u_kd");
    shaderUniforms->ksUniform = glGetUniformLocation(*gShaderProgramObject, "u_ks");
    shaderUniforms->shinynessUniform = glGetUniformLocation(*gShaderProgramObject, "u_shinyness");
    
    shaderUniforms->lightPoistionUniform = glGetUniformLocation(*gShaderProgramObject, "u_light_position");
    
    // reset
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	return true;
}

void bindVao(GLuint *theVao)
{
	glGenVertexArrays(1, theVao);
	glBindVertexArray(*theVao);
}

void unbindVao()
{
	glBindVertexArray(0);
}

void bindVbo(GLuint *theVbo)
{
	glGenBuffers(1, theVbo);
	glBindBuffer(GL_ARRAY_BUFFER, *theVbo);
}

void unbindVbo()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void bindVaoVbo(GLuint *theVao, GLuint *theVbo)
{
	glGenVertexArrays(1, theVao);
	glBindVertexArray(*theVao);

	glGenBuffers(1, theVbo);
	glBindBuffer(GL_ARRAY_BUFFER, *theVbo);
}

void unbindVboVao()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void fillBufferData(const float data[], int sizeofData, int noOfElementsInOneTuple, bool isStatic, int amc_attribute)
{
	if (isStatic)
	{
		glBufferData(GL_ARRAY_BUFFER, sizeofData, data, GL_STATIC_DRAW); // attachya atta oot
	}
	else
	{
		glBufferData(GL_ARRAY_BUFFER, sizeofData, NULL, GL_DYNAMIC_DRAW);
	}

	glVertexAttribPointer(amc_attribute,
		noOfElementsInOneTuple, // 3=xyx, 2=st
		GL_FLOAT,
		GL_FALSE, // isNormalized 0 to 1 NDC 
		0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
		NULL); // no stride therefore no offest

	glEnableVertexAttribArray(amc_attribute);
}


void initVaoSphere(GLuint *vao_sphere)
{
    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();
    
    bindVao(vao_sphere);
    
    bindVbo(&vbo_position_sphere_scn);
    fillBufferData(sphere_vertices, sizeof(sphere_vertices), 3, true, AMC_ATTRIBUTE_POSITION);
    unbindVbo();
    
    bindVbo(&vbo_light_sphere_scn);
    fillBufferData(sphere_normals, sizeof(sphere_normals), 3, true, AMC_ATTRIBUTE_NORMAL);
    unbindVbo();
    
    // element vbo
    glGenBuffers(1, &gVbo_sphere_element_scn);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element_scn);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    unbindVao();
}

void initVAOs(void)
{
    void initVaoSphere(GLuint *vao_sphere);
    
    initVaoSphere(&vao_sphere_scn);
    // initVaoSphere(&vao_sphere_pf); // IMP NOTE: Not at all needed, use only one vao+vbo, just change program, else wrong result
}
