#pragma once
#import "VarDeclarations.h"

int MyCount = 10;

GLuint vao_sphere_scn = -1;
GLuint vbo_position_sphere_scn = -1;
GLuint vbo_light_sphere_scn = -1;
GLuint gShaderProgramObject_pf_scn = 0;
GLuint gShaderProgramObject_pv_scn = 0;

GLuint modelUniform_scn = -1;
GLuint viewUniform_scn = -1;
GLuint projectionUniform_scn = -1;
mat4 perspectiveProjectionMatrix_scn;

GLfloat angleCube_scn = 0.0f;
GLfloat ascendingCube_scn = true;

bool gbShowLight_scn = true;
bool gbAnimate_scn = YES;

bool gbXKeyPressed = false;
bool gbYKeyPressed = false;
bool gbZKeyPressed = true;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gVbo_sphere_element_scn = -1;

int gNumVertices = -1;
int gNumElements = -1;

enum ShaderModes gSelectedMode_scn = PER_FRAGMENT;
struct ShaderUniforms PerVertexUniforms_scn, PerFragmentUniforms_scn; // Must declare vars else unresolved symbols linking error
