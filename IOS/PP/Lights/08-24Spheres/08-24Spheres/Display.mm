#import "Logic.h"

float light_ambient[4] = { 0.0f, 0.0f, 0.0f, 1.0f }; // 0.0f
//float light_diffuse[4] = { 0.0f, 0.6589f, 0.6667f, 1.0f };
float light_diffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Albedo
float light_specular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float light_position[4] = { 100.0f, 100.0f, 100.0f, 1.0f };

float material_ambient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float material_diffuse[4] = { 0.5f, 0.2f, 0.7f, 1.0f };
float material_specular[4] = { 0.7f, 0.7f, 0.7f, 1.0f };
float material_shinyness = 128.0f; // 128

/////////////////////////////////

GLfloat material_ambient_arr[][4] = {{0.0215f,0.1745f,0.0215f,1.0f},
    {0.135f,0.2225f,0.1575f,1.0f},
    {0.05375f,0.05f,0.06625f,1.0f},
    {0.25f,0.20725f,0.20725f,1.0f},
    {0.1745f,0.01175f,0.01175f,1.0f},
    {0.1f,0.18725f,0.1745f,1.0f},
    {0.329412f,0.223529f,0.027451f,1.0f},
    {0.2125f,0.1275f,0.054f,1.0f},
    {0.25f,0.25f,0.25f,1.0f},
    {0.19125f,0.0735f,0.0225f,1.0f},
    {0.24725f,0.1995f,0.0745f,1.0f},
    {0.19225f,0.19225f,0.19225f,1.0f},
    {0.0f,0.0f,0.0f,1.0f},
    {0.0f,0.1f,0.06f,1.0f},
    {0.0f,0.0f,0.0f,1.0f},
    {0.0f,0.0f,0.0f,1.0f},
    {0.0f,0.0f,0.0f,1.0f},
    {0.0f,0.0f,0.0f,1.0f},
    {0.02f,0.02f,0.02f,1.0f},
    {0.0f,0.05f,0.05f,1.0f},
    {0.0f,0.05f,0.0f,1.0f},
    {0.05f,0.0f,0.0f,1.0f},
    {0.05f,0.05f,0.05f,1.0f},
    {0.05f,0.05f,0.0f,1.0f}};

GLfloat material_diffuse_arr[][4]= {{0.07568f,0.61424f,0.07568f,1.0f},
    {0.54f,0.89f,0.63f,1.0f},
    {0.18275f,0.17f,0.22525f,1.0f},
    {1.0f,0.829f,0.829f,1.0f},
    {0.61424f,0.04136f,0.04136f,1.0f},
    {0.396f,0.74151f,0.69102f,1.0f},
    {0.780392f,0.568627f,0.113725f,1.0f},
    {0.714f,0.4284f,0.18144f,1.0f},
    {0.4f,0.4f,0.4f,1.0f},
    {0.7038f,0.27048f,0.0828f,1.0f},
    {0.75164f,0.60648f,0.22648f,1.0f},
    {0.50754f,0.50754f,0.50754f,1.0f},
    {0.01f,0.01f,0.01f,1.0f},
    {0.0f,0.50980392f,0.50980392f,1.0f},
    {0.0f,0.35f,0.1f,1.0f},
    {0.5f,0.0f,0.0f,1.0f},
    {0.55f,0.55f,0.55f,1.0f},
    {0.5f,0.5f,0.0f,1.0f},
    {0.01f,0.01f,0.01f,1.0f},
    {0.4f,0.5f,0.5f,1.0f},
    {0.4f,0.5f,0.4f,1.0f},
    {0.5f,0.4f,0.4f,1.0f},
    {0.5f,0.5f,0.5f,1.0f},
    {0.5f,0.5f,0.4f,1.0f}};

GLfloat material_specular_arr[][4] = {{0.633f,0.7278f,0.633f,1.0f},
    {0.316228f,0.316228f,0.316228f,1.0f},
    {0.332741f,0.328634f,0.346435f,1.0f},
    {0.296648f,0.296648f,0.296648f,1.0f},
    {0.727811f,0.626959f,0.626959f,1.0f},
    {0.297254f,0.30829f,0.306678f,1.0f},
    {0.992157f,0.941176f,0.807843f,1.0f},
    {0.393548f,0.271906f,0.166721f,1.0f},
    {0.774597f,0.774597f,0.774597f,1.0f},
    {0.256777f,0.137622f,0.086014f,1.0f},
    {0.628281f,0.555802f,0.366065f,1.0f},
    {0.508273f,0.508273f,0.508273f,1.0f},
    {0.50f,0.50f,0.50f,1.0f},
    {0.50196078f,0.50196078f,0.50196078f,1.0f},
    {0.45f,0.55f,0.45f,1.0f},
    {0.7f,0.6f,0.6f,1.0f},
    {0.70f,0.70f,0.70f,1.0f},
    {0.60f,0.60f,0.50f,1.0f},
    {0.4f,0.4f,0.4f,1.0f},
    {0.04f,0.7f,0.7f,1.0f},
    {0.04f,0.7f,0.04f,1.0f},
    {0.7f,0.04f,0.04f,1.0f},
    {0.7f,0.7f,0.7f,1.0f},
    {0.7f,0.7f,0.04f,1.0f}};

GLfloat material_shininess_arr[] = {0.6 * 128,
    0.1 * 128,
    0.3 * 128,
    0.088 * 128,
    0.6 * 128,
    0.1 * 128,
    0.21794872 * 128,
    0.2 * 128,
    0.6 * 128,
    0.1 * 128,
    0.4 * 128,
    0.4 * 128,
    0.25 * 128,
    0.25 * 128,
    0.25 * 128,
    0.25 * 128,
    0.25 * 128,
    0.25 * 128,
    0.078125 * 128,
    0.078125 * 128,
    0.078125 * 128,
    0.078125 * 128,
    0.078125 * 128,
    0.078125 * 128
};

////////////////////////////////

void update(void)
{
    // Rotate vice versa
    if (ascendingCube_scn)
    {
        angleCube_scn += 0.01f;
        if (angleCube_scn > 360.0f)
        {
            ascendingCube_scn = false;
        }
    }
    else
    {
        angleCube_scn -= 0.01f;
        if (angleCube_scn < 0.0f)
        {
            ascendingCube_scn = true;
        }
    }
}

void display(GLfloat width, GLfloat height)
{
    // NOTE: This makes existence functional and executes given order
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]
    
    struct ShaderUniforms *selectedUniforms;
    
    // Use Pass Through Shader Program
    if (gSelectedMode_scn == PER_FRAGMENT)
    {
        glUseProgram(gShaderProgramObject_pf_scn);
        selectedUniforms = &PerFragmentUniforms_scn;
        // glBindVertexArray(vao_sphere_pf);
    }
    else
    {
        glUseProgram(gShaderProgramObject_pv_scn); // Binding shader pgm to OpenGL pgm
        selectedUniforms = &PerVertexUniforms_scn;
        // glBindVertexArray(vao_sphere_pv);
    }
    
    // your code here
    // 9 steps
    // declaration of matrices
    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    mat4 rotationMatrix;
    mat4 translationMatrix;
    
    int ballNo = 0;
    for(int i = 0; i < 6; i++)
    {
        for(int j = 0; j < 4; j++, ballNo++)
        {
            
            glViewport((i * (int)width)/6, (j * (int)height)/4, (GLsizei) ((int)width/5), (GLsizei) ((int)height/5));
            // perspectiveProjectionMatrix_scn = vmath::perspective(45.0f, (width / height), 0.1f, 100.0f);
            
            // initialize above 2 matrices to identity
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();
    rotationMatrix = mat4::identity();
    translationMatrix = mat4::identity();
    
    // do necessary transformations like model scale, rotate, translate
    rotationMatrix = rotate(0.0f, angleCube_scn, 0.0f);
    translationMatrix = translate(0.0f, 0.0f, -2.0f);
            modelMatrix = translationMatrix; // * rotationMatrix;
    
    // do necessary matrix multiplication
    projectionMatrix = perspectiveProjectionMatrix_scn;// *modelViewMatrix;
    // In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum
    
    // send necessary matrices to shader in respective uniforms
    // display fn is dynamic, called in loop
    glUniformMatrix4fv(selectedUniforms->modelUniform, // kashat kombaychay. globally declared used in display
                       1, // how many matrices
                       GL_FALSE, // transpose?
                       modelMatrix); // kashala chiktavaychay // verify
    // OpenGL/GLSL is column major, DirectX is row major
    glUniformMatrix4fv(selectedUniforms->viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(selectedUniforms->projectionUniform, 1, GL_FALSE, projectionMatrix);
    
    if (gbShowLight_scn)
    {
        glUniform1i(selectedUniforms->isLKeyPressedUniform, 1);
        
        if(gbXKeyPressed)
        {
            light_position[0] = 0.0f;
            light_position[1] = 10.0f * cos(angleCube_scn);
            light_position[2] = 10.0f * sin(angleCube_scn);
        }
        else if(gbYKeyPressed)
        {
            light_position[0] = 10.0f * cos(angleCube_scn);
            light_position[1] = 0.0f;
            light_position[2] = 10.0f * sin(angleCube_scn);
        }
        else //if(gbZKeyPressed == YES)
        {
            light_position[0] = 10.0f * cos(angleCube_scn);
            light_position[1] = 10.0f * sin(angleCube_scn);
            light_position[2] = 0.0f;
        }
        glUniform4fv(selectedUniforms->lightPoistionUniform, 1, light_position);
        
        glUniform3fv(selectedUniforms->laUniform, 1, light_ambient);
        glUniform3fv(selectedUniforms->ldUniform, 1, light_diffuse);
        glUniform3fv(selectedUniforms->lsUniform, 1, light_specular);
        
        glUniform3fv(selectedUniforms->kaUniform, 1, material_ambient_arr[ballNo]);
        glUniform3fv(selectedUniforms->kdUniform, 1, material_diffuse_arr[ballNo]);
        glUniform3fv(selectedUniforms->ksUniform, 1, material_specular_arr[ballNo]);
        glUniform1f(selectedUniforms->shinynessUniform, material_shininess_arr[ballNo]);
    }
    else
    {
        glUniform1i(selectedUniforms->isLKeyPressedUniform, 0);
    }
    
    // bind with vao - this will avoid many vbo repetitive calls in display
    glBindVertexArray(vao_sphere_scn);
    // IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
    // if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data
    
    // similarly bin with textures, if any
    // glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);
    
    // draw the scene
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element_scn);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind vao
    glBindVertexArray(0);
    
        }
    }
    glUseProgram(0); // Unbinding
    
    // SwapBuffers(ghdc);
}
