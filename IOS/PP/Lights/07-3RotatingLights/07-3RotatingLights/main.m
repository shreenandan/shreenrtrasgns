//
//  main.m
//  07-3RotatingLights
//
//  Created by Nishchal Nandanwar on 16/01/20.
//  Copyright © 2020 Nishchal Nandanwar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
