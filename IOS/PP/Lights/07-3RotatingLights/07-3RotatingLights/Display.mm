#import "Logic.h"

#define DISTANCE_LIGHT 1000.0f

float light_ambient[9] =
{
    0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f
};
float light_diffuse[9] =
{
    1.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 1.0f
};
float light_specular[9] =
{
    1.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 1.0f
};
float light_position[12] =
{
    0.0f, DISTANCE_LIGHT, DISTANCE_LIGHT, 1.0f,
    DISTANCE_LIGHT, 0.0f, DISTANCE_LIGHT, 1.0f,
    DISTANCE_LIGHT, DISTANCE_LIGHT, -DISTANCE_LIGHT, 1.0f
};

float material_ambient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float material_diffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float material_specular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float material_shinyness = 150.0f; //128.0f; // 50

void update(void)
{
    // Rotate vice versa
    /*
     if (ascendingCube_scn)
    {
        angleCube_scn += 0.02f;
        if (angleCube_scn > 360.0f)
        {
            ascendingCube_scn = false;
        }
    }
    else
    {
        angleCube_scn -= 0.02f;
        if (angleCube_scn < 0.0f)
        {
            ascendingCube_scn = true;
        }
    }
     */
    angleCube_scn += 0.005f;
    if (angleCube_scn > 360.0f)
    {
        angleCube_scn = 0.0f;
    }
}

void display(void)
{
    // NOTE: This makes existence functional and executes given order
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]
    
    struct ShaderUniforms *selectedUniforms;
    
    // Use Pass Through Shader Program
    if (gSelectedMode_scn == PER_FRAGMENT)
    {
        glUseProgram(gShaderProgramObject_pf_scn);
        selectedUniforms = &PerFragmentUniforms_scn;
        // glBindVertexArray(vao_sphere_pf);
    }
    else
    {
        glUseProgram(gShaderProgramObject_pv_scn); // Binding shader pgm to OpenGL pgm
        selectedUniforms = &PerVertexUniforms_scn;
        // glBindVertexArray(vao_sphere_pv);
    }
    
    // your code here
    // 9 steps
    // declaration of matrices
    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    mat4 rotationMatrix;
    mat4 translationMatrix;
    
    // initialize above 2 matrices to identity
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();
    rotationMatrix = mat4::identity();
    translationMatrix = mat4::identity();
    
    // do necessary transformations like model scale, rotate, translate
    rotationMatrix = rotate(0.0f, angleCube_scn, 0.0f);
    translationMatrix = translate(0.0f, 0.0f, -3.0f);
    modelMatrix = translationMatrix * rotationMatrix;
    
    // do necessary matrix multiplication
    projectionMatrix = perspectiveProjectionMatrix_scn;// *modelViewMatrix;
    // In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum
    
    // send necessary matrices to shader in respective uniforms
    // display fn is dynamic, called in loop
    glUniformMatrix4fv(selectedUniforms->modelUniform, // kashat kombaychay. globally declared used in display
                       1, // how many matrices
                       GL_FALSE, // transpose?
                       modelMatrix); // kashala chiktavaychay // verify
    // OpenGL/GLSL is column major, DirectX is row major
    glUniformMatrix4fv(selectedUniforms->viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(selectedUniforms->projectionUniform, 1, GL_FALSE, projectionMatrix);
    
    if (gbShowLight_scn)
    {
        glUniform1i(selectedUniforms->isLKeyPressedUniform, 1);
        
        glUniform3fv(selectedUniforms->laUniform, 3, light_ambient);
        glUniform3fv(selectedUniforms->ldUniform, 3, light_diffuse);
        glUniform3fv(selectedUniforms->lsUniform, 3, light_specular);
        
        glUniform3fv(selectedUniforms->kaUniform, 1, material_ambient);
        glUniform3fv(selectedUniforms->kdUniform, 1, material_diffuse);
        glUniform3fv(selectedUniforms->ksUniform, 1, material_specular);
        glUniform1f(selectedUniforms->shinynessUniform, material_shinyness);
        
        light_position[1] = DISTANCE_LIGHT * cos(angleCube_scn);
        light_position[4] = DISTANCE_LIGHT * cos(angleCube_scn);
        light_position[8] = DISTANCE_LIGHT * cos(angleCube_scn);
        
        light_position[2] = DISTANCE_LIGHT * sin(angleCube_scn);
        light_position[6] = DISTANCE_LIGHT * sin(angleCube_scn);
        light_position[9] = DISTANCE_LIGHT * sin(angleCube_scn);
        glUniform4fv(selectedUniforms->lightPoistionUniform, 3, light_position);
    }
    else
    {
        glUniform1i(selectedUniforms->isLKeyPressedUniform, 0);
    }
    
    // bind with vao - this will avoid many vbo repetitive calls in display
    glBindVertexArray(vao_sphere_scn);
    // IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
    // if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data
    
    // similarly bin with textures, if any
    // glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);
    
    // draw the scene
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element_scn);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    // unbind vao
    glBindVertexArray(0);
    
    glUseProgram(0); // Unbinding
    
    // SwapBuffers(ghdc);
}
