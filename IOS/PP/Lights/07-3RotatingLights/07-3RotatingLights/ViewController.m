//
//  ViewController.m
//  BlueWnd
//
//  Created by Nishchal Nandanwar on 11/01/20.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// my addition
- (void) dealloc
{
    // code
    [super dealloc];
}

@end
