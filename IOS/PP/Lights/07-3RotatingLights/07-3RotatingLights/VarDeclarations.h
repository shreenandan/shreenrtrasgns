#pragma once
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import <UIKit/UIKit.h>

#import "vmath.h"

#import "mysphere.h"

extern int MyCount;

extern GLuint vao_sphere_scn;
extern GLuint vbo_position_sphere_scn;
extern GLuint vbo_light_sphere_scn;
extern GLuint gVbo_sphere_element_scn;
extern GLuint gShaderProgramObject_pf_scn;
extern GLuint gShaderProgramObject_pv_scn;

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD
};

using namespace vmath;
extern mat4 perspectiveProjectionMatrix_scn;

extern GLfloat angleCube_scn;
extern GLfloat ascendingCube_scn;

extern bool gbShowLight_scn;
extern bool gbAnimate_scn;

extern float sphere_vertices[1146];
extern float sphere_normals[1146];
extern float sphere_textures[764];
extern unsigned short sphere_elements[2280];

extern int gNumVertices;
extern int gNumElements;

enum ShaderModes { PER_VERTEX, PER_FRAGMENT };
extern enum ShaderModes gSelectedMode_scn;

struct ShaderUniforms
{
    GLuint modelUniform = -1;
    GLuint viewUniform = -1;
    GLuint projectionUniform = -1;
    
    GLuint laUniform = -1;
    GLuint ldUniform = -1;
    GLuint lsUniform = -1;
    
    GLuint kaUniform = -1;
    GLuint kdUniform = -1;
    GLuint ksUniform = -1;
    GLuint shinynessUniform = -1;
    
    GLuint lightPoistionUniform = -1;
    GLuint isLKeyPressedUniform = -1;
};

extern struct ShaderUniforms PerVertexUniforms_scn, PerFragmentUniforms_scn;
