#pragma once
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import <UIKit/UIKit.h>

#import "vmath.h"

#import "mysphere.h"

extern int MyCount;

extern GLuint vao_pyramid_scn;
extern GLuint vbo_position_pyramid_scn;
extern GLuint vbo_lights_scn;

extern GLuint vao_sphere_scn;
extern GLuint vbo_position_sphere_scn;
extern GLuint vbo_light_sphere_scn;
extern GLuint gVbo_sphere_element_scn;
extern GLuint gShaderProgramObject_scn;

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD
};

extern GLuint modelUniform_scn;
extern GLuint viewUniform_scn;
extern GLuint projectionUniform_scn;
using namespace vmath;
extern mat4 perspectiveProjectionMatrix_scn;

extern GLfloat angleCube_scn;
extern GLfloat ascendingCube_scn;

extern bool gbShowLight_scn;
extern bool gbAnimate_scn;

extern GLuint laUniform_scn;
extern GLuint ldUniform_scn;
extern GLuint lsUniform_scn;

extern GLuint kaUniform_scn;
extern GLuint kdUniform_scn;
extern GLuint ksUniform_scn;
extern GLuint shinynessUniform_scn;

extern GLuint lightPoistionUniform_scn;
extern GLuint isLKeyPressedUniform_scn;

extern GLuint la2Uniform_scn;
extern GLuint ld2Uniform_scn;
extern GLuint ls2Uniform_scn;
extern GLuint lightPoistion2Uniform_scn;

extern float sphere_vertices[1146];
extern float sphere_normals[1146];
extern float sphere_textures[764];
extern unsigned short sphere_elements[2280];

extern int gNumVertices;
extern int gNumElements;
