#pragma once
#import "VarDeclarations.h"

int MyCount = 10;

GLuint vao_pyramid_scn = -1;
GLuint vbo_position_pyramid_scn = -1;
GLuint vbo_lights_scn = -1;

GLuint vao_sphere_scn = -1;
GLuint vbo_position_sphere_scn = -1;
GLuint vbo_light_sphere_scn = -1;
GLuint gShaderProgramObject_scn = 0;

GLuint modelUniform_scn = -1;
GLuint viewUniform_scn = -1;
GLuint projectionUniform_scn = -1;
mat4 perspectiveProjectionMatrix_scn;

GLfloat angleCube_scn = 0.0f;
GLfloat ascendingCube_scn = true;

bool gbShowLight_scn = false;
bool gbAnimate_scn = false;

GLuint laUniform_scn = -1;
GLuint ldUniform_scn = -1;
GLuint lsUniform_scn = -1;

GLuint kaUniform_scn = -1;
GLuint kdUniform_scn = -1;
GLuint ksUniform_scn = -1;
GLuint shinynessUniform_scn = -1;

GLuint lightPoistionUniform_scn = -1;
GLuint isLKeyPressedUniform_scn = -1;

GLuint la2Uniform_scn = -1;
GLuint ld2Uniform_scn = -1;
GLuint ls2Uniform_scn = -1;
GLuint lightPoistion2Uniform_scn = -1;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gVbo_sphere_element_scn = -1;

int gNumVertices = -1;
int gNumElements = -1;
