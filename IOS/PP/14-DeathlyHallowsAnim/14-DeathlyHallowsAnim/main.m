//
//  main.m
//  14-DeathlyHallowsAnim
//
//  Created by Nishchal Nandanwar on 12/01/20.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
