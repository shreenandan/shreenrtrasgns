//
//  main.m
//  04-2DShapes
//
//  Created by Nishchal Nandanwar on 12/01/20.
//  Copyright © 2020 Nishchal Nandanwar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
