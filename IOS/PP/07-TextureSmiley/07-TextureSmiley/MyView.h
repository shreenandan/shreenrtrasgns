//
//  MyView.h
//  BlueWnd
//
//  Created by Nishchal Nandanwar on 11/01/20.
//
//

#import <UIKit/UIKit.h>

@interface MyView : UIView <UIGestureRecognizerDelegate>

@end
