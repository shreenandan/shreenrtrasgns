#pragma once
#import "VarDeclarations.h"

int MyCount = 10;

mat4 perspectiveProjectionMatrix;

struct ShaderUniforms GenericUniforms; // Must declare vars else unresolved symbols linking error

GLuint genericShaderProgramObject = 0;

Geometry GeometryList[NO_OF_GEOMETRIES];
GLuint vao_list[NO_OF_GEOMETRIES];
GLuint vbo_position_list[NO_OF_GEOMETRIES];
GLuint vbo_color_list[NO_OF_GEOMETRIES];
GLuint vbo_element_list[NO_OF_GEOMETRIES];

const float gStepRow = 2.0f / RATIO_ROWS;
const float gStepCol = 2.0f / RATIO_COLS;
