#pragma once
#import "VarDeclarations.h"

void incrCount(void);
int getCount(void);

void display(void);
void update(void);

void uninitializePP(void);
bool createFragmentShader(GLuint *fragmentShaderObject, enum ShaderModes shaderMode);
bool createVertexShader(GLuint *vertexShaderObject, enum ShaderModes shaderMode);
bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);
bool createShaders(enum ShaderModes shaderMode, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms);
bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms);
void initVAOs(void);
