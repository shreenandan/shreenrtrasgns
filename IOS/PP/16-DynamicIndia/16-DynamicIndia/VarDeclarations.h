#pragma once
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import <UIKit/UIKit.h>

#import "vmath.h"

extern int MyCount;

extern bool bShouldPlay;
extern bool isLoaded;
extern bool gbShowRatio;

enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD
};

using namespace vmath;
extern mat4 perspectiveProjectionMatrix;

enum ShaderModes { BASIC };

struct ShaderUniforms
{
    GLuint mvpUniform = -1;
};

extern struct ShaderUniforms GenericUniforms;

#define RATIO_COLS 16 //.0f
#define RATIO_ROWS 9 //.0f
#define NO_OF_COORDS_IN_3D_LINE 6
#define NO_OF_GEOMETRIES 10
#define NO_OF_LINES_IN_LETTER_N 3
#define NO_OF_LINES_IN_LETTER_A 2
#define NO_OF_LINES_IN_LETTER_D 4
#define NO_OF_RGBA_IN_3D_LINE 8
#define NO_OF_COORDS_IN_3D_VERTEX 3
#define NO_OF_LINES_IN_PLANE 63
#define NO_OF_VERTICES_IN_LETTER_A_TRICOLOR 12

extern GLuint genericShaderProgramObject;
extern GLuint vao_ratioline;
extern GLuint vbo_ratioline_position;
extern GLuint vbo_ratioline_color; // TODO Remove later

extern GLuint vao_staticindia;
extern GLuint vbo_staticindia_position;
extern GLuint vbo_staticindia_color;

typedef struct Coordinate
{
    float x;
    float y;
    float z;
}Coord;

typedef struct Triplet
{
    GLfloat red;
    GLfloat green;
    GLfloat blue;
}Triplet;

extern const Triplet gDeepSaffron;
extern const Triplet gIndiaGreen;

typedef struct GeometryInfo
{
    // StartX, StartY, StartZ, EndX, EndY, EndZ
    float Vertices[NO_OF_COORDS_IN_3D_LINE] =
    {
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f
    };
    float Colors[NO_OF_COORDS_IN_3D_LINE] =
    {
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f
    };
    
    float *VerticesDynamic = NULL;
    float *ColorsDynamic = NULL;
    int *ElementsDynamic = NULL;
    
    bool IsComplete = false;
    float InitialX = 0.0f;
    float StopMarker = 0.0f;
    float TranslationStep = 0.0f;
    float MovingX = 0.0f;
    
    float InitialY = 0.0f;
    float MovingY = 0.0f;
    
    float Alpha = 0.0f;
    
    int NumVertices = 0;
    int NumElements = 0;
    
}Geometry;

enum LineVerticesIndex
{
    START_X = 0,
    START_Y,
    START_Z,
    END_X,
    END_Y,
    END_Z
};

enum LineColorsIndex
{
    START_RED = 0,
    START_GREEN,
    START_BLUE,
    END_RED,
    END_GREEN,
    END_BLUE
};

extern Geometry GeometryList[NO_OF_GEOMETRIES];
extern GLuint vao_list[NO_OF_GEOMETRIES];
extern GLuint vbo_position_list[NO_OF_GEOMETRIES];
extern GLuint vbo_color_list[NO_OF_GEOMETRIES];
extern GLuint vbo_element_list[NO_OF_GEOMETRIES];

enum Geometries
{
    LETTER_I1 = 0,
    LETTER_N,
    LETTER_D,
    LETTER_I2,
    LETTER_A,
    LETTER_A_TRICOLOR,
    PLANE_A,
    PLANE_A_IAF,
    PLANE_B,
    PLANE_C
};

extern const float gStepRow;
extern const float gStepCol;
extern const float gLetterHeight;
extern const float gBigLetterWidth;
extern const float gUnitMagnitude;
