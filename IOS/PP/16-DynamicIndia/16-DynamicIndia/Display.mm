#import "Logic.h"

void display(void)
{
    // function declarations
    void drawRatioLine(void);
    void drawStaticIndia(void);
    bool drawDynamicIndiaLetters(void);
    void drawPlane(int geometryListIdx);
    void drawTricolor(int geometryListIdx);
    
    // if (!isLoaded)
    // {
    //     printf("\nwithin display.");
    //     isLoaded = true;
    // }
    // if (!bShouldPlay)
    // {
    // 	if (!isLoaded)
    // 		glClear(GL_COLOR_BUFFER_BIT);
    
    // 	// SwapBuffers(ghdc);
    // 	isLoaded = true;
    // 	return;
    // }
    
    static GLfloat angleA = M_PI - (M_PI_2 / 4.0f);
    static GLfloat angleA2 = M_PI_2;
    static GLfloat angleC = M_PI + M_PI_2;
    static GLfloat angleC2 = M_PI + (M_PI_2 / 4.0f);
    static GLfloat theX;
    static GLfloat theY;
    static float radius = 1.24f;
    static float tempRotF = 0.0f;
    
    // Code
    // NOTE: This makes existence functional and executes given order
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]
    
    struct ShaderUniforms *selectedUniforms;
    
    glUseProgram(genericShaderProgramObject);
    selectedUniforms = &GenericUniforms;
    
    // your code here
    // 9 steps
    // declaration of matrices
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
    
    // initialize above 2 matrices to identity
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    
    // do necessary transformations like model scale, rotate, translate
    // here in this pgm no transformation, but in later pgms
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    
    // do necessary matrix multiplication
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(selectedUniforms->mvpUniform, // kashat kombaychay. globally declared used in display
                       1, // how many matrices
                       GL_FALSE, // transpose?
                       modelViewProjectionMatrix); // kashala chiktavaychay  // OpenGL/GLSL is column major, DirectX is row major
    
    if (gbShowRatio)
        drawRatioLine();
    //drawStaticIndia();
    
    if (drawDynamicIndiaLetters())
    {
        modelViewMatrix = translate(GeometryList[PLANE_B].MovingX, 0.0f, -3.0f);
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(selectedUniforms->mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        
        drawPlane(PLANE_B);
        
        if (GeometryList[PLANE_A].MovingX <= (1.24f + 0.8f))
        {
            theX = cos(angleA) * radius;
            theY = sin(angleA) * radius;
            tempRotF = (angleA + M_PI_2) * (180.0f / M_PI);
            
            modelViewMatrix = translate(theX - 2.2f + 1.24f + GeometryList[PLANE_A].MovingX, theY + 1.24f, -3.0f);
            modelViewMatrix = modelViewMatrix * rotate(tempRotF, 0.0f, 0.0f, 1.0f);
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(selectedUniforms->mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            
            drawPlane(PLANE_A);
            //drawPlane(PLANE_C);
            
            if (angleA < M_PI + M_PI_2)
            {
                angleA += 0.001f; //0001f
            }
            else
            {
                GeometryList[PLANE_A].MovingX += 0.0098f; //000098f
            }
        }
        else if (GeometryList[PLANE_A].MovingX > (1.24f + 0.8f))
        {
            theX = cos(angleA2) * radius;
            theY = sin(angleA2) * radius;
            tempRotF = (angleA2 - M_PI_2) * (180.0f / M_PI);
            
            modelViewMatrix = translate(theX -2.2f + 1.24f + GeometryList[PLANE_A].MovingX, theY - 1.24f, -3.0f);
            modelViewMatrix = modelViewMatrix * rotate(tempRotF, 0.0f, 0.0f, 1.0f);
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(selectedUniforms->mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            
            drawPlane(PLANE_A);
            //drawPlane(PLANE_C);
            
            if (angleA2 > 0.0f - M_PI_4)
            {
                angleA2 -= 0.001f; //0001f
            }
        }
        
        if (GeometryList[PLANE_C].MovingX <= (1.24f + 0.8f))
        {
            theX = cos(angleC2) * radius;
            theY = sin(angleC2) * radius;
            tempRotF = (angleC2 - M_PI_2) * (180.0f / M_PI);
            
            modelViewMatrix = translate(theX - 2.2f + 1.24f + GeometryList[PLANE_C].MovingX, theY - 1.24f, -3.0f);
            modelViewMatrix = modelViewMatrix * rotate(tempRotF, 0.0f, 0.0f, 1.0f);
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(selectedUniforms->mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            
            drawPlane(PLANE_C);
            
            if (angleC2 > M_PI_2)
            {
                angleC2 -= 0.001f; //0001f
            }
            else
            {
                GeometryList[PLANE_C].MovingX += 0.0098f; // 000098f
            }
        }
        else if (GeometryList[PLANE_C].MovingX > (1.24f + 0.8f))
        {
            theX = cos(angleC) * radius;
            theY = sin(angleC) * radius;
            tempRotF = (angleC + M_PI_2) * (180.0f / M_PI);
            
            modelViewMatrix = translate(theX - 2.2f + 1.24f + GeometryList[PLANE_C].MovingX, theY + 1.24f, -3.0f);
            modelViewMatrix = modelViewMatrix * rotate(tempRotF, 0.0f, 0.0f, 1.0f);
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(selectedUniforms->mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            
            drawPlane(PLANE_C);
            
            if (angleC < (M_PI * 2.0f) + M_PI_4)
            {
                angleC += 0.001f; //0001f
            }
        }
        
        if (GeometryList[PLANE_B].MovingX > 0.9f)
        {
            modelViewMatrix = translate(gStepCol * 7.750f, 0.0f, -3.0f);
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(selectedUniforms->mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            
            drawTricolor(LETTER_A_TRICOLOR);
        }
        
        // update
        if (GeometryList[PLANE_B].MovingX < -2.2f + 1.24f)
        {
            GeometryList[PLANE_B].MovingX += 0.00098f; //000098f
        }
        else if (GeometryList[PLANE_B].MovingX < 1.24f + 0.8f)
        {
            GeometryList[PLANE_B].MovingX += 0.0098f; // 000098f
        }
        else if (GeometryList[PLANE_B].MovingX < 2.56f)
        {
            GeometryList[PLANE_B].MovingX += 0.0005f; //0001f
        }
    }
    
    glUseProgram(0); // Unbinding
    
    // SwapBuffers(ghdc);
}

void drawRatioLine(void)
{
    glLineWidth(2.0f);
    
    // bind with vao - this will avoid many vbo repetitive calls in display
    glBindVertexArray(vao_ratioline);
    // IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
    // if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data
    
    // similarly bin with textures, if any
    // glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);
    
    // draw the scene
    glDrawArrays(GL_LINES, 0, (RATIO_COLS + RATIO_ROWS + 2) * 2);
    glBindVertexArray(0);
}

void drawStaticIndia(void)
{
    glLineWidth(5.0f);
    
    glBindVertexArray(vao_staticindia);
    glDrawArrays(GL_LINES, 0, 14 * 2);
    
    glBindVertexArray(0);
}

bool drawDynamicIndiaLetters(void)
{
    // function declarations
    bool drawLetterI1(void);
    bool drawLetterN(void);
    bool drawLetterA(void);
    bool drawLetterI2(void);
    bool drawLetterD(void);
    
    static bool isPlacedI1 = false;
    static bool isPlacedN = false;
    static bool isPlacedD = false;
    static bool isPlacedI2 = false;
    static bool isPlacedA = false;
    
    glLineWidth(5.0f);
    
    isPlacedI1 = drawLetterI1();
    if (isPlacedI1)
    {
        isPlacedA = drawLetterA();
    }
    if (isPlacedA)
    {
        isPlacedN = drawLetterN();
    }
    if (isPlacedN)
    {
        isPlacedI2 = drawLetterI2();
    }
    if (isPlacedI2)
    {
        isPlacedD = drawLetterD();
    }
    
    glLineWidth(1.0f);
    
    return isPlacedD;
}

bool drawLetterI1(void)
{
    //glLineWidth(5.0f);
    
    glBindVertexArray(vao_list[LETTER_I1]);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_list[LETTER_I1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GeometryList[LETTER_I1].Vertices), GeometryList[LETTER_I1].Vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_LINES, 0, 1 * 2);
    
    glBindVertexArray(0);
    
    if (GeometryList[LETTER_I1].MovingX < GeometryList[LETTER_I1].StopMarker)
    {
        GeometryList[LETTER_I1].MovingX += GeometryList[LETTER_I1].TranslationStep;
        
        GeometryList[LETTER_I1].Vertices[START_X] = GeometryList[LETTER_I1].MovingX;
        GeometryList[LETTER_I1].Vertices[END_X] = GeometryList[LETTER_I1].MovingX;
        
        return false;
    }
    return true;
}

bool drawLetterN(void)
{
    static float tempY = 0.0f;
    
    //glLineWidth(5.0f);
    
    glBindVertexArray(vao_list[LETTER_N]);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_list[LETTER_N]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_N, GeometryList[LETTER_N].VerticesDynamic, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_LINES, 0, NO_OF_LINES_IN_LETTER_N * 2);
    
    glBindVertexArray(0);
    
    if (GeometryList[LETTER_N].MovingY > GeometryList[LETTER_N].StopMarker)
    {
        GeometryList[LETTER_N].MovingY -= GeometryList[LETTER_N].TranslationStep;
        tempY = GeometryList[LETTER_N].MovingY + (gLetterHeight * 2.0f);
        
        GeometryList[LETTER_N].VerticesDynamic[START_Y] = tempY;
        GeometryList[LETTER_N].VerticesDynamic[END_Y] = GeometryList[LETTER_N].MovingY;
        
        GeometryList[LETTER_N].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_Y] = tempY;
        GeometryList[LETTER_N].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_Y] = GeometryList[LETTER_N].MovingY;
        
        GeometryList[LETTER_N].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + START_Y] = tempY;
        GeometryList[LETTER_N].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + END_Y] = GeometryList[LETTER_N].MovingY;
        
        return false;
    }
    return true;
}

bool drawLetterA(void)
{
    glBindVertexArray(vao_list[LETTER_A]);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_list[LETTER_A]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_A, GeometryList[LETTER_A].VerticesDynamic, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_LINES, 0, NO_OF_LINES_IN_LETTER_A * 2);
    
    glBindVertexArray(0);
    
    if (GeometryList[LETTER_A].MovingX > GeometryList[LETTER_A].StopMarker)
    {
        GeometryList[LETTER_A].MovingX -= GeometryList[LETTER_A].TranslationStep;
        
        GeometryList[LETTER_A].VerticesDynamic[START_X] = GeometryList[LETTER_A].MovingX + (gBigLetterWidth / 2.0f);
        GeometryList[LETTER_A].VerticesDynamic[END_X] = GeometryList[LETTER_A].MovingX;
        
        GeometryList[LETTER_A].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_X] = GeometryList[LETTER_A].MovingX + (gBigLetterWidth / 2.0f);
        GeometryList[LETTER_A].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_X] = GeometryList[LETTER_A].MovingX + gBigLetterWidth;
        
        return false;
    }
    return true;
}

bool drawLetterI2(void)
{
    glBindVertexArray(vao_list[LETTER_I2]);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_list[LETTER_I2]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GeometryList[LETTER_I2].Vertices), GeometryList[LETTER_I2].Vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_LINES, 0, 1 * 2);
    
    glBindVertexArray(0);
    
    if (GeometryList[LETTER_I2].MovingY < GeometryList[LETTER_I2].StopMarker)
    {
        GeometryList[LETTER_I2].MovingY += GeometryList[LETTER_I2].TranslationStep;
        
        GeometryList[LETTER_I2].Vertices[START_Y] = GeometryList[LETTER_I2].MovingY;
        GeometryList[LETTER_I2].Vertices[END_Y] = GeometryList[LETTER_I2].MovingY - (gLetterHeight * 2.0f);
        
        return false;
    }
    return true;
}

bool drawLetterD(void)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glBindVertexArray(vao_list[LETTER_D]);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_list[LETTER_D]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * NO_OF_RGBA_IN_3D_LINE * NO_OF_LINES_IN_LETTER_D, GeometryList[LETTER_D].ColorsDynamic, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_LINES, 0, NO_OF_LINES_IN_LETTER_D * 2);
    
    glBindVertexArray(0);
    
    glDisable(GL_BLEND);
    
    if (GeometryList[LETTER_D].Alpha < 1.0f)
    {
        GeometryList[LETTER_D].Alpha += GeometryList[LETTER_D].TranslationStep;
        
        GeometryList[LETTER_D].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 3] = GeometryList[LETTER_D].Alpha;
        GeometryList[LETTER_D].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 7] = GeometryList[LETTER_D].Alpha;
        
        GeometryList[LETTER_D].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 3] = GeometryList[LETTER_D].Alpha;
        GeometryList[LETTER_D].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 7] = GeometryList[LETTER_D].Alpha;
        
        GeometryList[LETTER_D].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 3] = GeometryList[LETTER_D].Alpha;
        GeometryList[LETTER_D].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 7] = GeometryList[LETTER_D].Alpha;
        
        GeometryList[LETTER_D].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 3] = GeometryList[LETTER_D].Alpha;
        GeometryList[LETTER_D].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 7] = GeometryList[LETTER_D].Alpha;
        
        return false;
    }
    return true;
}

void drawPlane(int geometryListIdx)
{
    glBindVertexArray(vao_list[geometryListIdx]);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_list[geometryListIdx]);
    glDrawElements(GL_TRIANGLES, GeometryList[geometryListIdx].NumElements, GL_UNSIGNED_INT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
}

void drawTricolor(int geometryListIdx)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glBindVertexArray(vao_list[geometryListIdx]);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_list[geometryListIdx]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4 * NO_OF_VERTICES_IN_LETTER_A_TRICOLOR, GeometryList[geometryListIdx].ColorsDynamic, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_list[geometryListIdx]);
    glDrawElements(GL_TRIANGLES, GeometryList[geometryListIdx].NumElements, GL_UNSIGNED_INT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    glDisable(GL_BLEND);
    
    if (GeometryList[geometryListIdx].Alpha < 1.0f)
    {
        GeometryList[geometryListIdx].Alpha += GeometryList[geometryListIdx].TranslationStep;
        
        for (int tempdx = 0; tempdx < NO_OF_VERTICES_IN_LETTER_A_TRICOLOR; tempdx++)
        {
            GeometryList[geometryListIdx].ColorsDynamic[tempdx*4+3] = GeometryList[geometryListIdx].Alpha;
        }
    }
}

// void drawPlaneTry(void)
// {
// 	glBindVertexArray(vao_list[PLANE_A]);

// 	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()

// 	// draw the scene
// 	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_list[PLANE_A]);
// 	//glDrawElements(GL_TRIANGLES, GeometryList[PLANE_A].NumElements, GL_UNSIGNED_SHORT, 0);
// 	//glDrawElements(GL_TRIANGLES, sizeof(int) * NO_OF_COORDS_IN_3D_LINE * GeometryList[PLANE_A].NumElements, GL_UNSIGNED_SHORT, 0);

// 	//glDrawElements(GL_LINES, sizeof(int) * NO_OF_COORDS_IN_3D_LINE * GeometryList[PLANE_A].NumElements, GL_UNSIGNED_SHORT, 0);
// 	//glDrawElements(GL_TRIANGLES, GeometryList[PLANE_A].NumElements * NO_OF_COORDS_IN_3D_LINE, GL_UNSIGNED_SHORT, 0);

// 	//glDrawElements(GL_TRIANGLES, GeometryList[PLANE_A].NumElements, GL_UNSIGNED_INT, 0);
// 	//glDrawElements(GL_LINES, GeometryList[PLANE_A].NumElements, GL_UNSIGNED_INT, 0);
// 	//glDrawElements(GL_POINTS, GeometryList[PLANE_A].NumElements, GL_UNSIGNED_INT, 0);
// 	// GL_LINES

// 	//glDrawArrays(GL_TRIANGLES, 0, 15);
// 	// http://www.songho.ca/opengl/gl_vertexarray.html
// 	// http://www.songho.ca/opengl/gl_vertexarray_quad.html
// 	glDrawRangeElements(GL_TRIANGLES, 0, NO_OF_LINES_IN_PLANE, 3, GL_UNSIGNED_INT, 0);
// 	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
// 	glDrawRangeElements(GL_TRIANGLES, 12, NO_OF_LINES_IN_PLANE, 12, GL_UNSIGNED_INT, GeometryList[PLANE_A].ElementsDynamic);

// 	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_list[PLANE_A]);
// 	//glDrawRangeElements(GL_TRIANGLES, 0, 6, 6, GL_UNSIGNED_INT, 0);
// 	//glDrawRangeElements(GL_POINTS, 0, 6, 6 * 3, GL_UNSIGNED_INT, GeometryList[PLANE_A].ElementsDynamic + (3 * 6));
// 	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

// 	glBindVertexArray(0);
// }
