#pragma once
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

extern int MyCount;


enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

using namespace vmath;
extern mat4 perspectiveProjectionMatrix;
extern bool gbShowGraph;

enum ShaderModes { BASIC };

struct ShaderUniforms
{
    GLuint mvpUniform = -1;
};

extern struct ShaderUniforms GenericUniforms;

#define LINES_TO_DRAW 20
#define RATIO_COLS 16 //.0f
#define RATIO_ROWS 9 //.0f
#define NO_OF_COORDS_IN_3D_LINE 6
#define NO_OF_GEOMETRIES 5
#define NO_OF_COORDS_IN_3D_VERTEX 3
#define NO_OF_VERTEX_TRIANGLE 3
#define NO_OF_VERTEX_RECTANGLE 4

extern GLuint genericShaderProgramObject;

typedef struct Coordinate
{
    float x;
    float y;
    float z;
}Coord;

typedef struct Triplet
{
    GLfloat red;
    GLfloat green;
    GLfloat blue;
}Triplet;

typedef struct GeometryInfo
{
    // StartX, StartY, StartZ, EndX, EndY, EndZ
    float Vertices[NO_OF_COORDS_IN_3D_LINE] =
    {
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f
    };
    float Colors[NO_OF_COORDS_IN_3D_LINE] =
    {
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f
    };
    
    float *VerticesDynamic = 0; // VERIFY nil
    float *ColorsDynamic = 0;
    int *ElementsDynamic = 0;
    
    bool IsComplete = false;
    float InitialX = 0.0f;
    float StopMarker = 0.0f;
    float TranslationStep = 0.0f;
    float MovingX = 0.0f;
    
    float InitialY = 0.0f;
    float MovingY = 0.0f;
    
    float Alpha = 0.0f;
    
    int NumVertices = 0;
    int NumElements = 0;
    
}Geometry;

enum LineVerticesIndex
{
    START_X = 0,
    START_Y,
    START_Z,
    END_X,
    END_Y,
    END_Z
};

enum LineColorsIndex
{
    START_RED = 0,
    START_GREEN,
    START_BLUE,
    END_RED,
    END_GREEN,
    END_BLUE
};

extern Geometry GeometryList[NO_OF_GEOMETRIES];
extern GLuint vao_list[NO_OF_GEOMETRIES];
extern GLuint vbo_position_list[NO_OF_GEOMETRIES];
extern GLuint vbo_color_list[NO_OF_GEOMETRIES];
extern GLuint vbo_element_list[NO_OF_GEOMETRIES];

enum Geometries
{
    GRAPH = 0,
    TRIANGLE,
    RECTANGLE,
    INCIRCLE,
    OUTCIRCLE
};

extern const float gfMagnitude;
