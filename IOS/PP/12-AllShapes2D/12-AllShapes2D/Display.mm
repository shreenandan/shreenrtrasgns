#import "Logic.h"

void update(void)
{
}

void display(void)
{
    // function declarations
    void drawGraph(int geometryListIdx);
    void drawGeometryWithMode(int geometryListIdx, GLenum mode, GLint first, GLsizei count);
    
    // Code
    // NOTE: This makes existence functional and executes given order
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]
    
    struct ShaderUniforms *selectedUniforms;
    
    glUseProgram(genericShaderProgramObject);
    selectedUniforms = &GenericUniforms;
    
    // your code here
    // 9 steps
    // declaration of matrices
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
    
    // initialize above 2 matrices to identity
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    
    // do necessary transformations like model scale, rotate, translate
    // here in this pgm no transformation, but in later pgms
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    
    // do necessary matrix multiplication
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(selectedUniforms->mvpUniform, // kashat kombaychay. globally declared used in display
                       1, // how many matrices
                       GL_FALSE, // transpose?
                       modelViewProjectionMatrix); // kashala chiktavaychay  // OpenGL/GLSL is column major, DirectX is row major
    
    if (gbShowGraph)
    {
        drawGraph(GRAPH);
    }
    drawGeometryWithMode(TRIANGLE, GL_LINE_LOOP, 0, NO_OF_VERTEX_TRIANGLE);
    drawGeometryWithMode(RECTANGLE, GL_LINE_LOOP, 0, NO_OF_VERTEX_RECTANGLE);
    
    // INCIRCLE
    modelViewMatrix = translate(GeometryList[INCIRCLE].MovingX, GeometryList[INCIRCLE].MovingY, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(selectedUniforms->mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    drawGeometryWithMode(INCIRCLE, GL_LINE_LOOP, 0, GeometryList[INCIRCLE].NumVertices);
    
    // OUTCIRCLE
    modelViewMatrix = translate(GeometryList[OUTCIRCLE].MovingX, GeometryList[OUTCIRCLE].MovingY, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(selectedUniforms->mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    drawGeometryWithMode(OUTCIRCLE, GL_LINE_LOOP, 0, GeometryList[OUTCIRCLE].NumVertices);
    
    glUseProgram(0); // Unbinding
    
    // SwapBuffers(ghdc);
}

void drawGraph(int geometryListIdx)
{
    glLineWidth(1.0f);
    
    // bind with vao - this will avoid many vbo repetitive calls in display
    glBindVertexArray(vao_list[geometryListIdx]);
    // IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
    // if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data
    
    // similarly bin with textures, if any
    // glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);
    
    // draw the scene
    glDrawArrays(GL_LINES, 0, ((LINES_TO_DRAW + 2) * 4) * 2);
    glBindVertexArray(0);
}

void drawGeometryWithMode(int geometryListIdx, GLenum mode, GLint first, GLsizei count)
{
    glBindVertexArray(vao_list[geometryListIdx]);
    glDrawArrays(mode, first, count);
    glBindVertexArray(0);
}
