#include <stdio.h>

int main(int argc, char *argv[], char* envp[])
{
	// function declaration/prototype/signature
	void MyAddition(void);
	
	// function call
	MyAddition();
	return (0);
}

// method definition way 1 : No return value, no parameters
void MyAddition(void)
{
	// variable declarations
	int num1, num2, sum;
	
	// code
	printf("\n Enter num1 : ");
	scanf("%d", &num1);
	printf("\n Enter num2 : ");
	scanf("%d", &num2);
	
	sum = num1 + num2;
	printf("\n Sum of %d and %d is : %d\n", num1, num2, sum);
}
