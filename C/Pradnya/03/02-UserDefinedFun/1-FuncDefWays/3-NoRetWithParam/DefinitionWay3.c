#include <stdio.h>

int main(int argc, char *argv[], char* envp[])
{
	// function declaration/prototype/signature
	void MyAddition(int num1, int num2);
	
	// variable declarations: local to main
	int num1, num2;
	
	// code
	printf("\n Enter num1 : ");
	scanf("%d", &num1);
	printf("\n Enter num2 : ");
	scanf("%d", &num2);
	
	// function call
	MyAddition(num1, num2);
	return (0);
}

// method definition way 3 : No return value, valid parameters
void MyAddition(int num1, int num2)
{
	// variable declarations
	int sum;
	
	// code
	sum = num1 + num2;
	printf("\n Sum of %d and %d is : %d\n", num1, num2, sum);
}
