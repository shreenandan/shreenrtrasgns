#include <stdio.h>

int main(int argc, char *argv[], char* envp[])
{
	// function declaration/prototype/signature
	int MyAddition(void);
	
	// variable declarations: local to main
	int result;
	
	// function call
	result = MyAddition();
	printf("\n Sum is : %d\n", result);
	return (0);
}

// method definition way 2 : Valid return value, no parameters
int MyAddition(void)
{
	// variable declarations
	int num1, num2, sum;
	
	// code
	printf("\n Enter num1 : ");
	scanf("%d", &num1);
	printf("\n Enter num2 : ");
	scanf("%d", &num2);
	
	sum = num1 + num2;
	
	return sum;
}
