#include <stdio.h>

int main(int argc, char *argv[], char* envp[])
{
	// function declarations/prototypes/signatures
	void display_info(void);
	void Function_Country(void);
	
	display_info();
	Function_Country();
	
	return (0);
}

void display_info(void)
{
	void Function_My(void);
	void Function_Name(void);
	void Function_Is(void);
	void Function_FirstName(void);
	void Function_Surname(void);
	
	Function_My();
	Function_Name();
	Function_Is();
	Function_FirstName();
	Function_Surname();
}

void Function_My(void)
{
	printf("\n\nMy");
}
void Function_Name(void)
{
	printf("\nName");
}
void Function_Is(void)
{
	printf("\nIs");
}
void Function_FirstName(void)
{
	printf("\nShreerang");
}
void Function_Surname(void)
{
	printf("\nNandanwar");
}

void Function_Country(void)
{
	printf("\nI live in India.\n");
}
