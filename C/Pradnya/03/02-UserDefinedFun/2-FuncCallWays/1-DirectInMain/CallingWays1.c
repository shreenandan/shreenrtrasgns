#include <stdio.h>

int main(int argc, char *argv[], char* envp[])
{
	// function declarations/prototypes/signatures
	void MyAddition(void);
	int MySubtraction(void);
	void MyMultiplication(int num1, int num2);
	int MyDivision(int num1, int num2);
	
	// variable declarations: local to main
	int result_subtraction;
	int num1_mul, num2_mul;
	int num1_div, num2_div, result_div;	
	
	// code
	printf("\n\t*** ADDITION ***");
	MyAddition();

	printf("\n\t*** SUBTRACTION ***");
	result_subtraction = MySubtraction();
	printf("\n Subtraction is : %d\n", result_subtraction);
	
	printf("\n\t*** MULTIPLICATION ***");
	printf("\n Enter num1 : ");
	scanf("%d", &num1_mul);
	printf("\n Enter num2 : ");
	scanf("%d", &num2_mul);
	MyMultiplication(num1_mul, num2_mul);
	
	printf("\n\t*** DIVISION ***");
	printf("\n Enter num1 : ");
	scanf("%d", &num1_div);
	printf("\n Enter num2 : ");
	scanf("%d", &num2_div);
	result_div = MyDivision(num1_div, num2_div);
	printf("\n Division of %d and %d is : %d\n\n", num1_div, num2_div, result_div);
	
	return (0);
}

// method definition way 1 : No return value, no parameters
void MyAddition(void)
{
	// variable declarations
	int num1, num2, sum;
	
	// code
	printf("\n Enter num1 : ");
	scanf("%d", &num1);
	printf("\n Enter num2 : ");
	scanf("%d", &num2);
	
	sum = num1 + num2;
	printf("\n Sum of %d and %d is : %d\n", num1, num2, sum);
}

// method definition way 2 : Valid return value, no parameters
int MySubtraction(void)
{
	// variable declarations
	int num1, num2, subtraction;
	
	// code
	printf("\n Enter num1 : ");
	scanf("%d", &num1);
	printf("\n Enter num2 : ");
	scanf("%d", &num2);
	
	subtraction = num1 - num2;
	
	return(subtraction);
}

// method definition way 3 : No return value, valid parameters
void MyMultiplication(int num1, int num2)
{
	// variable declarations
	int multiplication;
	
	// code
	multiplication = num1 * num2;
	printf("\n Multiplication of %d and %d is : %d\n", num1, num2, multiplication);
}

// method definition way 4 : Valid return value, valid parameters
int MyDivision(int num1, int num2)
{
	// variable declarations
	int quotient;
	
	// code
	if(num1 > num2)
		quotient = num1 / num2;
	else
		quotient = num2 / num1;
	
	return(quotient);
}
