#include <stdio.h>

// 2 command line parameters
int main(int argc, char *argv[])
{
	// variable declarations
	int i;
	
	// code
	printf("Hello World!!!\n");
	printf("No of cmd line args: %d\n", argc);
	
	for(i = 0; i < argc; i++)
	{
		printf("Argument %d is : %s\n", (i+1), argv[i]);
	}
	
	return (0);
}
