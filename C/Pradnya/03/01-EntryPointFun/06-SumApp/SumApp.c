#include <stdio.h>
#include <ctype.h>

// 2 command line parameters
int main(int argc, char *argv[], char* envp[])
{
	// variable declarations
	int i;
	int num;
	int sum = 0;
	
	// code
	printf("Sum of all cmd line args is : ");
	
	for(i = 1; i < argc; i++)
	{
		num = atoi(argv[i]);
		sum += num;
	}
	
	printf("%d \n\n", sum);
	
	return (0);
}
