#include <stdio.h>

// 2 command line parameters
int main(int argc, char *argv[], char* envp[])
{
	// variable declarations
	int i;
	
	// code
	printf("Hello World!!!\n\n");
	printf("No of cmd line args: %d\n", argc);
	
	for(i = 0; i < argc; i++)
	{
		printf("Argument %d is : %s\n", (i+1), argv[i]);
	}

	printf("\nFirst 5 environmental args passed to the program are : \n");
	for(i = 0; i < 5; i++)
	{
		printf("%d : %s\n", (i + 1), envp[i]);
	}
	
	return (0);
}
