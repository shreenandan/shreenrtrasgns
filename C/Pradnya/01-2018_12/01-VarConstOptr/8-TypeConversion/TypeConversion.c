// TypeConversion
#include <stdio.h>

int main(void)
{
	// variable declarations
	int i_a, i_b, i_res, i_explicit;
	float f_a, f_res, f_explicit;
	char ch1, ch2;
	
	// code
	// int and char
	i_a = 70;
	ch1 = i_a;
	printf("i_a : %d\n", i_a);
	printf("Character after ch1 = i_a : %c\n\n", ch1);

	ch2 = 'Q';
	i_b = ch2;
	printf("ch2 : %c\n", ch2);
	printf("Integer after i_b = ch2 : %d\n\n", i_b);
	
	// implicit conversion of int to float
	i_a = 5;
	f_a = 7.8f;
	f_res = f_a + i_a;
	printf("Integer i_a = %d and floating point no f_a = %f gives floating point sum = %f\n\n", i_a, f_a, f_res);
	
	i_res = f_a + i_a;
	printf("Integer i_a = %d and floating point no f_a = %f gives integer sum = %d\n\n", i_a, f_a, i_res);
	
	// Explicit type casting using cast operator
	f_explicit = 12.345678f;
	i_explicit = (int)f_explicit;
	printf("Floating point no type casted explicitly = %f\n", f_explicit);
	printf("Resultant integer after explicit type casting of %f = %d\n", f_explicit, i_explicit);
	
	return (0);
}