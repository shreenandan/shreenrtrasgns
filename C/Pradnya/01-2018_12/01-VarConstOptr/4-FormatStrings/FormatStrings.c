// FormatStrings
#include <stdio.h>

int main(void)
{
	// code
	int a = 13;
	char ch = 'A';
	char str[] = "Shree Nandan";
	long num = 12345678L;
	unsigned int b = 7;
	float f_num = 1234.5678f;
	double d_pi = 3.14159265358979323846;

	printf("\n");
	printf("Hello World!!!\n\n");

	printf("int decimal a %%d = %d\n", a);
	printf("int octal a %%o = %o\n", a);
	printf("int hexadecimal lower a %%x = %x\n", a);
	printf("int hexadecimal upper a %%X = %X\n\n", a);
	
	printf("char ch %%c = %c\n", ch);
	printf("string str %%s = %s\n\n", str);
	
	printf("long int num %%ld = %ld\n\n", num);
	
	printf("unsigned int b %%u = %u\n\n", b);
	
	printf("float f_num %%f = %f\n", f_num);
	printf("float f_num %%4.2f = %4.2f\n", f_num);
	printf("float f_num %%2.5f = %2.5f\n\n", f_num);
	
	printf("Double float without exponential %%g = %g\n", d_pi);
	printf("Double float with exponential lower %%e = %e\n", d_pi);
	printf("Double float with exponential upper %%E = %E\n", d_pi);
	printf("Double hexadecimal lower %%a = %a\n", d_pi);
	printf("Double hexadecimal upper %%A = %A\n\n", d_pi);
	
	return (0);
}