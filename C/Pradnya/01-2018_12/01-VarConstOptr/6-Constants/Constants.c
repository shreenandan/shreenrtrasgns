// Constants
#include <stdio.h>

// NOTE: Macro
#define MY_PI 3.1415926535897932

// NOTE: Unnamed enums
enum
{
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY
};

enum
{
	JANUARY = 1,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER
};

// Named
enum Numbers
{
	ONE,
	TWO,
	THREE,
	FOUR = 4,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN
};

enum boolean
{
	TRUE = 1,
	FALSE = 0 
};

int main(void)
{
	// NOTE: local constant declaration
	const double epsilon = 0.000001;
	
	printf("Constants\n");
	printf("local constant epsilon = %lf\n", epsilon);
	
	printf("SUNDAY = %d\n",SUNDAY);
	printf("MONDAY = %d\n",MONDAY);
	printf("TUESDAY = %d\n",TUESDAY);
	printf("WEDNESDAY = %d\n",WEDNESDAY);
	printf("THURSDAY = %d\n",THURSDAY);
	printf("FRIDAY = %d\n",FRIDAY);
	printf("SATURDAY = %d\n\n",SATURDAY);
	
	printf("JANUARY  = %d\n", JANUARY );
	printf("FEBRUARY = %d\n", FEBRUARY);
	printf("MARCH = %d\n", MARCH);
	printf("APRIL = %d\n", APRIL);
	printf("MAY = %d\n", MAY);
	printf("JUNE = %d\n", JUNE);
	printf("JULY = %d\n", JULY);
	printf("AUGUST = %d\n", AUGUST);
	printf("SEPTEMBE = %d\n", SEPTEMBER);
	printf("OCTOBER = %d\n", OCTOBER);
	printf("NOVEMBER = %d\n", NOVEMBER);
	printf("DECEMBER = %d\n\n", DECEMBER);
	
	printf("ONE = %d\n", ONE);
	printf("TWO = %d\n", TWO);
	printf("THREE = %d\n", THREE);
	printf("FOUR = %d\n", FOUR );
	printf("FIVE = %d\n", FIVE);
	printf("SIX = %d\n", SIX);
	printf("SEVEN = %d\n", SEVEN);
	printf("EIGHT = %d\n", EIGHT);
	printf("NINE = %d\n", NINE);
	printf("TEN = %d\n\n", TEN);
	
	printf("TRUE = %d\n", TRUE);
	printf("FALSE = %d\n\n", FALSE);
	
	printf("MY_PI Macro value = %.10lf\n", MY_PI);
	printf("Area of circle of radius 2 = %f\n", (MY_PI * 2.0f * 2.0f));
	
	return (0);
}