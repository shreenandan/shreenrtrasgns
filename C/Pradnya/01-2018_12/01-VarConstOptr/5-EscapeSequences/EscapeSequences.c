// EscapeSequences
#include <stdio.h>

int main(void)
{
	printf("\nEscapeSequences\n");
	printf("\\n : Going to next line \n\n");
	printf("\\t : Showing \t Horizontal \t Tab\n");
	printf("\\\" : \"Double Quoted String\"\n");
	printf("\\\' : \'Single Quoted String\'\n");
	printf("\\b : Space    \b\b\b\b turned to backspace   \b\b\b  .\n");
	printf("\\r : \r Carriage return at start \\r\n");
	printf("\\r : Carriage \r return in between \\r\n");
	printf("\\r : Carriage return in \r between 2 \\r\n");
	printf("\\xhh : Hexadecimal sequence formatter x41 = \x41\n");
	printf("\\ooo : Octal sequence formatter 102 = \102\n");
	
	return (0);
}