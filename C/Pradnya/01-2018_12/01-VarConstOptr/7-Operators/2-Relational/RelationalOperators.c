// RelationalOperators
#include <stdio.h>

int main(void)
{
	// variable declaration
	int a;
	int b;
	int answer;

	// code
	printf("Enter integer one : ");
	scanf("%d",&a);
	printf("Enter integer two : ");
	scanf("%d",&b);
	
	printf("\nIf answer = 0, it is FALSE\n");
	printf("If answer = 1, it is TRUE\n\n");

	answer = a > b;
	printf("is %d > %d = %d\n", a, b, answer);
	answer = a < b;
	printf("is %d < %d = %d\n", a, b, answer);
	answer = a >= b;
	printf("is %d >= %d = %d\n", a, b, answer);
	answer = a <= b;
	printf("is %d <= %d = %d\n", a, b, answer);
	answer = a == b;
	printf("is %d == %d = %d\n", a, b, answer);
	answer = a != b;
	printf("is %d != %d = %d\n\n", a, b, answer);
	
	return (0);
}