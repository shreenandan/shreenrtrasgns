// TernaryOperator
#include <stdio.h>

int main(void)
{
	// variable declarations
	int a, b, p, q;
	char ch_result_1, ch_result_2;
	int i_result_1, i_result_2;

	// code
	printf("Ternary operator\n\n");
	
	a = 7;
	b = 5;
	ch_result_1 = (a > b) ? 'A' : 'B';
	i_result_1 = (a > b) ? a : b;
	printf("a = %d, b = %d Answer 1\n", a, b); 
	printf("(a > b) ? 'A' : 'B' = %c\n", ch_result_1);
	printf("(a > b) ? a : b = %d\n\n", i_result_1);
	
	p = 20;
	q = 20;
	ch_result_2 = (p != q) ? 'P' : 'Q';
	i_result_2 = (p != q) ? p : q;
	printf("p = %d, q = %d Answer 2\n", p, q);
	printf("(p != q) ? 'P' : 'Q' = %c\n", ch_result_2);
	printf("(p != q) ? p : q = %d\n\n", i_result_2);
	
	return (0);
}