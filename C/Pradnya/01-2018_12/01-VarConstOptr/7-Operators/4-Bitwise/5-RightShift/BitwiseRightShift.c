// BitwiseRightShift
#include <stdio.h>

int main(void)
{
	// function declarations
	void PrintBinaryFormOfNumber(unsigned int);
	
	// variable declarations
	unsigned int a, b, result;
	unsigned int right_shift_A, right_shift_B;
	int i;
	
	// code
	printf("\n\t\t  BITWISE RIGHT SHIFT\n\n");
	printf("Enter number A : ");
	scanf("%u", &a);
	printf("Enter number B : ");
	scanf("%u", &b);
	printf("By how many bits shift A to the right? : ", a);
	scanf("%u", &right_shift_A);
	printf("By how many bits shift B to the right? : ", b);
	scanf("%u", &right_shift_B);
	
	printf("\n\t\t\tRESULT TABLE\n");
	for(i = 0; i < ((12*5)+3); i++)
		printf("_");
	printf("\n%-15s%-12s%-12s%-12s%-12s|", "Operation", "Decimal", "Octal", "Hexadecimal", "Binary");
	printf("\n");
	for(i = 0; i < ((12*5)+3); i++)
		printf("-");
	printf("\n%-15s%-12u%-12o%-12X", "num A", a, a, a); PrintBinaryFormOfNumber(a);
	result = a >> right_shift_A;
	printf("\n%-15s%-12u%-12o%-12X", ">> RT Shft A", result, result, result); PrintBinaryFormOfNumber(result);
	printf("\n%-15s%-12u%-12o%-12X", "num B", b, b, b); PrintBinaryFormOfNumber(b);
	result = b >> right_shift_B;
	printf("\n%-15s%-12u%-12o%-12X", ">> RT Shft B", result, result, result); PrintBinaryFormOfNumber(result);
	
	printf("\n");
	for(i = 0; i < ((12*5)+3); i++)
		printf("-");
	
	printf("\n");
	
	return (0);
}

void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	unsigned int num, i;
	unsigned int quotient, remainder;
	unsigned int binary_array[8];
	
	for(i = 0; i < 8; i++)
		binary_array[i] = 0;
	
	num = decimal_number;
	i = 7; // Start with LSB
	
	while(num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}
	
	// printf output starting from MSB
	for(i = 0; i < 8; i++)
		printf("%u", binary_array[i]);

	printf("    |");
}
