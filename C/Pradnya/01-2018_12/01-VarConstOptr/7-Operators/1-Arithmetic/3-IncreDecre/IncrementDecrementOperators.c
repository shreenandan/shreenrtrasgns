// IncrementDecrementOperators
#include <stdio.h>

int main(void)
{
	// variable declarations
	int a = 5;
	int b = 10;
	
	// code
	printf("Increment operators\n");
	printf("a = %d\n", a);
	printf("Post incr a++ = %d\n", a++);
	printf("a = %d\n", a);
	printf("Pre incr ++a = %d\n\n", ++a);
	
	printf("Decrement operators\n");
	printf("b = %d\n", b);
	printf("Post decr b-- = %d\n", b--);
	printf("b = %d\n", b);
	printf("Pre decr --b = %d\n\n", --b);
	
	return (0);
}