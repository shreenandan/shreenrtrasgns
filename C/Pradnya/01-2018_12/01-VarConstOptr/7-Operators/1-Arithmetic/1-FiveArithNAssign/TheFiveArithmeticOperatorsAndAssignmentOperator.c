// TheFiveArithmeticOperatorsAndAssignmentOperator
#include <stdio.h>

int main(void)
{
	// variable declarations
	int a;
	int b;
	int result;
	
	// code
	printf("\nEnter no 1 : ");
	scanf("%d", &a);
	printf("\nEnter no 2 : ");
	scanf("%d", &b);
	
	result = a + b;
	printf("\nAddition %d + %d = %d\n", a, b, result);
	result = a - b;
	printf("Subtraction %d - %d = %d\n", a, b, result);
	result = a * b;
	printf("Multiplication %d * %d = %d\n", a, b, result);
	result = a / b;
	printf("Division Quotient %d / %d = %d\n", a, b, result);
	result = a % b;
	printf("Division Remainder %d %% %d = %d\n", a, b, result);
	
	return 0;
}
