// CompoundAssignmentOperators
#include <stdio.h>

int main(void)
{
	// variable declarations
	int a;
	int b;
	int temp; // used to maintain original value
	
	// code
	printf("\nEnter no 1 : ");
	scanf("%d", &a);
	printf("\nEnter no 2 : ");
	scanf("%d", &b);
	
	temp = a;
	a += b;
	printf("\nAddition %d + %d = %d\n", temp, b, a);
	
	a = temp;
	a -= b;
	printf("Subtraction %d - %d = %d\n", temp, b, a);

	a = temp;
	a *= b;
	printf("Multiplication %d * %d = %d\n", temp, b, a);

	a = temp;
	a /= b;
	printf("Division Quotient %d / %d = %d\n", temp, b, a);
	
	a = temp;
	a %= b;
	printf("Division Remainder %d %% %d = %d\n", temp, b, a);
	
	return 0;
}
