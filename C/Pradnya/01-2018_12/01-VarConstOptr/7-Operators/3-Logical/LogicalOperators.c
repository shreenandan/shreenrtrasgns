// LogicalOperators
#include <stdio.h>

int main(void)
{
	// variable declaration
	int a;
	int b;
	int c;
	int answer;

	// code
	printf("Enter integer one : ");
	scanf("%d",&a);
	printf("Enter integer two : ");
	scanf("%d",&b);
	printf("Enter integer three : ");
	scanf("%d",&c);
	
	printf("\nIf answer = 0, it is FALSE\n");
	printf("If answer = 1, it is TRUE\n\n");

	answer = (a <= b) && (b != c);
	printf("Logical AND && : ALL conditions must be TRUE\n");
	printf("is (a <= b) && (b != c) = %d\n\n", answer);
	
	printf("Logical OR || : ANYONE conditions should be TRUE\n");
	answer = (b >= a) || (a == c);
	printf("is (b >= a) || (a == c) = %d\n\n", answer);
	
	printf("Logical NOT !\n");
	answer = !a;
	printf("!%d = %d\n", a, answer);
	answer = !b;
	printf("!%d = %d\n", b, answer);
	answer = !c;
	printf("!%d = %d\n\n", c, answer);
	
	answer = (!(a <= b) && !(b != c));
	printf("Logical NOT on each and then ANDing afterwards: (!(a <= b) && !(b != c)) = %d\n", answer);
	answer = !((b >= a) || (a == c));
	printf("Logical NOT on entire expression: !((b >= a) || (a == c)) = %d\n\n", answer);
	
	return (0);
}
