// OrdinaryLocalVariables
#include <stdio.h>

int main(void)
{
	// NOTE: local var for main
	int a = 5;
	
	// function declarations
	void change_count(void);
	
	// code
	printf("\na = %d", a);
	
	change_count();
	change_count();
	change_count();
	
	return (0);
}

void change_count(void)
{
	int local_count = 0;
	local_count = local_count + 1;
	printf("\nLocal count = %d", local_count);
}
