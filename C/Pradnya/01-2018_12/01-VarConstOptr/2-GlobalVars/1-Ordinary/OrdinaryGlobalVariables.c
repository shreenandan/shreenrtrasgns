// OrdinaryGlobalVariables
#include <stdio.h>

// *** GLOBAL SCOPE ***
// NOTE: By Default global vars are initialized to default data type value.
// Discipline assign default values explicitly.

int global_count = 0;

int main(void)
{
	// function declarations
	void change_count_one(void);
	void change_count_two(void);	
	
	// code
	printf("\nGlobal count = %d", global_count);
	change_count_one();
	change_count_two();
	
	return (0);
}

void change_count_one(void)
{
	global_count = global_count + 1;
	printf("\nGlobal count = %d", global_count);
}

void change_count_two(void)
{
	global_count = global_count + 1;
	printf("\nGlobal count = %d", global_count);
}
