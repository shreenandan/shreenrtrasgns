// ExternalGlobalVariablesInMultipleFiles
#include <stdio.h>

// *** GLOBAL SCOPE ***
int global_count = 0;

int main(void)
{
	// function declarations
	void change_count(void);
	void change_count_one(void);
	void change_count_two(void);	
	
	// code
	printf("\nGlobal count = %d", global_count);
	change_count();
	change_count_one(); // in File1
	change_count_two(); // in File2
	
	return (0);
}

void change_count(void)
{
	global_count = global_count + 1;
	printf("\nGlobal count = %d", global_count);
}
