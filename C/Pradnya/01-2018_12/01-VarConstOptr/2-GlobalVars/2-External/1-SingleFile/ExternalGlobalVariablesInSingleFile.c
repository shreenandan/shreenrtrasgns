// ExternalGlobalVariablesInSingleFile
#include <stdio.h>

int main(void)
{
	// function declarations
	void change_count(void);
	
	// NOTE: variable declarations
	extern int global_count; // = 2; // NOTE: assignment not allowed, only declaration
	// ExternalGlobalVariablesInSingleFile.c(10) : error C2205: 'global_count' : cannot initialize extern variables with block scope
	
	// code
	printf("\nGlobal count = %d", global_count);
	change_count();
	
	return (0);
}

// *** GLOBAL SCOPE ***
// global_count is a global variable. 
// Since, it is declared before change_count(), it can be accssed and used as any ordinary global variable in change_count()
// Since, it is declared after main(), it must be firest re-declared in main() as an external global variable by means of the 'extern' keyword and the type of the variable.
// Once this is done, it can be used as an ordinary global variable in main as well.
int global_count = 5;

void change_count(void)
{
	global_count = global_count + 1;
	printf("\nGlobal count = %d", global_count);
}
