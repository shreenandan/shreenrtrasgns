#include <stdio.h>

int main(void)
{
	// variable declarations
	int a[5][3] = {{4,9,1},{2,3,5},{7,8,6},{13,14,11},{12,15,10}};
	int i, j;
	
	// code
	printf("\n (*(a+i)+j) \n");
	for(i = 0; i < 5; i++)
	{
		for(j = 0; j < 3; j++)
		{
			printf("%p ", (*(a+i)+j));
		}
		printf("\n");
	}
	
	printf("\n *(*(a+i)+j) \n");
	for(i = 0; i < 5; i++)
	{
		for(j = 0; j < 3; j++)
		{
			printf("%d ", *(*(a+i)+j));
		}
		printf("\n");
	}

	return 0;
}
