#include <stdio.h>

int main(void)
{
	// variable declarations
	int *a[5];
	int i, j;
	
	// code
	for(i = 0; i < 5; i++)
	{
		a[i] = (int *) malloc(3 * sizeof(int)); // variable column
		if(a[i] == NULL)
		{
			printf("\nmalloc failed for a[%d]! Exiting the program.", i);
			exit(0);
		}
	}
	
	printf("\n (*(a+i)+j) \n");
	for(i = 0; i < 5; i++)
	{
		for(j = 0; j < 3; j++)
		{
			printf("Enter element [%d][%d] : ", i,j);
			scanf("%d", (*(a+i)+j));
		}
		printf("\n");
	}
	
	printf("\n *(*(a+i)+j) \n");
	for(i = 0; i < 5; i++)
	{
		for(j = 0; j < 3; j++)
		{
			printf("%d ", *(*(a+i)+j));
		}
		printf("\n");
	}
	
	for(i = 4; i >= 0; i--)
	{
		free(a[i]);
		a[i] = NULL;
	}

	return 0;
}
