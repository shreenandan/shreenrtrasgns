#include <stdio.h>

int main(void)
{
	// variable declarations
	int num = 10;
	int *ptr = NULL;
	int** pptr = NULL;
	
	// code
	ptr = &num;
	pptr = &ptr;
	
	printf("\nnum : %d", num);
	printf("\n&num : %p", &num);
	printf("\n*(&num) : %d", *(&num));
	printf("\nptr : %p", ptr);
	printf("\n*ptr : %d\n", *ptr);
	
	printf("\n&ptr : %p", &ptr);
	printf("\npptr : %p", pptr);
	printf("\n*pptr : %p", *pptr);
	printf("\n*(&pptr) : %p", *(&pptr));
	printf("\n*(*(&ptr)) : %d", *(*(&ptr)));
	printf("\n**pptr : %d\n", **pptr);
	
	return 0;
}
