#include <stdio.h>

int main(void)
{
	// variable declarations
	int a[5] = {10, 20, 30, 40, 50};
	int *ptr = NULL;
	int *ptr2 = NULL;
	int i;
	
	// code
	ptr = a; // OR 
	ptr2 = &a[0];
	
	printf("\n ptr + i \n");
	for(i = 0; i < 5; i++)
	{
		printf("%p ", ptr + i);
	}
	
	printf("\n *(ptr + i) \n");
	for(i = 0; i < 5; i++)
	{
		printf("%d ", *(ptr + i));
	}
	
	
	printf("\n\n &ptr2[i] \n");
	for(i = 0; i < 5; i++)
	{
		printf("%p ", &ptr2[i]);
	}
	
	printf("\n ptr2[i] \n");
	for(i = 0; i < 5; i++)
	{
		printf("%d ", ptr2[i]);
	}
	
	printf("\n");
	printf("\n%d\n", a[5]); // Ideally should give error
	
	return 0;
}
