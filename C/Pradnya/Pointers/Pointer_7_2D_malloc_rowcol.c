#include <stdio.h>

int main(void)
{
	// variable declarations
	int **ptr;
	int i, j;
	
	// code
	ptr = (int **) malloc(5 * sizeof(int*)); // variable rows
	if(ptr == NULL)
	{
		printf("\nmalloc failed! Exiting the program.");
		exit(0);
	}
	
	for(i = 0; i < 5; i++)
	{
		ptr[i] = (int *) malloc(3 * sizeof(int)); // variable cols
		if(ptr[i] == NULL)
		{
			printf("\nmalloc failed for ptr[%d]! Exiting the program.", i);
			exit(0);
		}
	}
	
	printf("\n (*(ptr+i)+j) \n");
	for(i = 0; i < 5; i++)
	{
		for(j = 0; j < 3; j++)
		{
			printf("Enter element [%d][%d] : ", i,j);
			scanf("%d", (*(ptr+i)+j));
		}
		printf("\n");
	}
	
	printf("\n *(*(ptr+i)+j) \n");
	for(i = 0; i < 5; i++)
	{
		for(j = 0; j < 3; j++)
		{
			printf("%d ", *(*(ptr+i)+j));
		}
		printf("\n");
	}
	
	// release section in reverse order
	for(i = 4; i >= 0; i--)
	{
		free(ptr[i]);
		ptr[i] = NULL;
	}
	
	if(ptr)
	{
		free(ptr);
		ptr = NULL;
	}

	return 0;
}
