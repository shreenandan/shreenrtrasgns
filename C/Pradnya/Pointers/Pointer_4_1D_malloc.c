#include <stdio.h>

int main(void)
{
	// variable declarations
	int *ptr = NULL;
	int i;
	
	// code
	ptr = (int *) malloc(5 * sizeof(int));
	if(ptr == NULL)
	{
		printf("\nmalloc failed! Exiting the program.");
		exit(0);
	}
	
	printf("\n ptr + i \n");
	for(i = 0; i < 5; i++)
	{
		printf("Enter element %d : ", i);
		scanf("%d", ptr + i);
	}
	
	printf("\n *(ptr + i) \n");
	for(i = 0; i < 5; i++)
	{
		printf("%d ", *(ptr + i));
	}
	
	if(ptr)
	{
		free(ptr);
		// free(ptr); // error if trying to free already freed memory
		ptr = NULL;
	}
	
	printf("\n");
	free(ptr); // No error if ptr is NULL
	
	return 0;
}
