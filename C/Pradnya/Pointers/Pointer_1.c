#include <stdio.h>

int main(void)
{
	// variable declarations
	int num = 10;
	int *ptr = NULL;
	
	// code
	ptr = &num;
	
	printf("\nnum : %d", num);
	printf("\n&num : %p", &num);
	printf("\n*(&num) : %d", *(&num));
	printf("\nptr : %p", ptr);
	printf("\n*ptr : %d\n", *ptr);
	
	return 0;
}
