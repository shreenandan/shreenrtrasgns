/*
Program to implement
1] FileIO
2] Struct
3] LinkedList

NOTES:
1] Primary focus was on understanding concepts, code simplicity and not optimization, security.
2] Singly linked list with AddLast is used.
3] Tail is used to make insertions faster without need to traverse.
4] Performance was kept on priority over memory.
5] This is initial draft version with single file(without seperate header, source files).
6] Compiler's default struct multiplier used.

USAGE:
	ThisPgmExeName.exe [InputFilename] [OutputFilename]
	[InputFilename] - Optional cmd line arg. If not provided then asked at run time.
	[OutputFilename] - Optional cmd line arg. Default is InputFilename.out
EX:
	FileStructLinkedList.exe MonkeyHead.enc ParsedOutput.txt

SAMPLE INPUT

# Blender3D v249 OBJ File: 
# www.blender3d.org
v 0.437500 0.164062 0.765625
f 47/471/941 3/511/941 45/487/941
v -0.437500 0.164062 0.765625
vt 0.379420 0.521840
vt 0.620580 0.521840
vn 0.400039 -0.062344 0.914375
vn -0.400039 -0.062344 0.914375
v 0.500000 0.093750 0.687500

SAMPLE OUTPUT

v 0.437500 0.164062 0.765625
f 47 471 941 3 511 941 45 487 941
v -0.437500 0.164062 0.765625
vt 0.379420 0.521840
vt 0.620580 0.521840
vn 0.400039 -0.062344 0.914375
vn -0.400039 -0.062344 0.914375
v 0.500000 0.093750 0.687500
v -0.500000 0.093750 0.687500
*/

// FileStructLinkedList
#include <stdio.h>
#include <stdlib.h>

#define MAXPATH 256
#define MAXLINE 50
#define CNT_INSTRUCTIONS_L1 3
#define CNT_INSTRUCTIONS_L2 1
#define MAXCORDS 3
#define MAXSUBCORDS 3

// global variables
char fileToRead[MAXPATH + 1] = {0};
const char seperatorLevel1[] = " ";
const char seperatorLevel2[] = "/";
const char* instructionsToHandleLevel1[CNT_INSTRUCTIONS_L1] = { "v", "vn", "vt" };
const char* instructionsToHandleLevel2[] = { "f" };
char data[][15] = {"", "", "", "", "", "", "", "", ""};
char coordinates[MAXCORDS][15] = {"", "", ""};
char inst[5] = {""};

typedef struct Node
{
	char* instruction;
	float coordinates[MAXCORDS];
	int data[MAXCORDS][MAXSUBCORDS];
	struct Node* next;
}ParsedNode;

ParsedNode* sllHead = NULL;
ParsedNode* sllTail = NULL;
unsigned int cntNodes = 0;

// function declarations
unsigned int ReadFile(const char *inputFile);
void TokenizeLine(const char *rawLine);
unsigned int CreateAndAddNode(const char* inst, int foundCoords);
unsigned int AddLast(ParsedNode* theNode);
void DisplaySLL(void);
void WriteSLLToFile(char* outFile);

int main(int argc, char* argv[], char* envp[])
{
	printf("\n*** FileIO Struct LinkedList ***\n");
	
	if(argc < 2)
	{
		printf("\nPlease provide input file to read from: ");
		scanf("%s", fileToRead);
	}
	else
		sprintf(fileToRead, argv[1]);
	
	if(ReadFile(fileToRead) == 1)
	{
		printf("\nCompleted [%s] file reading.", fileToRead);
		WriteSLLToFile(argc >= 3 ? argv[2] : strcat(fileToRead, ".out"));
		printf("\nOutput saved into file [%s].\n", argc >= 3 ? argv[2] : fileToRead);
	}
	
	return (0);
}

unsigned int ReadFile(const char* inputFile)
{
	FILE *fileStream;
	char rawLine[MAXLINE];
	int index = 0;
	
	if( (fileStream = fopen(inputFile, "r" )) != NULL )
	{
		while( fgets(rawLine, MAXLINE, fileStream) != NULL)
		{
			if (rawLine[strlen(rawLine) - 1] == '\n')
				rawLine[strlen(rawLine) - 1] = '\0';

			TokenizeLine(rawLine);
		}
		fclose(fileStream);
		return 1;
	}
	else
		printf("\nOOPS!! File could not be opened.\n");
  
	return 0;
}

void TokenizeLine(const char *rawLine)
{
	char *token, *token2;
	token = strtok(rawLine, seperatorLevel1);
	int index = 0, j = 0, k = 0;
	int foundCoords = 0;
	
	strcpy(inst, token);
	
	for(; index < CNT_INSTRUCTIONS_L1; index++)
	{
		if( _strnicmp(token, instructionsToHandleLevel1[index], strlen(instructionsToHandleLevel1[index])) == 0)
		{
			j = 0;
			while(token != NULL)
			{
				token = strtok(NULL, seperatorLevel1);
				if(token != NULL)
				{
					strcpy(coordinates[j], token);
					j++;
				}
			}
			foundCoords = j;
			goto lblCreateNode;
		}
	}
	
	for(index = 0; index < CNT_INSTRUCTIONS_L2; index++)
	{
		if( _strnicmp(token, instructionsToHandleLevel2[index], strlen(instructionsToHandleLevel2[index])) == 0)
		{
			j = 0;
			while(token != NULL)
			{
				token = strtok(NULL, seperatorLevel1);
				if(token != NULL)
				{
					strcpy(data[j * MAXCORDS], token);
					j++;
				}
			}
			foundCoords = j;
			for(--j; j >= 0; j--)
			{
				token = strtok(data[j * MAXCORDS], seperatorLevel2);
				k = 0;
				while(token != NULL)
				{
					if(token != NULL)
					{
						strcpy(data[(j * MAXCORDS) + k], token);
						k++;
					}
					token = strtok(NULL, seperatorLevel2);
				}
			}
			goto lblCreateNode;
		}
	}
	return;
	
lblCreateNode:
	CreateAndAddNode(inst, foundCoords);
}

unsigned int CreateAndAddNode(const char* inst, int foundCoords)
{
	ParsedNode* newNode = (ParsedNode*)malloc(sizeof(ParsedNode));
	newNode->instruction = (char*)malloc(sizeof(char) * (strlen(inst)+1));
	newNode->next = NULL;
	memset(newNode->coordinates, NULL, sizeof(float) * MAXCORDS);
	memset(newNode->data, NULL, sizeof(int) * MAXCORDS * MAXSUBCORDS);
	int i = 0, j = 0;
	
	strcpy(newNode->instruction, inst);

	if( _strnicmp(inst, instructionsToHandleLevel2[0], strlen(instructionsToHandleLevel2[0])) == 0)
	{
		for(; i < MAXCORDS; i++)
		{		
			for(j = 0; j < MAXSUBCORDS; j++)
				newNode->data[i][j] = atoi(data[(i * MAXCORDS) + j]);
		}
	}
	else
	{
		for(; i < foundCoords; i++)
			newNode->coordinates[i] = atof(coordinates[i]);
	}

	AddLast(newNode);

	return 1;
}

unsigned int AddLast(ParsedNode* theNode)
{
	if(sllHead == NULL)
	{
		sllHead = sllTail = theNode;
	}
	else
	{
		sllTail->next = theNode;
		sllTail = theNode;
	}
	
	return 1;
}

void DisplaySLL(void)
{
	ParsedNode* current = sllHead;
	int i = 0, j = 0;
	
	for(; current != NULL; current = current->next)
	{
		printf("\n%s", current->instruction);
		
		if( _strnicmp(current->instruction, instructionsToHandleLevel2[0], strlen(instructionsToHandleLevel2[0])) == 0)
		{
			for(i = 0; i < MAXCORDS; i++)
			{
				for(j = 0; j< MAXSUBCORDS; j++)
					printf(" %d", current->data[i][j]);
			}
		}
		else
		{
			for(i = 0; i < MAXCORDS; i++)
			{
				if(current->coordinates[i] != '\0')
					printf(" %f", current->coordinates[i]);
			}
		}
	}
}

void WriteSLLToFile(char* outFile)
{
	ParsedNode* current = sllHead;
	int i = 0, j = 0;
	FILE *fileStream;
	
	if( (fileStream = fopen(outFile, "w" )) != NULL )
	{
		for(; current != NULL; current = current->next)
		{
			fprintf(fileStream, "%s", current->instruction);
			
			if( _strnicmp(current->instruction, instructionsToHandleLevel2[0], strlen(instructionsToHandleLevel2[0])) == 0)
			{
				for(i = 0; i < MAXCORDS; i++)
				{
					for(j = 0; j< MAXSUBCORDS; j++)
						fprintf(fileStream, " %d", current->data[i][j]);
				}
			}
			else
			{
				for(i = 0; i < MAXCORDS; i++)
				{
					if(current->coordinates[i] != '\0')
						fprintf(fileStream, " %f", current->coordinates[i]);
				}
			}
			
			fprintf(fileStream, "\n");
		}
		
		fclose(fileStream);
	}
	else
		printf("\nOOPS!! File could not be opened.\n");
  
}
