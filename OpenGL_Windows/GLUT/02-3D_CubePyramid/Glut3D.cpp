#include <GL/freeglut.h>

bool bIsFullscreen = false;

// variable declarations
GLfloat angleCube = 0.0f;
bool ascendingCube = false;
GLfloat anglePyramid = 0.0f;
bool ascendingPyramid = false;

int main(int argc, char *argv[])
{
	// function declarations
	void initialize(void);
	void uninitialize(void);
	void reshape(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void update(void);

	// code
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("3D OpenGL using GLUT");
	initialize();

	// callbacks
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutIdleFunc(update);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return (0);
}

void initialize(void)
{
	// code
	// convention 1 as per Red book // 3D change 5]
	glShadeModel(GL_SMOOTH); // Remember lecture, light, interpolation, flat

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
	glEnable(GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
	glDepthFunc(GL_LEQUAL); // 3D change 4.2] Less than or Equal to

	// convention 2 as per Red book // 3D change 6]
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // 3 modes: GL_NICEST, GL_FASTEST, GL_DONT_CARE
}

void uninitialize(void)
{
	// why empty??
}

void reshape(int width, int height)
{
	// code
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

	// Perspective change 2] NOTE:Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Perspective change 3]
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Perspective change 4]
	glTranslatef(1.5f, 0.0f, -6.0f); // Z negative = inside/away from you
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	// Perspective no change
	// 3D change 7] Z coord depth, NOTE: Raterizer transforms Z into depth values
	// Front Face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// Right Face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// Back Face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	// Left Face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(-1.5f, 0.0f, -6.0f); // Z negative = inside/away from you
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angleCube, 1.0f, 1.0f, 1.0f);

	glBegin(GL_QUADS);

	// top
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	// bottom
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// front
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// back
	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// right
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// left
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		// escape
	case 27:
		glutLeaveMainLoop();
		break;

	case 'f':
	case 'F':
		if (bIsFullscreen == false)
		{
			glutFullScreen();
			bIsFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullscreen = false;
		}
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	}
}
void update()
{
	// Rotate vice versa	
	if (ascendingPyramid)
	{
		anglePyramid += 0.025f;
		if (anglePyramid > 360.0f)
		{
			ascendingPyramid = false;
		}
	}
	else
	{
		anglePyramid -= 0.025f;
		if (anglePyramid < 0.0f)
		{
			ascendingPyramid = true;
		}
	}

	// Rotate vice versa	
	if (ascendingCube)
	{
		angleCube += 0.025f;
		if (angleCube > 360.0f)
		{
			ascendingCube = false;
		}
	}
	else
	{
		angleCube -= 0.025f;
		if (angleCube < 0.0f)
		{
			ascendingCube = true;
		}
	}

	glutPostRedisplay();
}
