/*
	Assignment: Implement Dynamic India in perspective projection.
	Date: 13 Mar 2019
 */

 // Headers
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h> // For Perspective change 3]
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include "MyMusic.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib") // For Perspective change 3]
#pragma comment(lib, "winmm.lib") // multi media for PlaySound

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define LINES_TO_DRAW_GRAPH 20
#define RATIO_COLS 16.0f
#define RATIO_ROWS 9.0f

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
bool bIsFullScreen = false;
DWORD dwStyle; // global default initialized to zero
HWND gHwnd;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
MONITORINFO mi = { sizeof(MONITORINFO) };

// Increamental code
HDC ghdc = NULL; // Common context
HGLRC ghrc = NULL; // Super context, OpenGL Rendering Context
bool gbActiveWindow = false;
FILE *gpFile = NULL;
bool gbShowGraph = false;
bool gbShowRatio = false;
const float stepRow = 2.0f / RATIO_ROWS;
const float stepCol = 2.0f / RATIO_COLS;
bool bShouldPlay = false;

const GLubyte gAiroplaneColorUb[3] = { 186, 226, 238 };
const GLfloat gColorDeepSaffron[3] = { 1.0f,  0.59765625f,  0.19921875f }; // FF9933
const GLfloat gColorIndiaGreen[3] = { 0.07421875f,  0.53125f,  0.03125f }; // 138808
const GLfloat gColorWhite[3] = { 1.0f, 1.0f, 1.0f };

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Increamental code
	// function declarations
	int initialize(void);
	int uninitialize(void);
	void display(void); // Double Buffer change 1]

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("DynamicIndia");

	// Increamental code
	bool bDone = false;
	int iRet = 0;

	// code
	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created..."), TEXT("ERROR"), MB_OK);
		exit(0); // Abortive but 0 and not 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Increamental code
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// Increamental code
	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Dynamic India"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		// own always on top, all others clipped
		// high performance rendering, WS_VISIBLE convention not compulsion
		// use in case of external painting. Makes your window visible even if ShowWindow() not called
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;

	// Removed -> UpdateWindow(hwnd);
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == 0)
	{
		fprintf(gpFile, "initialize() successful.\n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop code
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// Here call update()
			}
			// Here call display()
			display(); // Double Buffer change 2]
			// no call to uninitialize()
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void resize(int widht, int height);
	void uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		// In earlier past version this was LOWORD(wParam), now switch(wParam) only.
		// Because now there is no info in HIWORD. Earlier 'scanrate' was stored in HIWORD.
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd); // NOTE: Request OS send WM_DESTROY message to my window.
			break;

		case 0x46: // 'f' or 'F'
			//ToggleFullScreen();
			break;

		case 'T':
			gbShowGraph = !gbShowGraph;
			break;

		case 'R':
			gbShowRatio = !gbShowRatio;
			break;

		case 'P':
			bShouldPlay = !bShouldPlay;
			if (bShouldPlay)
				PlaySound(MAKEINTRESOURCE(MYMUSIC), NULL, SND_NODEFAULT | SND_ASYNC | SND_RESOURCE);
			//PlaySound(TEXT("HelloWin.wav"), NULL, SND_FILENAME | SND_ASYNC);
			//PlaySound(MAKEINTRESOURCE(MYMUSIC), GetModuleHandle(NULL), SND_ASYNC | SND_RESOURCE);
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_RBUTTONDOWN:
		break;

	case WM_DESTROY:
		uninitialize(); // NOTE
		PostQuitMessage(0);
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); // lParam contians resized window width, height
		break;

		// Double Buffer change 3] WM_PAINT removed, new added
	case WM_ERASEBKGND:
		return (0); // Don't go to DefWindowProc, a) bcoz it posts WM_PAINT, which we don't want
		// b) use my display() when WM_PAINT is not there

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		//ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}

void MakeFullScreen(void)
{
	dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

	if (dwStyle & WS_OVERLAPPEDWINDOW)
	{
		if (GetWindowPlacement(gHwnd, &wpPrev) &&
			GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
		{
			SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

			SetWindowPos(gHwnd,
				HWND_TOP,
				mi.rcMonitor.left,
				mi.rcMonitor.top,
				mi.rcMonitor.right - mi.rcMonitor.left,
				mi.rcMonitor.bottom - mi.rcMonitor.top,
				SWP_NOZORDER | SWP_FRAMECHANGED);
		}
	}

	ShowCursor(FALSE);
	bIsFullScreen = true;
}

// function implementations

int initialize(void)
{
	// function declarations
	void resize(int width, int height);
	void MakeFullScreen(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// step 1] initialize form descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; // NOTE: Hard coded, remember lecture
	// Windows OS stopped OpenGL support from OpenGL v1.5 bcoz of DirectX
	// Above 1.5+ versions are only bcoz of device driver support
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;  // Double Buffer change 6]
	pfd.iPixelType = PFD_TYPE_RGBA; // NOTE
	pfd.cColorBits = 32; // NOTE: Can have variable bits for each
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;

	// step 2] 
	ghdc = GetDC(gHwnd);

	// step 3] give form to OS
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	// return index is always 1 based. So 1 to 38 and not 0 to 37.
	// if 0 gets returned then it is failure

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	// NOTE: BRIDGING APIs, remember all platforms

	// step 4]
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return -3;
	}

	// step 5]
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	// NOTE: changed color to identify if repainting happens on resize 
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Cheks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when renedering starts, display().
	//    "OpenGL is a state machine."

	MakeFullScreen(); // NOTE

	// warm up call to resize, convention and not compulsion
	RECT rc;
	GetClientRect(gHwnd, &rc);
	resize(rc.right - rc.left, rc.bottom - rc.top);
	//resize(WIN_WIDTH, WIN_HEIGHT); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()

	return 0;
}

void uninitialize(void)
{
	/* Check whether fullscreen or not and
	if it is then resotre to normal size
	and then proceed for uninitializtion.
	Dots per inch problem, resolution disturbed. */
	if (bIsFullScreen == true)
	{
		// verify
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log file closing successfully.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void resize(int width, int height)
{
	// Perspective change 1]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

	 // Perspective change 2] NOTE: Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Perspective change 3]
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void)
{
	void drawGraph(void);
	void drawRatioLine(void);
	bool drawDynamicIndia(void);
	void drawPlane();
	bool drawCircle(float radius, float start, float end);
	bool drawCircleClockwise(float radius, float start, float end);
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth);
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth, float alpha);
	void drawTricolorInLetterA(void);

	static bool isLoaded = false;
	if (!bShouldPlay)
	{
		if (!isLoaded)
			glClear(GL_COLOR_BUFFER_BIT);

		SwapBuffers(ghdc);
		isLoaded = true;
		return;
	}

	static float planeBtx = -2.88f;
	static float planeAtx = 0.0f;
	static float planeCtx = 0.0f;
	static const float bigLetterWidth = stepCol * 2.0f;
	static float theHeight = stepRow * 3.0f;
	static float airoplanePassed = false;
	static float tricolorAlpha = 0.0f;

	glClear(GL_COLOR_BUFFER_BIT); // Double Buffer change 7] glFlush(); removed
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Perspective change 4]
	glTranslatef(0.0f, 0.0f, -3.0f); // Z negative = inside/away from you
	/*gluLookAt(0.0f, 0.0f, 3.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f);*/

	if (gbShowGraph)
		drawGraph();
	if (gbShowRatio)
		drawRatioLine();

	if (drawDynamicIndia())
	{
		glTranslatef(planeBtx, 0.0f, 0.0f);
		drawPlane();

		glLoadIdentity();
		if (planeAtx <= (1.24f + 0.8f))
		{
			glTranslatef(-2.2f + 1.24f + planeAtx, -1.24f, -3.0f);
			if (drawCircleClockwise(1.24f, M_PI + (M_PI_2 / 4.0f), M_PI_2))
				planeAtx += 0.000098f;
		}
		else if (planeAtx > 1.24f + 0.8f)
		{
			glTranslatef(-2.2f + 1.24f + planeAtx, 1.24f, -3.0f);
			drawCircle(1.24f, M_PI + M_PI_2, (M_PI * 2.0f) + M_PI_4);
		}

		glLoadIdentity();
		if (planeCtx <= (1.24f + 0.8f))
		{
			glTranslatef(-2.2f + 1.24f + planeCtx, 1.24f, -3.0f);
			if (drawCircle(1.24f, M_PI - (M_PI_2 / 4.0f), M_PI + M_PI_2))
				planeCtx += 0.000098f;
		}
		else if (planeCtx > 1.24f + 0.8f)
		{
			glTranslatef(-2.2f + 1.24f + planeAtx, -1.24f, -3.0f);
			drawCircleClockwise(1.24f, M_PI_2, 0.0f - M_PI_4);
		}

		if (planeBtx < -2.2f + 1.24f)
			planeBtx += 0.000098f;
		else if (planeBtx < 1.24f + 0.8f)
			planeBtx += 0.000098f;
		else if (planeBtx < 2.56f)
			planeBtx += 0.0001f;

		if (planeBtx > 0.9f) //9 best with 0.00015
		{
			glLoadIdentity();
			glTranslatef(stepCol * 8.0f, 0.0f, -3.0f);
			drawTricolorInLetterA();
		}
	}

	SwapBuffers(ghdc);
}

void drawGraph(void)
{
	// variable declarations
	int i = 0;
	float step = 1.0f / LINES_TO_DRAW_GRAPH;
	float xcoord = step; // 0.0f;
	float ycoord = step; // 0.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-xcoord, 1.0f);
		glVertex2f(-xcoord, -1.0f);
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord += step;
	}

	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		glVertex2f(-1.0f, -ycoord);
		glVertex2f(1.0f, -ycoord);
		ycoord += step;
	}

	glEnd();
}

void drawRatioLine(void)
{
	// variable declarations
	int i = 0;
	float stepRow = 2.0f / RATIO_ROWS;
	float stepCol = 2.0f / RATIO_COLS;
	float xcoord = 1.0f;
	float ycoord = 1.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	for (i = 0; i <= RATIO_COLS; i++)
	{
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord -= stepCol;
	}

	for (i = 0; i <= RATIO_ROWS; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		ycoord -= stepRow;
	}

	glEnd();
}

bool drawLetterI1(float *translationStepI1, const float stopMarkerI1, const float theHeight)
{
	glColor3fv(gColorDeepSaffron);
	glVertex2f(*translationStepI1, theHeight);
	glColor3fv(gColorIndiaGreen);
	glVertex2f(*translationStepI1, -theHeight);
	if (*translationStepI1 < stopMarkerI1)
	{
		*translationStepI1 += 0.0002f;
		return false;
	}
	return true;
}

bool drawLetterA(float *translationStepA, const float startMarkerAx, const float theHeight, const float bigLetterWidth)
{
	glColor3fv(gColorIndiaGreen);
	glVertex2f(*translationStepA, -theHeight);
	glColor3fv(gColorDeepSaffron);
	glVertex2f(*translationStepA + (bigLetterWidth / 2.0f), theHeight);

	glColor3fv(gColorDeepSaffron);
	glVertex2f(*translationStepA + (bigLetterWidth / 2.0f), theHeight);
	glColor3fv(gColorIndiaGreen);
	glVertex2f(*translationStepA + bigLetterWidth, -theHeight);
	if (*translationStepA > startMarkerAx)
	{
		*translationStepA -= 0.0002f;
		return false;
	}
	return true;
}

bool drawLetterN(float *translationStepN, const float startMarkerNx, const float theHeight, const float bigLetterWidth)
{
	glColor3fv(gColorDeepSaffron);
	glVertex2f(startMarkerNx, *translationStepN + (theHeight * 2.0f));
	glColor3fv(gColorIndiaGreen);
	glVertex2f(startMarkerNx, *translationStepN);

	glColor3fv(gColorDeepSaffron);
	glVertex2f(startMarkerNx, *translationStepN + (theHeight * 2.0f));
	glColor3fv(gColorIndiaGreen);
	glVertex2f(startMarkerNx + bigLetterWidth, *translationStepN);

	glColor3fv(gColorDeepSaffron);
	glVertex2f(startMarkerNx + bigLetterWidth, *translationStepN + (theHeight * 2.0f));
	glColor3fv(gColorIndiaGreen);
	glVertex2f(startMarkerNx + bigLetterWidth, *translationStepN);

	if (*translationStepN > -theHeight)
	{
		*translationStepN -= 0.0002f;
		return false;
	}
	return true;
}

bool drawLetterI2(float *translationStepI2, const float startMarkerI2x, const float theHeight)
{
	glColor3fv(gColorDeepSaffron);
	glVertex2f(startMarkerI2x, *translationStepI2);
	glColor3fv(gColorIndiaGreen);
	glVertex2f(startMarkerI2x, *translationStepI2 - (theHeight * 2.0f));

	if (*translationStepI2 < theHeight)
	{
		*translationStepI2 += 0.0002f;// 0.0002f;
		return false;
	}
	return true;
}

bool drawLetterD(float *alphaD, const float startMarkerDx, const float theHeight, const float bigLetterWidth)
{
	glColor4f(gColorDeepSaffron[0], gColorDeepSaffron[1], gColorDeepSaffron[2], *alphaD);
	glVertex2f(startMarkerDx, theHeight);
	glColor4f(gColorIndiaGreen[0], gColorIndiaGreen[1], gColorIndiaGreen[2], *alphaD);
	glVertex2f(startMarkerDx, -theHeight);

	glColor4f(gColorDeepSaffron[0], gColorDeepSaffron[1], gColorDeepSaffron[2], *alphaD);
	glVertex2f(startMarkerDx, theHeight);
	glVertex2f(startMarkerDx + bigLetterWidth, theHeight - (theHeight / 3.0f));

	glColor4f(gColorIndiaGreen[0], gColorIndiaGreen[1], gColorIndiaGreen[2], *alphaD);
	glVertex2f(startMarkerDx, -theHeight);
	glVertex2f(startMarkerDx + bigLetterWidth, -theHeight + (theHeight / 3.0f));

	glColor4f(gColorDeepSaffron[0], gColorDeepSaffron[1], gColorDeepSaffron[2], *alphaD);
	glVertex2f(startMarkerDx + bigLetterWidth, theHeight - (theHeight / 3.0f));
	glColor4f(gColorIndiaGreen[0], gColorIndiaGreen[1], gColorIndiaGreen[2], *alphaD);
	glVertex2f(startMarkerDx + bigLetterWidth, -theHeight + (theHeight / 3.0f));

	if (*alphaD < 1.0f)
	{
		*alphaD += 0.0001f;
		return false;
	}
	return true;
}

bool drawDynamicIndia(void)
{
	// function declarations
	bool drawLetterI1(float *translationStepI1, const float stopMarkerI1, const float theHeight);
	bool drawLetterA(float *translationStepA, const float startMarkerAx, const float theHeight, const float bigLetterWidth);
	bool drawLetterN(float *translationStepN, const float startMarkerNx, const float theHeight, const float bigLetterWidth);
	bool drawLetterI2(float *translationStepI2, const float startMarkerI2x, const float theHeight);
	bool drawLetterD(float *alphaD, const float startMarkerDx, const float theHeight, const float bigLetterWidth);

	// variable declarations
	static const float theHeight = stepRow * 2.50f;
	static const float bigLetterWidth = stepCol * 2.0f;
	static bool isPlacedI1 = false;
	static bool isPlacedN = false;
	static bool isPlacedD = false;
	static bool isPlacedI2 = false;
	static bool isPlacedA = false;
	static float translationStepI1 = -2.95f;
	static const float stopMarkerI1 = -1.0f + (stepCol); // on first column
	static float translationStepN = 1.24f;
	static const float startMarkerNx = -1.0f + (stepCol * 3.0f);
	static float translationStepA = 2.2f;
	static const float startMarkerAx = -1.0f + (stepCol * 13.0f);
	static float translationStepI2 = -1.24f;
	static const float startMarkerI2x = -1.0f + (stepCol * 11.0f);
	static float alphaD = 0.0f;
	static const float startMarkerDx = -1.0f + (stepCol * 7.0f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLineWidth(15.0f);
	glBegin(GL_LINES);

	isPlacedI1 = drawLetterI1(&translationStepI1, stopMarkerI1, theHeight);
	if (isPlacedI1)
		isPlacedA = drawLetterA(&translationStepA, startMarkerAx, theHeight, bigLetterWidth);
	if (isPlacedA)
		isPlacedN = drawLetterN(&translationStepN, startMarkerNx, theHeight, bigLetterWidth);
	if (isPlacedN)
		isPlacedI2 = drawLetterI2(&translationStepI2, startMarkerI2x, theHeight);
	if (isPlacedI2)
		isPlacedD = drawLetterD(&alphaD, startMarkerDx, theHeight, bigLetterWidth);

	glEnd();
	glDisable(GL_BLEND);

	return isPlacedD;
}

bool drawCircleClockwise(float radius, float start, float end)
{
	void drawPlane();

	static GLfloat angle = start;
	static GLfloat theX;
	static GLfloat theY;

	//glPointSize(2.0f);
	glBegin(GL_POINTS);
	//glColor3fv(gColorWhite);

	for (GLfloat angleTemp = start; angleTemp > angle; angleTemp -= 0.001f)
	{
		theX = cos(angleTemp) * radius;
		theY = sin(angleTemp) * radius;
		//glVertex3f(theX, theY, 0.0f);
	}
	glEnd();

	glTranslatef(theX, theY, 0.0f);
	glRotatef((angle - M_PI_2) * (180.0f / M_PI), 0.0f, 0.0f, 1.0f);
	drawPlane();

	if (angle > end)
	{
		angle -= 0.0001f;
		return false;
	}
	else
		return true;
}

bool drawCircle(float radius, float start, float end)
{
	void drawPlane();

	static GLfloat angle = start;
	static GLfloat theX;
	static GLfloat theY;

	//glPointSize(2.0f);
	glBegin(GL_POINTS);
	//glColor3fv(gColorIndiaGreen);

	for (GLfloat angleTemp = start; angleTemp < angle; angleTemp += 0.001f)
	{
		theX = cos(angleTemp) * radius;
		theY = sin(angleTemp) * radius;
		//glVertex3f(theX, theY, 0.0f);
	}
	glEnd();

	glTranslatef(theX, theY, 0.0f);
	glRotatef((angle + M_PI_2) * (180.0f / M_PI), 0.0f, 0.0f, 1.0f);
	drawPlane();

	if (angle < end)
	{
		angle += 0.0001f;
		return false;
	}
	else
		return true;
}

// step 1] find mid points of both sides
// step 2] can draw middle line joining both mid points
// step 3] find lengths of all three sides
// step 4] Upper: now we have lengths of all sides. find one angle. Inverse cos
//			cos A = (b2 + c2 - a2) /2bc		A = cos inverse(value)
// step 5] Lower: angle = 180 - upper. Inverse cos
// step 6].a find next point of side 1 nextS1A'(x + pos * cos(A), y + pos * sin(A))
// step 6].b find next point of side 2 nextS2A'(x + pos * cos(A), y + pos * sin(A))
// step 7] draw line from obtained points nextS1A' <--> nextS2A'
// step 8].a find prev point of side 1 prevS1A'(x + pos * cos(A), y + pos * sin(A))
// step 8].b find prev point of side 2 prevS2A'(x + pos * cos(A), y + pos * sin(A))
// step 9] draw line from obtained points prevS1A' <--> prevS2A'
void drawTricolorInLetterA(float side1X1, float side1Y1, float side1X2, float side1Y2,
	float side2X1, float side2Y1, float side2X2, float side2Y2)
{
	typedef struct Coordinate
	{
		float x;
		float y;
		float z;
	}Coord;

	// midpoint = (x1+x2)/2, (y1+y2)/2
	static Coord side1MidPoint, side2MidPoint;

	side1MidPoint.x = (side1X1 + side1X2) / 2.0f;
	side1MidPoint.y = (side1Y1 + side1Y2) / 2.0f;
	side1MidPoint.z = 0.0f;

	side2MidPoint.x = (side2X1 + side2X2) / 2.0f;
	side2MidPoint.y = (side2Y1 + side2Y2) / 2.0f;
	side2MidPoint.z = 0.0f;

	static float lengthB = sqrtf(powf((side1MidPoint.x - side1X2), 2) + powf((side1MidPoint.y - side1Y2), 2));
	static float lengthC = sqrtf(powf((side1MidPoint.x - side2MidPoint.x), 2) + powf((side1MidPoint.y - side2MidPoint.y), 2));
	static float lengthA = sqrtf(powf((side2MidPoint.x - side2X2), 2) + powf((side2MidPoint.y - side2Y2), 2));

	static float side1CosA = ((lengthB*lengthB) + (lengthC*lengthC) - (lengthA*lengthA)) / (2.0f *lengthB * lengthC);
	static float side1AngleA = acosf(side1CosA) * (180.0f / M_PI); // radian to degree
	static float side1CosB = ((lengthA*lengthA) + (lengthC*lengthC) - (lengthB*lengthB)) / (2.0f *lengthA * lengthC);
	static float side1AngleB = acosf(side1CosB) * (180.0f / M_PI); // radian to degree

	static float nextS1Ax = side1MidPoint.x + (0.008f * cosf(side1AngleA));
	static float nextS1Ay = side1MidPoint.y + (0.040f * sinf(side1AngleA));
	static float nextS2Ax = side2MidPoint.x - (0.008f * cosf(side1AngleB));
	static float nextS2Ay = side2MidPoint.y + (0.040f * sinf(side1AngleB));

	static float prevS1Ax = side1MidPoint.x - (0.001f * cosf(side1AngleA));
	static float prevS1Ay = side1MidPoint.y - (0.080f * sinf(side1AngleA));
	static float prevS2Ax = side2MidPoint.x + (0.001f * cosf(side1AngleB));
	static float prevS2Ay = side2MidPoint.y - (0.080f * sinf(side1AngleB));

	glColor3fv(gColorDeepSaffron);
	glVertex3f(prevS1Ax, prevS1Ay, side1MidPoint.z);
	glVertex3f(prevS2Ax, prevS2Ay, side2MidPoint.z);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(side1MidPoint.x, side1MidPoint.y, side1MidPoint.z);
	glVertex3f(side2MidPoint.x, side2MidPoint.y, side2MidPoint.z);

	glColor3fv(gColorIndiaGreen);
	glVertex3f(nextS1Ax, nextS1Ay, side1MidPoint.z);
	glVertex3f(nextS2Ax, nextS2Ay, side2MidPoint.z);
}

void drawPlane()
{
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLubyte colorVector[3], const float lineWidth);
	void drawLines(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth);
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth);

	static const float unitMagnitude = stepCol * 1.25f;
	static const float half = (unitMagnitude / 2.0f);
	static const float percent10 = (unitMagnitude * 0.1f);
	static const float percent15 = (unitMagnitude * 0.15f);
	static const float percent20 = (unitMagnitude * 0.2f);
	static const float percent25 = (unitMagnitude * 0.25f);
	static const float percent30 = (unitMagnitude * 0.3f);
	static const float percent35 = (unitMagnitude * 0.35f);
	static const float percent40 = (unitMagnitude * 0.4f);
	static const GLfloat airoplaneTail[8][2] =
	{
		{-unitMagnitude, 0.0f },
		{-unitMagnitude - percent10, half},
		{-unitMagnitude + percent30, half},
		{-unitMagnitude + percent40, percent15},
		{-unitMagnitude + percent40, -percent15},
		{-unitMagnitude + percent30, -half},
		{-unitMagnitude - percent10, -half},
		{-unitMagnitude, 0.0f}
	};
	static const GLfloat airoplaneMiddle[4][2] =
	{
		{0.0f, percent25},
		{-unitMagnitude + percent20, percent15},
		{-unitMagnitude + percent20, -percent15},
		{0.0f, -percent25}
	};
	static const GLfloat airoplaneHead[12][2] =
	{
		{0.0f - percent10, percent25},
		{0.0f - percent10, unitMagnitude},
		{0.0f + percent30, unitMagnitude},
		{0.0f + half, percent30},
		{unitMagnitude, percent35},
		{unitMagnitude + percent40, percent10},
		{unitMagnitude + percent40, -percent10},
		{unitMagnitude, -percent35},
		{0.0f + half, -percent30},
		{0.0f + percent30, -unitMagnitude},
		{0.0f - percent10, -unitMagnitude},
		{0.0f - percent10, -percent25}
	};

	GLfloat side1MidPointx = (percent40 + percent20 + percent30 + percent15) / 2.0f;
	GLfloat midPointy = (percent20 + (-percent20)) / 2.0f;
	GLfloat side2MidPointx = (percent40 + percent20 + percent15 + percent40 + percent20) / 2.0f;

	static const GLfloat IAF[14][2] =
	{
		{percent40, percent20},
		{percent40, -percent20},
		{percent30 + percent15, -percent20},
		{percent40 + percent20, percent20},
		{percent40 + percent20, percent20},
		{percent40 + percent20 + percent15,-percent20},
		{side1MidPointx, midPointy},
		{side2MidPointx, midPointy},
		{percent40 * 2.0f, percent20},
		{percent40 * 2.0f, -percent20},
		{percent40 * 2.0f, percent20},
		{(percent40 * 2.0f) + percent20, percent20},
		{percent40 * 2.0f, 0.0f},
		{(percent40 * 2.0f) + percent15, 0.0f}
	};
	static const GLfloat exhaustSaffron[5][2] =
	{
		{-unitMagnitude, percent10 },
		{-unitMagnitude - percent10, percent15 },
		{-unitMagnitude * 2.0f, percent15 },
		{-unitMagnitude * 2.0f, percent15 - percent10 },
		{-unitMagnitude - percent10, percent15 - percent10 }
	};
	static const GLfloat exhaustWhite[5][2] =
	{
		{-unitMagnitude, 0.0f },
		{-unitMagnitude - percent10, percent15 - percent10 },
		{-unitMagnitude * 2.0f, percent15 - percent10 },
		{-unitMagnitude * 2.0f, -(percent15 - percent10) },
		{-unitMagnitude - percent10, -(percent15 - percent10) }
	};
	static const GLfloat exhaustGreen[5][2] =
	{
		{-unitMagnitude, -percent10 },
		{-unitMagnitude - percent10, -percent15 },
		{-unitMagnitude * 2.0f, -percent15 },
		{-unitMagnitude * 2.0f, -(percent15 - percent10) },
		{-unitMagnitude - percent10, -(percent15 - percent10) }
	};

	//glRotatef(45.0f, 0.0f, 0.0f, 1.0f);
	//glRotatef(60.0f, 1.0f, 0.0f, 0.0f);
	drawPolygon(airoplaneTail, 8, gAiroplaneColorUb, 1.0f);
	drawPolygon(airoplaneMiddle, 4, gAiroplaneColorUb, 1.0f);
	drawPolygon(airoplaneHead, 12, gAiroplaneColorUb, 1.0f);
	drawLines(IAF, 14, gColorWhite, 1.50f);
	drawPolygon(exhaustSaffron, 5, gColorDeepSaffron, 1.0f);
	drawPolygon(exhaustWhite, 5, gColorWhite, 1.0f);
	drawPolygon(exhaustGreen, 5, gColorIndiaGreen, 1.0f);
}

void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLubyte colorVector[3], float lineWidth)
{
	glLineWidth(lineWidth);
	glBegin(GL_POLYGON);
	glColor3ubv(colorVector);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawLines(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth)
{
	glLineWidth(lineWidth);
	glBegin(GL_LINES);
	glColor3fv(colorVector);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth)
{
	glLineWidth(lineWidth);
	glBegin(GL_POLYGON);
	glColor3fv(colorVector);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth, float alpha)
{
	GLfloat params[4] = { colorVector[0], colorVector[1], colorVector[2], alpha };
	glLineWidth(lineWidth);
	glBegin(GL_POLYGON);
	glColor4fv(params);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawLines(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth, float alpha)
{
	GLfloat params[4] = { colorVector[0], colorVector[1], colorVector[2], alpha };
	glLineWidth(lineWidth);
	glBegin(GL_LINES);
	glColor4fv(params);
	for (int i = 0; i < vectorCoordSize; i++)
	{
		glVertex2fv(vectorCoord[i]);
	}
	glEnd();
}

void drawTricolorInLetterA(void)
{
	void drawPolygon(const GLfloat vectorCoord[][2], int vectorCoordSize, const GLfloat colorVector[3], float lineWidth, float alpha);

	static float tricolorAlpha = 0.0f;
	static const float unitMagnitude = stepCol * 1.25f;
	static const float half = (unitMagnitude / 2.0f);
	static const float percent10 = (unitMagnitude * 0.1f);
	static const float percent15 = (unitMagnitude * 0.15f);
	static const float percent20 = (unitMagnitude * 0.2f);
	static const float percent25 = (unitMagnitude * 0.25f);
	static const float percent30 = (unitMagnitude * 0.3f);
	static const float percent35 = (unitMagnitude * 0.35f);
	static const float percent40 = (unitMagnitude * 0.4f);
	static const GLfloat exhaustSaffron[4][2] =
	{
		{-unitMagnitude - percent20, percent15 },
		{-unitMagnitude * 2.0f, percent15 },
		{-unitMagnitude * 2.0f, percent15 - percent10 },
		{-unitMagnitude - percent20, percent15 - percent10 }
	};
	static const GLfloat exhaustWhite[4][2] =
	{
		{-unitMagnitude - percent20, percent15 - percent10 },
		{-unitMagnitude * 2.0f, percent15 - percent10 },
		{-unitMagnitude * 2.0f, -(percent15 - percent10) },
		{-unitMagnitude - percent20, -(percent15 - percent10) }
	};
	static const GLfloat exhaustGreen[4][2] =
	{
		{-unitMagnitude - percent20, -percent15 },
		{-unitMagnitude * 2.0f, -percent15 },
		{-unitMagnitude * 2.0f, -(percent15 - percent10) },
		{-unitMagnitude - percent20, -(percent15 - percent10) }
	};

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	drawPolygon(exhaustSaffron, 4, gColorDeepSaffron, 1.0f, tricolorAlpha);
	drawPolygon(exhaustWhite, 4, gColorWhite, 1.0f, tricolorAlpha);
	drawPolygon(exhaustGreen, 4, gColorIndiaGreen, 1.0f, tricolorAlpha);

	glDisable(GL_BLEND);

	if (tricolorAlpha < 1.0f)
		tricolorAlpha += 0.00025;
}
