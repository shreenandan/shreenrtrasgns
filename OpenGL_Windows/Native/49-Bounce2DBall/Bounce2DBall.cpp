/*
	Assignment: Create ANIMATION: 2D Bouncing Ball(shrink, expand near fall)
	Date: 02 Apr 2019
 */

 // Headers
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h> // For perspective change 3]
#include <stdio.h>
// MSDN https://docs.microsoft.com/en-us/cpp/c-runtime-library/math-constants?view=vs-2017
#define _USE_MATH_DEFINES
#include <cmath>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib") // For perspective change 3]

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define LINES_TO_DRAW_GRAPH 20
#define LINES_TO_DRAW_CIRCLE 1000.0f

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
bool bIsFullScreen = false;
DWORD dwStyle; // global default initialized to zero
HWND gHwnd;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
MONITORINFO mi = { sizeof(MONITORINFO) };

HDC ghdc = NULL; // Common context
HGLRC ghrc = NULL; // Super context, OpenGL Rendering Context
bool gbActiveWindow = false;
FILE *gpFile = NULL;
bool gbShowGraph = false;

// Drawing global variable declarations
float radius = 0.250f;
GLfloat yPos = 0.75f;
GLfloat xPos = -0.75f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void); // Double Buffer change 1]
	void update(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Bouncing2DBall");

	bool bDone = false;
	int iRet = 0;

	// code
	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, "Log file can not be created...", TEXT("ERROR"), MB_OK);
		exit(0); // Abortive but 0 and 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Animation: Bouncing 2D Ball"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		// own always on top, all others clipped
		// high performance rendering, WS_VISIBLE convention not compulsion
		// use in case of external painting. Makes your window visible even if ShowWindow() not called
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;

	// Removed -> UpdateWindow(hwnd);
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == 0)
	{
		fprintf(gpFile, "initialize() successful.\n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop code
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// Here call update()
				update();
			}
			// Here call display()
			display(); // Double Buffer change 2]
			// no call to unitialize()
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void resize(int width, int height);
	void uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		// In earlier past version this was LOWORD(wParam), now switch(wParam) only.
		// Becuase now ther is no info in HIWORD. Earlier 'scanrate' was stored in HIWORD.
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd); // NOTE: Request OS send WM_DESTROY message to my window.
			break;

		case 0x46: // 'f' or 'F'
			ToggleFullScreen();
			break;

		case 'T':
			gbShowGraph = !gbShowGraph;
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_RBUTTONDOWN:
		break;

	case WM_DESTROY:
		uninitialize(); // NOTE
		PostQuitMessage(0);
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); // lParam contains resized window width, height
		break;

		// Double Buffer change 3] WM_PAINT removed, new added
	case WM_ERASEBKGND:
		return (0); // Don't go to DefWindowProc, a) bcoz it posts WM_PAINT, which we don't want
		// b) use my display() when WM_APINT is not there

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

		}

		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}

// function implementations
int initialize(void)
{
	// function declarations
	void resize(int width, int height);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// step 1] initialize form descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; // NOTE: Hard coded, remember lecture
	// Windows OS stopped OpenGL support from OpenGL v1.5 bcoz of DirectX
	// Above 1.5+ versions are only bcoz of device driver support
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Double Buffer change 6]
	pfd.iPixelType = PFD_TYPE_RGBA; // NOTE
	pfd.cColorBits = 32; // NOTE: Can have variable bits for each
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;

	// step 2]
	ghdc = GetDC(gHwnd);

	// step 3] give form to OS
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	// return index is always 1 based. So 1 to 38 and not 0 to 37.
	// if 0 gets returned then it is failure

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	// NOTE: BRIDGING APIs, remember all platforms

	// step 4]
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return -3;
	}

	// step 5]
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Checks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when rendering starts, display().
	//	"OpenGL is a state machine."

	// warm up call to resize, convention and not compulsion
	RECT rc;
	GetClientRect(gHwnd, &rc);
	resize(rc.right - rc.left, rc.bottom - rc.top);
	//resize(WIN_WIDTH, WIN_HEIGHT); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()

	return 0;
}

void uninitialize(void)
{
	/* Check whether fullscreen or not and
	if it is then restore to normal size
	and then proceed for uninitialization.
	Dots per inch problem, resolution disturbed.
	*/
	if (bIsFullScreen == true)
	{
		// verify
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Closing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void resize(int width, int height)
{
	// Perspective change 1]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

	// Perspective change 2] NOTE:Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Perspective change 3]
	//gluPerspective(66.0f,
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void update(void)
{
	if (xPos < 0.75f)
		xPos += 0.0015f;

	if (yPos > -0.75f && xPos < 0.0f)
		yPos -= 0.003f;
	else
		yPos += 0.003f;

	if (yPos < -0.5f && yPos > -0.75 && xPos < 0.0f)
		radius -= 0.00015f;
	if (yPos < -0.5f && yPos > -0.75 && xPos >= 0.0f)
		radius += 0.00015f;

	if (yPos > 0.75f)
	{
		yPos = 0.75;
		xPos = -0.75;
	}
}

void display(void)
{
	// function declarations
	void drawCircle(float radius, float red, float green, float blue);
	void drawGraph(void);

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -2.5f); // Z negative = inside/away from you

	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-0.250f, -1.0f);
	glVertex2f(0.250f, -1.0f);
	glEnd();

	//drawGraph();
	glPointSize(1.0f);
	//drawCircle(0.250f, 0.0f, 1.0f, 0.0f);

	glTranslatef(xPos, yPos, 0.0f);
	drawCircle(radius, 0.0f, 1.0f, 0.0f);

	SwapBuffers(ghdc);
}

void drawGraph(void)
{
	// variable declarations
	int i = 0;
	float step = 1.0f / LINES_TO_DRAW_GRAPH;
	float xcoord = step; // 0.0f;
	float ycoord = step; // 0.0f;

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-xcoord, 1.0f);
		glVertex2f(-xcoord, -1.0f);
		glVertex2f(xcoord, 1.0f);
		glVertex2f(xcoord, -1.0f);
		xcoord += step;
	}

	for (i = 0; i < LINES_TO_DRAW_GRAPH; i++)
	{
		glVertex2f(-1.0f, ycoord);
		glVertex2f(1.0f, ycoord);
		glVertex2f(-1.0f, -ycoord);
		glVertex2f(1.0f, -ycoord);
		ycoord += step;
	}

	glEnd();
}

void drawCircle(float radius, float red, float green, float blue)
{
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(red, green, blue);
	for (GLfloat angle = 0.0f; angle <= 2.0f * M_PI; angle += 0.001f)
	{
		glVertex3f(cos(angle) * radius, sin(angle) * radius, 0.0f);
	}
	glEnd();
}
