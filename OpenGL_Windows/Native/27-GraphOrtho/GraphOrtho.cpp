/*
	Assignment: Draw graph in orthographic.
	Date: 13 Feb 2019
 */

 // Headers
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <stdio.h>
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define LINES_TO_DRAW 20

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
bool bIsFullScreen = false;
DWORD dwStyle; // global default initialized to zero
HWND gHwnd;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
MONITORINFO mi = { sizeof(MONITORINFO) };

// Increamental code
HDC ghdc = NULL; // Common context
HGLRC ghrc = NULL; // Super context, OpenGL Rendering Context
bool gbActiveWindow = false;
FILE *gpFile = NULL;
int wndWidth = 800;
int wndHeight = 600;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Increamental code
	// function declarations
	int initialize(void);
	int uninitialize(void);
	void display(void); // Double Buffer change 1]

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("GraphOrthographic");

	// Increamental code
	bool bDone = false;
	int iRet = 0;

	// code
	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file can not be created..."), TEXT("ERROR"), MB_OK);
		exit(0); // Abortive but 0 and not 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Increamental code
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// Increamental code
	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Native FFP Orthographic Graph"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		// own always on top, all others clipped
		// high performance rendering, WS_VISIBLE convention not compulsion
		// use in case of external painting. Makes your window visible even if ShowWindow() not called
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;

	// Removed -> UpdateWindow(hwnd);
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == 0)
	{
		fprintf(gpFile, "initialize() successful.\n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop code
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// Here call update()
			}
			// Here call display()
			display(); // Double Buffer change 2]
			// no call to uninitialize()
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void resize(int widht, int height);
	void uninitialize(void);
	
	// variable declarations
	static RECT rect;

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		//MessageBox(hwnd, TEXT("This is Create message."), TEXT("My Caption"), MB_OK);
		break;

	case WM_KEYDOWN:
		// In earlier past version this was LOWORD(wParam), now switch(wParam) only.
		// Because now there is no info in HIWORD. Earlier 'scanrate' was stored in HIWORD.
		switch (wParam)
		{
		case VK_ESCAPE:
			//MessageBox(hwnd, TEXT("This is Escape message."), TEXT("My Caption"), MB_OK);
			DestroyWindow(hwnd); // NOTE: Request OS send WM_DESTROY message to my window.
			break;

		case 0x46: // 'f' or 'F'
			ToggleFullScreen();
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("This is Left Mouse Button Click message."), TEXT("My Caption"), MB_OK);
		break;

	case WM_RBUTTONDOWN:
		MessageBox(hwnd, TEXT("This is Right Mouse Button Click message."), TEXT("My Caption"), MB_OK);
		break;

	case WM_DESTROY:
		//MessageBox(hwnd, TEXT("This is Destroy message."), TEXT("My Caption"), MB_OK);
		uninitialize(); // NOTE
		PostQuitMessage(0);
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		GetClientRect(hwnd, &rect);
		wndWidth = rect.right - rect.left;
		wndHeight = rect.top - rect.bottom;
		resize(LOWORD(lParam), HIWORD(lParam)); // lParam contians resized window width, height
		break;

		// Double Buffer change 3] WM_PAINT removed, new added
	case WM_ERASEBKGND:
		return (0); // Don't go to DefWindowProc, a) bcoz it posts WM_PAINT, which we don't want
		// b) use my display() when WM_PAINT is not there
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}

// function implementations

int initialize(void)
{
	// function declarations
	void resize(int width, int height);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// step 1] initialize form descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; // NOTE: Hard coded, remember lecture
	// Windows OS stopped OpenGL support from OpenGL v1.5 bcoz of DirectX
	// Above 1.5+ versions are only bcoz of device driver support
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;  // Double Buffer change 6]
	pfd.iPixelType = PFD_TYPE_RGBA; // NOTE
	pfd.cColorBits = 32; // NOTE: Can have variable bits for each
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;

	// step 2] 
	ghdc = GetDC(gHwnd);

	// step 3] give form to OS
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	// return index is always 1 based. So 1 to 38 and not 0 to 37.
	// if 0 gets returned then it is failure

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	// NOTE: BRIDGING APIs, remember all platforms

	// step 4]
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return -3;
	}

	// step 5]
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	// NOTE: changed color to identify if repainting happens on resize 
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Cheks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when renedering starts, display().
	//    "OpenGL is a state machine."

	// warm up call to resize, convention and not compulsion
	resize(WIN_WIDTH, WIN_HEIGHT); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()

	return 0;
}

void uninitialize(void)
{
	/* Check whether fullscreen or not and
	if it is then resotre to normal size
	and then proceed for uninitializtion.
	Dots per inch problem, resolution disturbed. */
	if (bIsFullScreen == true)
	{
		// verify
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log file closing successfully.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void resize(int width, int height)
{
	// Orthographic change 1]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

	 // Orthographic change 2] NOTE: Important
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Ortho change 5]
	if (width <= height)
	{
		// width is small. height/width
		glOrtho(-100.0f,
			100.0f,
			(-100.0f * (GLfloat)height / (GLfloat)width),
			(100.0f * (GLfloat)height / (GLfloat)width),
			-100, 100);
	}
	else
	{
		// height is small. width/height
		glOrtho((-100.0f * (GLfloat)width / (GLfloat)height),
			(100.0f * (GLfloat)width / (GLfloat)height),
			-100.0f,
			100.0f,
			-100, 100);
	}
}

void display(void)
{
	// variable declarations
	int i = 0;
	float ratio = (wndWidth <= wndHeight) ?
		(100.0f * (GLfloat)wndHeight / (GLfloat)wndWidth) :
		(100.0f * (GLfloat)wndWidth / (GLfloat)wndHeight);
	float xstep = ratio / LINES_TO_DRAW;
	float xcoord = xstep;
	float ystep = 100 / LINES_TO_DRAW;;
	float ycoord = ystep;

	glClear(GL_COLOR_BUFFER_BIT);
	// Double Buffer change 7] glFlush(); removed

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(-ratio, 0.0f);
	glVertex2f(ratio, 0.0f);
	glVertex2f(0.0f, -100);
	glVertex2f(0.0f, 100);
	glEnd();

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	for (i = 0; i < LINES_TO_DRAW - 1; i++)
	{
		glVertex2f(-xcoord, 100.0f);
		glVertex2f(-xcoord, -100.0f);
		glVertex2f(xcoord, 100.0f);
		glVertex2f(xcoord, -100.0f);
		xcoord += xstep;
	}
	glEnd();

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	for (i = 0; i < LINES_TO_DRAW - 1; i++)
	{
		glVertex2f(-ratio, ycoord);
		glVertex2f(ratio, ycoord);
		glVertex2f(-ratio, -ycoord);
		glVertex2f(ratio, -ycoord);
		ycoord += ystep;
	}
	glEnd();

	SwapBuffers(ghdc);
}
