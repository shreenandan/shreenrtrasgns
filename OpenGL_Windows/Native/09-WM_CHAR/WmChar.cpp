// Headers
#include <Windows.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("WmChar");
	int iWinWidth = 350;
	int iWinHeight = 200;
	int iMaxWidth;
	int iMaxHeight;
	RECT rc;

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	iMaxWidth = GetSystemMetrics(SM_CXSCREEN);
	iMaxHeight = GetSystemMetrics(SM_CYSCREEN);

	hwnd = CreateWindow(szAppName,
		TEXT("WM_CHAR"),
		WS_OVERLAPPEDWINDOW,
		((iMaxWidth - iWinWidth) / 2),
		(iMaxHeight - iWinHeight) / 2,
		iWinWidth,
		iWinHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	RECT rc;
	bool bGotClientRect;
	HDC hdc;
	COLORREF cr;
	TCHAR str[] = TEXT("Hello World!!!");
	int iRes;

	// code
	switch (iMsg)
	{
	case WM_PAINT:
		bGotClientRect = GetClientRect(hwnd, &rc);
		hdc = GetDC(hwnd);
		cr = SetBkColor(hdc, RGB(0, 0, 0));
		cr = SetTextColor(hdc, RGB(0, 255, 0));
		iRes = DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		iRes = ReleaseDC(hwnd, hdc);
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'f':
			MessageBox(hwnd, TEXT("Small 'f'"), TEXT("f"), MB_OK);
			break;

		case 'F':
			MessageBox(hwnd, TEXT("Capital 'F'"), TEXT("F"), MB_OK);
			break;

		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}
