/*
	Assignment: PP 3] Orthographic Triangle.
	Base App: 2nd Pass Through Shader pgm
	Date: 27 Jul 2019
 */

#pragma once
 // Headers
#include <Windows.h>
#include <GL/glew.h>
#include <gl/GL.h>
#include <stdio.h>
#include "vmath.h"

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
bool bIsFullScreen = false;
DWORD dwStyle; // global default initialized to zero
HWND gHwnd;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
MONITORINFO mi = { sizeof(MONITORINFO) };

HDC ghdc = NULL; // Common context
HGLRC ghrc = NULL; // Super context, OpenGL Rendering Context
bool gbActiveWindow = false;
FILE *gpFile = NULL;

GLenum result;

// PP
//GLuint gVertexShaderObject;
//GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

GLuint vao; //vertex array object
GLuint vbo;	//vertex buffer object
GLuint mvpUniform; // model view projection
mat4 orthographicProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void); // Double Buffer change 1]

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PPOrthographicTriangle");

	bool bDone = false;
	int iRet = 0;

	// code
	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, "Log file can not be created...", TEXT("ERROR"), MB_OK);
		exit(0); // Abortive but 0 and 1 = system error
	}
	else
	{
		fprintf(gpFile, "Log file successfully created.\n");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("PP 3] Orthographic Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		// own always on top, all others clipped
		// high performance rendering, WS_VISIBLE convention not compulsion
		// use in case of external painting. Makes your window visible even if ShowWindow() not called
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;

	// Removed -> UpdateWindow(hwnd);
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == 0)
	{
		fprintf(gpFile, "\ninitialize() successful.\n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop code
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//// Here call update()
				//update(); // 3D change 8] New update approach introduced with game loop
			}
			// Here call display()
			display(); // Double Buffer change 2]
			// no call to unitialize()
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void resize(int width, int height);
	void uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		// In earlier past version this was LOWORD(wParam), now switch(wParam) only.
		// Becuase now ther is no info in HIWORD. Earlier 'scanrate' was stored in HIWORD.
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd); // NOTE: Request OS send WM_DESTROY message to my window.
			break;

		case 0x46: // 'f' or 'F'
			ToggleFullScreen();
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_RBUTTONDOWN:
		break;

	case WM_DESTROY:
		uninitialize(); // NOTE
		PostQuitMessage(0);
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); // lParam contains resized window width, height
		break;

		// Double Buffer change 3] WM_PAINT removed, new added
	case WM_ERASEBKGND:
		return (0); // Don't go to DefWindowProc, a) bcoz it posts WM_PAINT, which we don't want
		// b) use my display() when WM_APINT is not there

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

		}

		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}

// function implementations
int initialize(void)
{
	// function declarations
	void resize(int width, int height);
	BOOL loadTexture(GLuint *texture, TCHAR imageResourceId[]);
	void uninitialize(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// PP variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// step 1] initialize form descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; // NOTE: Hard coded, remember lecture
	// Windows OS stopped OpenGL support from OpenGL v1.5 bcoz of DirectX
	// Above 1.5+ versions are only bcoz of device driver support
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Double Buffer change 6]
	pfd.iPixelType = PFD_TYPE_RGBA; // NOTE
	pfd.cColorBits = 32; // NOTE: Can have variable bits for each
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; // 3D change 1]

	// step 2]
	ghdc = GetDC(gHwnd);

	// step 3] give form to OS
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	// return index is always 1 based. So 1 to 38 and not 0 to 37.
	// if 0 gets returned then it is failure

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	// NOTE: BRIDGING APIs, remember all platforms

	// step 4]
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return -3;
	}

	// step 5]
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}


	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "\nGLEW init failed!!\n");
		uninitialize();
		DestroyWindow(gHwnd);
	}
	else
	{
		fwprintf(gpFile, L"\nglewInit successful.");
	}

	// Pass Through Shader code

#pragma region Vertex Shader

	// step 1] define vertex shader obj
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// step 2] write vertex shader code
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"	gl_Position = u_mvp_matrix * vPosition;" \
		"}";

	// step 3] specify above source code to vertex shader obj
	glShaderSource(gVertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode,
		NULL);

	// step 4] compile the vertex shader
	glCompileShader(gVertexShaderObject);


	// steps for catching errors
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(gVertexShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fwprintf(gpFile, L"\nVertex Shader: Compilation Error: %hs", TEXT(szInfoLog));
				free(szInfoLog);
			}
			else
			{
				fwprintf(gpFile, L"\nVertex Shader: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fwprintf(gpFile, L"\nVertex Shader: Something went wrong, infoLogLength is zero...");
		}
		uninitialize();
		DestroyWindow(gHwnd);
		exit(0); // Why zero? Bcoz error is from GPU, not from CPU/OS
	}
	else
	{
		fwprintf(gpFile, L"\nVertex Shader compiled successfully.");
	}


	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

#pragma endregion


#pragma region Fragment Shader

	// step 1] define fragment shader obj
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// step 2] write fragment shader code
	const GLchar *fragmentShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}";

	// step 3] specify above source code to fragment shader obj
	glShaderSource(gFragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	// step 4] compile the fragment shader
	glCompileShader(gFragmentShaderObject);


	// steps for catching errors
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(gFragmentShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fwprintf(gpFile, L"\nFragment Shader: Compilation Error: %hs", TEXT(szInfoLog));
				free(szInfoLog);
			}
			else
			{
				fwprintf(gpFile, L"\nFragment Shader: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fwprintf(gpFile, L"\nFragment Shader: Something went wrong, infoLogLength is zero...");
		}
		uninitialize();
		DestroyWindow(gHwnd);
		exit(0); // Why zero? Bcoz error is from GPU, not from CPU/OS
	}
	else
	{
		fwprintf(gpFile, L"\nFragment Shader compiled successfully.");
	}

	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

#pragma endregion


#pragma region Program Link

	// create shader program obj
	// step 1] create
	gShaderProgramObject = glCreateProgram();

	// step 2] Attach shaders
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Pre-Linking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	// step 3] Link program
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetProgramInfoLog(gShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fwprintf(gpFile, L"\nShader Program: Link Error: %hs", TEXT(szInfoLog));
				free(szInfoLog);
			}
			else
			{
				fwprintf(gpFile, L"\nShader Program: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fwprintf(gpFile, L"\nShader Program: Something went wrong, infoLogLength is zero...");
		}
		uninitialize();
		DestroyWindow(gHwnd);
		exit(0); // Why zero? Bcoz error is from GPU, not from CPU/OS
	}
	else
	{
		fwprintf(gpFile, L"\nShader program linked successfully.");
	}

	// Post-Linking retrieving uniform location
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");


	// reset
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

#pragma endregion


	const GLfloat triangleVertices[] =
	{
		0.0f, 50.0f, 0.0f,
		-50.0f, -50.0f, 0.0f,
		50.0f, -50.0f, 0.0f
	};

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER,
		sizeof(triangleVertices),
		triangleVertices,
		GL_STATIC_DRAW); // attachya atta oot

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // xyx
		GL_FLOAT,
		GL_FALSE, // isNormalized 0 to 1 NDC 
		0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
		NULL); // no stride therefore no offest

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
	glBindVertexArray(0);


	// NOTE: This provides existence, releases order
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // float more accuracy
	// 1. Checks if OGL is enabled
	// 2. Screen is not colored immediately. Colored when rendering starts, display().
	//	"OpenGL is a state machine."

	glClearDepth(1.0f); // 3D change 2] all set to MAX and compared against MAX
	glEnable(GL_DEPTH_TEST); // 3D change 4.1] Hidden Surface Removal
	glDepthFunc(GL_LEQUAL); // 3D change 4.2] Less than or Equal to


	orthographicProjectionMatrix = mat4::identity();


	// warm up call to resize, convention and not compulsion
	resize(WIN_WIDTH, WIN_HEIGHT); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()

	return 0;
}

void uninitialize(void)
{
	/* Check whether fullscreen or not and
	if it is then restore to normal size
	and then proceed for uninitialization.
	Dots per inch problem, resolution disturbed.
	*/
	if (bIsFullScreen == true)
	{
		// verify
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		bIsFullScreen = false;
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	// PP shader dtor
	// Safe Release
	// Don't declare shader objects globally, use locally in initialize n use as necessary
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNo;

		glUseProgram(gShaderProgramObject);

		// ask pgm how many shaders attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject,
				shaderCount,
				&shaderCount, /// using same var
				pShaders);

			for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNo]);
				glDeleteShader(pShaders[shaderNo]);
				pShaders[shaderNo] = 0;
			}
			free(pShaders);
		}

		glDeleteProgram(gShaderProgramObject); // Not actually deleting but changing machine state. Not shaikh chilli
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nClosing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void resize(int width, int height)
{
	// Perspective change 1]
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // NOTE: Remember Bioscope Cinema and not binacular

	if (width < height)
	{
		// width is small. height/width
		// ortho is from vmath
		orthographicProjectionMatrix = ortho(-100.0f,
			100.0f,
			(-100.0f * (GLfloat)height / (GLfloat)width),
			(100.0f * (GLfloat)height / (GLfloat)width),
			-100.0f,
			100.0f);
	}
	else
	{
		// height is small. width/height
		orthographicProjectionMatrix = ortho((-100.0f * (GLfloat)width / (GLfloat)height),
			(100.0f * (GLfloat)width / (GLfloat)height),
			-100.0f,
			100.0f,
			-100.0f,
			100.0f);
	}
}

void display(void)
{
	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]

	// Use Pass Through Shader Program
	glUseProgram(gShaderProgramObject); // Binding shader pgm to OpenGL pgm

	// your code here
	// 9 steps
	// declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	// initialize above 2 matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	// here in this pgm no transformation, but in later pgms

	// do necessary matrix multiplication
	modelViewProjectionMatrix = orthographicProjectionMatrix * modelViewMatrix;
	// In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum

	// send necessary matrices to shader in respective uniforms
	// display fn is dynamic, called in loop
	glUniformMatrix4fv(mvpUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelViewProjectionMatrix); // kashala chiktavaychay // verify
	// OpenGL/GLSL is column major, DirectX is row major

	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	glDrawArrays(GL_TRIANGLES,
		0, // from which array element to start. You can put different geometries in single array-interleaved
		3); // how many to draw? Ex: Triangle - 3 vertices, Quad 4 vert

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0); // Unbinding

	SwapBuffers(ghdc);
}
