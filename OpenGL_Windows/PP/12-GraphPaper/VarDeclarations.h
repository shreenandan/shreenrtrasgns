#pragma once
#include <stdio.h> // file
#include <GL/glew.h>
#include "vmath.h"
#include <Windows.h> // For HDC only

extern HDC ghdc; // Common context

extern int MyCount;
extern FILE *gpFile;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

using namespace vmath;
extern mat4 perspectiveProjectionMatrix;

enum ShaderModes { BASIC };

struct ShaderUniforms
{
	GLuint mvpUniform = -1;
};

extern struct ShaderUniforms GenericUniforms;

#define LINES_TO_DRAW 20
#define RATIO_COLS 16 //.0f
#define RATIO_ROWS 9 //.0f
#define NO_OF_COORDS_IN_3D_LINE 6
#define NO_OF_GEOMETRIES 1

extern GLuint genericShaderProgramObject;

typedef struct Coordinate
{
	float x;
	float y;
	float z;
}Coord;

typedef struct Triplet
{
	GLfloat red;
	GLfloat green;
	GLfloat blue;
}Triplet;

typedef struct GeometryInfo
{
	// StartX, StartY, StartZ, EndX, EndY, EndZ
	float Vertices[NO_OF_COORDS_IN_3D_LINE] =
	{
		0.0f, 0.0f, 0.0f, 
		0.0f, 0.0f, 0.0f 
	};
	float Colors[NO_OF_COORDS_IN_3D_LINE] =
	{
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f
	};

	float *VerticesDynamic = NULL;
	float *ColorsDynamic = NULL;
	int *ElementsDynamic = NULL;
	
	bool IsComplete = false;
	float InitialX = 0.0f;
	float StopMarker = 0.0f;
	float TranslationStep = 0.0f;
	float MovingX = 0.0f;

	float InitialY = 0.0f;
	float MovingY = 0.0f;

	float Alpha = 0.0f;

	int NumVertices = 0;
	int NumElements = 0;

}Geometry;

enum LineVerticesIndex
{ 
	START_X = 0, 
	START_Y, 
	START_Z, 
	END_X, 
	END_Y, 
	END_Z
};

enum LineColorsIndex
{
	START_RED = 0,
	START_GREEN,
	START_BLUE,
	END_RED,
	END_GREEN,
	END_BLUE
};

extern Geometry GeometryList[NO_OF_GEOMETRIES];
extern GLuint vao_list[NO_OF_GEOMETRIES];
extern GLuint vbo_position_list[NO_OF_GEOMETRIES];
extern GLuint vbo_color_list[NO_OF_GEOMETRIES];
extern GLuint vbo_element_list[NO_OF_GEOMETRIES];

enum Geometries
{
	GRAPH
};

extern const float gStepRow;
extern const float gStepCol;
