#include "Logic.h"
#include <Windows.h>

//float light_ambient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
////float light_diffuse[4] = { 0.0f, 0.6589f, 0.6667f, 1.0f };
//float light_diffuse[4] = { 0.5f, 0.2f, 0.7f, 1.0f }; // Albedo
//float light_specular[4] = { 0.7f, 0.7f, 0.7f, 1.0f };
//float light_position[4] = { 100.0f, 100.0f, 100.0f, 100.0f };
//
//float material_ambient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
//float material_diffuse[4] = { 0.5f, 0.2f, 0.7f, 1.0f };
//float material_specular[4] = { 0.7f, 0.7f, 0.7f, 1.0f };
//float material_shinyness = 128.0f; // 128

float light_ambient[4] = { 0.25f, 0.25f, 0.25f, 0.25f };
float light_diffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float light_specular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float light_position[4] = { 100.0f, 100.0f, 100.0f, 1.0f };

float material_ambient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float material_diffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float material_specular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float material_shinyness = 128.0f;	//also try with 50.0f 

void update(void)
{
	// Rotate vice versa
	if (ascendingCube)
	{
		angleCube += 0.05f;
		if (angleCube > 360.0f)
		{
			ascendingCube = false;
		}
	}
	else
	{
		angleCube -= 0.05f;
		if (angleCube < 0.0f)
		{
			ascendingCube = true;
		}
	}
}

void displayOrg(void)
{
	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]

	struct ShaderUniforms *selectedUniforms;

	// Use Pass Through Shader Program
	if (gSelectedMode == PER_FRAGMENT)
	{
		glUseProgram(gShaderProgramObject_pf);
		selectedUniforms = &PerFragmentUniforms;
	}
	else
	{
		glUseProgram(gShaderProgramObject_pv); // Binding shader pgm to OpenGL pgm
		selectedUniforms = &PerVertexUniforms;
	}

	// your code here
	// 9 steps
	// declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 rotationMatrix;
	mat4 translationMatrix;

	// initialize above 2 matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	translationMatrix = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	rotationMatrix = rotate(0.0f, angleCube, 0.0f);
	translationMatrix = translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix * rotationMatrix;

	// do necessary matrix multiplication
	projectionMatrix = perspectiveProjectionMatrix;// *modelViewMatrix;
	// In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum

	// send necessary matrices to shader in respective uniforms
	// display fn is dynamic, called in loop
	glUniformMatrix4fv(selectedUniforms->modelUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelMatrix); // kashala chiktavaychay // verify
	// OpenGL/GLSL is column major, DirectX is row major
	glUniformMatrix4fv(selectedUniforms->viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(selectedUniforms->projectionUniform, 1, GL_FALSE, projectionMatrix);

	if (gbShowLight)
	{
		glUniform1i(selectedUniforms->isLKeyPressedUniform, 1);
		glUniform4fv(selectedUniforms->lightPoistionUniform, 1, light_position);

		glUniform3fv(selectedUniforms->laUniform, 1, light_ambient);
		glUniform3fv(selectedUniforms->ldUniform, 1, light_diffuse);
		glUniform3fv(selectedUniforms->lsUniform, 1, light_specular);

		glUniform3fv(selectedUniforms->kaUniform, 1, material_ambient);
		glUniform3fv(selectedUniforms->kdUniform, 1, material_diffuse);
		glUniform3fv(selectedUniforms->ksUniform, 1, material_specular);
		glUniform1f(selectedUniforms->shinynessUniform, material_shinyness);
	}
	else
	{
		glUniform1i(selectedUniforms->isLKeyPressedUniform, 0);
	}
	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao_sphere_pv);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0); // Unbinding

	SwapBuffers(ghdc);
}

void display(void)
{
	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]

	struct ShaderUniforms *selectedUniforms;

	// Use Pass Through Shader Program
	if (gSelectedMode == PER_FRAGMENT)
	{
		glUseProgram(gShaderProgramObject_pf);
		selectedUniforms = &PerFragmentUniforms;
	}
	else
	{
		glUseProgram(gShaderProgramObject_pv); // Binding shader pgm to OpenGL pgm
		selectedUniforms = &PerVertexUniforms;
	}

	// your code here
	// 9 steps
	// declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 rotationMatrix;
	mat4 translationMatrix;

	// initialize above 2 matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	translationMatrix = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	rotationMatrix = rotate(angleCube, angleCube, angleCube);
	translationMatrix = translate(0.0f, 0.0f, -6.0f);
	modelMatrix = translationMatrix * rotationMatrix;

	// do necessary matrix multiplication
	projectionMatrix = perspectiveProjectionMatrix;// *modelViewMatrix;
	// In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum

	// send necessary matrices to shader in respective uniforms
	// display fn is dynamic, called in loop
	glUniformMatrix4fv(selectedUniforms->modelUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelMatrix); // kashala chiktavaychay // verify
	// OpenGL/GLSL is column major, DirectX is row major
	glUniformMatrix4fv(selectedUniforms->viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(selectedUniforms->projectionUniform, 1, GL_FALSE, projectionMatrix);

	if (gbShowLight)
	{
		glUniform1i(selectedUniforms->isLKeyPressedUniform, 1);
		glUniform4fv(selectedUniforms->lightPoistionUniform, 1, light_position);

		glUniform3fv(selectedUniforms->laUniform, 1, light_ambient);
		glUniform3fv(selectedUniforms->ldUniform, 1, light_diffuse);
		glUniform3fv(selectedUniforms->lsUniform, 1, light_specular);

		glUniform3fv(selectedUniforms->kaUniform, 1, material_ambient);
		glUniform3fv(selectedUniforms->kdUniform, 1, material_diffuse);
		glUniform3fv(selectedUniforms->ksUniform, 1, material_specular);
		glUniform1f(selectedUniforms->shinynessUniform, material_shinyness);
	}
	else
	{
		glUniform1i(selectedUniforms->isLKeyPressedUniform, 0);
	}
	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao_cube_pf);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_marble);
	glUniform1i(selectedUniforms->samplerUniform, 0);

	// draw the scene
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0); // Unbinding

	SwapBuffers(ghdc);
}
