#pragma once
#include "Logic.h"
#include <GL/glew.h>
#include <stdio.h> // file
#include <Windows.h> // malloc free

void incrCount(void)
{
	++MyCount;
}

int getCount(void)
{
	return MyCount;
}

void unmarkVaoVboList(void)
{
	for (int i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		vao_list[i] = -1;
		vbo_position_list[i] = -1;
		vbo_color_list[i] = -1;
		vbo_element_list[i] = -1;
	}
}

void uninitializePP(void)
{
	void uninitSPO(GLuint *theSpo);
	void uninitVAO(GLuint *theVao);
	void uninitVBO(GLuint *theVbo);

	// PP shader dtor
	// Safe Release
	// Don't declare shader objects globally, use locally in initialize n use as necessary
	uninitVBO(&vbo_ratioline_position);
	uninitVAO(&vao_ratioline);

	uninitVBO(&vbo_staticindia_position);
	uninitVBO(&vbo_staticindia_color);
	uninitVAO(&vao_staticindia);

	for (int i = 0; i < NO_OF_GEOMETRIES; i++)
	{
		uninitVAO(&vao_list[i]);
	}

	uninitSPO(&genericShaderProgramObject);

	if (gpFile)
	{
		fprintf(gpFile, "\nClosing log file.");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void uninitVBO(GLuint *theVbo)
{
	if (*theVbo)
	{
		glDeleteBuffers(1, theVbo);
		*theVbo = 0;
	}
}

void uninitVAO(GLuint *theVao)
{
	if (*theVao)
	{
		glDeleteVertexArrays(1, theVao);
		*theVao = 0;
	}
}

void uninitSPO(GLuint *theSpo)
{
	if (*theSpo)
	{
		GLsizei shaderCount;
		GLsizei shaderNo;

		glUseProgram(*theSpo);

		// ask pgm how many shaders attached to you
		glGetProgramiv(*theSpo, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount); // create array of shaders
		if (pShaders)
		{
			glGetAttachedShaders(*theSpo, shaderCount, &shaderCount, pShaders); // using same var

			for (shaderNo = 0; shaderNo < shaderCount; shaderNo++)
			{
				glDetachShader(*theSpo, pShaders[shaderNo]);
				glDeleteShader(pShaders[shaderNo]);
				pShaders[shaderNo] = 0;
			}
			free(pShaders);
		}

		glDeleteProgram(*theSpo); // Not actually deleting but changing machine state. Not shaikh chilli
		*theSpo = 0;
		glUseProgram(0);
	}
}

const char* GLenumToString(GLenum shaderType)
{
	switch (shaderType)
	{
	case GL_VERTEX_SHADER:
		return "Vertex Shader";
	case GL_FRAGMENT_SHADER:
		return "Fragment Shader";
	default:
		return "Wrong Shader";
	}
}

bool createShaders(enum ShaderModes shaderMode, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms)
{
	bool createVertexShader(GLuint *vertexShaderObjec, enum ShaderModes shaderMode);
	bool createFragmentShader(GLuint *fragmentShaderObject, enum ShaderModes shaderMode);
	bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms);

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	bool successful = false;

	if (!createVertexShader(&vertexShaderObject, shaderMode))
	{
		return false;
	}
	if (!createFragmentShader(&fragmentShaderObject, shaderMode))
	{
		return false;
	}
	return createShaderProgramAndLink(vertexShaderObject, fragmentShaderObject, gShaderProgramObject, shaderUniforms);
}

bool createVertexShader(GLuint *vertexShaderObject, enum ShaderModes shaderMode)
{
	bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);

	const GLchar *vertexShaderSourceCode_basic =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"	gl_Position = u_mvp_matrix * vPosition;" \
		"	out_color = vColor;" \
		"}";

	switch (shaderMode)
	{
	case BASIC:
		return createShader(vertexShaderObject, GL_VERTEX_SHADER, vertexShaderSourceCode_basic);
		break;

	default:
		return false;
		break;
	}
}

bool createFragmentShader(GLuint *fragmentShaderObject, enum ShaderModes shaderMode)
{
	bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode);

	const GLchar *fragmentShaderSourceCode_basic =
		"#version 430 core" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec4 out_color;" \
		"void main(void)" \
		"{" \
		"	FragColor = out_color;" \
		"}";

	switch (shaderMode)
	{
	case BASIC:
		return createShader(fragmentShaderObject, GL_FRAGMENT_SHADER, fragmentShaderSourceCode_basic);
		break;

	default:
		return false;
		break;
	}
}

bool createShader(GLuint *shaderObject, GLenum typeOfShader, const GLchar *shaderSourceCode)
{
	const char* GLenumToString(GLenum shaderType);

	*shaderObject = glCreateShader(typeOfShader);
	glShaderSource(*shaderObject, 1, (const GLchar**)&shaderSourceCode, NULL);
	glCompileShader(*shaderObject);

	// steps for catching errors
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(*shaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(*shaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetShaderInfoLog(*shaderObject, iInfoLogLength, &written, szInfoLog);
				fwprintf(gpFile, L"\n%hs: Compilation Error: %hs", GLenumToString(typeOfShader), TEXT(szInfoLog));
				free(szInfoLog);
			}
			else
			{
				fwprintf(gpFile, L"\n%hs: failed to malloc szInfoLog...", GLenumToString(typeOfShader));
			}
		}
		else
		{
			fwprintf(gpFile, L"\n%hs: Something went wrong, infoLogLength is zero...", GLenumToString(typeOfShader));
		}
		return false;
	}
	else
	{
		fwprintf(gpFile, L"\n%hs compiled successfully.", GLenumToString(typeOfShader));
	}

	// reset flags
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	return true;
}

bool createShaderProgramAndLink(GLuint vertexShaderObject, GLuint fragmentShaderObject, GLuint *gShaderProgramObject, struct ShaderUniforms *shaderUniforms)
{
	// step 1] create shader program obj
	*gShaderProgramObject = glCreateProgram();

	// step 2] Attach shaders
	glAttachShader(*gShaderProgramObject, vertexShaderObject);
	glAttachShader(*gShaderProgramObject, fragmentShaderObject);

	// Pre-Linking binding to vertex attribute
	glBindAttribLocation(*gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(*gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor"); // Worked even if was not bind

	// step 3] Link program
	glLinkProgram(*gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetProgramiv(*gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(*gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written = 0;
				glGetProgramInfoLog(*gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fwprintf(gpFile, L"\nShader Program: Link Error: %hs", TEXT(szInfoLog));
				free(szInfoLog);
			}
			else
			{
				fwprintf(gpFile, L"\nShader Program: failed to malloc szInfoLog...");
			}
		}
		else
		{
			fwprintf(gpFile, L"\nShader Program: Something went wrong, infoLogLength is zero...");
		}
		return false;
	}
	else
	{
		fwprintf(gpFile, L"\nShader program linked successfully.");
	}

	// Post-Linking retrieving uniform location
	shaderUniforms->mvpUniform = glGetUniformLocation(*gShaderProgramObject, "u_mvp_matrix");

	// reset
	iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	return true;
}

void bindVao(GLuint *theVao)
{
	glGenVertexArrays(1, theVao);
	glBindVertexArray(*theVao);
}

void unbindVao()
{
	glBindVertexArray(0);
}

void bindVbo(GLuint *theVbo)
{
	glGenBuffers(1, theVbo);
	glBindBuffer(GL_ARRAY_BUFFER, *theVbo);
}

void unbindVbo()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void bindVaoVbo(GLuint *theVao, GLuint *theVbo)
{
	glGenVertexArrays(1, theVao);
	glBindVertexArray(*theVao);

	glGenBuffers(1, theVbo);
	glBindBuffer(GL_ARRAY_BUFFER, *theVbo);
}

void unbindVboVao()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void fillBufferData(const float data[], int sizeofData, int noOfElementsInOneTuple, bool isStatic, int amc_attribute)
{
	if (isStatic)
	{
		glBufferData(GL_ARRAY_BUFFER, sizeofData, data, GL_STATIC_DRAW); // attachya atta oot
	}
	else
	{
		glBufferData(GL_ARRAY_BUFFER, sizeofData, NULL, GL_DYNAMIC_DRAW);
	}

	glVertexAttribPointer(amc_attribute,
		noOfElementsInOneTuple, // 3=xyx, 2=st
		GL_FLOAT,
		GL_FALSE, // isNormalized 0 to 1 NDC 
		0, // dhanga, no stride. useful in case of multiple v, color, normal, texcoord
		NULL); // no stride therefore no offest

	glEnableVertexAttribArray(amc_attribute);
}

void initVAOs(void)
{
	// function declarations
	void initVaoRatioLine(GLuint *vao_ratioline, GLuint *vbo_ratio_position);
	void initVaoSI(GLuint *vao_si, GLuint *vbo_si_position, GLuint *vbo_si_color);
	void unmarkVaoVboList(void);
	void initVaoI1(GLuint *theVao, GLuint *theVboPosition, GLuint *theVboColor); // static Vertices
	void initVaoN(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor);
	void initVaoA(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor);
	void initVaoI2(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor);
	void initVaoD(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor);
	void initVaoPlane(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor, GLuint *theElementVbo);
	void initVaoTricolor(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor, GLuint *theElementVbo);

	// code
	unmarkVaoVboList();

	initVaoRatioLine(&vao_ratioline, &vbo_ratioline_position);
	initVaoSI(&vao_staticindia, &vbo_ratioline_position, &vbo_staticindia_color);

	initVaoI1(&vao_list[LETTER_I1], &vbo_position_list[LETTER_I1], &vbo_color_list[LETTER_I1]);
	initVaoN(&vao_list[LETTER_N], LETTER_N, &vbo_position_list[LETTER_N], &vbo_color_list[LETTER_N]);
	initVaoA(&vao_list[LETTER_A], LETTER_A, &vbo_position_list[LETTER_A], &vbo_color_list[LETTER_A]);
	initVaoI2(&vao_list[LETTER_I2], LETTER_I2, &vbo_position_list[LETTER_I2], &vbo_color_list[LETTER_I2]);
	initVaoD(&vao_list[LETTER_D], LETTER_D, &vbo_position_list[LETTER_D], &vbo_color_list[LETTER_D]);

	initVaoPlane(&vao_list[PLANE_A], PLANE_A, &vbo_position_list[PLANE_A], &vbo_color_list[PLANE_A], &vbo_element_list[PLANE_A]);
	initVaoPlane(&vao_list[PLANE_B], PLANE_B, &vbo_position_list[PLANE_B], &vbo_color_list[PLANE_B], &vbo_element_list[PLANE_B]);
	initVaoPlane(&vao_list[PLANE_C], PLANE_C, &vbo_position_list[PLANE_C], &vbo_color_list[PLANE_C], &vbo_element_list[PLANE_C]);

	GeometryList[PLANE_A].InitialX = 0.0f;
	GeometryList[PLANE_A].MovingX = GeometryList[PLANE_A].InitialX;

	GeometryList[PLANE_C].InitialX = 0.0f;
	GeometryList[PLANE_C].MovingX = GeometryList[PLANE_C].InitialX;

	GeometryList[PLANE_B].InitialX = -2.88f;
	GeometryList[PLANE_B].MovingX = GeometryList[PLANE_B].InitialX;
	GeometryList[PLANE_B].StopMarker = 2.56f;
	GeometryList[PLANE_B].TranslationStep = 0.000098f;

	initVaoTricolor(&vao_list[LETTER_A_TRICOLOR], LETTER_A_TRICOLOR, &vbo_position_list[LETTER_A_TRICOLOR], &vbo_color_list[LETTER_A_TRICOLOR], &vbo_element_list[LETTER_A_TRICOLOR]);
}

void initVaoRatioLine(GLuint *vao_ratio, GLuint *vbo_ratio_position)
{
	GLfloat ratioVertices[((RATIO_COLS + 1) * 6) + ((RATIO_ROWS + 1) * 6)]; // 6 = 3 x,y,z * 2 start,end

	int i = 0, j = 0;
	float stepRow = 2.0f / RATIO_ROWS;
	float stepCol = 2.0f / RATIO_COLS;
	float xcoord = 1.0f;
	float ycoord = 1.0f;

	for (i = 0; i <= RATIO_COLS; i++)
	{
		ratioVertices[j++] = xcoord;
		ratioVertices[j++] = 1.0f;
		ratioVertices[j++] = 0.0f;
		ratioVertices[j++] = xcoord;
		ratioVertices[j++] = -1.0f;
		ratioVertices[j++] = 0.0f;
		xcoord -= stepCol;
	}

	for (i = 0; i <= RATIO_ROWS; i++)
	{
		ratioVertices[j++] = -1.0f;
		ratioVertices[j++] = ycoord;
		ratioVertices[j++] = 0.0f;
		ratioVertices[j++] = 1.0f;
		ratioVertices[j++] = ycoord;
		ratioVertices[j++] = 0.0f;
		ycoord -= stepRow;
	}

	bindVaoVbo(vao_ratio, vbo_ratio_position);
	fillBufferData(ratioVertices, sizeof(ratioVertices), 3, true, AMC_ATTRIBUTE_POSITION);

	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.12f, 0.12f, 0.12f);
	unbindVboVao();
}

void initVaoSI(GLuint *vao_si, GLuint *vbo_si_position, GLuint *vbo_si_color)
{
	// function declarations
	void drawTricolorInLetterA(float side1X1, float side1Y1, float side1X2, float side1Y2,
		float side2X1, float side2Y1, float side2X2, float side2Y2, GLfloat siVertices[], int index);

	// variable declarations
	int index = 0;
	static float stepRow = 2.0f / RATIO_ROWS;
	static float stepCol = 2.0f / RATIO_COLS;
	static float xcoord = 1.0f;
	static float ycoord = 1.0f;
	static float theHeight = stepRow * 2.5f;
	static float IposX; // = -1.0f + (stepCol * 3.0f);
	static float bigLetterWidth = stepCol * 1.5f;
	static float distanceInLetter = stepCol / 2.0f;
	static Coord p1 = { 0,0,0 }, p2 = { 0,0,0 }, p3 = { 0,0,0 };
	IposX = -1.0f + (stepCol * 4.75f);// +distanceInLetter;

	/*const Triplet gDeepSaffron = { 1.0f,  0.59765625f,  0.19921875f };
	const Triplet gIndiaGreen = { 0.07421875f,  0.53125f,  0.03125f };*/

	// Logic
	GLfloat siVertices[85];

	// I
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	
	// N
	IposX += distanceInLetter;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	IposX += bigLetterWidth;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	// D
	IposX += distanceInLetter;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX + bigLetterWidth; siVertices[index++] = theHeight; siVertices[index++] = 0;

	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX + bigLetterWidth; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	IposX += bigLetterWidth;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	// I
	IposX += distanceInLetter;
	siVertices[index++] = IposX; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = IposX; siVertices[index++] = -theHeight; siVertices[index++] = 0;

	// A
	IposX += distanceInLetter;
	p1.x = IposX, p1.y = -theHeight;
	IposX += (bigLetterWidth / 2.0f);
	p2.x = IposX, p2.y = theHeight;
	IposX += (bigLetterWidth / 2.0f);
	p3.x = IposX, p3.y = -theHeight;

	//drawTricolorInLetterA(p1.x, p1.y, p2.x, p2.y, p2.x, p2.y, p3.x, p3.y, siVertices, index);

	siVertices[index++] = p1.x; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	siVertices[index++] = p2.x; siVertices[index++] = theHeight; siVertices[index++] = 0;
	
	siVertices[index++] = p2.x; siVertices[index++] = theHeight; siVertices[index++] = 0;
	siVertices[index++] = p3.x; siVertices[index++] = -theHeight; siVertices[index++] = 0;
	
	drawTricolorInLetterA(p1.x, p1.y, p2.x, p2.y, p2.x, p2.y, p3.x, p3.y, siVertices, index);

	GLfloat siColor[] =
	{
		// I
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// N
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// D
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,

		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// I
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		// A
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,

		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		gDeepSaffron.red, gDeepSaffron.green, gDeepSaffron.blue,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue,
		gIndiaGreen.red, gIndiaGreen.green, gIndiaGreen.blue
	};

	bindVao(vao_si);

	bindVbo(vbo_si_position);
	fillBufferData(siVertices, sizeof(siVertices), 3, true, AMC_ATTRIBUTE_POSITION);
	unbindVbo();

	bindVbo(vbo_si_color);
	fillBufferData(siColor, sizeof(siColor), 3, true, AMC_ATTRIBUTE_COLOR);
	unbindVbo();

	unbindVao();
}

// step 1] find mid points of both sides
// step 2] can draw middle line joining both mid points
// step 3] find lengths of all three sides
// step 4] Upper: now we have lengths of all sides. find one angle. Inverse cos
//			cos A = (b2 + c2 - a2) /2bc		A = cos inverse(value)
// step 5] Lower: angle = 180 - upper. Inverse cos
// step 6].a find next point of side 1 nextS1A'(x + pos * cos(A), y + pos * sin(A))
// step 6].b find next point of side 2 nextS2A'(x + pos * cos(A), y + pos * sin(A))
// step 7] draw line from obtained points nextS1A' <--> nextS2A'
// step 8].a find prev point of side 1 prevS1A'(x + pos * cos(A), y + pos * sin(A))
// step 8].b find prev point of side 2 prevS2A'(x + pos * cos(A), y + pos * sin(A))
// step 9] draw line from obtained points prevS1A' <--> prevS2A'
void drawTricolorInLetterA(float side1X1, float side1Y1, float side1X2, float side1Y2,
	float side2X1, float side2Y1, float side2X2, float side2Y2, GLfloat siVertices[], int index)
{
	// midpoint = (x1+x2)/2, (y1+y2)/2
	static Coord side1MidPoint, side2MidPoint;

	side1MidPoint.x = (side1X1 + side1X2) / 2.0f;
	side1MidPoint.y = (side1Y1 + side1Y2) / 2.0f;
	side1MidPoint.z = 0.0f;

	side2MidPoint.x = (side2X1 + side2X2) / 2.0f;
	side2MidPoint.y = (side2Y1 + side2Y2) / 2.0f;
	side2MidPoint.z = 0.0f;

	static float lengthB = sqrtf(powf((side1MidPoint.x - side1X2), 2) + powf((side1MidPoint.y - side1Y2), 2));
	static float lengthC = sqrtf(powf((side1MidPoint.x - side2MidPoint.x), 2) + powf((side1MidPoint.y - side2MidPoint.y), 2));
	static float lengthA = sqrtf(powf((side2MidPoint.x - side2X2), 2) + powf((side2MidPoint.y - side2Y2), 2));

	static float side1CosA = ((lengthB*lengthB) + (lengthC*lengthC) - (lengthA*lengthA)) / (2.0f *lengthB * lengthC);
	static float side1AngleA = acosf(side1CosA) * (180.0f / M_PI); // radian to degree
	static float side1CosB = ((lengthA*lengthA) + (lengthC*lengthC) - (lengthB*lengthB)) / (2.0f *lengthA * lengthC);
	static float side1AngleB = acosf(side1CosB) * (180.0f / M_PI); // radian to degree

	static float nextS1Ax = side1MidPoint.x + (0.002f * cosf(side1AngleA));
	static float nextS1Ay = side1MidPoint.y + (0.035f * sinf(side1AngleA));
	static float nextS2Ax = side2MidPoint.x - (0.002f * cosf(side1AngleB));
	static float nextS2Ay = side2MidPoint.y + (0.035f * sinf(side1AngleB));

	static float prevS1Ax = side1MidPoint.x - (0.002f * cosf(side1AngleA));
	static float prevS1Ay = side1MidPoint.y - (0.035f * sinf(side1AngleA));
	static float prevS2Ax = side2MidPoint.x + (0.002f * cosf(side1AngleB));
	static float prevS2Ay = side2MidPoint.y - (0.035f * sinf(side1AngleB));

	siVertices[index++] = prevS1Ax; siVertices[index++] = prevS1Ay; siVertices[index++] = side1MidPoint.z;
	siVertices[index++] = prevS2Ax; siVertices[index++] = prevS2Ay; siVertices[index++] = side2MidPoint.z;

	siVertices[index++] = side1MidPoint.x; siVertices[index++] = side1MidPoint.y; siVertices[index++] = side1MidPoint.z;
	siVertices[index++] = side2MidPoint.x; siVertices[index++] = side2MidPoint.y; siVertices[index++] = side2MidPoint.z;

	siVertices[index++] = nextS1Ax; siVertices[index++] = nextS1Ay; siVertices[index++] = side1MidPoint.z;
	siVertices[index++] = nextS2Ax; siVertices[index++] = nextS2Ay; siVertices[index++] = side2MidPoint.z;
}

void initVaoI1(GLuint *theVao, GLuint *theVboPosition, GLuint *theVboColor)
{
	GeometryList[LETTER_I1].InitialX = -2.95f;
	GeometryList[LETTER_I1].MovingX = GeometryList[LETTER_I1].InitialX;
	GeometryList[LETTER_I1].StopMarker = -1.0f + (gStepCol);
	GeometryList[LETTER_I1].TranslationStep = 0.0002f;

	GeometryList[LETTER_I1].Vertices[START_X] = -1.0f;
	GeometryList[LETTER_I1].Vertices[START_Y] = gLetterHeight;
	GeometryList[LETTER_I1].Vertices[START_Z] = 0.0f;
	GeometryList[LETTER_I1].Vertices[END_X] = -1.0f;
	GeometryList[LETTER_I1].Vertices[END_Y] = -gLetterHeight;
	GeometryList[LETTER_I1].Vertices[END_Z] = 0.0f;

	GeometryList[LETTER_I1].Colors[START_X] = gDeepSaffron.red;
	GeometryList[LETTER_I1].Colors[START_Y] = gDeepSaffron.green;
	GeometryList[LETTER_I1].Colors[START_Z] = gDeepSaffron.blue;
	GeometryList[LETTER_I1].Colors[END_X] = gIndiaGreen.red;
	GeometryList[LETTER_I1].Colors[END_Y] = gIndiaGreen.green;
	GeometryList[LETTER_I1].Colors[END_Z] = gIndiaGreen.blue;

	bindVao(theVao);

	bindVbo(theVboPosition);
	fillBufferData(GeometryList[LETTER_I1].Vertices, sizeof(GeometryList[LETTER_I1].Vertices), 3, false, AMC_ATTRIBUTE_POSITION);
	unbindVbo();

	bindVbo(theVboColor);
	fillBufferData(GeometryList[LETTER_I1].Colors, sizeof(GeometryList[LETTER_I1].Colors), 3, true, AMC_ATTRIBUTE_COLOR);
	unbindVbo();

	unbindVao();
}

void initVaoN(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor)
{
	GeometryList[geometryListIdx].InitialY = 1.24f;
	GeometryList[geometryListIdx].MovingY = GeometryList[geometryListIdx].InitialY;
	GeometryList[geometryListIdx].StopMarker = -gLetterHeight;
	GeometryList[geometryListIdx].TranslationStep = 0.0002f;
	float theX = -1.0f + (gStepCol * 3.0f);

	GeometryList[geometryListIdx].VerticesDynamic = (float*) malloc(sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_N); //6); // 3 xyz, 6 vertices

	GeometryList[geometryListIdx].VerticesDynamic[START_X] = theX;
	GeometryList[geometryListIdx].VerticesDynamic[START_Y] = GeometryList[geometryListIdx].InitialY + (gLetterHeight * 2.0f);
	GeometryList[geometryListIdx].VerticesDynamic[START_Z] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[END_X] = theX;
	GeometryList[geometryListIdx].VerticesDynamic[END_Y] = GeometryList[geometryListIdx].InitialY;
	GeometryList[geometryListIdx].VerticesDynamic[END_Z] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_X] = theX;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_Y] = GeometryList[geometryListIdx].InitialY + (gLetterHeight * 2.0f);
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_Z] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_X] = theX + gBigLetterWidth;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_Y] = GeometryList[geometryListIdx].InitialY;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_Z] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + START_X] = theX + gBigLetterWidth;
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + START_Y] = GeometryList[geometryListIdx].InitialY + (gLetterHeight * 2.0f);
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + START_Z] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + END_X] = theX + gBigLetterWidth;
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + END_Y] = GeometryList[geometryListIdx].InitialY;
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + END_Z] = 0.0f;

	GeometryList[geometryListIdx].ColorsDynamic = (float*)malloc(sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_N);
	
	for (int i = 0; i < NO_OF_LINES_IN_LETTER_N; i++)
	{
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + START_X] = gDeepSaffron.red;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + START_Y] = gDeepSaffron.green;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + START_Z] = gDeepSaffron.blue;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + END_X] = gIndiaGreen.red;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + END_Y] = gIndiaGreen.green;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + END_Z] = gIndiaGreen.blue;
	}

	bindVao(theVao);

	bindVbo(theVboPosition);
	fillBufferData(GeometryList[geometryListIdx].VerticesDynamic, sizeof(GeometryList[geometryListIdx].VerticesDynamic), 3, false, AMC_ATTRIBUTE_POSITION);
	unbindVbo();

	bindVbo(theVboColor);
	fillBufferData(GeometryList[geometryListIdx].ColorsDynamic, sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_N, 3, true, AMC_ATTRIBUTE_COLOR);
	unbindVbo();

	unbindVao();
}

void initVaoA(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor)
{
	GeometryList[geometryListIdx].InitialX = 2.2f;
	GeometryList[geometryListIdx].MovingX = GeometryList[geometryListIdx].InitialX;
	GeometryList[geometryListIdx].StopMarker = -1.0f + (gStepCol * 13.0f);
	GeometryList[geometryListIdx].TranslationStep = 0.0002f;

	GeometryList[geometryListIdx].VerticesDynamic = (float*)malloc(sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_A);

	GeometryList[geometryListIdx].VerticesDynamic[START_X] = GeometryList[geometryListIdx].InitialX + (gBigLetterWidth / 2.0f);
	GeometryList[geometryListIdx].VerticesDynamic[START_Y] = gLetterHeight;
	GeometryList[geometryListIdx].VerticesDynamic[START_Z] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[END_X] = GeometryList[geometryListIdx].InitialX;
	GeometryList[geometryListIdx].VerticesDynamic[END_Y] = -gLetterHeight;
	GeometryList[geometryListIdx].VerticesDynamic[END_Z] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_X] = GeometryList[geometryListIdx].InitialX + (gBigLetterWidth / 2.0f);
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_Y] = gLetterHeight;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_Z] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_X] = GeometryList[geometryListIdx].InitialX + gBigLetterWidth;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_Y] = -gLetterHeight;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_Z] = 0.0f;
	
	GeometryList[geometryListIdx].ColorsDynamic = (float*)malloc(sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_A);

	for (int i = 0; i < NO_OF_LINES_IN_LETTER_A; i++)
	{
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + START_X] = gDeepSaffron.red;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + START_Y] = gDeepSaffron.green;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + START_Z] = gDeepSaffron.blue;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + END_X] = gIndiaGreen.red;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + END_Y] = gIndiaGreen.green;
		GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + END_Z] = gIndiaGreen.blue;
	}

	bindVao(theVao);

	bindVbo(theVboPosition);
	fillBufferData(GeometryList[geometryListIdx].VerticesDynamic, sizeof(GeometryList[geometryListIdx].VerticesDynamic), 3, false, AMC_ATTRIBUTE_POSITION);
	unbindVbo();

	bindVbo(theVboColor);
	fillBufferData(GeometryList[geometryListIdx].ColorsDynamic, sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_A, 3, true, AMC_ATTRIBUTE_COLOR);
	unbindVbo();

	unbindVao();
}

void initVaoI2(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor)
{
	static float theX = -1.0f + (gStepCol * 11.0f);

	GeometryList[geometryListIdx].InitialY = -1.24f;
	GeometryList[geometryListIdx].MovingY = GeometryList[geometryListIdx].InitialY;
	GeometryList[geometryListIdx].StopMarker = gLetterHeight;
	GeometryList[geometryListIdx].TranslationStep = 0.0002f;
	
	GeometryList[geometryListIdx].Vertices[START_X] = theX;
	GeometryList[geometryListIdx].Vertices[START_Y] = GeometryList[geometryListIdx].InitialY;
	GeometryList[geometryListIdx].Vertices[START_Z] = 0.0f;
	GeometryList[geometryListIdx].Vertices[END_X] = theX;
	GeometryList[geometryListIdx].Vertices[END_Y] = GeometryList[geometryListIdx].InitialY  - (gLetterHeight * 2.0f);
	GeometryList[geometryListIdx].Vertices[END_Z] = 0.0f;
	
	GeometryList[geometryListIdx].Colors[START_X] = gDeepSaffron.red;
	GeometryList[geometryListIdx].Colors[START_Y] = gDeepSaffron.green;
	GeometryList[geometryListIdx].Colors[START_Z] = gDeepSaffron.blue;
	GeometryList[geometryListIdx].Colors[END_X] = gIndiaGreen.red;
	GeometryList[geometryListIdx].Colors[END_Y] = gIndiaGreen.green;
	GeometryList[geometryListIdx].Colors[END_Z] = gIndiaGreen.blue;

	bindVao(theVao);

	bindVbo(theVboPosition);
	fillBufferData(GeometryList[geometryListIdx].Vertices, sizeof(GeometryList[geometryListIdx].Vertices), 3, false, AMC_ATTRIBUTE_POSITION);
	unbindVbo();

	bindVbo(theVboColor);
	fillBufferData(GeometryList[geometryListIdx].Colors, sizeof(GeometryList[geometryListIdx].Colors), 3, true, AMC_ATTRIBUTE_COLOR);
	unbindVbo();

	unbindVao();
}

void initVaoD(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor)
{
	static float theX = -1.0f + (gStepCol * 7.0f);

	GeometryList[geometryListIdx].Alpha = 0.0f;
	GeometryList[geometryListIdx].TranslationStep = 0.0001f; // 0.0001f; // *0.01f;

	GeometryList[geometryListIdx].VerticesDynamic = (float*)malloc(sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_D);

	GeometryList[geometryListIdx].VerticesDynamic[START_X] = theX;
	GeometryList[geometryListIdx].VerticesDynamic[START_Y] = gLetterHeight;
	GeometryList[geometryListIdx].VerticesDynamic[START_Z] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[END_X] = theX;
	GeometryList[geometryListIdx].VerticesDynamic[END_Y] = -gLetterHeight;
	GeometryList[geometryListIdx].VerticesDynamic[END_Z] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_X] = theX;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_Y] = gLetterHeight;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + START_Z] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_X] = theX + gBigLetterWidth;
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_Y] = gLetterHeight - (gLetterHeight / 3.0f);
	GeometryList[geometryListIdx].VerticesDynamic[NO_OF_COORDS_IN_3D_LINE + END_Z] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + START_X] = theX;
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + START_Y] = -gLetterHeight;
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + START_Z] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + END_X] = theX + gBigLetterWidth;
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + END_Y] = -gLetterHeight + (gLetterHeight / 3.0f);
	GeometryList[geometryListIdx].VerticesDynamic[2 * NO_OF_COORDS_IN_3D_LINE + END_Z] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[3 * NO_OF_COORDS_IN_3D_LINE + START_X] = theX + gBigLetterWidth;
	GeometryList[geometryListIdx].VerticesDynamic[3 * NO_OF_COORDS_IN_3D_LINE + START_Y] = gLetterHeight - (gLetterHeight / 3.0f);
	GeometryList[geometryListIdx].VerticesDynamic[3 * NO_OF_COORDS_IN_3D_LINE + START_Z] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[3 * NO_OF_COORDS_IN_3D_LINE + END_X] = theX + gBigLetterWidth;
	GeometryList[geometryListIdx].VerticesDynamic[3 * NO_OF_COORDS_IN_3D_LINE + END_Y] = -gLetterHeight + (gLetterHeight / 3.0f);
	GeometryList[geometryListIdx].VerticesDynamic[3 * NO_OF_COORDS_IN_3D_LINE + END_Z] = 0.0f;

	GeometryList[geometryListIdx].ColorsDynamic = (float*)malloc(sizeof(float) * (NO_OF_COORDS_IN_3D_LINE + 2) * NO_OF_LINES_IN_LETTER_D); // for alpha

	//for (int i = 0; i < NO_OF_LINES_IN_LETTER_N; i++)
	//{
	//	GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + 0] = 1.0f;//gDeepSaffron.red;
	//	GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + 1] = 1.0f;//gDeepSaffron.green;
	//	GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + 2] = 1.0f;//gDeepSaffron.blue;
	//	GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + 3] = GeometryList[geometryListIdx].Alpha;
	//	GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + 4] = 1.0f;//gIndiaGreen.red;
	//	GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + 5] = 1.0f;//gIndiaGreen.green;
	//	GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + 6] = 1.0f;//gIndiaGreen.blue;
	//	GeometryList[geometryListIdx].ColorsDynamic[(i * NO_OF_COORDS_IN_3D_LINE) + 7] = GeometryList[geometryListIdx].Alpha;
	//}

	GeometryList[geometryListIdx].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 0] = gDeepSaffron.red;
	GeometryList[geometryListIdx].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 1] = gDeepSaffron.green;
	GeometryList[geometryListIdx].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 2] = gDeepSaffron.blue;
	GeometryList[geometryListIdx].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 3] = GeometryList[geometryListIdx].Alpha;
	GeometryList[geometryListIdx].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 4] = gIndiaGreen.red;
	GeometryList[geometryListIdx].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 5] = gIndiaGreen.green;
	GeometryList[geometryListIdx].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 6] = gIndiaGreen.blue;
	GeometryList[geometryListIdx].ColorsDynamic[(0 * NO_OF_RGBA_IN_3D_LINE) + 7] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 0] = gDeepSaffron.red;
	GeometryList[geometryListIdx].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 1] = gDeepSaffron.green;
	GeometryList[geometryListIdx].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 2] = gDeepSaffron.blue;
	GeometryList[geometryListIdx].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 3] = GeometryList[geometryListIdx].Alpha;
	GeometryList[geometryListIdx].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 4] = gDeepSaffron.red;
	GeometryList[geometryListIdx].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 5] = gDeepSaffron.green;
	GeometryList[geometryListIdx].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 6] = gDeepSaffron.blue;
	GeometryList[geometryListIdx].ColorsDynamic[(1 * NO_OF_RGBA_IN_3D_LINE) + 7] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 0] = gIndiaGreen.red;
	GeometryList[geometryListIdx].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 1] = gIndiaGreen.green;
	GeometryList[geometryListIdx].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 2] = gIndiaGreen.blue;
	GeometryList[geometryListIdx].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 3] = GeometryList[geometryListIdx].Alpha;
	GeometryList[geometryListIdx].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 4] = gIndiaGreen.red;
	GeometryList[geometryListIdx].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 5] = gIndiaGreen.green;
	GeometryList[geometryListIdx].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 6] = gIndiaGreen.blue;
	GeometryList[geometryListIdx].ColorsDynamic[(2 * NO_OF_RGBA_IN_3D_LINE) + 7] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 0] = gDeepSaffron.red;
	GeometryList[geometryListIdx].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 1] = gDeepSaffron.green;
	GeometryList[geometryListIdx].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 2] = gDeepSaffron.blue;
	GeometryList[geometryListIdx].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 3] = GeometryList[geometryListIdx].Alpha;
	GeometryList[geometryListIdx].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 4] = gIndiaGreen.red;
	GeometryList[geometryListIdx].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 5] = gIndiaGreen.green;
	GeometryList[geometryListIdx].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 6] = gIndiaGreen.blue;
	GeometryList[geometryListIdx].ColorsDynamic[(3 * NO_OF_RGBA_IN_3D_LINE) + 7] = GeometryList[geometryListIdx].Alpha;

	bindVao(theVao);

	bindVbo(theVboPosition);
	fillBufferData(GeometryList[geometryListIdx].VerticesDynamic, sizeof(float) * NO_OF_COORDS_IN_3D_LINE * NO_OF_LINES_IN_LETTER_D, 3, true, AMC_ATTRIBUTE_POSITION);
	unbindVbo();

	bindVbo(theVboColor);
	fillBufferData(GeometryList[geometryListIdx].ColorsDynamic, sizeof(float) * NO_OF_RGBA_IN_3D_LINE * NO_OF_LINES_IN_LETTER_D, 4, false, AMC_ATTRIBUTE_COLOR);
	unbindVbo();

	unbindVao();
}

void initVaoPlane(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor, GLuint *theElementVbo)
{
	float unitMagnitude = gStepCol * 1.25f;
	float half = (unitMagnitude / 2.0f);
	float percent10 = (unitMagnitude * 0.1f);
	float percent15 = (unitMagnitude * 0.15f);
	float percent20 = (unitMagnitude * 0.2f);
	float percent25 = (unitMagnitude * 0.25f);
	float percent30 = (unitMagnitude * 0.3f);
	float percent35 = (unitMagnitude * 0.35f);
	float percent40 = (unitMagnitude * 0.4f);
	int tempIdx = 0;

	GeometryList[geometryListIdx].VerticesDynamic = (float*)malloc(sizeof(float) * NO_OF_COORDS_IN_3D_VERTEX * NO_OF_LINES_IN_PLANE);
	
	// airoplaneTail
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -gUnitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -gUnitMagnitude - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = half;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -gUnitMagnitude + percent30;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = half;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -gUnitMagnitude + percent40;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -gUnitMagnitude + percent40;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -gUnitMagnitude + percent30;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -half;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -gUnitMagnitude - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -half;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// airoplaneMiddle
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude + percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude + percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent25;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent25;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// airoplaneHead
	// 11
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent25;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 12
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = unitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 13
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f + percent30;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = unitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 14
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f + half;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent30;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 15
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = unitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent35;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 16
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = unitMagnitude + percent40;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 17
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = unitMagnitude + percent40;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 18
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = unitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent35;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 19
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f + half;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent30;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 20
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f + percent30;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 21
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 22
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent25;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// exhaustSaffron
	// 23
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 24
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 25
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 26
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15 - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 27
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15 - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// exhaustWhite
	// 28
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 29
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15 - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 30
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15 - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 31
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -(percent15 - percent10);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 32
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -(percent15 - percent10);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// exhaustGreen
	// 33
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 34
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 35
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 36
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -(percent15 - percent10);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 37
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -(percent15 - percent10);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// IAF
	GLfloat side1MidPointx = (percent40 + percent20 + percent30 + percent15) / 2.0f;
	GLfloat midPointy = (percent20 + (-percent20)) / 2.0f;
	GLfloat side2MidPointx = (percent40 + percent20 + percent15 + percent40 + percent20) / 2.0f;
	float tempX1 = 0.0f, tempX2 = 0.0f, tempY = 0.0f, reduceBy = 0.1f;

	// 38
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent40;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 39
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1 = percent40;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 40
	tempX1 = tempX1 - (percent10 * 0.6f);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 41
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// A
	// 42
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1 = percent30 + percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent25;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 43
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX2 = percent40 + percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 44
	tempX2 = tempX2 + (tempX2 * 0.1f);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX2;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 45
	tempX1 = tempX1 + (tempX1 * 0.1f);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent25;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 46
	tempX2 = percent40 + percent20 + percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX2;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 47
	tempX2 = tempX2 + (tempX2 * 0.1f);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX2;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 48
	tempX1 = side1MidPointx;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = side1MidPointx;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = midPointy;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 49
	tempX2 = side2MidPointx;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = side2MidPointx;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = midPointy;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 50
	tempY = midPointy - 0.01f;
	tempX2 = tempX2 + (percent10 * 0.1f);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX2;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempY;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 51
	tempX1 = tempX1 - (percent10 * 0.1f);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempY;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// F
	// 52
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent10 + percent40 * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 53
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1 = percent10 + percent40 * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 54
	tempX1 = tempX1 + (percent10 * 0.5f);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 55
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 56
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent30 + percent40 * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 57
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent30 + percent40 * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent20 - 0.01f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 58
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent20 - 0.01f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 59
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 60
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -0.01f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 61
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1 + percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -0.01f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 62
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = tempX1 + percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].NumVertices = tempIdx / 3;

	// element vbo
	tempIdx = 0;
	GeometryList[geometryListIdx].ElementsDynamic = (int*)malloc(sizeof(int) * NO_OF_COORDS_IN_3D_VERTEX * 40);

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 1;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 2;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 0;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 2;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 3;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 0;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 3;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 4;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 0;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 4;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 5;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 0;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 5;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 6;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 0;

	// airoplaneMiddle
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 8;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 9;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 7;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 9;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 10;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 7;

	// airoplaneHead
	// 7
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 11;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 12;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 13;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 11;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 13;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 14;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 11;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 14;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 19;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 11;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 22;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 19;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 22;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 19;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 20;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 20;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 21;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 22;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 14;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 15;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 18;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 18;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 19;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 14;

	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 15;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 16;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 17;

	// 16
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 17;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 18;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 15;

	// exhaustSaffron
	// 17
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 23;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 24;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 25;

	// 18
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 25;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 26;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 27;

	// 19
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 23;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 25;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 27;

	// exhaustWhite
	// 20
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 28;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 29;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 30;

	// 21
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 30;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 31;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 32;

	// 22
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 28;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 30;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 32;

	// exhaustGreen
	// 23
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 33;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 34;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 35;

	// 24
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 35;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 36;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 37;

	// 25
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 33;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 35;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 37;

	// IAF
	// 26
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 38;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 39;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 40;

	// 27
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 40;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 41;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 38;

	// A
	// 28
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 42;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 43;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 44;

	// 29
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 43;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 44;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 45;

	// 30
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 43;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 46;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 44;

	// 31
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 44;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 46;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 47;

	// 32
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 48;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 49;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 50;

	// 33
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 50;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 51;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 48;

	// F
	// 34
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 52;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 53;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 54;

	// 35
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 52;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 55;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 54;

	// 36
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 55;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 56;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 57;

	// 37
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 55;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 58;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 57;

	// 38
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 59;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 60;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 61;

	// 39
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 59;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 62;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 61;

	GeometryList[geometryListIdx].NumElements = tempIdx; // / 3;

	int idxColor = 0;
	GeometryList[geometryListIdx].ColorsDynamic = (float*)malloc(sizeof(float) * NO_OF_COORDS_IN_3D_VERTEX * NO_OF_LINES_IN_PLANE);

	for (idxColor = 0; idxColor < 23; idxColor++)
	{
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_RED] = 0.7294117647058824f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_GREEN] = 0.8862745098039216f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_BLUE] = 0.9333333333333333f;
	}
	// exhaustSaffron
	for (; idxColor < 28; idxColor++)
	{
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_RED] = 1.0f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_GREEN] = 0.59765625f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_BLUE] = 0.19921875f;
	}
	// exhaustWhite
	for (; idxColor < 33; idxColor++)
	{
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_RED] = 1.0f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_GREEN] = 1.0f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_BLUE] = 1.0f;
	}
	// exhaustGreen
	for (; idxColor < 38; idxColor++)
	{
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_RED] = 0.07421875f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_GREEN] = 0.53125f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_BLUE] = 0.03125f;
	}
	// IAF
	for (; idxColor < NO_OF_LINES_IN_PLANE; idxColor++)
	{
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_RED] = 1.0f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_GREEN] = 1.0f;
		GeometryList[geometryListIdx].ColorsDynamic[(idxColor * NO_OF_COORDS_IN_3D_VERTEX) + START_BLUE] = 1.0f;
	}

	bindVao(theVao);

	bindVbo(theVboPosition);
	fillBufferData(GeometryList[geometryListIdx].VerticesDynamic, sizeof(float) * NO_OF_COORDS_IN_3D_VERTEX * NO_OF_LINES_IN_PLANE, 3, true, AMC_ATTRIBUTE_POSITION);
	unbindVbo();

	bindVbo(theVboColor);
	fillBufferData(GeometryList[geometryListIdx].ColorsDynamic, sizeof(float) * NO_OF_COORDS_IN_3D_VERTEX * NO_OF_LINES_IN_PLANE, 3, true, AMC_ATTRIBUTE_COLOR);
	unbindVbo();

	glGenBuffers(1, theElementVbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *theElementVbo);
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * NO_OF_COORDS_IN_3D_LINE * GeometryList[geometryListIdx].NumElements, GeometryList[geometryListIdx].ElementsDynamic, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * GeometryList[geometryListIdx].NumElements, GeometryList[geometryListIdx].ElementsDynamic, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	unbindVao();
}

void initVaoTricolor(GLuint *theVao, int geometryListIdx, GLuint *theVboPosition, GLuint *theVboColor, GLuint *theElementVbo)
{
	float unitMagnitude = gStepCol * 1.25f;
	float half = (unitMagnitude / 2.0f);
	float percent10 = (unitMagnitude * 0.1f);
	float percent15 = (unitMagnitude * 0.15f);
	float percent20 = (unitMagnitude * 0.2f);
	float percent25 = (unitMagnitude * 0.25f);
	float percent30 = (unitMagnitude * 0.3f);
	float percent35 = (unitMagnitude * 0.35f);
	float percent40 = (unitMagnitude * 0.4f);
	int tempIdx = 0;

	GeometryList[geometryListIdx].Alpha = 0.0f;
	GeometryList[geometryListIdx].TranslationStep = 0.00025f;

	GeometryList[geometryListIdx].VerticesDynamic = (float*)malloc(sizeof(float) * NO_OF_COORDS_IN_3D_VERTEX * NO_OF_VERTICES_IN_LETTER_A_TRICOLOR);

	// 0
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 1
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 2
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15 - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 3
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15 - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 4
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15 - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 5
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = percent15 - percent10;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 6
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -(percent15 - percent10);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 7
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -(percent15 - percent10);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 8
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -(percent15 - percent10);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 9
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -(percent15 - percent10);
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 10
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude * 2.0f;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	// 11
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -unitMagnitude - percent20;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = -percent15;
	GeometryList[geometryListIdx].VerticesDynamic[tempIdx++] = 0.0f;

	GeometryList[geometryListIdx].NumVertices = tempIdx / 3;

	// element vbo
	tempIdx = 0;
	GeometryList[geometryListIdx].ElementsDynamic = (int*)malloc(sizeof(int) * NO_OF_COORDS_IN_3D_VERTEX * 6);

	// 0
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 0;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 1;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 2;

	// 1
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 0;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 3;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 2;

	// 2
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 4;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 5;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 6;

	// 3
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 4;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 7;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 6;

	// 4
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 8;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 9;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 10;

	// 4
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 8;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 11;
	GeometryList[geometryListIdx].ElementsDynamic[tempIdx++] = 10;

	GeometryList[geometryListIdx].NumElements = tempIdx;

	GeometryList[geometryListIdx].ColorsDynamic = (float*)malloc(sizeof(float) * (NO_OF_COORDS_IN_3D_VERTEX + 1) * NO_OF_VERTICES_IN_LETTER_A_TRICOLOR); // for alpha
	
	tempIdx = 0;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.red;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.green;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.blue;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.red;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.green;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.blue;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.red;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.green;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.blue;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.red;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.green;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gDeepSaffron.blue;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	// white
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = 1.0f;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	// 
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.red;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.green;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.blue;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.red;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.green;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.blue;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.red;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.green;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.blue;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.red;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.green;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = gIndiaGreen.blue;
	GeometryList[geometryListIdx].ColorsDynamic[tempIdx++] = GeometryList[geometryListIdx].Alpha;

	bindVao(theVao);

	bindVbo(theVboPosition);
	fillBufferData(GeometryList[geometryListIdx].VerticesDynamic, sizeof(float) * NO_OF_COORDS_IN_3D_VERTEX * NO_OF_VERTICES_IN_LETTER_A_TRICOLOR, 3, true, AMC_ATTRIBUTE_POSITION);
	unbindVbo();

	bindVbo(theVboColor);
	fillBufferData(GeometryList[geometryListIdx].ColorsDynamic, sizeof(float) * 4 * NO_OF_VERTICES_IN_LETTER_A_TRICOLOR, 4, false, AMC_ATTRIBUTE_COLOR);
	unbindVbo();

	glGenBuffers(1, theElementVbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *theElementVbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * GeometryList[geometryListIdx].NumElements, GeometryList[geometryListIdx].ElementsDynamic, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	unbindVao();
}
