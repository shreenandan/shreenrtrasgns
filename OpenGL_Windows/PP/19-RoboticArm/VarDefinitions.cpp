#pragma once
#include "VarDeclarations.h"
#include <stdio.h>

int MyCount = 10;
FILE *gpFile = NULL;

mat4 perspectiveProjectionMatrix;

struct ShaderUniforms GenericUniforms; // Must declare vars else unresolved symbols linking error

GLuint genericShaderProgramObject = 0;

Geometry GeometryList[NO_OF_GEOMETRIES];
GLuint vao_list[NO_OF_GEOMETRIES];
GLuint vbo_position_list[NO_OF_GEOMETRIES];
GLuint vbo_color_list[NO_OF_GEOMETRIES];
GLuint vbo_element_list[NO_OF_GEOMETRIES];

const float gStepRow = 2.0f / RATIO_ROWS;
const float gStepCol = 2.0f / RATIO_COLS;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gVbo_sphere_element = -1;

int gNumVertices = -1;
int gNumElements = -1;

GLuint vao_sphere = -1;
GLuint vbo_position_sphere = -1;
GLuint vbo_light_sphere = -1;

int shoulder = 0;
int elbow = 0;