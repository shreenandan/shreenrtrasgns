#include "Logic.h"
#include <Windows.h>

void update(void)
{
}

void display(void)
{
	// function declarations
	void drawGraph(int geometryListIdx);

	// Code
	static const GLfloat skinR = 1.0f;
	static const GLfloat skinG = 227.0f / 255.0f;
	static const GLfloat skinB = 159.0f / 255.0f;

	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]

	struct ShaderUniforms *selectedUniforms;

	glUseProgram(genericShaderProgramObject);
	selectedUniforms = &GenericUniforms;

	// your code here
	// 9 steps
	// declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	// initialize above 2 matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	// here in this pgm no transformation, but in later pgms
	modelViewMatrix = translate(0.0f, 0.0f, -12.0f) * rotate((GLfloat)shoulder, 0.0f, 0.0f, 1.0f) * scale(2.0f, 0.5f, 1.0f);

	// do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(selectedUniforms->mvpUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelViewProjectionMatrix); // kashala chiktavaychay  // OpenGL/GLSL is column major, DirectX is row major

	//drawGraph(GRAPH);

	// 1] SHOULDER
	glBindVertexArray(vao_sphere);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, skinR, skinG, skinB);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	// 2] ELBOW
	modelViewMatrix = translate(0.0f, 0.0f, -12.0f) * rotate((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = modelViewMatrix * translate(1.0f, 0.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * rotate((GLfloat)elbow, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = modelViewMatrix * translate(1.0f, 0.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * scale(2.0f, 0.5f, 1.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(selectedUniforms->mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao_sphere);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, skinR, skinG, skinB);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	glUseProgram(0); // Unbinding

	SwapBuffers(ghdc);
}

void drawGraph(int geometryListIdx)
{
	glLineWidth(1.0f);

	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao_list[geometryListIdx]);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	glDrawArrays(GL_LINES, 0, ((LINES_TO_DRAW + 2) * 4) * 2);
	glBindVertexArray(0);
}
