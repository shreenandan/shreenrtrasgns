#include "Logic.h"
#include <Windows.h>

void update(void)
{
}

void display(void)
{
	// function declarations
	void drawCurve(int geometryListIdx);

	// Code
	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]

	struct ShaderUniforms *selectedUniforms;

	glUseProgram(genericShaderProgramObject);
	selectedUniforms = &GenericUniforms;

	// your code here
	// 9 steps
	// declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	// initialize above 2 matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	// here in this pgm no transformation, but in later pgms
	//modelViewMatrix = translate(0.50f, 0.50f, -2.0f);
	modelViewMatrix = translate(0.0f, 0.0f, -4.0f);

	// do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(selectedUniforms->mvpUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelViewProjectionMatrix); // kashala chiktavaychay  // OpenGL/GLSL is column major, DirectX is row major

	// Tessellation
	glUniform1i(selectedUniforms->noOfSegmentsUniform, gNoOfLineSegments);
	glUniform1i(selectedUniforms->noOfStripsUniform, 2);
	glUniform4fv(selectedUniforms->lineColorUniform, 1, vec4((float)gNoOfLineSegments/50.0f, 1.0f - ((float)gNoOfLineSegments/50.0f), 0.0f, 1.0f));

	TCHAR str[255];
	wsprintf(str, TEXT("OpenGL Tessellation Segments = %d"), gNoOfLineSegments);
	SetWindowText(gHwnd, str);

	drawCurve(BEZIER_CURVE);

	glUseProgram(0); // Unbinding

	SwapBuffers(ghdc);
}

void drawCurve(int geometryListIdx)
{
	//glLineWidth(5.0f);

	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao_list[geometryListIdx]);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data
	
	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	glPatchParameteri(GL_PATCH_VERTICES, 4); // gl_in[]
	// draw the scene
	//glDrawArrays(GL_LINES, 0, ((LINES_TO_DRAW + 2) * 4) * 2);

	glDrawArrays(GL_PATCHES, 0, 4);

	glBindVertexArray(0);
}
