#include "Logic.h"
#include <Windows.h>

#define DISTANCE_LIGHT 1000.0

//float light_diffuse[8] = { 0.5f, 0.2f, 0.7f, 1.0f,
//							1.0f, 0.0f, 0.0f, 1.0f };
float light_ambient[9] = 
{
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f 
};
float light_diffuse[9] = 
{ 
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 
	0.0f, 0.0f, 1.0f 
};
float light_specular[9] = 
{ 
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 1.0f
};
float light_position[12] = 
{
	3.0f, 2.0f, 0.0f, 1.0f,
	-3.0f, 2.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f
};
float light_positionWay2[12] =
{
	0.0, DISTANCE_LIGHT, DISTANCE_LIGHT, 1.0,
	DISTANCE_LIGHT, 0.0, DISTANCE_LIGHT, 1.0,
	DISTANCE_LIGHT, DISTANCE_LIGHT, -DISTANCE_LIGHT, 1.0
};

float material_ambient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float material_diffuse[4] = { 0.5f, 0.2f, 0.7f, 1.0f };
float material_specular[4] = { 0.7f, 0.7f, 0.7f, 1.0f };
float material_shinyness = 128.0f; // 128
double radius = 50.0;

void update(void)
{
	// Rotate vice versa
	/*if (ascendingCube)
	{
		angleCube += 0.002f;
		if (angleCube > 360.0f)
		{
			ascendingCube = false;
		}
	}
	else
	{
		angleCube -= 0.002f;
		if (angleCube < 0.0f)
		{
			ascendingCube = true;
		}
	}*/
	angleCube += 0.01f;
	if (angleCube > 360.0f)
		angleCube = 0.0f;

	angleCubeD += 0.01f;
	if (angleCubeD > 360.0f)
		angleCubeD = 0.0f;
}

void display(void)
{
	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]

	struct ShaderUniforms *selectedUniforms;

	// Use Pass Through Shader Program
	if (gSelectedMode == PER_FRAGMENT)
	{
		glUseProgram(gShaderProgramObject_pf);
		selectedUniforms = &PerFragmentUniforms;
	}
	else
	{
		glUseProgram(gShaderProgramObject_pv); // Binding shader pgm to OpenGL pgm
		selectedUniforms = &PerVertexUniforms;
	}

	// your code here
	// 9 steps
	// declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 viewMatrix2;
	mat4 viewMatrix3;
	mat4 projectionMatrix;
	mat4 rotationMatrix;
	mat4 translationMatrix;
	mat4 modelMatrixX;
	mat4 modelMatrixY;
	mat4 modelMatrixZ;
	vec4 translationVectorZ = vec4(0.0f, 0.0f, -3.0f, 1.0f);
	vec4 unitVec4 = vec4(1, 1, -3.5f, 1);

	// initialize above 2 matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	viewMatrix2 = mat4::identity();
	viewMatrix3 = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	modelMatrixX = mat4::identity();
	modelMatrixY = mat4::identity();
	modelMatrixZ = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	rotationMatrix = rotate(0.0f, angleCube, 0.0f);
	translationMatrix = translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // *rotationMatrix;
	
	//viewMatrix = translate(1.0f, 0.0f, 0.0f);

	modelMatrixX = rotate(angleCube, 0.0f, 0.0f);
	modelMatrixY = rotate(0.0f, angleCube, 0.0f);
	modelMatrixZ = rotate(0.0f, 0.0f, angleCube);

	float temp = 0.0f;
	for (int i = 0; i < 4; i++)
	{
		temp = 0.0f;
		for (int j = 0; j < 4; j++)
		{
			temp += (modelMatrixX[j][i] * translationVectorZ[j]);
		}
		light_position[i] = temp;
	}
	/*if (light_position[1] > 360)
		light_position[1] = 0.0f;
	light_position[1] += angleCube;*/
	for (int i = 0; i < 4; i++)
	{
		temp = 0.0f;
		for (int j = 0; j < 4; j++)
		{
			temp += (modelMatrixY[j][i] * translationVectorZ[j]);
		}
		light_position[4+i] = temp;
	}
	for (int i = 0; i < 4; i++)
	{
		temp = 0.0f;
		for (int j = 0; j < 4; j++)
		{
			temp += (modelMatrixZ[i][j] * unitVec4[j]); // translationVectorZ[j]);
		}
		light_position[8+i] = temp;
	}
	//light_position[8] = angleCube;

	/*temp = light_position[10];
	light_position[10] = light_position[9];
	light_position[9] = temp;*/

	/*light_position[0] = 0.0f;
	light_position[1] = angleCube;
	light_position[2] = 0.0f;
	light_position[3] = 1.0f;

	light_position[4] = angleCube;
	light_position[5] = 0.0f;
	light_position[6] = 0.0f;
	light_position[7] = 1.0f;

	light_position[8] = angleCube;
	light_position[9] = 0.0f;
	light_position[10] = 0.0f;
	light_position[11] = 1.0f;*/

	/*light_position[0] = 0.0f;
	light_position[1] = angleCube;
	light_position[2] = 0.0f;
	light_position[3] = 1.0f;

	light_position[4] = angleCube;
	light_position[5] = 0.0f;
	light_position[6] = 0.0f;
	light_position[7] = 1.0f;

	light_position[8] = angleCube;
	light_position[9] = 0.0f;
	light_position[10] = 0.0f;
	light_position[11] = 1.0f;*/

	// do necessary matrix multiplication
	projectionMatrix = perspectiveProjectionMatrix;// *modelViewMatrix;
	// In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum

	// send necessary matrices to shader in respective uniforms
	// display fn is dynamic, called in loop
	glUniformMatrix4fv(selectedUniforms->modelUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelMatrix); // kashala chiktavaychay // verify
	// OpenGL/GLSL is column major, DirectX is row major
	glUniformMatrix4fv(selectedUniforms->viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(selectedUniforms->projectionUniform, 1, GL_FALSE, projectionMatrix);

	if (gbShowLight)
	{
		glUniform1i(selectedUniforms->isLKeyPressedUniform, 1);
		glUniform4fv(selectedUniforms->lightPoistionUniform, 3, light_position);

		glUniform3fv(selectedUniforms->laUniform, 3, light_ambient);
		glUniform3fv(selectedUniforms->ldUniform, 3, light_diffuse);
		glUniform3fv(selectedUniforms->lsUniform, 3, light_specular);

		glUniform3fv(selectedUniforms->kaUniform, 1, material_ambient);
		glUniform3fv(selectedUniforms->kdUniform, 1, material_diffuse);
		glUniform3fv(selectedUniforms->ksUniform, 1, material_specular);
		glUniform1f(selectedUniforms->shinynessUniform, material_shinyness);
	}
	else
	{
		glUniform1i(selectedUniforms->isLKeyPressedUniform, 0);
	}
	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao_sphere_pv);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0); // Unbinding

	SwapBuffers(ghdc);
}

void displayWay2(void)
{
	// NOTE: This makes existence functional and executes given order
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 3D change 3]

	struct ShaderUniforms *selectedUniforms;

	// Use Pass Through Shader Program
	if (gSelectedMode == PER_FRAGMENT)
	{
		glUseProgram(gShaderProgramObject_pf);
		selectedUniforms = &PerFragmentUniforms;
	}
	else
	{
		glUseProgram(gShaderProgramObject_pv); // Binding shader pgm to OpenGL pgm
		selectedUniforms = &PerVertexUniforms;
	}

	// your code here
	// 9 steps
	// declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;

	// initialize above 2 matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();

	// do necessary transformations like model scale, rotate, translate
	modelMatrix = translate(0.0f, 0.0f, -2.50f);

	/*light_positionWay2[1] = sin((angleCubeD * M_PI)/180.0) * radius;
	
	light_positionWay2[4] = cos((angleCubeD * M_PI) / 180.0) * radius;

	light_positionWay2[8] = cos((angleCubeD * M_PI) / 180.0) * radius;
	light_positionWay2[9] = sin((angleCubeD * M_PI)/180.0) * radius;*/

	light_positionWay2[1] = DISTANCE_LIGHT * cos((angleCubeD * M_PI) / 180.0);
	light_positionWay2[4] = DISTANCE_LIGHT * cos((angleCubeD * M_PI) / 180.0);
	light_positionWay2[8] = DISTANCE_LIGHT * cos((angleCubeD * M_PI) / 180.0);

	light_positionWay2[2] = DISTANCE_LIGHT * sin((angleCubeD * M_PI)/180.0);
	light_positionWay2[6] = DISTANCE_LIGHT * sin((angleCubeD * M_PI)/180.0);
	light_positionWay2[9] = DISTANCE_LIGHT * sin((angleCubeD * M_PI)/180.0);
	
	// do necessary matrix multiplication
	projectionMatrix = perspectiveProjectionMatrix;// *modelViewMatrix;
	// In FFP for us this was done by gluOrtho2d/glOrtho, gluPerspective/glFrustum

	// send necessary matrices to shader in respective uniforms
	// display fn is dynamic, called in loop
	glUniformMatrix4fv(selectedUniforms->modelUniform, // kashat kombaychay. globally declared used in display
		1, // how many matrices
		GL_FALSE, // transpose?
		modelMatrix); // kashala chiktavaychay // verify
	// OpenGL/GLSL is column major, DirectX is row major
	glUniformMatrix4fv(selectedUniforms->viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(selectedUniforms->projectionUniform, 1, GL_FALSE, projectionMatrix);

	if (gbShowLight)
	{
		glUniform1i(selectedUniforms->isLKeyPressedUniform, 1);
		glUniform4fv(selectedUniforms->lightPoistionUniform, 3, light_positionWay2);

		glUniform3fv(selectedUniforms->laUniform, 3, light_ambient);
		glUniform3fv(selectedUniforms->ldUniform, 3, light_diffuse);
		glUniform3fv(selectedUniforms->lsUniform, 3, light_specular);

		glUniform3fv(selectedUniforms->kaUniform, 1, material_ambient);
		glUniform3fv(selectedUniforms->kdUniform, 1, material_diffuse);
		glUniform3fv(selectedUniforms->ksUniform, 1, material_specular);
		glUniform1f(selectedUniforms->shinynessUniform, material_shinyness);
	}
	else
	{
		glUniform1i(selectedUniforms->isLKeyPressedUniform, 0);
	}
	// bind with vao - this will avoid many vbo repetitive calls in display
	glBindVertexArray(vao_sphere_pv);
	// IMP NOTE: Don't skip unbinding in initialize(), we may do mistake. Follow habbit start, stop again start, stop...
	// if no vao then would have repeated 4 steps 1.Bind Buffer 2.Pointer 3.Enable Attrib 4.Buffer Data

	// similarly bin with textures, if any
	// glBindTexture(GL_TEXTURE_2D, myTextId); glEnable(GL_TEXTURE_2D);

	// draw the scene
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0); // Unbinding

	SwapBuffers(ghdc);
}
