#pragma once
#include <stdio.h> // file
#include <GL/glew.h>
#include "vmath.h"
#include <Windows.h> // For HDC only

extern HDC ghdc; // Common context

extern int MyCount;
extern FILE *gpFile;
extern GLuint vao_pyramid;
extern GLuint vbo_position_pyramid;
extern GLuint vbo_lights;
extern GLuint gVbo_sphere_element;
extern GLuint gShaderProgramObject;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD
};

extern GLuint modelUniform;
extern GLuint viewUniform;
extern GLuint projectionUniform;
using namespace vmath;
extern mat4 perspectiveProjectionMatrix;

extern GLfloat angleCube;
extern GLfloat ascendingCube;

extern bool gbShowLight;
extern bool gbAnimate;

extern GLuint laUniform;
extern GLuint ldUniform;
extern GLuint lsUniform;

extern GLuint kaUniform;
extern GLuint kdUniform;
extern GLuint ksUniform;
extern GLuint shinynessUniform;

extern GLuint lightPoistionUniform;
extern GLuint isLKeyPressedUniform;

extern GLuint la2Uniform;
extern GLuint ld2Uniform;
extern GLuint ls2Uniform;
extern GLuint lightPoistion2Uniform;

extern float sphere_vertices[1146];
extern float sphere_normals[1146];
extern float sphere_textures[764];
extern unsigned short sphere_elements[2280];

extern int gNumVertices;
extern int gNumElements;
