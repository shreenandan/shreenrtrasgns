/*
	Assignment: DirectX D3D Texture Pyramid Kundali
 */

#pragma once
 // Headers
#include <Windows.h>
#include <stdio.h> // for file IO
#include <vector>

#include <dxgi.h>
#include <d3d11.h>
#include <d3dcompiler.h> // NOTE: new addition For shader compilation
#include "WICTextureLoader.h" // Windows Imaging Component

//1 > c:\rtr\direct3d\02 - orthotriangle\02 - orthotriangle\xnamath\xnamath.h(2908) : warning C4838 : 
// conversion from 'unsigned int' to 'INT' requires a narrowing conversion
#pragma warning(disable: 4838) // NOTE: new addition int to unsigned int narrowing
#include "XNAMath/xnamath.h" // NOTE: new addition
// 4 .inl files included in xnamath.h, faster execution than stack execution
// better to continue xnamath(instead of DirectXMath), even supports Windows mobile, DX11
// Warning nako pahije tar DirectXMath vapara... Learning curve, no xbox, DX12+ :)

#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3dcompiler.lib") // NOTE: new addition, Case sensitive, yes
#pragma comment(lib, "DXGI.lib")
#pragma comment (lib, "DirectXTK.lib") // For loading image

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace std;

// Custom macro instead of function, textual replacement rather than fn call, faster since compile time resolving
#define LOG(msg) fopen_s(&gpFile, gszLogFileName, "a+"); fprintf_s(gpFile, msg); fclose(gpFile);
#define LOGFMT1(fmt, msg) fopen_s(&gpFile, gszLogFileName, "a+"); fprintf_s(gpFile, fmt, msg); fclose(gpFile);

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variables
FILE *gpFile = NULL;
char gszLogFileName[] = "log.txt";

HWND gHwnd = NULL;

DWORD dwStyle; // global default initialized to zero
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbIsFullScreen = false;
bool gbIsEscapeeyPressed = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

//NOTE: 5 new interface additions
ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Pyramid_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Pyramid_Texture = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Cube_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Cube_Texture = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL; // For culling
float angleRotation = 0.0f;

//for Depth
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL; //DSV

// for Texture
ID3D11ShaderResourceView *gpID3D11ShaderResourceView_Texture_Pyramid = NULL;
ID3D11SamplerState *gpID3D11SamplerState_Texture_Pyramid = NULL;
ID3D11ShaderResourceView *gpID3D11ShaderResourceView_Texture_Cube = NULL;
ID3D11SamplerState *gpID3D11SamplerState_Texture_Cube = NULL;

// custom struct, will be used later
// mapped with vertex shader in uniform, UPPERCASE deliberately
struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix;
};

XMMATRIX gPerspectiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void); // Double Buffer change 1]
	void update(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("D3D11TexturePyramidKundali");
	bool bDone = false;

	// code
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, "Log file can not be created. Exitting...", TEXT("ERROR"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0); // Abortive but 0 and 1 = system error
	}
	else
	{
		fprintf_s(gpFile, "Log file successfully created.\n");
		fclose(gpFile);
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("DirectD3D11 Texture Pyramid Kundali"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, ////
		// own always on top, all others clipped
		// high performance rendering, WS_VISIBLE convention not compulsion
		// use in case of external painting. Makes your window visible even if ShowWindow() not called
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// initialize D3D
	HRESULT hr;
	hr = initialize();

	if (FAILED(hr))
	{
		LOG("initialize() failed. Exitting...\n");
		DestroyWindow(hwnd); // NOTE: Request OS send WM_DESTROY message to my window.
		hwnd = NULL;
	}
	else
	{
		LOG("initialize() successful.\n");
	}

	// Game loop code
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Here call display()
			display();

			if (gbActiveWindow == true)
			{
				if (gbIsEscapeeyPressed == true)
					bDone = true;

				//// Here call update()
				update();
			}
		}
	}

	// clean-up
	uninitialize();

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	HRESULT resize(int width, int height);
	void uninitialize(void);

	// variable declarations
	HRESULT hr;

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) // if 0, window is active
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		// In earlier past version this was LOWORD(wParam), now switch(wParam) only.
		// Becuase now ther is no info in HIWORD. Earlier 'scanrate' was stored in HIWORD.
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbIsEscapeeyPressed == false)
				gbIsEscapeeyPressed = true;
			break;

		case 0x46: // 'f' or 'F'
			if (gbIsFullScreen == false)
			{
				ToggleFullScreen();
				gbIsFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbIsFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_RBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam)); // lParam contains resized window width, height
			if (FAILED(hr))
			{
				LOG("resize() failed...\n");
				return (hr);
			}
			else
			{
				LOG("resize() successful.\n");
			}
		}
		break;

		// Double Buffer change 3] WM_PAINT removed, new added
	case WM_ERASEBKGND:
		return (0); // Don't go to DefWindowProc, a) bcoz it posts WM_PAINT, which we don't want
		// b) use my display() when WM_APINT is not there

	case WM_CLOSE:
		uninitialize();
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

		}

		ShowCursor(FALSE);
		gbIsFullScreen = true; // VERIFY
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbIsFullScreen = false; // VERIFY
	}
}

const char* DXenumToString(int dxEnum)
{
	switch (dxEnum)
	{
	case D3D_DRIVER_TYPE_HARDWARE:
		return "Hardware Type";

	case D3D_DRIVER_TYPE_WARP:
		return "Warp Type";

	case D3D_DRIVER_TYPE_REFERENCE:
		return "Reference Type";

	case D3D_FEATURE_LEVEL_11_0:
		return "D3D FL 11.0";

	case D3D_FEATURE_LEVEL_10_1:
		return "D3D FL 10.1";

	case D3D_FEATURE_LEVEL_10_0:
		return "D3D FL 10.0";

	default:
		return "Unknown";
	}
}

// function implementations
HRESULT initialize(void)
{
	// function declarations
	HRESULT resize(int width, int height);
	void uninitialize(void);

	// variable declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = 
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
		D3D_DRIVER_TYPE_UNKNOWN
	};
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default lowest

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1; // based on d3dFeatureLevel_required

	// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]); // calculating size of array

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1; // Back only front already considered
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = gHwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0; // Ranges from 1 to 8, performance, MSAA AntiAliasing
	dxgiSwapChainDesc.Windowed = TRUE;

	/////////////////////
	// Get Nvidia
	IDXGIFactory *pIDXGIFactory = NULL;
	IDXGIAdapter *pIDXGIAdapter = NULL;
	IDXGIOutput *pIDXGIOutput = NULL;
	std::vector<IDXGIAdapter*> vAdapters;

	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	if (FAILED(hr))
	{
		LOG("CreateDXGIFactory() failed...\n");
	}
	else
	{
		LOG("CreateDXGIFactory() succeeded.\n");
	}

	// collect all adapters
	for (unsigned int i = 0; pIDXGIFactory->EnumAdapters(i, &pIDXGIAdapter) != DXGI_ERROR_NOT_FOUND; i++)
	{
		vAdapters.push_back(pIDXGIAdapter);
	}

	int obtainedAdapterIndex = 0;
	size_t bestVideoMemory = 0;
	DXGI_ADAPTER_DESC adapterDesc;

	for (unsigned int iIndexAdapter = 0; iIndexAdapter < vAdapters.size(); iIndexAdapter++)
	{
		vAdapters[iIndexAdapter]->GetDesc(&adapterDesc);
		LOGFMT1("\tAdapter obtained: %ls\n", adapterDesc.Description);

		// select as per highest video memory
		if ((adapterDesc.DedicatedVideoMemory!=0) && (adapterDesc.DedicatedVideoMemory > bestVideoMemory))
		{
			bestVideoMemory = adapterDesc.DedicatedVideoMemory;
			obtainedAdapterIndex = iIndexAdapter;
		}
	}

	// choose the best one after searching all
	pIDXGIAdapter = vAdapters[obtainedAdapterIndex];

	// print best adapter
	pIDXGIAdapter->GetDesc(&adapterDesc);
	LOGFMT1("Best Adapter is: %S\n", adapterDesc.Description);

	/////////////////////

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(
			pIDXGIAdapter,				// Adapter
			d3dDriverType,		// Driver Type
			NULL,				// Software
			createDeviceFlags,	// Flags
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
	{
		LOG("D3D11CreateDeviceAndSwapChain() failed...\n");
		return (hr);
	}
	else
	{
		LOG("D3D11CreateDeviceAndSwapChain() succeeded.\n");
		LOGFMT1("The chosen driver is of %s.\n", DXenumToString(d3dDriverType));
		LOGFMT1("The supported highest feature level is %s.\n", DXenumToString(d3dFeatureLevel_acquired));
	}

	/////////////////////////////////////////////////////////////

	// cbuffer small case, keyword HLSL, mapped with defined struct globally
	// Mappings: ConstantBuffer uniform
	// float4x4 --> XMMATRIX
	// initialize shaders, input layouts, constant buffers
	// pos : fn variable name
	// POSITION : shader semantic, our custom defined, not HLSL keyword, AMC_ATTRIB_POS
	// aadhi built in var hote, ata kadhale, apanach return value dyayacho
	// SV_ : Shader variable gl_Position
	const char *vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"	float4x4 worldViewProjectionMatrix;" \
		"}" \
		"\n" \
		"struct VertexOutput" \
		"{" \
		"	float4 position: SV_POSITION;" \
		"	float2 texcoord: TEXCOORD;" \
		"}output;" \
		"\n" \
		"VertexOutput main(float4 pos: POSITION, float2 texcoord: TEXCOORD)" \
		"{" \
		"	VertexOutput output;" \
		"	output.position = mul(worldViewProjectionMatrix, pos);" \
		"	output.texcoord = texcoord;" \
		"	return output;" \
		"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1, // Must add one, TCHAR asta tar wcslen
		"VS",
		NULL, // if your shader has #define macros
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main", // entry point fn
		"vs_5_0", // shader feature level, model
		0, // 0 : Debug, Optimize, Validity, Row Major Mat, Backward Compatibility
		0, // 0 means ekahi effect nahi, wrt DX each shader is effect, external file, offline hlsl compilation
		&pID3DBlob_VertexShaderCode, // &VSO
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			LOGFMT1("D3DCompile() failed for Vertex Shader : %s...\n", (char*)pID3DBlob_Error->GetBufferPointer());
			// blob Binary Loarge Object, raw binary, typecast to char
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return (hr);
		}
	}
	else
	{
		LOG("D3DCompile() succedded for Vertex Shader.\n");
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), // older GetBufferData()
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL,
		&gpID3D11VertexShader);
	if (FAILED(hr))
	{
		LOG("ID3D11Device::CreateVertexShader() failed...\n");
		return (hr);
	}
	else
	{
		LOG("ID3D11Device::CreateVertexShader() succedded.\n");
	}

	// ata milalela VSO pipeliine madhe set kara
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
		NULL, // konte shareable arrays
		0); // kiti variables


	//////
	// SV_TARGET : FragColor
	const char *pixelShaderSourceCode =
		"Texture2D myTexture2D;" \
		"SamplerState mySamplerState;" \
		"\n" \
		"float4 main(float4 position: SV_POSITION, float2 texcoord : TEXCOORD): SV_TARGET" \
		"{" \
		"	float4 color = myTexture2D.Sample(mySamplerState, texcoord);" \
		"	return color;" \
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			LOGFMT1("D3DCompile() failed for Pixel Shader : %s...\n", (char*)pID3DBlob_Error->GetBufferPointer());
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return (hr);
		}
	}
	else
	{
		LOG("D3DCompile() succedded for Pixel Shader.\n");
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader);
	if (FAILED(hr))
	{
		LOG("ID3D11Device::CreatePixelShader() failed...\n");
		return (hr);
	}
	else
	{
		LOG("ID3D11Device::CreatePixelShader() succedded.\n");
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, NULL, NULL); // 0, 0
	
	/////////////

	// NOTE: Ideally to map with OGL input layout should be here
	// create and set input layout
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
	ZeroMemory(inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC) * 2);

	// POSITION
	inputElementDesc[0].SemanticName = "POSITION"; // NOTE: Must be same like shader pos : POSITION
	inputElementDesc[0].SemanticIndex = 0; // can be any pan POSITION apan zeroth la takatoy
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;  // glVertexAttribPointer 3,3,2 - st
	inputElementDesc[0].InputSlot = 0; // similar to OGL layout. Pos=0, Color=1, Norm=2, Text0=3,... AMC_ATTRIB_VERTEX
	inputElementDesc[0].AlignedByteOffset = 0; // 0 : packed alignment, no offset b/w 2 , multiple vertex data helpful in interleaved, STATIC 
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].InstanceDataStepRate = 0; // no stride

	// TEXCOORD
	inputElementDesc[1].SemanticName = "TEXCOORD"; // NOTE: Must be same like shader pos : POSITION
	inputElementDesc[1].SemanticIndex = 0; // can be any pan COLOR apan zeroth la takatoy
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32_FLOAT; // NOTE: ALPHA IS MUST, else no interpolation  // glVertexAttribPointer 3,3,2 - st -> UV
	inputElementDesc[1].InputSlot = 1; // similar to OGL layout. Pos=0, Color=1, Norm=2, Text0=3,... AMC_ATTRIB_COLOR
	inputElementDesc[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT; // 0 : packed alignment, no offset b/w 2 , multiple vertex data helpful in interleaved, STATIC 
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].InstanceDataStepRate = 0; // no stride

	hr = gpID3D11Device->CreateInputLayout(inputElementDesc,
		ARRAYSIZE(inputElementDesc),
		pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		&gpID3D11InputLayout);

	if (FAILED(hr))
	{
		LOG("ID3D11Device::CreateInputLayout() failed...\n");
		return (hr);
	}
	else
	{
		LOG("ID3D11Device::CreateInputLayout() succedded.\n");
	}
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout); // Input Assembly

	/////////////
	// TRIANGLE
	// NOTE: Winding Order is Clockwise, Left Hand Rule
	float vertices_pyramid[] =
	{
		// front side
		+0.0f, +1.0f, +0.0f,
		+1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		// right side
		+0.0f, +1.0f, +0.0f,
		+1.0f, -1.0f, +1.0f,
		+1.0f, -1.0f, -1.0f,

		// back side
		+0.0f, +1.0f, +0.0f,
		-1.0f, -1.0f, +1.0f,
		+1.0f, -1.0f, +1.0f,

		// left side
		+0.0f, +1.0f, +0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, +1.0f
	};

	// create vertex buffer
	D3D11_BUFFER_DESC bufferDesc_Vertex_Pyramid_Position;
	ZeroMemory(&bufferDesc_Vertex_Pyramid_Position, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_Vertex_Pyramid_Position.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_Vertex_Pyramid_Position.ByteWidth = sizeof(float) * ARRAYSIZE(vertices_pyramid);
	bufferDesc_Vertex_Pyramid_Position.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_Vertex_Pyramid_Position.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_Vertex_Pyramid_Position, NULL, &gpID3D11Buffer_VertexBuffer_Pyramid_Position);

	if (FAILED(hr))
	{
		LOG("ID3D11Device::CreateBuffer() failed for Vertex Shader Pyramid Position...\n");
		return (hr);
	}
	else
	{
		LOG("ID3D11Device::CreateBuffer() succedded for Vertex Shader Pyramid Position.\n");
	}

	/////
	// copy vertices into above buffer
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Pyramid_Position, // GPU madhla buffer ha de
		0, //NULL, buffer madhe kuthun lihayala suruvat karu
		D3D11_MAP_WRITE_DISCARD, // write kar ani nantar discard kar
		0, //NULL, A/synchronous, single/multi threaded DX decides internally
		&mappedSubresource);
	memcpy(mappedSubresource.pData, vertices_pyramid, sizeof(vertices_pyramid));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Pyramid_Position, 0);  //NULL
	// pipeline madhe atta init thro set nahi karnar. Delay kartoy display madhe. rajputra cha ghoda..
	// unmap must, nasta kela tar rikama gela asta, andhaar..
	// glGetUniformLocation ConstantBuffers

	///// TEXTURE
	float texcoord_pyramid[] =
	{
		//front
		0.5f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f,

		//Right
		0.5f,1.0f,
		1.0f,0.0f,
		0.0f,0.0f,

		//Back
		0.5f,1.0f,
		1.0f,0.0f,
		0.0f,0.0f,

		//left
		0.5f,1.0f,
		0.0f,0.0f,
		1.0f,0.0f
	};

	// create vertex buffer
	D3D11_BUFFER_DESC bufferDesc_Vertex_Pyramid_Texture;
	ZeroMemory(&bufferDesc_Vertex_Pyramid_Texture, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_Vertex_Pyramid_Texture.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_Vertex_Pyramid_Texture.ByteWidth = sizeof(float) * ARRAYSIZE(texcoord_pyramid);
	bufferDesc_Vertex_Pyramid_Texture.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_Vertex_Pyramid_Texture.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_Vertex_Pyramid_Texture, NULL, &gpID3D11Buffer_VertexBuffer_Pyramid_Texture);

	if (FAILED(hr))
	{
		LOG("ID3D11Device::CreateBuffer() failed for Vertex Shader Pyramid Texture...\n");
		return (hr);
	}
	else
	{
		LOG("ID3D11Device::CreateBuffer() succedded for Vertex Shader Pyramid Texture.\n");
	}

	/////
	// copy Textures into buffer
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Pyramid_Texture, // GPU madhla buffer ha de
		0, //NULL, buffer madhe kuthun lihayala suruvat karu
		D3D11_MAP_WRITE_DISCARD, // write kar ani nantar discard kar
		0, //NULL, A/synchronous, single/multi threaded DX decides internally
		&mappedSubresource);
	memcpy(mappedSubresource.pData, texcoord_pyramid, sizeof(texcoord_pyramid));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Pyramid_Texture, 0);  //NULL

	/////////////
	// RECTANGLE
	// NOTE: Winding Order is Clockwise, Left Hand Rule
	float vertices_cube[] =
	{
		// TOP
		-1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f,
		-1.0f, +1.0f, -1.0f,
		-1.0f, +1.0f, -1.0f,
		+1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, -1.0f,

		// BOTTOM
		+1.0f, -1.0f, -1.0f,
		+1.0f, -1.0f, +1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		+1.0f, -1.0f, +1.0f,
		-1.0f, -1.0f, +1.0f,

		// FRONT
		-1.0f, +1.0f, -1.0f,
		+1.0f, +1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		+1.0f, +1.0f, -1.0f,
		+1.0f, -1.0f, -1.0f,

		// BACK
		+1.0f, -1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f,
		-1.0f, -1.0f, +1.0f,
		-1.0f, -1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f,
		-1.0f, +1.0f, +1.0f,

		// LEFT
		-1.0f, +1.0f, +1.0f,
		-1.0f, +1.0f, -1.0f,
		-1.0f, -1.0f, +1.0f,
		-1.0f, -1.0f, +1.0f,
		-1.0f, +1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		// RIGHT
		+1.0f, -1.0f, -1.0f,
		+1.0f, +1.0f, -1.0f,
		+1.0f, -1.0f, +1.0f,
		+1.0f, -1.0f, +1.0f,
		+1.0f, +1.0f, -1.0f,
		+1.0f, +1.0f, +1.0f
	};

	// create vertex buffer
	D3D11_BUFFER_DESC bufferDesc_Vertex_Cube_Position;
	ZeroMemory(&bufferDesc_Vertex_Cube_Position, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_Vertex_Cube_Position.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_Vertex_Cube_Position.ByteWidth = sizeof(float) * ARRAYSIZE(vertices_cube);
	bufferDesc_Vertex_Cube_Position.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_Vertex_Cube_Position.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_Vertex_Cube_Position, NULL, &gpID3D11Buffer_VertexBuffer_Cube_Position);

	if (FAILED(hr))
	{
		LOG("ID3D11Device::CreateBuffer() failed for Vertex Shader Cube Postion...\n");
		return (hr);
	}
	else
	{
		LOG("ID3D11Device::CreateBuffer() succedded for Vertex Shader Cube Postion.\n");
	}

	/////
	// copy vertices into above buffer
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Cube_Position, // GPU madhla buffer ha de
		0, //NULL, buffer madhe kuthun lihayala suruvat karu
		D3D11_MAP_WRITE_DISCARD, // write kar ani nantar discard kar
		0, //NULL, A/synchronous, single/multi threaded DX decides internally
		&mappedSubresource);
	memcpy(mappedSubresource.pData, vertices_cube, sizeof(vertices_cube));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Cube_Position, 0);  //NULL
	// pipeline madhe atta init thro set nahi karnar. Delay kartoy display madhe. rajputra cha ghoda..
	// unmap must, nasta kela tar rikama gela asta, andhaar..
	// glGetUniformLocation ConstantBuffers

	///// TEXTURE
	float texcoord_cube[] =
	{
		// top
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,

		// bottom
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,

		// front
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,

		// back
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,

		// left
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,

		// right
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,0.0f,
		1.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f
	};

	// create vertex buffer
	D3D11_BUFFER_DESC bufferDesc_Vertex_Cube_Texture;
	ZeroMemory(&bufferDesc_Vertex_Cube_Texture, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_Vertex_Cube_Texture.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_Vertex_Cube_Texture.ByteWidth = sizeof(float) * ARRAYSIZE(texcoord_cube);
	bufferDesc_Vertex_Cube_Texture.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_Vertex_Cube_Texture.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_Vertex_Cube_Texture, NULL, &gpID3D11Buffer_VertexBuffer_Cube_Texture);

	if (FAILED(hr))
	{
		LOG("ID3D11Device::CreateBuffer() failed for Vertex Shader Cube Texture...\n");
		return (hr);
	}
	else
	{
		LOG("ID3D11Device::CreateBuffer() succedded for Vertex Shader Cube Texture.\n");
	}

	/////
	// copy Textures into buffer
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Cube_Texture, // GPU madhla buffer ha de
		0, //NULL, buffer madhe kuthun lihayala suruvat karu
		D3D11_MAP_WRITE_DISCARD, // write kar ani nantar discard kar
		0, //NULL, A/synchronous, single/multi threaded DX decides internally
		&mappedSubresource);
	memcpy(mappedSubresource.pData, texcoord_cube, sizeof(texcoord_cube));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Cube_Texture, 0);  //NULL

	//////////

	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;

	// define and set the constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);

	if (FAILED(hr))
	{
		LOG("ID3D11Device::CreateBuffer() failed for Constant Buffer...\n");
		return (hr);
	}
	else
	{
		LOG("ID3D11Device::CreateBuffer() succedded for Constant Buffer.\n");
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0, //slot
		1, // kiti
		&gpID3D11Buffer_ConstantBuffer);

	/////////////////////////////////////////////////////////////

	// load texture
	HRESULT LoadD3DTexture(const wchar_t *textureFileName, ID3D11ShaderResourceView **ppID3D11ShaderResourceView);

	hr = LoadD3DTexture(L"Stone.bmp", &gpID3D11ShaderResourceView_Texture_Pyramid);
	if (FAILED(hr))
	{
		LOG("Error: LoadD3DTexture() failed to create texture Stone...\n")
			return (hr);
	}
	else
	{
		LOG("LoadD3DTexture() succeeded to create texture Stone.\n")
	}

	//sample state
	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = gpID3D11Device->CreateSamplerState(&samplerDesc, &gpID3D11SamplerState_Texture_Pyramid);

	if (FAILED(hr))
	{
		LOG("Error: CreateSamplerState() failed to create texture Stone\n") // No semi colon needed as included in macro
	}
	else
	{
		LOG("CreateSamplerState() succeeded to create texture Stone\n")
	}


	hr = LoadD3DTexture(L"Kundali.bmp", &gpID3D11ShaderResourceView_Texture_Cube);
	if (FAILED(hr))
	{
		LOG("Error: LoadD3DTexture() failed to create texture Kundali...\n")
			return (hr);
	}
	else
	{
		LOG("LoadD3DTexture() succeeded to create texture Kundali.\n")
	}

	//sample state
	//D3D11_SAMPLER_DESC samplerDesc; // Verify consequence
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = gpID3D11Device->CreateSamplerState(&samplerDesc, &gpID3D11SamplerState_Texture_Cube);

	if (FAILED(hr))
	{
		LOG("Error: CreateSamplerState() failed to create texture Kundali\n") // No semi colon needed as included in macro
	}
	else
	{
		LOG("CreateSamplerState() succeeded to create texture Kundali\n")
	}

	/////////////////////////////////////////////////////////////


	//////////////////////////////
	// Back-face culling step 2]
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	// Not useful now, might need as per your requirement
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	// similar to OGL
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

	if (FAILED(hr))
	{
		LOG("Error: CreateRasterizerState() failed for culling\n");
		return(hr);
	}
	else
	{
		LOG("CreateRasterizerState() Succeed for culling\n");
	}

	// culling step 3] set RS
	//gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState); // Note: Important to enable culling in 3D

	/////////////////////////////////////////////////////////////

	// d3d clear color blue
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	/////////
	gPerspectiveProjectionMatrix = XMMatrixIdentity();
	/////////

	// call resize for first time
	// warm up call to resize, convention and not compulsion
	hr = resize(WIN_WIDTH, WIN_HEIGHT); // Must in DirectX, custom handling and not auto unlike OGL
	// NOTE: Size dependent resources must be handled in SIZE, so call resize()
	if (FAILED(hr))
	{
		LOG("resize() failed...\n");
		return (hr);
	}
	else
	{
		LOG("resize() succeeded.\n");
	}

	return (S_OK);
}

void uninitialize(void)
{
	/* Check whether fullscreen or not and
	if it is then restore to normal size
	and then proceed for uninitialization.
	Dots per inch problem, resolution disturbed.
	*/
	if (gbIsFullScreen == true)
	{
		// verify
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbIsFullScreen = false;
	}

	// code
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}
	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}
	
	// Buffers
	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}
	if (gpID3D11Buffer_VertexBuffer_Cube_Texture)
	{
		gpID3D11Buffer_VertexBuffer_Cube_Texture->Release();
		gpID3D11Buffer_VertexBuffer_Cube_Texture = NULL;
	}
	if (gpID3D11Buffer_VertexBuffer_Cube_Position)
	{
		gpID3D11Buffer_VertexBuffer_Cube_Position->Release();
		gpID3D11Buffer_VertexBuffer_Cube_Position = NULL;
	}
	if (gpID3D11Buffer_VertexBuffer_Pyramid_Texture)
	{
		gpID3D11Buffer_VertexBuffer_Pyramid_Texture->Release();
		gpID3D11Buffer_VertexBuffer_Pyramid_Texture = NULL;
	}
	if (gpID3D11Buffer_VertexBuffer_Pyramid_Position)
	{
		gpID3D11Buffer_VertexBuffer_Pyramid_Position->Release();
		gpID3D11Buffer_VertexBuffer_Pyramid_Position = NULL;
	}
	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}
	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}
	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}
	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}
	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		LOG("uninitialize() successful.\nClosing log file.");
		gpFile = NULL;
	}
}

HRESULT resize(int width, int height)
{
	// code
	HRESULT hr = S_OK;

	// free any size dependent resources
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// again get back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	// again get render target view from d2d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(hr))
	{
		LOG("ID3D11Device::CreateRenderTargetView() failed...\n");
		return (hr);
	}
	else
	{
		LOG("ID3D11Device::CreateRenderTargetView() succeeded.\n");
	}
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// set render target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

	// set viewport
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	/////////////////////////////////////////////////
	// set perspective matrix
	// LH : Left handed, built in matrix nahi
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width/(float)height, 0.1f, 100.0f);
	/////////////////////////////////////////////////

	return (hr);
}

void display(void)
{
	// code
	// clear render target view to a chosen color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	/////////////////////////////////////////////////
	// 5 Substeps, DeviceContext responsible for all
	// select which vertex buffer to display
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0, //slot no
		1, // kiti vertex buffer dyayache
		&gpID3D11Buffer_VertexBuffer_Pyramid_Position,
		&stride, // glVertexAttribPointer cha 2nd param
		&offset); // zero bcoz now no array, can pass array, plural Buffersss

	//// COLOR
	//stride = sizeof(float) * 4, offset = 0; // sizeof() was missing here onwards, whole mess because of it

	//gpID3D11DeviceContext->IASetVertexBuffers(1, //slot no
	//	1, // kiti vertex buffer dyayache
	//	&gpID3D11Buffer_VertexBuffer_Pyramid_Texture,
	//	&stride, // glVertexAttribPointer cha 2nd param
	//	&offset); // zero bcoz now no array, can pass array, plural Buffersss

	// ==============
	stride = sizeof(float) * 2;
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Pyramid_Texture, &stride, &offset);

	// bind texture and sampler as pixel shader resource
	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpID3D11ShaderResourceView_Texture_Pyramid);
	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerState_Texture_Pyramid);
	// ==============

	// select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// translation is concerned with world matrix transformation
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	
	// -ve angle means rotate from left to right
	worldMatrix = XMMatrixRotationY(angleRotation) * XMMatrixTranslation(-1.50f, 0.0f, +7.0f);
	// final WorldViewProjection matrix
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	// load the data into the constant buffer
	CBUFFER constantBuffer;
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	// Map init madhe pan data ikde takato
	// UpdateSubresource combination of 3 Map, Memcpy, Unmap
	// params 3,5,6 Compute pipeline madhe karto. Remember CUDA terms
	// 4th param CBUFFER kasa kay pass zalay? bcoz iherited from ID2D11Resource
	// fn cha aim 1st param aslelya GPU buffer la 4th param const buffer dene 
	// jo ki 2nd param value aselelya slot/index la ahe

	// draw vertex buffer to render target
	gpID3D11DeviceContext->Draw(12, 0);

	//////////////
	// RECTANGLE
	stride = sizeof(float) * 3, offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0, //slot no
		1, // kiti vertex buffer dyayache
		&gpID3D11Buffer_VertexBuffer_Cube_Position,
		&stride, // glVertexAttribPointer cha 2nd param
		&offset); // zero bcoz now no array, can pass array, plural Buffersss

	//// COLOR
	//stride = sizeof(float) * 4, offset = 0;

	//gpID3D11DeviceContext->IASetVertexBuffers(1, //slot no
	//	1, // kiti vertex buffer dyayache
	//	&gpID3D11Buffer_VertexBuffer_Cube_Texture,
	//	&stride, // glVertexAttribPointer cha 2nd param
	//	&offset); // zero bcoz now no array, can pass array, plural Buffersss

	// ==============
	stride = sizeof(float) * 2;
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Cube_Texture, &stride, &offset);

	// bind texture and sampler as pixel shader resource
	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpID3D11ShaderResourceView_Texture_Cube);
	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerState_Texture_Cube);
	// ==============

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();

	worldMatrix = XMMatrixScaling(0.75f, 0.75f, 0.75f) * XMMatrixRotationX(angleRotation)* XMMatrixRotationY(angleRotation)* XMMatrixRotationZ(angleRotation) * XMMatrixTranslation(1.50f, 0.0f, +7.0f);
	wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	// load the data into the constant buffer
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	// draw vertex buffer to render target
	gpID3D11DeviceContext->Draw(6, 0);
	gpID3D11DeviceContext->Draw(6, 6);
	gpID3D11DeviceContext->Draw(6, 12);
	gpID3D11DeviceContext->Draw(6, 18);
	gpID3D11DeviceContext->Draw(6, 24);
	gpID3D11DeviceContext->Draw(6, 30);

	/////////////////////////////////////////////////

	// switch between front and back buffers
	gpIDXGISwapChain->Present(0, 0);
}

void update(void)
{
	angleRotation += 0.001f;
	if (angleRotation > 360.0f)
	{
		angleRotation = 0.0f;
	}
}

HRESULT LoadD3DTexture(const wchar_t *textureFileName, ID3D11ShaderResourceView **ppID3D11ShaderResourceView)
{
	HRESULT hr;

	// create texture using DirectXTK helper function
	hr = DirectX::CreateWICTextureFromFile(gpID3D11Device, gpID3D11DeviceContext, textureFileName, NULL, ppID3D11ShaderResourceView);

	if (FAILED(hr))
	{
		LOG("Error: CreateWICTextureFromFile() failed to create texture...\n")
	}
	else
	{
		LOG("CreateWICTextureFromFile() succeeded to create texture.\n")
	}

	return(hr);
}
